<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
    public function render_page($content, $data = NULL){
        $data['header']     = $this->load->view('template/page/header', $data, TRUE);
        $data['content']    = $this->load->view($content, $data, TRUE);
        $data['footer']     = $this->load->view('template/page/footer', $data, TRUE);

        return $this->load->view('template/main', $data);
    }   
}
