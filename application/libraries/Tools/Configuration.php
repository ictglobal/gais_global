<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration{
	public function config_import($file, $path, $type='', $max_size=NULL){
		if($type==1 || $type=='doc'):
			$ty = 'doc|docx|docm|docb';

		elseif($type==2 || $type=='spr'):
			$ty = 'xls|xlsx|xlsm|xlsb|csv|ods|ots';

		elseif($type==3 || $type=='ppt'):
			$ty = 'ppt|pptx|pptm|pptb';

		elseif($type==4 || $type=='img'):
			$ty = 'jpg|jpeg|png|gif|svg|webp|tiff';

		elseif($type==5 || $type=='pdf'):
			$ty = 'pdf';

		else:
			$ty= "Invalid extension file";
		endif;

		return $config = array(
			'allowed_types'	=> $ty,
			'encrypt_name'	=> TRUE,
			'file_name'		=> $file,
			'max_size'		=> 100000,
			'upload_path' 	=> $path,
		);		
	}
}