<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Functions {
	public function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split = explode('-', $tanggal);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}

	public function hexToStr($hex){
	    $string='';
	    for ($i=0; $i < strlen($hex)-1; $i+=2){
	        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
	    }
	    return $string;
	}

	public function get_nama_hari($hari){
		$hari_indonesia = array(
								'Monday'  => 'Senin',
							    'Tuesday'  => 'Selasa',
							    'Wednesday' => 'Rabu',
							    'Thursday' => 'Kamis',
							    'Friday' => 'Jumat',
							    'Saturday' => 'Sabtu',
							    'Sunday' => 'Minggu'
						);
		return $hari_indonesia[$hari];
	}
}