<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// panggil autoload dompdf nya
require_once 'dompdf/autoload.inc.php';
use Dompdf\Dompdf;
use Dompdf\Options;

class Pdfgenerator {
    public function generate($html, $filename='', $paper = '', $orientation = '', $stream=TRUE){   
        try{
            // ob_end_clean();
            $options = new Options();
            $options->set('isRemoteEnabled', TRUE);
            $dompdf = new Dompdf($options);
            $dompdf->loadHtml($html);
            $dompdf->setPaper($paper, $orientation);
            $dompdf->render();
            if ($stream) {
                ob_end_clean();
                $dompdf->stream($filename.".pdf", array("Attachment" => 0));
            } else {
                return $dompdf->output();
            }
        }catch (DOMPDF_Exception $e){
            echo $e->message();
        };
    }
}