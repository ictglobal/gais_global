<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <?php if($this->session->userdata('level')==6): ?>
                <div class="btn-group btn-group-md" role="group" aria-label="...">
                    <!-- <button type="button" class="btn btn-success" onclick="export_jad('<?= $thn; ?>')">Export</button> -->
                    <!-- <button class="btn btn-success" id="ButtonExcel" onclick="getExcel()" ></button> -->
                    <button class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#export_jadwal">Export</button>
                    <!-- <button class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#import_jadwal">Import</button> -->
                </div><br> <!--<input type="text" id="offset" name="offset" value="<?= $thn; ?>">-->
            <?php endif; ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Jadwal Akademik <?= $thn; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form id='formselect'>
                                    <div class="col-sm-3">
                                        <label for="">Filter</label>
                                        <select class="form-control" id="tahun_akademik2" onchange="selection()" required>
                                            <option value="">Pilih Tahun Akademik</option>
                                            <?php
                                                if($this->session->userdata('level')==2): 
                                                    foreach ($tahun_akademik as $key => $value):
                                                        $tetew = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$value[id_tahunakademik]'")[0];
                                            ?>
                                            <option value="<?= $this->encryption->encrypt($value['id_tahunakademik']);?>">
                                                <?= $tetew['tahun_akademik'];?>
                                            </option>
                                            <?php 
                                                    endforeach; 
                                                elseif($this->session->userdata('level')==6):
                                                    foreach ($tahun_akademik as $key => $value):
                                                        $tetew = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$value[id_tahunakademik]' ORDER BY tahun_akademik")[0];
                                                        // $kode = $this->_security->_encrypt($value['id_tahunakademik']);
                                                        $kode = $this->encryption->encrypt($value['id_tahunakademik']);
                                            ?>
                                            <option value="<?= $kode?>">
                                                <?php echo $tetew['tahun_akademik']; ?>
                                            </option>
                                            <?php 
                                                    endforeach;
                                                endif; 
                                            ?>
                                        </select>
                                    </div>
                                </form>
                                <br>

                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <?php if($this->session->userdata('level')==2): ?>
                                                <th class="text-center">No</th>
                                                <th class="text-center">Kode</th>
                                                <th class="text-center">Nama Matakuliah</th>
                                                <th class="text-center">Kelas</th>
                                                <th class="text-center">Hari</th>
                                                <th class="text-center">Waktu</th>
                                                <th class="text-center">Ruang</th>
                                                <th class="text-center">Mulai || Selesai</th>

                                            <?php elseif($this->session->userdata('level')==6): ?>
                                                <th class="text-center">No</th>
                                                <th class="text-center" width="4%">Kode Matakuliah</th>
                                                <th class="text-center" width="5%">Nama Matakuliah</th>
                                                <th class="text-center">Kelas</th>
                                                <th class="text-center">Nama Dosen</th>
                                                <th class="text-center">Mulai || Selesai</th>
                                                <th class="text-center">Ruang</th>
                                                <th class="text-center" width="11%">Opsi</th>
                                            <?php endif;?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if($this->session->userdata('level')==2): ?>
                                            <?php  
                                                foreach($jadwal_dosen as $key => $value):
                                            ?>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $value['kode_matkul'];?></td>
                                                <td>
                                                    <?php 
                                                        $kd_mt = $this->user->get_data("*", "tbl_matakuliah", "WHERE kode_matkul = '$value[kode_matkul]'");
                                                        foreach($kd_mt as $key => $lav):
                                                            echo $lav['nama_matkul'];
                                                        endforeach;
                                                    ?> 
                                                </td>
                                                <td><?= $value['id_kelas'];?></td>
                                                <td><?= $value['hari'];?></td>
                                                <td><?= $value['waktu'];?></td>
                                                <td><?= $value['ruang'];?></td>
                                                <td><?= $value['mulai']." || ".$value['selesai'];?></td>
                                            </tr>
                                            <?php endforeach; ?>

                                        <?php elseif($this->session->userdata('level')==6): ?>
                                            <?php 
                                                foreach ($jadwal_thn as $key => $value):
                                            ?>
                                            <tr>
                                                <td align="center"></td>
                                                <td>
                                                    <?= $value['kode_matkul'];?>
                                                </td>
                                                <td>
                                                    <?php  
                                                        $mtk = $this->user->get_data("*", "tbl_matakuliah", "WHERE kode_matkul = '$value[kode_matkul]'");
                                                        foreach ($mtk as $key3 => $value3) {
                                                            echo $value3['nama_matkul'];
                                                        }
                                                    ?>
                                                </td>
                                                <td><?= $value['id_kelas'];?></td>
                                                <td>
                                                    <?php 
                                                        $ds = $this->user->get_data("dosen", "tbl_dosen", "WHERE username_dosen = '$value[username_dosen]'");
                                                        foreach ($ds as $key4 => $value4) {
                                                            echo $value4['dosen'];
                                                        }
                                                    ?>
                                                </td>
                                                <td><?= $value['mulai']." || ".$value['selesai'];?></td>
                                                <td><?= $value['ruang'];?></td>
                                                <td align="center">
                                                    <button class="btn btn-success btn-sm" data-toggle="modal" data-keyboard="false" data-target="#" data-placement="top" data-tooltip="tooltip" title="Edit Jadwal"> 
                                                        <span class="fa fa-edit fa-lg"></span>
                                                    </button>
                                                    <?php 
                                                        if($this->session->userdata('jabatan')=="Kepala BAAK"): 
                                                    ?>
                                                    <button id="ay" class="btn btn-danger btn-sm" onclick="ayam_boiler('<?=$value['id_jadwal']?>', '<?= $th?>')" data-toggle="tooltip" data-placement="top" title="Hapus Jadwal">
                                                        <span class="fa fa-trash-o fa-lg"></span>
                                                    </button>
                                                    <?php endif; ?>
                                                </td>                                            
                                            </tr>
                                            <?php endforeach; ?>
                                        <?php endif;  ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
        function export_jad(){
            // $.ajax({
            //         url         :'<?= base_url();?>perkuliahan/jadwal/export_jadwal2',
            //         destroy     : true,
            //         type        : 'POST',
            //         data        : '',
            //         beforeSend: function() {
            //             // $("#ButtonExcel").removeClass("buttonExcel");
            //             // $("#ButtonExcel").addClass("buttonExcel-getExcel");
            //             // $("#ButtonExcel").blur();
            //         },
            //         success: function(response){
            //             window.open('<?= base_url();?>perkuliahan/jadwal/export_jadwal2','_blank');
            //         },
            //         error: function(){
            //             alert("error when get data");
            //         }
            // });
        }
        function ayam_boiler(id_jadwal, id_th){
            // var encoded_url = encodeURIComponent(id_th);
            swal.fire({
                title: 'Konfirmasi',
                text: "Apakah anda yakin ingin menghapus data ini?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes'
            }).then(function(result) { 
                if (result.value) {
                    $.ajax({
                        url : '<?= base_url();?>perkuliahan/jadwal/hapus_jadwal/'+id_jadwal+'?th='+id_th,
                        type : 'GET',
                        data : {id:$("#ay").val()},
                        dataType:'json',
                        beforeSend: function() {
                            swal.fire({
                                title: 'Please Wait..!',
                                text: 'Is working..',
                                onOpen: function() {
                                    swal.showLoading()
                                }
                            })
                        },
                        success : function(data) { 
                            swal.fire({
                                position: 'top-right',
                                type: 'success',
                                title: 'Data berhasil dihapus',
                                showConfirmButton: false,
                                timer: 3000
                            });
                         },
                        complete: function() {
                            swal.hideLoading();
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal.hideLoading();
                            // swal.fire("!Opps ", "Something went wrong, try again later", "error");
                            // window.location.href = '<?= base_url()?>perkuliahan/jadwal/tampil_jadwal';
                            swal.fire({
                                // position: 'top-right',
                                type: 'success',
                                title: 'Data berhasil dihapus',
                                showConfirmButton: false,
                                timer: 3000
                            });
                            window.location.href = '<?= base_url()?>perkuliahan/jadwal/tampil_jadwal?th='+id_th;
                        }
                    });
                }
            });
        }

        function selection() {
            var id = $('#tahun_akademik2').val();
            var encoded_url = encodeURIComponent(id);
            window.location.href = '<?= base_url()?>perkuliahan/jadwal/tampil_jadwal?th='+encoded_url;
        }
    </script>
  </body>
</html>