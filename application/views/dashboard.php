<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <?php if($this->session->userdata('level')==2 || $this->session->userdata('level')==3):?> 
          <div class="right_col" role="main">
          <div style="text-align: left;">&nbsp;&nbsp;
                <button class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalRegisterForm">Edit Data</button>
          </div>
        <?php endif;?>
        
        <?php if($this->session->userdata('level')==2 || $this->session->userdata('level')==3):?>
          <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Biodata Pribadi 
                      <!-- <small><?//php echo $this->session->userdata('role');?></small> -->
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown" style="visibility: hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li> -->
                      <!-- <li><a class="close-link"><i class="fa fa-close"></i></a> -->
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <!-- <p>Laman ini berisi biodata pribadi <?= $this->session->userdata('role');?>. Silahkan klik Tombol <code>Edit Data</code> Jika ada perbaikan data</p> -->

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                        <?php 
                              if ($this->session->userdata('level')==2): 
                                foreach ($biodata_dosen as $key => $value) {
                        ?> 
                            <tr class="even pointer">
                                <td>Nomor Induk Dosen Nasional (NIDN)</td>
                                <td>:</td>
                                <td><?= $value->nidn;?>
                                    <i class="success fa fa-long-arrow-up"></i>
                                </td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Nama Lengkap</td>
                                <td>:</td>
                                <td><?= $value->dosen;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Gelar Akademik</td>
                                <td>:</td>
                                <td><?= $value->gelar;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td><?= $value->jenis_kelamin;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Jabatan Fungsional</td>
                                <td>:</td>
                                <td><?= $value->jafung;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Jenjang Pendidikan</td>
                                <td>:</td>
                                <td><?= $value->jenjang;?></td>
                            </tr>

                            <tr class="even pointer">
                                <td>Email</td>
                                <td>:</td>
                                <td><?= $value->email;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Nomor Ponsel</td>
                                <td>:</td>
                                <td><?= $value->telp;?></td>                           
                            </tr>

                            <tr class="even pointer">
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?= $value->alamat;?></td>
                            </tr>
                          
                          <?php } ?>
                        
                        <?php 
                            elseif ($this->session->userdata('level')==3): 
                              foreach ($biodata_mahasiswa as $key => $value) {
                        ?>   
                            <tr class="even pointer">
                                <td>Nomor Induk Mahasiswa (NIM)</td>
                                <td>:</td>
                                <td><?= $value->nim;?>
                                    <i class="success fa fa-long-arrow-up"></i>
                                </td>
                            </tr>

                            <tr class="even pointer">
                                <td>NIK Mahasiswa</td>
                                <td>:</td>
                                <td><?= $value->nik;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Nama Lengkap</td>
                                <td>:</td>
                                <td><?= $value->nama;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>
                                  <?php 
                                      if ($value->jenis_kelamin == "P") :
                                          echo "Perempuan";
                                      else:
                                          echo "Laki - laki";
                                      endif; 
                                  ?>
                                </td>
                            </tr>

                            <tr class="even pointer">
                                <td>Tempat dan Tanggal Lahir</td>
                                <td>:</td>
                                <?php 
                                
                                $tanggal = date('d F Y', strtotime($value->tgl_lahir2)); ?>
                                
                                <td><?= $value->tempat_lahir;?>, <?= $tanggal;?></td>
                            </tr>

                            <tr class="even pointer">
                                <td>Agama dan Kepercayaan</td>
                                <td>:</td>
                                <td><?= $value->agama;?></td>
                            </tr>

                            <tr class="even pointer">
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?= $value->alamat;?></td>
                            </tr>

                            <tr class="even pointer">
                                <td>Nomor Ponsel</td>
                                <td>:</td>
                                <td><?= $value->telp;?></td>
                            </tr>

                            <tr class="even pointer">
                                <td>Tahun Masuk Akademik</td>
                                <td>:</td>
                                <td><?= $value->tahun_akademik;?></td>
                            </tr>

                            <tr class="even pointer">
                                <td>Status</td>
                                <td>:</td>
                                <td><?= $value->status;?></td>
                            </tr>

                          <?php } ?>
                        <?php endif; ?>
                        </tbody>
                      </table>

                    </div>
                  </div>
                </div>
              </div>

              <?php 
                    if ($this->session->userdata('level')==3): 
                        $this->load->view('user/mahasiswa/dashboard_mahasiswa.php');
                    endif;
              ?> 
            </div>
          </div>
          </div>
        <?php 
              elseif($this->session->userdata('level')==4):
                $this->load->view('user/fakultas/dashboard_fakultas_dekan');
              elseif($this->session->userdata('level')==5):
                $this->load->view('user/kaprodi/dashboard_kaprodi');
              elseif($this->session->userdata('level')==6):
                $this->load->view('user/akademik/dashboard_akademik');
              elseif($this->session->userdata('level')==7):
                $this->load->view('user/keuangan/dashboard_keuangan');
              elseif($this->session->userdata('level')==8):
                $this->load->view('user/pemasaran/dashboard_pemasaran');
              elseif($this->session->userdata('level')==9):
                $this->load->view('user/ict/dashboard_ict');
              endif;
        ?>
      </div>
    </div>
    
  </body>
</html>

