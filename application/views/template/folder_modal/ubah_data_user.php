<?php 
	if($this->session->userdata('level')==2): 
		foreach ($biodata_dosen as $key => $value):
?>
	<div class="form-group">
		<label for="modal_contact_email">Nomor Induk Dosen Nasional (NIDN)</label>
	    <div class="input-group">
			<span class="input-group-addon"><i class="fa fa-user"></i></span>
			<input name="nidn" class="form-control" value="<?= $value->nidn;?>" placeholder="Masukan NIDN 10 digit" onkeypress="if(event.which < 48 || event.which > 57 ) if(event.which != 8) if(event.keyCode != 9) return false;" maxlength="10" />
		</div>
	</div>

    <div class="form-group">
		<label>Nama Dosen</label>
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-user"></i></span>
			<input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" value="<?= $value->dosen;?>" required/>
		</div>
	</div>

	<div class="form-row">
		<div class="col-sm-6">
			<p class="md-form mb-0">Jenis Kelamin</p>
		<div class="form-check">
			<input type="radio" name="jk" value="Laki - laki" <?php if ($value->jenis_kelamin == "Laki - laki" || $value->jenis_kelamin == "Laki - Laki") echo "checked";?> required/>
			<label class="form-check-label" for="flexRadioDefault3">Laki - laki</label>
		</div>
                        
		<div class="form-check">
			<input type="radio" name="jk" value="Perempuan"<?php if ($value->jenis_kelamin == "Perempuan") echo "checked";?> required/>
			<label class="form-check-label" for="flexRadioDefault4">Perempuan</label>
		</div>
		</div>            
		<div class="col-sm-6">
			<div class="form-group">
				<label>Gelar Akademik</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-graduation-cap"></i></span>
					<input type="text" name="gelar" class="form-control" placeholder="Gelar Anda" value="<?= $value->gelar;?>" required/>
				</div>
			</div>
		</div>
	</div>  

	<div class="form-row">
		<div class="col-sm-6">
			<div class="form-group">
				<label>Jabatan Fungsional</label>
				<select required name="jafung" class="form-control">-- Pilih Jabatan ---
					<option value="">-- Pilih Jabatan ---</option>
					<?php
                        $jafung = array('-', 'Asisten Ahli', 'Lektor', 'Lektor Kepala', "Guru Besar");
                        foreach($jafung as $fungsi){
					?>
                   	<option <?= isset($value) ? ($value->jafung == $fungsi ? 'selected' : '') : '' ?> value="<?= $fungsi; ?>"><?= $fungsi; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>            
		<div class="col-sm-6">
			<div class="form-group">
				<label>Jenjang Pendidikan</label>
				<select required name="jenjang" class="form-control">-- Pilih Jenjang ---
					<option value="">-- Pilih Jenjang Pendidikan ---</option>
					<?php
						$jenjang = array('-', 'Diploma I/II/III/IV', 'Sarjana', 'Magister', 'Doktoral');
						foreach($jenjang as $jj){
					?>
                    <option <?= isset($value) ? ($value->jenjang == $jj ? 'selected' : '') : '' ?> value="<?= $jj; ?>"><?= $jj; ?></option>
                    <?php } ?>
				</select>
			</div>
		</div>
    </div>  

	<!-- <div class="form-row">
		<div class="col-sm-6">
			<div class="form-group">
				<label>E-mail</label>
					<div class="input-group">
						<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                         <input type="email" name="email" class="form-control" placeholder="Masukan E-mail" value="<?= $value->email;?>" required/>
					</div>
			</div>
		</div>            
		<div class="col-sm-6">
			<div class="form-group">
				<label>Nomor Ponsel</label>
                    <div class="input-group">
						<span class="input-group-addon"><i class="fa fa-phone"></i></span>
						<input type="text" name="no" class="form-control" placeholder="Nomor Ponsel" value="<?= $value->telp;?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="12" required/>
					</div>
			</div>
		</div>
	</div> -->

	<div class="form-group">
		<label>Email</label>
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
        	<input type="email" name="email" class="form-control" placeholder="Masukan E-mail" value="<?= $value->email;?>" required/>
		</div>
	</div>	

	<div class="form-group">
		<label>Nomor Ponsel</label>
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
        	<input type="text" name="no" class="form-control" placeholder="Nomor Ponsel" value="<?= $value->telp;?>" onkeypress='return event.charCode >= 48 && event.charCode <= 57' maxlength="12" required/>
		</div>
	</div>	

    <div class="form-group">
		<label>Alamat</label>
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-address-card"></i></span>
            <textarea class="md-textarea form-control" rows="2" name="alamat" required><?= $value->alamat;?></textarea>
		</div>
	</div>
<?php
		endforeach;
	elseif($this->session->userdata('level')==2):
		foreach ($biodata_mahasiswa as $key => $value):
?>

	<div class="form-row">
		<div class="col-sm-6">
			<div class="form-group">
				<label>Nomor Induk Mahasiswa (NIM)</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-edit"></i></span>
					<input type="text" name="nim" class="form-control" value="<?= $value->nim;?>" readonly/>
				</div>
			</div>
		</div>            
		<div class="col-sm-6">
			<div class="form-group">
				<label>NIK Mahasiswa</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<input type="text" name="nik_mahasiswa" class="form-control" placeholder="NIK Mahasiswa" value="<?= $value->nik;?>" required/>
				</div>
			</div>
		</div>
	</div> 
                    
	<div class="form-group">
		<label>Nama Mahasiswa</label>
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-user"></i></span>
			<input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" value="<?= $value->nama;?>" required/>
		</div>
	</div>

    <div class="form-row">
    	<div class="col-sm-6">
			<div class="form-group">
				<label>Tempat Lahir</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-edit"></i></span>
					<input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?= $value->tempat_lahir;?>" required/>
				</div>
			</div>
		</div>            
        <div class="col-sm-6">
        	<div class="form-group">
				<label>Tanggal Lahir</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<input type="text" name="tanggal_lahir" class="form-control" placeholder="Tanggal Lahir" value="<?= $value->tgl_lahir;?>" required/>
				</div>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="form-group">
                <label>Tempat Tanggal Lahir</label>
                <input name ="tgl" class="date-picker form-control" placeholder="dd-mm-yyyy" type="text" required="required" type="text" onfocus="this.type='date'" onmouseover="this.type='date'" onclick="this.type='date'" onblur="this.type='text'" onmouseout="timeFunctionLong(this)">
				<script>
					function timeFunctionLong(input) {
						setTimeout(function() {
						input.type = 'text';
						}, 60000);
					}
				</script>
			</div>
		</div>
	</div> 

    <div class="form-group">
		<p class="md-form mb-0">Jenis Kelamin</p>
		<div class="form-check">
        	<input class="" type="radio" name="jk" value="L" <?php if ($value->jenis_kelamin == "L") echo "checked";?> required/>
            <label class="form-check-label" for="flexRadioDefault3">Laki - laki</label>
		</div>
		<div class="form-check">
			<input class="" type="radio" name="jk" value="P" <?php if ($value->jenis_kelamin == "P") echo "checked";?> required/>
			<label class="form-check-label" for="flexRadioDefault4">Perempuan</label>
		</div>     
	</div> 

    <div class="form-row">
    	<div class="col-sm-6">
			<div class="form-group">
				<label>Agama</label>
				<select required name="agama" class="form-control">-- Pilih Agama ---
					<option value="">-- Pilih ---</option>
                    <?php
                    	$agama = array('Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Konghucu');
                        foreach($agama as $agama){
                    ?>
                    <option <?= isset($value) ? ($value->agama == $agama ? 'selected' : '') : '' ?> value="<?= $agama; ?>"><?= $agama; ?></option>
                    <?php } ?>
				</select>
			</div>
		</div>            
        <div class="col-sm-6">
        	<div class="form-group">
				<label>Nomor Ponsel</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-phone"></i></span>
					<input type="text" name="no" class="form-control" value="<?= $value->telp;?>" required/>
				</div>
			</div>
		</div>
    </div> 

    <div class="form-group">
    	<label>Alamat</label>
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-address-card"></i></span>
				<textarea class="md-textarea form-control" rows="3" name="alamat" required><?= $value->alamat;?></textarea>
		</div>
	</div>

    <div class="form-row">
        <div class="col-sm-6">
        	<div class="form-group">
				<label>NIK Ayah</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-edit"></i></span>
					<input type="text" name="nik_ayah" class="form-control" placeholder="NIK Ayah" value="<?= $value->nik_ayah;?>" required/>
				</div>
			</div>
		</div>            
		<div class="col-sm-6">
        	<div class="form-group">
				<label>NIK Ibu</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-edit"></i></span>
                    <input type="text" name="nik_ibu" class="form-control" placeholder="NIK Ibu" value="<?= $value->nik_ibu;?>" required/>
				</div>
			</div>
		</div>
	</div>

    <div class="form-row">
    	<div class="col-sm-6">
			<div class="form-group">
				<label>Nama Ayah</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-edit"></i></span>
					<input type="text" name="nama_ayah" class="form-control" placeholder="Nama Ayah" value="<?= $value->nama_ayah;?>" required/>
				</div>
			</div>
		</div>            
		<div class="col-sm-6">
            <div class="form-group">
				<label>Nama Ibu</label>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-edit"></i></span>
					<input type="text" name="nama_ibu" class="form-control" placeholder="Nama Ibu"value="<?= $value->nama_ibu;?>" required/> 
				</div>
            </div>
		</div>
	</div>

<?php 
		endforeach;
	endif;

?>