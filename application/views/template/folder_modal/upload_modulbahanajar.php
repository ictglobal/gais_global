<div class="modal-dialog" role="document">
    <form action="<?= base_url();?>perkuliahan/modul_belajar/upload_file" method="post" enctype="multipart/form-data">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <i class="fa fa-cloud-upload mr-1"></i>Unggah Modul 
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Mata Kuliah</label>
                    <select class="form-control" id="theSelect" style="width:100%;" name="matkul">
                        <?php foreach ($jadwal2 as $key => $jdws):
                            $mtkl = $this->user->get_data("*", "tbl_matakuliah", "WHERE kode_matkul='$jdws->kode_matkul'" )[0];
                            $id = $this->user->get_data("*", "tbl_programstudi", "WHERE id_programstudi='$mtkl[id_programstudi]'")[0];
                        ?>
                        <option value="<?= $jdws->kode_matkul ?>">
                            <?= $mtkl['nama_matkul']." || ".$id['nama_programstudi']." || ".$mtkl['sks']." SKS"?> 
                        </option>
                        <?php endforeach; ?>                      
                    </select>
                </div>

                <div class="form-group">
                    <div class="mb-3">
                        <label class="form-label">File Upload</label>
                        <code>Silahkan upload file disini</code>
                        <input class="form-control form-control-sm" name="upload_file" type="file" accept=".doc, docx, .xls, .xlsx, .ppt, .pptx, .pdf, .rar, .zip" required>
                        <code>Support doc, .docx, .xls, .xlsx, .ppt, .pptx, .pdf, .rar, .zip</code>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </form>
</div>