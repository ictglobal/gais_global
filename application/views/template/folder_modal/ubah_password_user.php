<?php 
	$id = array('username_dosen' => $this->session->userdata('username'));
	$id_mhs = array('nim' => $this->session->userdata('nim'));
	$id_fak = array('id_user_fakultas' => $this->session->userdata('id'));
	$id_prd = array('id_kaprodi' => $this->session->userdata('id'));
	$id_aka = array('id_user_akademik' => $this->session->userdata('id'));
	$id_keu = array('id_finance' => $this->session->userdata('id'));
	$id_mkt = array('id_pemasaran' => $this->session->userdata('id'));


	$bio_dsn = $this->user->query_where('tbl_dosen', $id)->result();
	$bio_mhs = $this->user->query_where('tbl_mahasiswa', $id_mhs)->result();
	$bio_fak = $this->user->query_where('tbl_user_fakultas', $id_fak)->result();
	$bio_prd = $this->user->query_where('tbl_user_kaprodi', $id_prd)->result();
	$bio_aka = $this->user->query_where('tbl_user_akademik', $id_aka)->result();
	$bio_keu = $this->user->query_where('tbl_user_keuangan', $id_keu)->result();
	$bio_mkt = $this->user->query_where('tbl_user_pemasaran', $id_mkt)->result();

	if ($this->session->userdata('level')==2):
		foreach ($bio_dsn as $key => $value):
?>	
	<label>Username Dosen</label>
	<div class="input-group">
        <span class="input-group-addon">
        	<i class="fa fa-user"></i>
		</span>
		<input type="text" name="id" class="form-control" value="<?= $value->username_dosen;?>" readonly />
	</div>
<?php
		endforeach;
	elseif ($this->session->userdata('level')==3):
         foreach ($bio_mhs as $key => $value):
?>
	<label>Nomor Induk Mahasiswa (NIM)</label>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="fa fa-user"></i>
		</span>
		<input type="text" name="id" class="form-control" value="<?= $value->nim;?>" readonly />
	</div>
<?php
         endforeach;
    elseif ($this->session->userdata('level')==4):
    	 foreach ($bio_fak as $key => $value):
?>
	<label>Username Fakultas</label>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="fa fa-user"></i>
		</span>
		<input type="text" name="id" class="form-control" value="<?= $value->username;?>" readonly />
	</div>
<?php
		 endforeach;
	elseif ($this->session->userdata('level')==5):
    	 foreach ($bio_prd as $key => $value):
?>
	<label>Username Prodi</label>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="fa fa-user"></i>
		</span>
		<input type="text" name="id" class="form-control" value="<?= $value->username;?>" readonly />
	</div>
<?php
		 endforeach;
	elseif ($this->session->userdata('level')==6):
    	 foreach ($bio_aka as $key => $value):
?>
	<label>Username Akademik</label>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="fa fa-user"></i>
		</span>
		<input type="text" name="id" class="form-control" value="<?= $value->username;?>" readonly />
	</div>
<?php
		 endforeach;
	elseif ($this->session->userdata('level')==7):
    	 foreach ($bio_keu as $key => $value):
?>
	<label>Username keuangan</label>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="fa fa-user"></i>
		</span>
		<input type="text" name="id" class="form-control" value="<?= $value->username;?>" readonly />
	</div>
<?php
		endforeach;
	elseif ($this->session->userdata('level')==8):
    	 foreach ($bio_mkt as $key => $value):	
?>
	<label>Username Marketing</label>
	<div class="input-group">
		<span class="input-group-addon">
			<i class="fa fa-user"></i>
		</span>
		<input type="text" name="id" class="form-control" value="<?= $value->username;?>" readonly />
	</div>
<?php 
		endforeach;
	endif; 
?>