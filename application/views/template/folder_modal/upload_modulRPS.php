<div class="modal-dialog" role="document">
    <form action="<?= base_url();?>perkuliahan/modul_belajar/upload_rps" method="post" enctype="multipart/form-data">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <i class="fa fa-cloud-upload mr-1"></i>Unggah Rencana Pembelajaran Semester  
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <!-- <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </span>
                        <input class="form-control" type="text" name="search_data" id="search3" placeholder="Cari matkul disini" value="" required/>
                    </div> -->
                    <label class="form-label">Tahun Akademik</label>
                    <div class="input-group">
                        <select class="form-control" id="select_tahun_akademik" style="width: 100%;" name="tahun_akademik" required>
                                <option value="">Pilih Tahun Akademik</option>
                                    <?php 
                                        $tg_awal= date('Y')-4;
                                        $tgl_akhir= date('Y')+1;
                                        for ($i=$tgl_akhir; $i>=$tg_awal; $i--){
                                            $tgl_akhir = $i + 1;
                                            $th = $i."-".$tgl_akhir;
                                            // $kode = $this->encrypt->encode($th);
                                        ?>
                                <option value="<?= $th?>"><?= $i." - ".$tgl_akhir; ?></option>
                                    <?php } ?>
                        </select>
                    </div>
                    <div class="input-group">
                        <label>Mata Kuliah</label>
                        <select name="matkul" class="form-control" id="theSelect" style="width:100%;">
                            <option value="">Pilih Matakuliah</option>
                            <?php foreach ($matkul as $key => $value) {?>
                            <option value="<?= $value['kode_matkul']?>"><?= $value['kode_matkul']." || ".$value['nama_matkul']." || ".$value['kurikulum']." || ".$value['semester']?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                        <div class="mb-3">
                        <label class="form-label">File Upload</label>
                        <code>Silahkan upload file disini</code>
                        <input class="form-control form-control-sm" name="upload_file" type="file" accept=".pdf" required>
                        <code>hanya .pdf</code>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            </div>
        </div>
    </form>
</div>