<!-- footer content -->
  <footer class="footer_fixed">
    <div class="clearfix"></div>
        <div class="pull-right">
            &copy; 2022 | Powered IT Global Institute<a href="gais.global.ac.id"> GAIS V.4</a>
        </div>
    <div class="clearfix"></div>
  </footer>
<!-- /footer content -->
<?php $this->load->view('template/script/js_script.php');?>