<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php 
        $this->load->view('template/script/css_script.php');
        $this->load->view('template/page/title.php');
    ?>
  </head>

  <div class="container body">
    <div class="main_container">
        
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <nav class="nav navbar-nav">
              <ul class=" navbar-right">
                <li class="nav-item dropdown open" style="padding-left: 15px;">
                  <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?= base_url();?>front/images/user.png" alt="">Hai, <?php echo $this->session->userdata('nama')?>
                  </a>

                  <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalprofile" href="#modalprofile"> 
                      <i class="fa fa-user pull-right"></i>Ubah Password
                    </a>
                    <a class="dropdown-item" href="<?php echo site_url('login/logout');?>">
                      <i class="fa fa-sign-out pull-right"></i> Log Out
                    </a>
                  </div>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 4;">
              <a href="<?= base_url();?>dashboard" class="site_title">
                <img src="<?php echo base_url();?>temp/images/logo-global-institute.png" height="40" width="40">
                <span style="font-size: 20px;">GAIS - <?= $this->session->userdata('role');?></span>
              </a>
            </div>
            <br/><br/><br/>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo base_url();?>temp/images/logo-global-institute.png" class="img-circle profile_img">
              </div>

              <div class="profile_info">
                <span>Hello</span>
                <h2><?php echo $this->session->userdata('role');?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

              <?php if ($this->session->userdata('level')==2): ?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a><i class="fa fa-edit"></i> Perkuliahan<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a>Perangkat Pembelajaran<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu">
                                        <a href="<?= base_url();?>perkuliahan/modul_belajar/tampil_modulbahanajar">Modul Bahan Ajar</a>
                                      </li>
                                      <li class="sub_menu">
                                        <a href="<?= base_url();?>perkuliahan/modul_belajar/tampil_modulRPS">Rencana Pembelajaran Semester</a>
                                      </li>
                                      <li class="sub_menu">
                                        <a href="<?= base_url();?>perkuliahan/dokumen">SK Mengajar</a>
                                      </li>
                                  </ul>
                              </li>
                              <li><a href="<?= base_url();?>perkuliahan/jadwal">Jadwal Mengajar</a></li>
                              <li><a href="<?= base_url();?>perkuliahan/evaluasi_umpan_balik">Evaluasi Umpan Balik</a></li>
                              <?php $jdw = $this->user->cekjadwalinput(); ?>
                              <?php if($jdw > 0): ?>
                                <li><a href="<?= base_url();?>perkuliahan/nilai">Nilai</a></li>
                              <?php else:?>
                                <li><a href="<?= base_url();?>perkuliahan/nilai" style="pointer-events: none;">Nilai</a></li>
                              <?php endif; ?>
                          </ul>
                      </li> 

                      <li>
                          <a><i class="fa fa-file-text"></i> Download<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Repository</a></li>
                          </ul>
                      </li>
                  </ul>
                </div>
            
              <?php elseif ($this->session->userdata('level')==3): ?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li> 
                          <a><i class="fa fa-edit"></i> Akademik<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">KRS</a></li>
                              <li><a href="#">Jadwal Kuliah</a></li>
                              <li><a href="#">Nilai</a></li>
                              <li><a href="#">KHS</a></li>
                              <li><a href="#">Transkrip</a></li>
                              <li><a href="#">Kartu Ujian</a></li>
                              <li><a href="#">Susulan / Remedial</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-desktop"></i> KKP/KKN <span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Pendaftaran</a></li>
                              <li><a href="#">Surat Izin</a></li>
                              <li><a href="#">Dosen Pembimbing</a></li>
                              <li><a href="#">Prosedur KKP/KKN</a></li>
                              <li><a href="#">Buku Panduan KKP</a></li>
                              <li><a href="#">Buku Panduan KKN</a></li>
                              <li><a href="#">Format Proposal KKN</a></li>
                              <li><a href="#">Format Proposal KKP</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-table"></i> Pembayaran <span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Riwayat</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-bar-chart-o"></i> Skripsi <span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Pendaftaran Skripsi</a></li>
                              <li><a href="#">Dosen Pembimbing</a></li>
                              <li><a href="#">Nilai Sidang</a></li>
                              <li><a href="#">Surat Keterangan Lulus</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-graduation-cap"></i> Wisuda<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Informasi Teknis</a></li>
                              <li><a href="#">No. PIN</a></li>
                              <li><a href="#">Ijazah Digital</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-users"></i> Alumni<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Pelacakan Alumni</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-comments"></i> Quesioner Institusi<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Saran</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-file-text"></i> Download<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Repository</a></li>
                          </ul>
                      </li>
                  </ul>
                </div>
                
                <div class="menu_section">
                    <h3>Non Akademik / SKPI</h3>
                    <ul class="nav side-menu">
                      <li><a><i class="fa fa-edit"></i> Kemahasiswaan<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a>DIKTI<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li class="sub_menu"><a href="#">Prestasi</a></li>
                                </ul>
                            </li>
                            <li><a>Non DIKTI<span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li class="sub_menu"><a href="#">Prestasi</a></li>
                                </ul>
                            </li>
                        </ul>
                      </li>
                    </ul>
                </div>

              <?php elseif ($this->session->userdata('level')==4): ?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="#"><i class="fa fa-user"></i> Mahasiswa</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a><i class="fa fa-edit"></i> Tenaga Kependidikan<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Dosen Pengajar</a></li>
                              <li><a href="#">Penasehat Akademik</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-edit"></i> Perkuliahan<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a>Perangkat Pembelajaran<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url();?>perkuliahan/modul_belajar/tampil_modulRPS">Rencana Pembelajaran Semester</a></li>
                                  </ul>
                              </li>
                              <li><a>KKP/KKN<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Pendaftaran</a></li>
                                      <li class="sub_menu"><a href="#">Dokumen Berkas</a></li>
                                  </ul>
                              </li>
                              <li><a>Skripsi<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Pendaftaran Seminar Proposal</a></li>
                                      <li class="sub_menu"><a href="#">Pembimbing</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-comments"></i> Quesioner Institusi<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Saran</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-file-text"></i> Download<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Repository</a></li>
                          </ul>
                      </li>
                  </ul>
                </div>

              <?php elseif ($this->session->userdata('level')==5): ?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="#"><i class="fa fa-user"></i> Mahasiswa</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a><i class="fa fa-edit"></i> Perkuliahan<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a>Perangkat Pembelajaran<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url();?>perkuliahan/modul_belajar/tampil_modulRPS">Rencana Pembelajaran Semester</a></li>
                                  </ul>
                              </li>

                              <li><a>KKP/KKN<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Pendaftaran</a></li>
                                      <li class="sub_menu"><a href="#">Pembimbing</a></li>
                                      <li class="sub_menu"><a href="#">Dokumen Berkas</a></li>
                                      <li class="sub_menu"><a href="#">Penilaian</a></li>
                                  </ul>
                              </li>
                              <li><a>Seminar Proposal<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Pendaftaran</a></li>
                                      <li class="sub_menu"><a href="#">Dokumen Berkas </a></li>
                                      <li class="sub_menu"><a href="#">Pembimbing</a></li>
                                      <li class="sub_menu"><a href="#">Penilaian</a></li>
                                  </ul>
                              </li>
                              <li><a>Skripsi dan Sidang<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Pendaftaran</a></li>
                                      <li class="sub_menu"><a href="#">Pembimbing</a></li>
                                      <li class="sub_menu"><a href="#">Penilaian</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-comments"></i> Quesioner Institusi<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Saran</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-file-text"></i> Download<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li class="sub_menu"><a href="#">Repository</a></li>
                              <li><a>Panduan<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">KKP/KKN</a></li>
                                      <li class="sub_menu"><a href="#">Seminar Proposal</a></li>
                                      <li class="sub_menu"><a href="#">Skripsi</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </li>

                  </ul>
                </div>
              
              <?php elseif ($this->session->userdata('level')==6):
                      if($this->session->userdata('jabatan')=="Wakil Rektor Akademik"):
              ?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="#"><i class="fa fa-user"></i> Mahasiswa</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a><i class="fa fa-edit"></i> Tenaga Kependidikan<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Dosen Pengajar</a></li>
                              <li><a href="#">Penasehat Akademik</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-edit"></i> Perkuliahan<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a>Jadwal Matakuliah<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url();?>perkuliahan/jadwal">Jadwal Mengajar Dosen</a></li>
                                  </ul>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Jadwal Matakuliah Mahasiswa</a></li>
                                  </ul>
                              </li>                                      
                              <!-- <li><a href="<?= base_url();?>perkuliahan/jadwal">Jadwal Mengajar</a></li> -->

                              <li><a>Perangkat Pembelajaran<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url();?>perkuliahan/modul_belajar/tampil_modulRPS">Rencana Pembelajaran Semester</a></li>
                                  </ul>
                              </li>
                              <li><a>KKP/KKN<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Pendaftaran</a></li>
                                      <li class="sub_menu"><a href="#">Dokumen Berkas</a></li>
                                  </ul>
                              </li>
                              <li><a>Skripsi dan Proposal<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Data Seminar Proposal</a></li>
                                      <li class="sub_menu"><a href="#">Data Skripsi</a></li>
                                      <li class="sub_menu"><a href="#">Data Pembimbing</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-comments"></i> Quesioner Institusi<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Saran</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-file-text"></i> Download<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Repository</a></li>
                          </ul>
                      </li>
                  </ul>
                </div>

              <?php
                      elseif($this->session->userdata('jabatan')=="Kepala BAAK"):
              ?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <!-- <li>
                          <a href="#"><i class="fa fa-user"></i> Mahasiswa</a>
                          <span class="label label-success pull-right"></span>
                      </li> -->

                      <li>
                          <a><i class="fa fa-edit"></i> Master Data<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a>Fakultas & Program Studi<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/fakultas">Fakultas</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/prodi">Program Studi</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/konsentrasi">Konsentrasi</a></li>
                                  </ul>                                 
                              </li>
                              <li><a>Data Pokok<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/mahasiswa">Mahasiswa</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/dosen">Dosen</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/matakuliah">Matakuliah</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/kelas">Kelas</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/penasehat_akademik">Penasehat Akademik</a></li>
                                  </ul>                                 
                              </li> 
                              <!-- <li><a href="#">Kelas</a></li> -->
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-edit"></i> Perkuliahan<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a>Matakuliah<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url();?>perkuliahan/jadwal">Jadwal</a></li>
                                      <li><a>Nilai<span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                              <li class="sub_menu">
                                                <a href="<?= base_url();?>perkuliahan/nilai/tampil_atur_jadwal_input_nilai">Set Jadwal Input Nilai</a>
                                              </li>
                                              <li class="sub_menu">
                                                <a href="<?= base_url();?>perkuliahan/nilai/tampil_nilai_akademik">Lihat Input Dosen</a>
                                              </li>
                                              <li class="sub_menu">
                                                <a href="<?= base_url();?>perkuliahan/nilai/tampil_update_nilai_akademik">Nilai Mata Kuliah</a>
                                              </li>
                                          </ul>    
                                      </li>
                                  </ul>
                              </li>

                              <li><a>Perangkat Pembelajaran<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url();?>perkuliahan/modul_belajar/tampil_modulRPS">Rencana Pembelajaran Semester</a></li>
                                      <li class="sub_menu"><a href="#">Modul Bahan Ajar</a></li>
                                      <li class="sub_menu"><a href="<?= base_url();?>perkuliahan/dokumen/sk_mengajar">SK Mengajar Dosen</a></li>
                                  </ul>
                              </li>
                              <li><a>KKP/KKN<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Pendaftaran</a></li>
                                      <li class="sub_menu"><a href="#">Dokumen Berkas</a></li>
                                  </ul>
                              </li>
                              <li><a>Skripsi dan Proposal<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Data Seminar Proposal</a></li>
                                      <li class="sub_menu"><a href="#">Data Skripsi</a></li>
                                      <li class="sub_menu"><a href="#">Data Pembimbing</a></li>
                                  </ul>
                              </li>
                              <li><a>Skripsi dan Proposal<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Data Seminar Proposal</a></li>
                                      <li class="sub_menu"><a href="#">Data Skripsi</a></li>
                                      <li class="sub_menu"><a href="#">Data Pembimbing</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-comments"></i> Quesioner Institusi<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="<?= base_url()?>perkuliahan/quesioner">Setting Quesioner</a></li>
                              <li><a href="#">Saran</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-file-text"></i> Download<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Repository</a></li>
                          </ul>
                      </li>
                  </ul>
                </div>

              <?php
                      elseif($this->session->userdata('jabatan')=="Staff"):
              ?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <!-- <li>
                          <a href="#"><i class="fa fa-user"></i> Mahasiswa</a>
                          <span class="label label-success pull-right"></span>
                      </li> -->

                      <li>
                          <a><i class="fa fa-edit"></i> Master Data<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a>Fakultas & Program Studi<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/fakultas">Fakultas</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/prodi">Program Studi</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/konsentrasi">Konsentrasi</a></li>
                                  </ul>                                 
                              </li>
                              <li><a>Data Pokok<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/mahasiswa">Mahasiswa</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/dosen">Dosen</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/matakuliah">Matakuliah</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/kelas">Kelas</a></li>
                                      <li class="sub_menu"><a href="<?= base_url(); ?>akademik/penasehat_akademik">Penasehat Akademik</a></li>
                                  </ul>                                 
                              </li> 
                              <!-- <li><a href="#">Kelas</a></li> -->
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-edit"></i> Perkuliahan<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a>Matakuliah<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url();?>perkuliahan/jadwal">Jadwal</a></li>
                                      <li><a>Nilai<span class="fa fa-chevron-down"></span></a>
                                          <ul class="nav child_menu">
                                              <li class="sub_menu">
                                                <a href="<?= base_url();?>perkuliahan/nilai/tampil_atur_jadwal_input_nilai">Set Jadwal Input Nilai</a>
                                              </li>

                                              <li class="sub_menu">
                                                <a href="<?= base_url();?>perkuliahan/nilai/tampil_nilai_akademik">Lihat Input Dosen</a>
                                              </li>
                                              <li class="sub_menu">
                                                <a href="<?= base_url();?>perkuliahan/nilai/tampil_update_nilai_akademik">Nilai Mata Kuliah</a>
                                              </li>
                                          </ul>    
                                      </li>
                                  </ul>
                              </li>

                              <li><a>Perangkat Pembelajaran<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="<?= base_url();?>perkuliahan/modul_belajar/tampil_modulRPS">Rencana Pembelajaran Semester</a></li>
                                      <li class="sub_menu"><a href="#">Modul Bahan Ajar</a></li>
                                      <li class="sub_menu"><a href="#">SK Mengajar Dosen</a></li>
                                  </ul>
                              </li>
                              <li><a>KKP/KKN<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Pendaftaran</a></li>
                                      <li class="sub_menu"><a href="#">Dokumen Berkas</a></li>
                                  </ul>
                              </li>
                              <li><a>Skripsi dan Proposal<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">Data Seminar Proposal</a></li>
                                      <li class="sub_menu"><a href="#">Data Skripsi</a></li>
                                      <li class="sub_menu"><a href="#">Data Pembimbing</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-comments"></i> Quesioner Institusi<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Saran</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-file-text"></i> Download<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Repository</a></li>
                          </ul>
                      </li>
                  </ul>
                </div>

              <?php 
                      endif;
              ?>

              <?php  
                    elseif ($this->session->userdata('level')==7):     
                        if($this->session->userdata('jabatan')=="Kepala Keuangan"):
              ?>  

                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a><i class="fa fa-file-o"></i> Draft PMB<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#"> Aplikan</a></li>
                              <li><a href="#"> Mahasiswa Registrasi</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-money"></i> Transaksi<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="<?= base_url();?>finance/keuangan"> Status Pembayaran</a></li>
                          </ul>
                      </li>

                  </ul>
                </div>
              <?php 
                        elseif($this->session->userdata('jabatan')=="Staff Keuangan"):
              ?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>finance/keuangan"><i class="fa fa-user"></i> Pembayaran Mahasiswa</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                  </ul>
                </div>
              <?php 
                        elseif($this->session->userdata('jabatan')=="Kasir"):
              ?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>finance/keuangan"><i class="fa fa-user"></i> Pembayaran Mahasiswa</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                  </ul>
                </div>
              <?php 
                        elseif($this->session->userdata('jabatan')=="Akuntansi"):
              ?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>finance/keuangan"><i class="fa fa-user"></i> Pembayaran Mahasiswa</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                  </ul>
                </div>
              <?php  
                        endif;
                    elseif ($this->session->userdata('level')==8): 
                        if($this->session->userdata('jabatan')=="Kepala Marketing"):
              ?>
              <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a><i class="fa fa-file-o"></i> Draft PMB<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="<?= base_url();?>pemasaran/aplikan"> Aplikan</a></li>
                              <li><a href="<?= base_url();?>pemasaran/aplikan/registrasi_aplikan"> Mahasiswa Registrasi</a></li>
                          </ul>
                      </li>

                  </ul>
                </div>
              <?php 
                        elseif($this->session->userdata('jabatan')=="Staff Marketing"):
              ?>
              <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                  </ul>
                </div>
              <?php
                        endif;
                    elseif ($this->session->userdata('level')==9):?>
                <div class="menu_section">
                  <h3>Menu Utama</h3>
                  <ul class="nav side-menu">
                      <li>
                          <a href="<?= base_url();?>dashboard"><i class="fa fa-home"></i> Dashboard</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>perkuliahan/kalender_akademik"><i class="fa fa-calendar"></i> Kalender Akademik</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a href="<?= base_url();?>ict/ict"><i class="fa fa-user"></i> Kartu Tanda Mahasiswa</a>
                          <span class="label label-success pull-right"></span>
                      </li>

                      <li>
                          <a><i class="fa fa-comments"></i> Quesioner Institusi<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li><a href="#">Saran</a></li>
                          </ul>
                      </li>

                      <li>
                          <a><i class="fa fa-file-text"></i> Download<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                              <li class="sub_menu"><a href="#">Repository</a></li>
                              <li><a>Panduan<span class="fa fa-chevron-down"></span></a>
                                  <ul class="nav child_menu">
                                      <li class="sub_menu"><a href="#">KKP/KKN</a></li>
                                      <li class="sub_menu"><a href="#">Seminar Proposal</a></li>
                                      <li class="sub_menu"><a href="#">Skripsi</a></li>
                                  </ul>
                              </li>
                          </ul>
                      </li>

                  </ul>
                </div>

              <?php  
                    endif;
              ?>

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small"> 
                <a data-toggle="tooltip" data-placement="top" title="Settings" href="#">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="FullScreen" href="#">
                    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="Lock" href="#">
                    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="Logout" href="<?php echo site_url('login/logout');?>">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
            </div>
            <div class="clearfix"></div>
            <!-- /menu footer buttons -->
          </div>
        </div>
    </div>
  </div>
  <?php $this->load->view('template/modal');?>
</html>