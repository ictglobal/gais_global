    <!-- Sweet Alert -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.css">

    <!-- Logo Global -->
    <link rel="icon" href="<?= base_url()?>temp/images/logo-global-institute.png" type="image/ico" />
    
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
    
    <!-- Nucleo Icons -->
    <link href="<?= base_url()?>front/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="<?= base_url()?>front/assets/css/nucleo-svg.css" rel="stylesheet" />

    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- CSS Files -->
    
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url()?>front/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?= base_url()?>temp/images/logo-global-institute.png">
    <link href="<?= base_url()?>front/assets/css/material-dashboard.css?v=3.0.0" rel="stylesheet" />