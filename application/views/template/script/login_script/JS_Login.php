  <footer class="footer position-absolute bottom-2 py-2 w-100">
        <div class="container">
          <div class="row align-items-center justify-content-lg-between">
            <div class="col-12 col-md-6 my-auto">
              <div class="copyright text-center text-sm text-white text-lg-start">
                ©<script>
                  document.write(new Date().getFullYear())
                </script>
                Copyright IT Global Institute&nbsp;<i class="fa fa-heart" aria-hidden="true"></i>&nbsp;
                <a href="https://www.gais.global.ac.id" class="font-weight-bold text-white" target="_blank">Think Smartly & Globally</a>
              </div>
            </div>
          </div>
        </div>
  </footer>

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
  <script src="<?= base_url();?>front/assets/js/core/popper.min.js"></script>
  <script src="<?= base_url();?>front/assets/js/core/bootstrap.min.js"></script>

 <!--  <script src="<?= base_url();?>temp/vendors/bootstrap/dist/js/bootstrap.min.js.map"></script>
   -->
  <script src="<?= base_url();?>front/assets/js/plugins/perfect-scrollbar.min.js"></script>
  <script src="<?= base_url();?>front/assets/js/plugins/smooth-scrollbar.min.js"></script>
 
  <script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
      var options = {
        damping: '0.5'
      }
      Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
  </script>
  
  <!-- Github buttons -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?= base_url();?>front/assets/js/material-dashboard.js?v=3.0.0"></script>
  <!-- Font Awesome Icons -->
  <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>

   <script type="text/javascript">
        $(document).ready(function () {
            <?php if($this->session->flashdata('warning')): ?>
                Swal({
                    title     : 'Message',
                    text      : '<?= $this->session->flashdata('warning') ?>',
                    type      : 'warning',
                    // timer     : 3000,
                }); 
            <?php elseif($this->session->flashdata('success')): ?>
                Swal({
                    title     : 'Message',
                    text      : '<?= $this->session->flashdata('success') ?>',
                    type      : 'success',
                    // timer     : 1000,
                });
            <?php elseif($this->session->flashdata('error')): ?>
                Swal({
                    title     : 'Message',
                    text      : '<?= $this->session->flashdata('error') ?>',
                    type      : 'error',
                    // timer     : 1000,
                });
            <?php endif; ?>
        })
    </script>

    