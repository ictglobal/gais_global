<!-- Modal Form untuk Export Aplikan -->
<div class="modal fade" id="input_ayam" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>perkuliahan/nilai/import_nilai2" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Upload Nilai Mahasiswa
                    </h5>
                    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button> -->
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <!-- <label class="form-label"></label> -->
                        Silahkan klik upload
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->