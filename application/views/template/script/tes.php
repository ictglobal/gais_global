    < !--Sweet Alert-- >
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <!--jQuery -->
    <script src="<?= base_url();?>temp/vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/jquery/dist/jquery-ui.js"></script>

    <!--Bootstrap -->
    <script src="<?= base_url();?>temp/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!--FastClick -->
    <script src="<?= base_url();?>temp/vendors/fastclick/lib/fastclick.js"></script>
    <!--NProgress -->
    <script src="<?= base_url();?>temp/vendors/nprogress/nprogress.js"></script>
    <!--Chart.js -->
    <script src="<?= base_url();?>temp/vendors/Chart.js/dist/Chart.min.js"></script>
    <!--gauge.js -->
    <script src="<?= base_url();?>temp/vendors/gauge.js/dist/gauge.min.js"></script>
    <!--bootstrap - progressbar-- >
    <script src="<?= base_url();?>temp/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!--iCheck -->
    <script src="<?= base_url();?>temp/vendors/iCheck/icheck.min.js"></script>
    <!--Skycons -->
    <script src="<?= base_url();?>temp/vendors/skycons/skycons.js"></script>
    <!--Flot -->
    <script src="<?= base_url();?>temp/vendors/Flot/jquery.flot.js"></script>
    <script src="<?= base_url();?>temp/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?= base_url();?>temp/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?= base_url();?>temp/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?= base_url();?>temp/vendors/Flot/jquery.flot.resize.js"></script>
    <!--Flot plugins-- >
    <script src="<?= base_url();?>temp/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?= base_url();?>temp/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/flot.curvedlines/curvedLines.js"></script>
    <!--DateJS -->
    <script src="<?= base_url();?>temp/vendors/DateJS/build/date.js"></script>
    <!--JQVMap -->
    <script src="<?= base_url();?>temp/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?= base_url();?>temp/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?= base_url();?>temp/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!--bootstrap - daterangepicker-- >
    <script src="<?= base_url();?>temp/vendors/moment/min/moment.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!--jQuery autocomplete-- >
    <script src="../vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>

    <!--Custom Theme Scripts-- >
    <script src="<?= base_url();?>temp/build/js/custom.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
    
    <!--Datatables -->
    <script src="<?= base_url();?>temp/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/pdfmake/build/vfs_fonts.js"></script>

    <script>
        $("#theSelect").select2();
    </script>

    <script type="text/javascript">
        columns: [
    // ....
    {
        data: 'Nomor',
        render: function (data, type, row, meta) {
            return meta.row
        }
    }
];
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            <?php if($this->session->flashdata('success')): ?>
                Swal({
                    title     : 'Message',
                    text      : '<?= $this->session->flashdata('success') ?>',
                    type      : 'success'
                });
            <?php endif; ?>

            <?php if($this->session->flashdata('warning')): ?>
                Swal({
                    title     : 'Message',
                    text      : '<?= $this->session->flashdata('warning') ?>',
                    type      : 'warning'
                }); 
            <?php endif; ?>

            <?php if($this->session->flashdata('error')): ?>
                Swal({
                    title     : 'Message',
                    text      : '<?= $this->session->flashdata('error') ?>',
                    type      : 'error'
                }); 
            <?php endif; ?>
            })
    </script>

    <script>
    // <![CDATA[
    // const url = '<?php //echo site_url('dashboard/cari_matkul');?>';
        $(document).ready(function () {
            $(function () {
                $("#search").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: '<?php echo site_url('dashboard/ cari_matkul');?>',
                            data: {search_data: $("#search").val()},
        dataType: "json",
        type: "POST",
        success: function(data){
            console.log(data);
        response(data);
                        }    
                    });
                },
            });
        });

        $(function () {
            $("#search1").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<?php echo site_url('dashboard/ cari_dosen');?>',
                        data: {search_data: $("#search1").val()},
        dataType: "json",
        type: "POST",
        success: function(data){
            console.log(data);
        response(data);
                        }    
                    });
                },
            });
        });

        $(function () {
            $("#search2").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<?php echo site_url('dashboard/ cari_dosen');?>',
                        data: {search_data: $("#search2").val()},
        dataType: "json",
        type: "POST",
        success: function(data){
            console.log(data);
        response(data);
                        }    
                    });
                },
            });
        });

        $(function () {
            $("#search3").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<?php echo site_url('dashboard/ cari_matkul2');?>',
                        data: {search_data: $("#search3").val()},
        dataType: "json",
        type: "POST",
        success: function(data){
            console.log(data);
        response(data);
                        }    
                    });
                },
            });
        });

        $(function () {
            $("#search10").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<?php echo site_url('dashboard/ cari_matkul2');?>',
                        data: {search_data: $("#search10").val()},
        dataType: "json",
        type: "POST",
        success: function(data){
            console.log(data);
        response(data);
                        }    
                    });
                },
            });
        });
    });
    // ]]>
    </script>

    <script>
        function ngasal(id,aksi) {
            $("#search1" + id + aksi).autocomplete({

                source: function (request, response) {
                    $.ajax({
                        url: '<?php echo site_url('dashboard/ cari_dosen');?>',
                        data: {search_data: $("#search1"+id+aksi).val()},
        dataType: "json",
        type: "POST",
        success: function(data){
            console.log(data);
        response(data);
                        }    
                    });
                },
            });
    }

        function ngasal2(id,aksi) {
            $("#search5" + id + aksi).autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: '<?= base_url()?>dashboard/cari_dosen2',
                        data: { search_data: $("#search5" + id + aksi).val() },
                        dataType: "json",
                        type: "POST",
                        success: function (data) {
                            console.log(data);
                            response(data);
                        }
                    });
                },
            });
    }
    </script>

    <script type="text/javascript">
        $("#filter input").prop("disabled", true);
    </script>
