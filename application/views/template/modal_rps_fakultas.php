<style type="text/css">
    .ui-autocomplete{
    z-index:1500;
    }
</style>

<?php 
    foreach ($rps as $key => $value) {
        $matkul = $this->user->get_data('*', 'tbl_matakuliah', "where kode_matkul = '$value->kode_matkul'")[0];
?>
<!-- Modal Form untuk Tambah RPS Dosen -->
<div class="modal fade" id="getDosen<?= $value->id_rps ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>dashboard/tambah_detail_RPS_Detail" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Tambah Dosen Pengampu RPS
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <?php
                        $modul = $this->user->get_data('*', 'tbl_rps', "where kode_matkul = '$value->kode_matkul'")[0];
                    ?>
                    <input class="form-control" type="hidden" name="id" placeholder="Cari modul" value="<?= $modul['id_rps'];?>" required readonly/>

                    <div class="form-group">
                        <label>Nama Dosen </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </span>
                            <input class="form-control " type="text" name="search_data" onkeyup="ngasal('<?= $value->kode_matkul ?>','tambah')" id="search1<?= $value->kode_matkul ?>tambah" placeholder="Cari Dosen" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Mata Kuliah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </span>
                            <input class="form-control" type="text" name="matkul" value="<?= $matkul['nama_matkul'];?>" readonly/>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Section -->
<?php } ?>

<?php 
    foreach ($rps as $key => $val) {
        $matkul = $this->user->get_data('*', 'tbl_matakuliah', "where kode_matkul = '$val->kode_matkul'")[0];
        $rpsdetail = $this->user->get_data('*', 'tbl_rpsdetail', "where kode_matkul = '$val->kode_matkul'");
?>
    <!-- Modal Form untuk Hapus RPS Dosen -->
<div class="modal fade" id="get_hapusRPSDosen<?= $val->id_rps ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>dashboard/hapus_file_RPSDosen" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Pilih Dosen yang mau dihapus
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <?php
                        $modul = $this->user->get_data('*', 'tbl_rps', "where kode_matkul = '$val->kode_matkul'")[0];
                    ?>
                    <input class="form-control" type="hidden" name="id" value="<?= $modul['id_rps'];?>" required readonly/>

                    <div class="form-group">
                        <label>Nama Dosen</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </span>
                            <select required name="search_data"  class="form-control">
                                <?php foreach ($rpsdetail as $key => $rpds):
                                    $username_dosen = $rpds['username_dosen'];
                                    $dtdosen = $this->user->get_data('*', 'tbl_dosen', "where username_dosen = '$username_dosen'")[0]; 
                                ?>
                                <option value="<?= $username_dosen ?>">
                                    <?= $dtdosen['dosen'] ?>
                                </option>
                                <?php endforeach ?>
                            </select>

                           <!--  <input class="form-control " type="text" name="cari_dosen" onkeyup="ngasal('<?//= $val->kode_matkul ?>','hapus')" id="search1<?= $val->kode_matkul ?>hapus" placeholder="Cari Dosen" required/> -->
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Mata Kuliah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </span>
                            <input class="form-control" type="text" name="matkul" placeholder="Cari matkul disini" value="<?= $matkul['nama_matkul'];?>" readonly/>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">Hapus</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Section -->
<?php } ?>