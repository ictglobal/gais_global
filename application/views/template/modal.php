<style type="text/css">
    .ui-autocomplete{
    z-index:1500;
    }
</style>

<!-- Modal Form untuk Edit Profile -->
<div class="modal fade" id="modalprofile" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url(); ?>user/edit_profile/ubah_password" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-lock mr-1"></i>Ubah Password
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <?php $this->load->view('template/folder_modal/ubah_password_user.php'); ?>
                    </div>
                      
                    <div class="form-group">
                        <label for="modal_contact_password">Password</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock"></i>
                            </span>
                            <input type="password" class="form-control" name="password" required>
                        </div>

                        <label for="modal_contact_password">Konfirmasi</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock"></i>
                            </span>
                            <input type="password" class="form-control" name="konfirm" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Section -->

<!-- Modal Form untuk Edit Data User -->
<div class="modal fade" id="modalRegisterForm" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>user/edit_profile/edit_data_user" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-user mr-1"></i>Ubah Data Pribadi
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <?php $this->load->view('template/folder_modal/ubah_data_user.php'); ?>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Section -->

<!-- Modal Form untuk Buat Modul -->
<div class="modal fade" id="buatmodul" role="dialog">
    <?php
        if ($this->session->userdata('edit_file')==1 || $this->session->userdata('edit_file')==2):
            $this->load->view('template/folder_modal/upload_modulbahanajar.php');
        endif;
    ?>
</div>
<!-- End Section -->

<!-- Modal Form untuk Buat RPS -->
<div class="modal fade" id="buatrps" role="dialog">
    <?php 
        if ($this->session->userdata('edit_file')==3):
            $this->load->view('template/folder_modal/upload_modulRPS.php');
        endif;
    ?>
</div>

<?php 
    if ($this->session->userdata('edit_file')==3):
        foreach ($rps as $key => $value) {
        $matkul = $this->user->get_data('*', 'tbl_matakuliah', "where kode_matkul = '$value->kode_matkul'")[0];
?>
<!-- Modal Form untuk Edit RPS -->
<div class="modal fade" id="editrps<?= $value->id_rps ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>perkuliahan/modul_belajar/edit_file" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Edit Modul
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <?php
                        $modul = $this->user->get_data('*', 'tbl_rps', "where kode_matkul = '$value->kode_matkul'")[0];
                    ?>
                    <input class="form-control" type="hidden" name="search_data" placeholder="Cari modul" value="<?= $modul['id_rps'];?>" required readonly/>

                    <div class="form-group">
                        <label>Mata Kuliah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </span>
                            <input class="form-control" type="text" name="search_data" placeholder="Cari matkul disini" value="<?= $matkul['nama_matkul'];?>" required readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mb-3">
                            <label class="form-label">File Upload</label>
                            <code>Silahkan upload file disini</code>
                            <input class="form-control form-control-sm" name="upload_file" type="file" accept=".pdf" required>
                            <code>hanya .pdf</code>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Section -->
<?php } ?>
<?php endif;?>

<!-- Modal Form untuk Tambah MataKuliah -->
<div class="modal fade" id="tambah_matkul" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>akademik/matakuliah/save" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Tambah Matakuliah
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Kode Mata Kuliah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="kode_matkul" required>
                        </div>

                        <label>Nama Mata Kuliah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="nama_matkul" required>
                        </div>

                        <label>Fakultas</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select required name="fakultas" class="form-control" id="id_fakultas" onchange="selFakultas()">
                                <option value="">--Pilih Fakultas---</option>
                            <?php 
                                foreach($fakultas as $key => $value):
                            ?>  
                                <option value="<?= $value->id_fakultas;?>"><?= $value->nama_fakultas;?></option>
                            <?php endforeach   ?>   
                            </select>  
                        </div>

                        <label>Program Studi</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <!-- id prsi = id_program_studi -->
                            <select required name="prodi" class="form-control" id="id_prsi" onchange="selProdi()">
                                 <option value="">--Pilih Program Studi---</option>
                            </select>
                        </div>

                        <!-- <label>Konsentrasi</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select required name="konsentrasi" class="form-control">
                                 <option value="">--Pilih Konsentrasi---</option>
                            </select>
                        </div> -->

                        <label>Semester</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="semester" required>
                        </div>

                        <label>SKS</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="number" class="form-control" name="sks" required>
                        </div>

                        <label>Kurikulum</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="kurikulum" required>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 

<!-- End Section -->

<!-- Modal Form untuk Edit Matakuliah -->
<div class="modal fade" id="editmatkul" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>akademik/matakuliah/save" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Edit Matakuliah
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Kode Mata Kuliah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="kode_matkul" required>
                        </div>

                        <label>Nama Mata Kuliah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="nama_matkul" required>
                        </div>

                        <label>Fakultas</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select required name="fakultas" class="form-control" id="id_fakultas2" onchange="selFakultas2()">
                                <option value="">--Pilih Fakultas---</option>
                            <?php 
                                foreach($fakultas as $key => $value):
                            ?>  
                                <option value="<?= $value->id_fakultas;?>"><?= $value->nama_fakultas;?></option>
                            <?php endforeach   ?>   
                            </select>  
                        </div>

                        <label>Program Studi</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <!-- id prsi = id_program_studi -->
                            <select required name="prodi" class="form-control" id="id_prsi2" onchange="selProdi2()">
                                 <option value="">--Pilih Program Studi---</option>
                            </select>
                        </div>

                      <!--   <label>Konsentrasi</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select required name="konsentrasi" class="form-control">
                                 <option value="">--Pilih Konsentrasi---</option>
                            </select>
                        </div> -->

                        <label>Semester</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="semester" required>
                        </div>

                        <label>SKS</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="number" class="form-control" name="sks" required>
                        </div>

                        <label>Kurikulum</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="kurikulum" required>
                        </div>

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->


<!-- Modal Form untuk Export Aplikan -->
<div class="modal fade" id="export_aplikan" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>pemasaran/aplikan/export_aplikan" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Upload File Aplikan
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Upload</label>
                        <div class="input-group">
                            <input type="file" class="form-control" name="file" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->

<!-- Modal Form untuk Export Kalender -->
<div class="modal fade" id="export_kalender" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>perkuliahan/kalender_akademik/export_kalender" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Upload File Kalender
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <div class="input-group">

                            <select class="form-control" id="select_tahun_akademik" style="width: 100%;" name="tahun_akademik" required>
                                <option value="">Pilih Tahun Akademik</option>
                                    <?php 
                                        $tg_awal= date('Y')-4;
                                        $tgl_akhir= date('Y')+1;
                                        for ($i=$tgl_akhir; $i>=$tg_awal; $i--){
                                            $tgl_akhir = $i + 1;
                                            $th = $i." - ".$tgl_akhir;
                                            // $kode = $this->encrypt->encode($th);
                                        ?>
                                <option value="<?= $th?>"><?= $i." - ".$tgl_akhir; ?></option>
                                    <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Upload</label>
                        <div class="input-group">
                            <input type="file" class="form-control" name="file" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->

<!-- Modal Form untuk Import Nilai -->
<div class="modal fade" id="export_nilai" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url()?>perkuliahan/nilai/export_nilai_bagian_akademik" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Export Nilai Mahasiswa
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <br>
                        <select class="form-control" name="tahun_akademik" id="select_tahun_akademik2" style="width:100%;" required>
                            <option value="">Pilih Tahun Akademik</option>
                            <?php foreach($tahunAkademik as $thn): ?>
                                <option value="<?= $thn['id_tahunakademik'] ?>">
                                         <?= $thn['tahun_akademik'] ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Proses</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->

<!-- Modal Form untuk Import Nilai -->
<div class="modal fade" id="import_nilai" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url()?>perkuliahan/nilai/import_nilai" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Upload Nilai Mahasiswa
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <br>
                        <select class="form-control" name="tahun_akademik" id="select_tahun_akademik4" style="width:100%;" required>
                            <option value="">Pilih Tahun Akademik</option>
                            <?php foreach($tahunAkademik as $thn): ?>
                                <option value="<?= $thn['id_tahunakademik'] ?>">
                                         <?= $thn['tahun_akademik'] ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Upload</label>
                        <div class="input-group">
                            <input type="file" class="form-control" name="file" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-success" onclick="window.location.href='<?=base_url() ?>file_upload/akademik/nilai/format/data_khs.xlsx'" >Download File</button>
                    <button type="submit" class="btn btn-primary">Proses</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->


<!-- Modal Form untuk Tambah Mahasiswa -->
<div class="modal fade" id="tambah_mahasiswa" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?=base_url()?>akademik/mahasiswa/tambah" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-edit mr-1"></i>Tambah Mahasiswa
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body"> 
                    <div class="form-group">

                        <label class="form-label">Tahun Akademik</label>
                        <div class="input-group">
                            <select class="form-control" id="theSelect3" name="ta" required style="width:100%;">
                                <option value="">Pilih Tahun Akademik</option>
                                    <?php 
                                        $tg_awal= date('Y')-13;
                                        $tgl_akhir= date('Y')+1;
                                        for ($i=$tgl_akhir; $i>=$tg_awal; $i--){
                                            $tgl_akhir = $i + 1;
                                            $th = $i." - ".$tgl_akhir;
                                    ?>
                                <option value="<?= $th?>"><?= $i." - ".$tgl_akhir; ?></option>
                                    <?php } ?>
                            </select>
                        </div>

                        <label class="form-label">NIM</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" class="form-control" name="nim" required>
                        </div>

                        <label class="form-label">NISN</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" class="form-control" name="nisn" required>
                        </div>

                        <label class="form-label">NIK</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="16" class="form-control" name="nik_mahasiswa" required>
                        </div>

                        <label class="form-label">Nama</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                            <input type="text" class="form-control" name="nama" style="text-transform:uppercase" required>
                        </div>

                        <label class="form-label">Jenis Kelamin</label>
                        <div class="input-group">
                            <select class="form-control" name="jk_tambah" id="jk3" style="width:100%;" required>
                                <option value="">--Pilih Jenis Kelamin---</option>
                                <option value="L">Laki - laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>

                        <label class="form-label">Tempat Lahir</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="tempat_lahir" required>
                        </div>

                        <label class="form-label">Tanggal Lahir</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input id="birthday" class="date-picker form-control" name="tanggal_lahir" placeholder="mm/dd/yyyy" type="text" required="required" type="text" onfocus="this.type='date'" onmouseover="this.type='date'" onclick="this.type='date'" onblur="this.type='text'" onmouseout="timeFunctionLong(this)">
                                <script>
                                    function timeFunctionLong(input) {
                                        setTimeout(function() {
                                        input.type = 'text';
                                    }, 60000);
                                }
                                </script>
                        </div>  

                        <label class="form-label">Kelas</label>
                        <div class="input-group">
                            <select class="form-control" name="kelas" id="kelas2" style="width:100%;" required>
                                <option value="">--Pilih Kelas---</option>
                                <?php foreach ($kelas as $key => $value) { ?>
                                    <option value="<?= $value->id_kelas ?>"><?=$value->id_kelas ?></option>
                                <?php }  ?>
                            </select>
                        </div>

                        <label class="form-label">Agama</label>
                        <div class="input-group">
                            <select class="form-control" name="agama" id="agama" style="width:100%;" required>
                                <option value="">--Pilih Agama---</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                                <option value="Konghucu">Konghucu</option>
                            </select>
                        </div>

                        <label class="form-label">Fakultas</label>
                        <div class="input-group">
                            <select required name="fakultas" class="form-control" id="fakultas" style="width:100%;" onchange="get_fakultas()">
                                <option value="">--Pilih Fakultas---</option>
                            <?php 
                                foreach($fakultas as $key => $value):
                            ?>  
                                <option value="<?= $value->id_fakultas;?>"><?= $value->nama_fakultas;?></option>
                            <?php endforeach   ?>   
                            </select>  
                        </div>

                        <label class="form-label">Program Studi</label>
                        <div class="input-group">
                            <select required name="prodi" class="form-control" id="prodi" style="width:100%;" onchange="get_prodi()">
                                 <option value="">--Pilih Program Studi---</option>
                            </select>
                        </div>

                        <!-- <label class="form-label">Konsentrasi</label>
                        <div class="input-group">
                            <select class="form-control" name="konsentrasi" id="konsentrasi2">
                                <option value="">--Pilih Konsentrasi---</option>
                            </select>
                        </div> -->

                        <label class="form-label">Program Kuliah</label>
                        <div class="input-group">
                            <select class="form-control" name="pilihan_kuliah" id="pilihan_kuliah" style="width:100%;" required>
                                <option>--Pilih Program Kuliah---</option>
                                <option value="R">Reguler (Kelas Pagi)</option>
                                <option value="NR">Non Reguler (Kelas Malam/Shift/JSM)</option>
                            </select>
                        </div>

                        <label class="form-label">NIK Ayah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="16" class="form-control" name="nik_ayah" required>
                        </div>

                        <label class="form-label">Nama Ayah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="nama_ayah" style="text-transform:uppercase" required>
                        </div>

                        <label class="form-label">NIK Ibu</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="16" class="form-control" name="nik_ibu" required>
                        </div>

                        <label class="form-label">Nama Ibu</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" style="text-transform:uppercase" name="nama_ibu" required>
                        </div>

                        <label class="form-label">Nomor Telepon</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control"  name="telp" style="text-transform:uppercase" required>
                        </div>

                        <label class="form-label">Alamat Lengkap</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <textarea name="alamat_lengkap" class="form-control" required></textarea>
                        </div>

                            <!-- <label class="form-label">Password</label>
                            <div class="input-group">
                                <input type="password" name="pass" id="password" class="form-control" data-toggle="password"required>
                            </div> -->
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Section -->

<!-- Modal Form untuk Import Jadwal -->
<div class="modal fade" id="import_jadwal" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>perkuliahan/jadwal/import_jadwal" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Import Jadwal
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <div class="input-group">

                            <select class="form-control" id="Select_Oi" style="width: 100%;" name="tahun_akademik" required>
                                <option value="">Pilih Tahun Akademik</option>
                                <?php  
                                    foreach($tahun_akademik as $key => $value):
                                ?>
                                <option value="<?= $value['id_tahunakademik'] ?>">
                                    <?php 
                                        $kajew = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$value[id_tahunakademik]' ORDER BY tahun_akademik");
                                        foreach ($kajew as $key3 => $thn) {
                                            echo $thn['tahun_akademik'];
                                        }
                                    ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Upload</label>
                        <div class="input-group">
                            <input type="file" class="form-control" name="file" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->

<!-- Modal Form untuk Export Jadwal -->
<div class="modal fade" id="export_jadwal" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>perkuliahan/jadwal/export_jadwal" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Export Jadwal
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <div class="input-group">
                            <select class="form-control" id="select_tahun_akademik3" style="width: 100%;" name="tahun_akademik" required>
                                <option value="">Pilih Tahun Akademik</option>
                                <?php  
                                    foreach($tahun_akademik as $key => $value):
                                ?>
                                <option value="<?= $value['id_tahunakademik'] ?>">
                                    <?php 
                                        $kajew = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$value[id_tahunakademik]' ORDER BY tahun_akademik");
                                        foreach ($kajew as $key3 => $thn) {
                                            echo $thn['tahun_akademik'];
                                        }
                                    ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Proses</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->



