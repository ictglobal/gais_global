<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
          <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Bahan Ajar <small><?= $this->session->userdata('role');?></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown" style="visibility: hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <div>
                        &nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-primary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#buatmodul">Tambah</button>
                    </div>

                    <div class="table-responsive">
                      <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                              <th class="text-center" width="4%">No</th>
                              <th class="text-center" width="10%">Kode</th>
                              <th class="text-center">Mata Kuliah</th>
                              <th class="text-center" width="5%">SKS</th>
                              <th class="text-center" width="20%">Kelas</th>
                              <th class="text-center" width="15%">Program Studi</th>
                              <!-- <th class="text-center">File</th> -->
                              <?php if ($this->session->userdata('level')==2): ?>
                                  <th class="text-center" width="8%">Opsi</th>
                              <?php elseif ($this->session->userdata('level')==5): ?>
                                  <th class="text-center" width="17%">Opsi</th>
                              <?php endif;?>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i=1;
                            foreach ($modul_ajar as $key => $value) { 
                        ?>
                            <tr>
                                <td align="center"><?= $i;?></td>
                                <td><?= $value->kode_matkul;?></td>
                                <td>
                                  <?php
                                      $matkul = $this->user->get_data('*', 'tbl_matakuliah', "where kode_matkul = '$value->kode_matkul'")[0];
                                      echo $matkul['nama_matkul'];
                                  ?>    
                                </td>
                                <td align="center">
                                  <?php
                                      echo $matkul['sks'];
                                  ?>
                                </td>
                                <td>
                                    <?php 
                                        $j = 0;
                                        foreach ($jadwal3 as $vla) {
                                          if ($vla->kode_matkul == $value->kode_matkul) {
                                           echo "<b>".++$j.". </b>".$vla->id_kelas."<br>";
                                          }
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                        $kkk = 0;
                                        foreach ($jadwal3 as $keyval => $vla) {
                                         if ($kkk == 0) {
                                            if ($vla->kode_matkul == $value->kode_matkul) {
                                                $mt_kul = $this->user->get_data("*", "tbl_matakuliah", "where kode_matkul = '$vla->kode_matkul'")[0];
                                                $m = $mt_kul['id_programstudi'];

                                                $id = $this->user->get_data("*", "tbl_programstudi", "where id_programstudi = '$m'")[0];
                                                echo $id['nama_programstudi']."<br>";
                                                $kkk++;
                                            }
                                          }
                                        }
                                    ?>
                                </td>
                                <!-- <td><?php //echo basename($value->file);?></td> -->
                                <td align="center">
                                    <button type="button" class="btn btn-primary btn-sm" onclick="location.href='<?php echo base_url();?>file_upload/modul/<?php echo $value->file?>';">
                                        <span class="fa fa-download fa-lg"></span>
                                    </button>
                                    <?php //if ($this->session->userdata('level')==5): ?>
                                        <button type="button" class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#editmodul<?= $value->id_modul ?>">
                                            <span class="fa fa-edit fa-lg"></span>
                                        </button>
                                        <button type="button" class="btn btn-danger btn-sm" onclick="location.href='<?= base_url().'perkuliahan/modul_belajar/delete_/'.$value->id_modul?>';">
                                            <span class="fa fa-trash-o fa-lg"></span>
                                        </button>
                                    <?php //endif;?>
                                </td>
                            </tr>
                        <?php $i++; } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>          
          <!-- top tiles -->
    </div>
      </div>
    </div>
    <?php 
    if ($this->session->userdata('edit_file')==1 || $this->session->userdata('edit_file')==2):
        foreach ($modul_ajar as $key => $value) {
        $matkul = $this->user->get_data('*', 'tbl_matakuliah', "where kode_matkul = '$value->kode_matkul'")[0];
?>
<!-- Modal Form untuk Edit Modul -->
<div class="modal fade" id="editmodul<?= $value->id_modul ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>perkuliahan/modul_belajar/edit_file" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Edit Modul
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <?php
                        $modul = $this->user->get_data('*', 'tbl_modulajar', "where kode_matkul = '$value->kode_matkul'")[0];
                    ?>
                    <input class="form-control" type="hidden" name="id_modul" placeholder="Cari modul" value="<?= $modul['id_modul'];?>" required readonly/>

                    <div class="form-group">
                        <label>Mata Kuliah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-search"></i>
                            </span>
                            <input class="form-control" type="text" name="search_data" placeholder="Cari matkul disini" value="<?= $matkul['nama_matkul'];?>" required readonly/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="mb-3">
                            <label class="form-label">File Upload</label>
                            <code>Silahkan upload file disini</code>
                            <input class="form-control form-control-sm" name="upload_file" type="file" accept=".doc, docx, .xls, .xlsx, .ppt, .pptx, .pdf, .rar, .zip" required>
                            <code>Support doc, .docx, .xls, .xlsx, .ppt, .pptx, .pdf, .rar, .zip</code>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Section -->
<?php } ?>
<?php endif;?>
  </body>
</html>