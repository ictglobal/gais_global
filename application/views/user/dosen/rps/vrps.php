<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
          <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Rencana Pembelajaran Semester</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown" style="visibility: hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <?php if($this->session->userdata('level')==5):?>
                    <div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                      <?php ?>
                        <button class="btn btn-primary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#buatrps">
                          Tambah
                        </button>
                      <?php ?>
                    </div>
                    <?php endif; ?>
                    
                    <?php 
                        if($this->session->userdata('level')==2):
                          $this->load->view('user/dosen/rps/vrps_dosen.php');
                          $this->load->view('template/modal');
                        elseif($this->session->userdata('level')==4):
                          $this->load->view('user/dosen/rps/vrps_fakultas.php');
                          $this->load->view('template/modal_rps_fakultas.php');
                        elseif($this->session->userdata('level')==5):
                          $this->load->view('user/dosen/rps/vrps_kaprodi.php');
                          $this->load->view('template/modal');
                        endif;
                    ?>  
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>          
          <!-- top tiles -->
    </div>
      </div>
    </div>
    <?php //$this->load->view('template/modal');?>
  </body>
</html>