<div class="table-responsive">
    <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th class="text-center" width="4%">No</th>
                <th class="text-center">Mata Kuliah</th>
                <th class="text-center" width="15%">Program Studi</th>
                <th class="text-center" width="15%">SKS</th>
                <th class="text-center" width="15%">Unduh</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1;
                foreach ($rps as $key => $value) { 
            ?>
            <tr>
                <td align="center"><?= $i;?></td>
                <td>
                    <?php
                        $matkul   = $this->user->get_data("*", "tbl_rps", "where id_rps = '$value->id_rps'")[0];
                        $ayam     = $matkul['kode_matkul'];
                        $matkul2  = $this->user->get_data("*", "tbl_matakuliah", "where kode_matkul = '$ayam'")[0];
                        echo $matkul2['nama_matkul'];
                    ?>
                </td>
                <td align="center">
                    <?php echo $matkul2['prodi'];?>
                </td>
                <td align="center">
                    <?php echo $matkul2['sks'];?>
                </td>
                <td align="center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="location.href='<?php echo base_url();?>file_upload/rps/<?php echo $matkul['file'];?>'"> 
                        <span class="fa fa-download fa-lg"></span>
                    </button>             
                </td>
            </tr>
            <?php $i++; } ?>
        </tbody>
    </table>
</div>