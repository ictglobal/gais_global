<div class="table-responsive">
    <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th class="text-center" width="1%">No</th>
                <th class="text-center" width="1%">Kode</th>
                <th class="text-center" width="18%">Mata Kuliah</th>
                <th class="text-center" width="2%">SKS</th>
                <th class="text-center" width="20%">Dosen Pengampu</th>
                <th class="text-center" width="10%">Program Studi</th>
                <th class="text-center" width="3%">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1;
                foreach ($rps as $key => $value) { 
            ?>
            <tr>
                <td align="center"><?= $i;?></td>
                <td align="center">
                    <?php
                        $rps1         = $this->user->get_data("*", "tbl_rps", "where id_rps = '$value->id_rps'")[0];
                        $bebek        = $rps1['kode_matkul'];

                        $matkul2  = $this->user->get_data("*", "tbl_matakuliah", "where kode_matkul = '$bebek'")[0];
                        echo $bebek;
                    ?>
                </td>
                <td> <?= $matkul2['nama_matkul']; ?></td>
                <td align="center"> <?= $matkul2['sks']; ?></td>
                <td>
                    <?php
                        $ayam = $this->user->get_data("*", "tbl_rpsdetail", "WHERE id_rps='$value->id_rps'");
                        foreach ($ayam as $key => $ays) {
                            $ay     = $ays['username_dosen'];

                            $bebek = $this->user->get_data("*", "tbl_dosen", "WHERE username_dosen='$ay' ORDER BY username_dosen ASC")[0];
                            if($key != 0){
                                echo ' || ';
                            }
                            echo $bebek['dosen']." ".$bebek['gelar'];
                        }
                    ?>
                </td>
                <td>
                    <?php
                        $tolol = $this->user->get_data("*", "tbl_rps", "WHERE id_rps='$value->id_rps'")[0];
                        $cicak = $tolol['id_programstudi'];

                        $bego  = $this->user->get_data("nama_programstudi", "tbl_programstudi", "WHERE id_programstudi = '$cicak'")[0];
                        echo $bego['nama_programstudi'];
                    ?>
                </td>                
                <td align="center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="location.href='<?php echo base_url();?>file_upload/rps/<?php echo $value->file?>';">
                        <span class="fa fa-download fa-lg"></span>
                    </button>
                    <button type="button" class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#getDosen<?= $value->id_rps ?>">
                    </button>
                    <?php 
                        $j = count($ayam);
                        if ($j > 0):
                    ?>
                    <button type="button" class="btn btn-danger btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#get_hapusRPSDosen<?= $value->id_rps ?>">
                        <span class="fa fa-trash fa-lg"></span>
                    </button>
                    <?php endif ?>
                </td>
            </tr>
            <?php $i++; } ?>
        </tbody>
    </table>
</div>
