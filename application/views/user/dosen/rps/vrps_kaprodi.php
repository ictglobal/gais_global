<div class="table-responsive">
    <table id="datatable-fixed-header" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th class="text-center" width="4%">No</th>
                <th class="text-center" width="5%">Kode</th>
                <th class="text-center">Mata Kuliah</th>
                <th class="text-center" width="2%">SKS</th>
                <!-- <th class="text-center">Dosen Pengampu</th> -->
                <th class="text-center" width="25%">Opsi</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1;
                foreach ($rps as $key => $value) { 
            ?>
            <tr>
                <td align="center"><?= $i;?></td>
                <td>
                    <?php
                        $detail_rps   = $this->user->get_data("*", "tbl_rps", "where id_rps = '$value->id_rps'")[0]; 
                        $matkul2  = $this->user->get_data("*", "tbl_matakuliah", "where kode_matkul = '$detail_rps[kode_matkul]'")[0];
                        echo $matkul2['kode_matkul'];
                    ?>    
                </td>
                <td><?= $matkul2['nama_matkul'];?></td>
                <td align="center"><?= $matkul2['sks'];?></td>
                <!-- <td>
                    <?php
                        // $ayam = $this->user->get_data("*", "tbl_rpsdetail", "WHERE id_rps='$value->id_rps'");
                        // foreach ($ayam as $key => $ays) {
                        //     $ay     = $ays['username_dosen'];

                        //     $bebek = $this->user->get_data("*", "tbl_dosen", "WHERE username_dosen='$ay' ORDER BY username_dosen ASC")[0];
                        //     if($key != 0){
                        //         echo ' || ';
                        //     }
                        //     echo $bebek['dosen']." ".$bebek['gelar'];
                        // }
                    ?>
                </td> -->
                <td align="center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="location.href='<?php echo base_url();?>file_upload/rps/<?php echo $value->file?>';">
                        <span class="fa fa-download fa-lg"></span>
                    </button>
                    <button type="button" class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#editrps<?= $value->id_rps ?>">
                        <span class="fa fa-edit fa-lg"></span>
                    </button>                                  
                    <button type="button" class="btn btn-danger btn-sm" onclick="location.href='<?= base_url().'perkuliahan/modul_belajar/delete_/'.$value->id_rps?>';"> 
                        <span class="fa fa-trash-o fa-lg"></span> 
                    </button>
                </td>
            </tr>
            <?php $i++; } ?>
        </tbody>
    </table>
</div>