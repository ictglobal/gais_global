<!DOCTYPE html>
<html lang="en">
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
          <div class="right_col" role="main">
          <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Detail Dosen</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li class="dropdown" style="visibility: hidden;">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
<!--                         <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div> -->
                        </li>
                      <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li> -->
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <!-- <p>Laman ini berisi biodata pribadi <?= $this->session->userdata('role');?>. Silahkan klik Tombol <code>Edit Data</code> Jika ada perbaikan data</p> -->

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                        <?php 
                            foreach ($tampil_dosen as $key => $value) {
                        ?> 
                            <tr class="even pointer">
                                <td>Username</td>
                                <td>:</td>
                                <td><?= $value->username_dosen;?>
                                    <i class="success fa fa-long-arrow-up"></i>
                                </td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Nama Lengkap</td>
                                <td>:</td>
                                <td><?= $value->dosen;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>
                                    <?php 
                                        if($value->jenis_kelamin == "L"):
                                            echo "Laki -laki";
                                        else:
                                            echo "Perempuan";
                                        endif;
                                    ?>
                                </td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Gelar</td>
                                <td>:</td>
                                <td><?= $value->gelar;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Jenjang</td>
                                <td>:</td>
                                <td><?= $value->jenjang;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Jabatan Fungsional</td>
                                <td>:</td>
                                <td><?= $value->jafung;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Email</td>
                                <td>:</td>
                                <td><?= $value->email;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Nomor Telepon</td>
                                <td>:</td>
                                <td><?= $value->telp;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?= $value->alamat;?></td>
                            </tr>

                          <?php } ?>
                        </tbody>
                      </table>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
      </div>
    </div>
  </body>
</html>

<script type="text/javascript">
    function cetak_form_aplikan(){
        window.open('<?= base_url()?>pemasaran/aplikan/cetak_form_aplikan/', '_blank')
    }
</script>


