<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="btn-group btn-group-md" role="group" aria-label="...">
                <button class="btn btn-primary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#tambah_sk_mengajar">Tambah</button> <br>
                <button class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#import_sk_mengajar">Import</button>
            </div><br>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Surat Keterangan Mengajar <code>Dosen</code></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form id='formselect'>
                                    <div class="col-sm-3">
                                        <label for="">Filter</label>
                                        <select class="form-control" id="tahun_akademik2" onchange="selection()" required>
                                            <option value="">Pilih Tahun Akademik</option>
                                            <?php
                                                foreach ($tahun_akademik as $key => $value):
                                                    $tetew = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$value[id_tahunakademik]'")[0];
                                            ?>
                                            <option value="<?= $this->encryption->encrypt($value['id_tahunakademik']);?>">
                                                <?= $tetew['tahun_akademik'];?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </form>
                                <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="7%">No</th>
                                            <th class="text-center" width="40%">Kode SK</th>
                                            <th class="text-center">Matakuliah</th>
                                            <th class="text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url().'temp/jquery-3.6.0.js'?>"></script>
    <script type="text/javascript">
        function selection() {
            var id = $('#tahun_akademik2').val();
            var encoded_url = encodeURIComponent(id);
            window.location.href = '<?= base_url()?>perkuliahan/dokumen/tampil_sk_mengajar?th='+encoded_url;
        }
    </script>
  </body>
</html>

<!-- Modal Form untuk tambah sk mengajar -->
<div class="modal fade" id="tambah_sk_mengajar" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>perkuliahan/dokumen/tambah_sk" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Upload File SK
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <div class="input-group">
                            <select class="form-control" id="theSelect" style="width: 100%;" name="tahun_akademik" required>
                                <option value="">Pilih Tahun Akademik</option>
                                <?php
                                    foreach ($tahun_akademik as $key => $value):
                                        $tetew = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$value[id_tahunakademik]'")[0];
                                ?>
                                    <option value="<?= $this->encryption->encrypt($value['id_tahunakademik']);?>">
                                        <?= $tetew['tahun_akademik'];?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Dosen</label>
                        <div class="input-group">
                            <select class="form-control" id="theSelect3" style="width: 100%;" name="dosen" required>
                                <option value="">Pilih Dosen</option>
                                <?php
                                    foreach ($get_dosen as $key => $value):
                                ?>
                                    <option value="<?= $value->username_dosen?>">
                                        <?= $value->dosen;?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Upload</label>
                        <div class="input-group">
                            <input type="file" class="form-control" name="file_sk" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Proses</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->

<!-- Modal Form untuk import sk mengajar dari file excel -->
<div class="modal fade" id="import_sk_mengajar" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Import File SK dari Excel
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Upload <code>hanya file excel *xls, *xlsx</code></label>
                        <div class="input-group">
                            <input type="file" class="form-control" name="file" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Proses</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->