<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Evaluasi Umpan Balik <small>Periode <?= $kode; ?></small></h2>
                    <!-- <input type="text" name="ayam" value="<?= $kode; ?>" id="kode_tahun_akademik"> -->
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form method="post">
                                    <!-- <input type="text" name="tahun_akad" value="<?= $this->encryption->decrypt($tahun_akademik);?>" > -->
                                    <div class="col-sm-3">
                                            <label for="">Filter Tahun Akademik</label>
                                            <select class="form-control" id="theSelect2" onchange="select_eub()" required>
                                            <option value="">Pilih Tahun Akademik</option>
                                            <?php foreach($filter_ta as $key => $value): ?>
                                            <option value="<?= $this->encryption->encrypt($value->id_tahunakademik);?>">
                                                <?= $value->tahun_akademik; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                        </div>
                                    </form>
                                    <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>    
                                            <th class="text-center" rowspan="2" width="5%" style="vertical-align: text-top;">No&nbsp;</th> <!-- column 1 -->
                                            <th class="text-center" colspan="5">Penilaian Evaluasi Umpan Balik&nbsp;</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center" width="7%">Kode&nbsp;</th> <!-- column 2 -->
                                            <th class="text-center">Nama Matakuliah&nbsp;</th> <!-- column 3 -->
                                            <th class="text-center" width="14%">Kelas&nbsp;</th> <!-- column 3 -->
                                            <th class="text-center" width="6%%">Jumlah&nbsp;</th> <!-- column 4 -->
                                            <th class="text-center" width="20%">Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php 
                                            foreach ($eub as $key => $value):
                                                $tot = number_format($value->imk,2, ',', '');
                                        ?>
                                            <tr>
                                                <td align="center"></td>
                                                <td align="center"><?= $value->kode_matkul ?></td>
                                                <td><?= $value->nama_matkul ?></td>
                                                <td><?= $value->id_kelas ?></td>
                                                <td align="center">
                                                    <?php
                                                        echo $tot;
                                                    ?>
                                                </td>
                                                <td align="center">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-success btn-sm" 
                                                        onclick="click_detail_eub('<?= $this->encryption->encrypt($value->id_kelas."_".$value->id_jadwal."||".$value->id_tahunakademik)?>')"> 
                                                            <span class="fa fa-eye fa-lg"></span> Detail
                                                        </button>&nbsp;&nbsp;
                                                        <button type="button" class="btn btn-primary btn-sm" 
                                                        onclick="click_saran('<?= $this->encryption->encrypt($value->id_kelas."_".$value->id_tahunakademik)?>')"> 
                                                            <span class="fa fa-eye fa-lg"></span> Saran
                                                        </button>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                    <thead>
                                        <tr>    
                                            <th class="text-right" colspan ="5" style="color:red;">Total Keseluruhan&nbsp;</th> 
                                            <th class="text-center" colspan="2"><?= $totttt;?></th>
                                        </tr>
                                        <tr>    
                                            <th class="text-right" colspan ="5" style="color:red;">Indeks Kumulatif (Interval 0-4)&nbsp;</th> 
                                            <th class="text-center" colspan="2"><?= $indeks;?></th>
                                        </tr>
                                        <tr>    
                                            <th class="text-right" colspan ="5" style="color:red;">Keterangan Penilaian&nbsp;</th> 
                                            <th class="text-justify" colspan ="2"><?= $keterangan_nilai  ?>&nbsp;</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
          <?php //$this->load->view('template/modal'); ?>
    </div>

    <script type="text/javascript">

        function click_detail_eub(id_kls){
            var encoded_url = encodeURIComponent(id_kls);
            window.location.href = '<?= base_url()?>perkuliahan/evaluasi_umpan_balik/detail_eub?detail='+encoded_url;
        }

        function select_eub(){
            var id = $('#theSelect2').val();
            var encoded_url = encodeURIComponent(id);
            window.location.href = '<?= base_url()?>perkuliahan/evaluasi_umpan_balik/tampil_eub?th='+encoded_url;
        }

        function click_saran(id_kelas){
            var id    = id_kelas;
            var encoded_url = encodeURIComponent(id);
            // console.log(encoded_url);
            window.location.href = '<?= base_url()?>perkuliahan/evaluasi_umpan_balik/detail_saran?ayam='+encoded_url;   
        }
    </script>
  </body>
</html>