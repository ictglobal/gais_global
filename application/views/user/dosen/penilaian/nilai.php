<!DOCTYPE html>
<html lang="en">
    <style type="text/css">
        .cursor{
            cursor: pointer;
        }
        .tableedit{
            display: none;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Penilaian Mahasiswa <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form action="<?= base_url()?>perkuliahan/nilai/input_nilai" method="post">
                        <div class="row mb-5">
                            <div class="col-sm-3" id="tahun_akademik">
                                <label for="">Tahun Akademik</label>
                                <select id="select_page" class="form-control" required disabled="true">
                                    <!-- <option value="">Pilih Tahun Akademik</option> -->
                                    <option value="<?= $tahun_akademik ?>">
                                        <?= $tahun_akademik ?>
                                    </option>
                                </select>
                                <input type="hidden" name="tahun_akademik" value="<?= $id_tahunakademik ?>" readonly>
                            </div>

                            <!-- <div class="col-sm-3">
                                <label for="">Kelas</label>
                                <select name="kelas" id="smt" class="form-control" onchange="getSemester()"   required>
                                    <option value="">Pilih Kelas</option>
                                        <?php foreach ($pilih_matkul2 as $key => $modulaja): ?>
                                            <?php 
                                            $id_kelas = explode(' ', $modulaja['id_kelas']);
                                            $id_kelas = implode('_',$id_kelas);
                                             ?>
                                            <option value="<?= $id_kelas ?>">
                                                <?= $modulaja['id_kelas'] ?>
                                            </option>
                                        <?php endforeach; ?>
                                </select>
                            </div> -->

                            <div class="col-sm-3">
                                <label for="">Mata Kuliah</label>
                                <select name="matakuliah" class="form-control" id="matakuliah_select" required onchange="select_kelas_matkul()">
                                <option value="">Pilih Mata Kuliah</option>
                                    <?php foreach ($pilih_matkul as $key => $modulaja): 
                                            $kode_matkul = $modulaja['kode_matkul'];
                                            $matakuliah = $this->user->get_data('*','tbl_matakuliah',"  where kode_matkul = '$kode_matkul'")[0];
                                            $id_programstudi = $matakuliah['id_programstudi'];
                                            $programstudy = $this->user->get_data('*','tbl_programstudi'," where id_programstudi = '$id_programstudi'")[0];
                                    ?>
                                            <option value="<?= $kode_matkul ?>"><?= $matakuliah['nama_matkul'] ?> || <?= $programstudy['nama_programstudi'] ?>  </option>            
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <div class="col-sm-3" hidden>
                                <label class="form_control">Semester</label>
                                <select name="semester" id="smt2" class="form-control" required>
                                    <option>Pilih Semester</option>
                                    <!-- <?php foreach ($semester as $key => $smt): ?>
                                            <option value="<?= $smt['semester'] ?>">
                                                <?= $smt['semester'] ?>
                                            </option>
                                    <?php endforeach; ?> -->                                           
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-md btn-round btn-block">Filter</button>
                    </form>                   
                </div>
            </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url().'temp/jquery-3.6.0.js'?>"></script>
    <script>
        function select_kelas_matkul(){
            var matkul = document.getElementById("matakuliah_select").value;
            console.log(matkul);
            // $('smt2').load('<?= base_url()?>perkuliahan/nilai/select_smt/'+matkul);
            $('#smt2').load('nilai/select_smt/'+matkul);
        }

        // function getSemester(){
        //     var smt = document.getElementById("smt").value;
        //     console.log(smt);
        //     $('#smt2').load('nilai/selectSemeser/'+smt);

        // }

        $(document).ready(function () {
            //change selectboxes to selectize mode to be searchable
            $("select").select2();
            // $("#smt2").load("");
        });
        
    </script>
  </body>
</html>