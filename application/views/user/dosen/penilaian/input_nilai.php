<!DOCTYPE html>
  <html lang="en">
  <style type="text/css">
      .cursor{
        cursor: pointer;
      }
      .tableedit{
        display: none;
      }
  </style>
  <link href="https://printjs-4de6.kxcdn.com/print.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="row">
                &nbsp;&nbsp;&nbsp;
                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                    <div class="btn-group" role="group">

                        <button type="button" class="btn btn-success" target="_blank" 
                        <?php if($this->session->userdata('level')==2): ?>
                            onclick="tombol_export_pdf();" 
                        <?php elseif($this->session->userdata('level')==6): ?>
                            onclick="tombol_export_pdf_akademik();"
                        <?php endif; ?>
                        >
                         <i class="fa fa-print"></i> Cetak
                        </button>
                        <button type="button" class="btn btn-info" onclick="tombol_export_excel();"><i class="fa fa-file-excel-o"></i> Export</button>
                        <?php if($this->session->userdata('level') == 2): ?>
                        <button type="button" class="btn btn-danger" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#import_nilai2"><i class="fa fa-file-excel-o"></i> Import</button>
                        <?php endif; ?>
                        <br>
                    </div>      
                </div>
            </div>             
            <!-- <button class="btn btn-success btn-sm" id="btn_export" onclick="tombol_export_excel()">Export Excel</button>
             <button class="btn btn-primary btn-sm" id="btn_import" value="Choose Files!" onclick="document.getElementById('fileInput').click();">Import Excel</button>    
            <button class="btn btn-primary btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#import_nilai2">Import Excel</button>
            <input id="fileInput" type="file" style="display:none;" /> -->

            <div class="x_panel">
                <div class="x_title">
                    <h2>Penilaian Mahasiswa <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                
                <?php 
                    $get = $this->user->get_data('*','tbl_matakuliah',"  where kode_matkul = '$matakuliah'")[0];
                    if($this->session->userdata('level')=='6'):
                        $k   = $this->user->get_data('*','tbl_kelas',"  where id_kelas = '$kelas'")[0];
                        $dsn = $this->user->get_data('*','tbl_dosen',"where username_dosen = '$dosen'")[0];
                ?>
                <table>
                    <th>Nama Dosen</th>
                    <td>:</td>
                    <td><?= $dsn['dosen'];?></td>
                </table>
                
                <input type="hidden" id="kelas_1" name="set_kelas" value="<?= $kelas ?>">
                <input type="hidden" id="ds" name="set_dosen" value="<?= $dosen ?>">
                <?php endif; ?>
                <input type="hidden" id="bambang" name="set_matkul" value="<?= $matakuliah ?>">
                <input type="hidden" id="tri" name="set_semester" value="<?= $semester ?>">
                <input type="hidden" id="novianto" name="set_tahun" value="<?= $tahun_akademik ?>">
                <table>
                    <th>Mata Kuliah</th>
                    <td>:</td>
                    <td><?= $get['nama_matkul'];?></td>
                </table>
<!--                 <table>
                    <th>Kelas</th>
                    <td>:</td>
                    <td><?= $k['id_kelas'];?></td>
                </table> -->
                
                <table>
                    <th>Semester</th>
                    <td>:</td>
                    <td><?= $get['semester'];?></td>
                </table>
                <div class="x_content">
                    <div class="row">
                            
                        <div class="col-sm-12">
                            
      <!-- <form class="tes0" method="post"><input type="text" name="yyy"></form> -->
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th class="text-center">NIM</th>
                                            <th class="text-center">Nama Mahasiswa</th>
                                            <th class="text-center">Kelas</th>
                                            <th class="text-center">Kehadiran 10%</th>
                                            <th class="text-center">Tugas 20%</th>
                                            <th class="text-center">Formative 10%</th>
                                            <th class="text-center">Sikap 5%</th>
                                            <th class="text-center">UTS 25%</th>
                                            <th class="text-center">UAS 30%</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-center">Grade</th>
                                        </tr>
                                    </thead>
                                 
                                    <tbody>
                                        <?php $a = 1;

                                        // echo count($muncul);
                                         ?>
                                        <?php foreach ($muncul as $i => $ms): ?>
                                            <form class="form<?php echo $i ?>" id="formnilai" method="post">
                                            <?php 
                                            
                                            $khs = $this->user->cekviewkhs($ms['nim'],$tahun_akademik,$ms['id_kelas'],$matakuliah,$semester,$dosen);
                                            $cek = $this->user->cekviewktmp($khs['khs_id'],$ms['nim'],$tahun_akademik,$ms['id_kelas'],$matakuliah,$semester,$dosen);
                                            ?>
                                           <tr >
                                            <td></td>
                                            <td><?= $ms['nim']?></td>
                                            <td><?= $ms['nama'] ?></td>
                                            <td><?= $ms['id_kelas']?></td>
                                            <td>
                                                <input type="hidden" id="idkhs<?= $i  ?>" value="<?= $cek['id_temp'] ?>">
                                                <p class="cursor" id="tdbbg1<?= $i  ?>" onclick="edittable('<?= $i  ?>')"><?= $cek['absen'] ?></p>
                                                <input type="text" onchange="simpan2('1','<?= $i  ?>')" name="kehadiran" class="tableedit form-control" onkeyup="cek('1','<?= $i  ?>')"  id="bbg1<?= $i  ?>" value="<?= $cek['absen'] ?>" onkeydown="simpan('1','<?= $i  ?>')" onkeypress="return isNumber(event)" maxlength="5">
                                            </td>
                                            <td>
                                                <p class="cursor" id="tdbbg2<?= $i  ?>" onclick="edittable('<?= $i  ?>')"><?= $cek['tugas'] ?></p>
                                                <input type="text" onchange="simpan2('2','<?= $i  ?>')" name="tugas" max="100"  onkeyup="cek('2','<?= $i  ?>')" class="tableedit form-control" onkeydown="simpan('2','<?= $i  ?>')" value="<?= $cek['tugas'] ?>" id="bbg2<?= $i  ?>" onkeypress="return isNumber(event)" maxlength="5">
                                            </td>
                                            <td>
                                                <p class="cursor" id="tdbbg6<?= $i  ?>" onclick="edittable('<?= $i  ?>')"><?= $cek['formatif'] ?></p>
                                                <input type="text" name="tugas" onchange="simpan2('6','<?= $i  ?>')" onkeyup="cek('6','<?= $i  ?>')" class="tableedit form-control" onkeydown="simpan('6','<?= $i  ?>')" value="<?= $cek['formatif'] ?>" id="bbg6<?= $i  ?>" onkeypress="return isNumber(event)" maxlength="5" >
                                            </td>
                                            <td>
                                                <p class="cursor" id="tdbbg3<?= $i  ?>" onclick="edittable('<?= $i  ?>')"><?= $cek['perilaku'] ?></p>
                                                <input type="text" name="sikap" onchange="simpan2('3','<?= $i  ?>')" onkeyup="cek('3','<?= $i  ?>')" class="tableedit form-control" onkeydown="simpan('3','<?= $i  ?>')" value="<?= $cek['perilaku'] ?>" id="bbg3<?= $i  ?>" onkeypress="return isNumber(event)" maxlength="5">
                                            </td>
                                            <td>
                                                <p class="cursor" id="tdbbg4<?= $i  ?>" onclick="edittable('<?= $i  ?>')"><?= $cek['uts'] ?></p>
                                                <input type="text" name="uts" onchange="simpan2('4','<?= $i  ?>')" onkeyup="cek('4','<?= $i  ?>')" class="tableedit form-control" onkeydown="simpan('4','<?= $i  ?>')" value="<?= $cek['uts'] ?>" id="bbg4<?= $i  ?>" onkeypress="return isNumber(event)" maxlength="5">
                                            </td>
                                            <td>
                                                <p class="cursor" id="tdbbg5<?= $i  ?>" onclick="edittable('<?= $i  ?>')"><?= $cek['uas'] ?></p>
                                                <input type="text" name="uas" onchange="simpan2('5','<?= $i  ?>')" onkeyup="cek('5','<?= $i  ?>')" class="tableedit form-control" onkeydown="simpan('5','<?= $i  ?>')" value="<?= $cek['uas'] ?>"  id="bbg5<?= $i  ?>" onkeypress="return isNumber(event)" maxlength="5">
                                            </td>
                                            <td>
                                                <p id="total<?= $i  ?>" ><?= $cek['total'] ?></p>
                                            </td>
                                            <td>
                                                <p id="grade<?= $i  ?>"><?= $cek['grade'] ?></p>
                                            </td>
                                        </tr>
                                    </form>
                                        <?php endforeach; ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  
    <div id="tess2"></div>
    <script>

        $(document).ready(function(){
          
            $('#forminport').submit(function(e){
                  // alert();
            e.preventDefault();
            $.ajax({
                        type: 'POST',
                        url: '<?= base_url()?>perkuliahan/nilai/import_nilai_mhs_dosen',
                         data:new FormData(this),
                         processData:false,
                         contentType:false,
                         cache:false,
                         async:false,
                        success: function(data) {

                        
                            setTimeout("location.reload(true);", e);

                        }
                    });
        });
        });
        
        const list = document.querySelectorAll('.tableedit');
        function edittable(id){
            // console.log('btnbbg1'+id);
            var i = 1;
            while (i <= 6) {
                onof(i,id);
            i++;
            }            
        }

        function simpan(i,idInput) {
            var code = event.keyCode || event.which;
            // console.log('---code--'+code);          
            if (code == 13 || code == 9) {  
                simpan2(i,idInput);
            }
        }

        function simpan2(i,idInput) {
            var input = document.getElementById('bbg'+i+idInput);
            var input = $('#bbg'+i+idInput).val();
            var id = $('#idkhs'+idInput).val(); 
                 
            if(i == 1){
                var field = 'absen'; 
            }else if(i == 2){
                var field = 'tugas'; 
            }else if(i == 3){
                var field = 'perilaku'; 
            }else if(i == 4){
                var field = 'uts'; 
            }else if(i == 5){
                var field = 'uas'; 
            }else{
                var field = 'formatif'; 
            }
                
            var data = $('#formnilai').serialize();
            $.ajax({
            type: 'POST',
            url: '<?= base_url()?>perkuliahan/nilai/hitung/'+input+'/'+field+'/'+id,
            data: data,
            success: function(data) {
                document.getElementById('tdbbg'+i+idInput).innerHTML = input;
                $.ajax({
                        type: 'POST',
                        url: '<?= base_url()?>perkuliahan/nilai/hitungtotal/'+id,
                        data: data,
                        success: function(data) {
                            $('#total'+idInput).load('<?= base_url()?>perkuliahan/nilai/hitungtotal/'+id);
                            $('#grade'+idInput).load('<?= base_url()?>perkuliahan/nilai/grade/'+id);
                            onof(i,idInput,99); 
                        }
                    });
                }
            });            
        }

        function onof(i,id,kk) { 
            var x = document.getElementById('bbg'+i+id);
            var td = document.getElementById('tdbbg'+i+id);
            if(kk == 99){
                x.innerHTML = "";
                x.style.display = "";
                td.style.display = "block";
            }else{
                if(x.style.display == ''){
                // var element = document.getElementById("header");
                // x.innerHTML = '<input class="form-control" type="" name=""><input  type="hidden" name="">'; 
                x.style.display = "block";
                td.style.display = "none";
                }else{
                    
                }
            }
            
        }
        
        function number(i,id) {         
            var x = document.createElement("bbg"+i+id);
            x.setAttribute("type", "number");
        }

        function cek(i,idInput) {
            var input = $('#bbg'+i+idInput).val();
            if(input < 0){
                $('#bbg'+i+idInput).val('0');
            }
            if(input > 100){
               
                $('#bbg'+i+idInput).val('100');
            }
            console.log(input);
        }

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            // if (charCode > 31 && (charCode < 48 || charCode > 57 ) ) {
            //     return false;
            // }
            // return true
            if (charCode >= 48 && charCode <= 57 || charCode == 46) {
                return true;
            }
            return false;
        }

        function tombol_export_excel(){
            var bambang = document.getElementById("bambang").value;
            var tri = document.getElementById("tri").value;
            var myvar=tri.split('/').join('K');
            var hujan = myvar.split(' ').join('_');
            var novianto = document.getElementById("novianto").value;

            console.log(bambang+' '+hujan);

            // window.location.href = '<?= base_url()?>perkuliahan/nilai/export_nilai/'+bambang+'/'+hujan+'/'+novianto;

            window.location.href='<?= base_url()?>perkuliahan/nilai/export_nilai_excel/'+bambang+'/'+hujan+'/'+novianto;
        }

        function tombol_export_pdf(){
            var bambang = document.getElementById("bambang").value;
            var tri = document.getElementById("tri").value;
            var myvar=tri.split('/').join('K');
            var hujan = myvar.split(' ').join('_');
            var novianto = document.getElementById("novianto").value;

            console.log(bambang+' '+hujan);
            window.open('<?= base_url()?>perkuliahan/nilai/export_nilaipdf/'+bambang+'/'+hujan+'/'+novianto, '_blank')
            // window.location.href = '<?= base_url()?>perkuliahan/nilai/export_nilaipdf/'+bambang+'/'+hujan+'/'+novianto;
        }

        function tombol_export_pdf_akademik(){
            var bambang = document.getElementById("bambang").value;
            var tri = document.getElementById("tri").value;
            var myvar=tri.split('/').join('K');
            var hujan = myvar.split(' ').join('_');
            var novianto = document.getElementById("novianto").value;
            var ds = document.getElementById("ds").value;
            var kelas_1 = document.getElementById("kelas_1").value;

            console.log(bambang+' '+hujan);
            window.open('<?= base_url()?>perkuliahan/nilai/export_nilaipdf/'+bambang+'/'+hujan+'/'+novianto+'/'+ds+'/'+kelas_1, '_blank')
            // window.location.href = '<?= base_url()?>perkuliahan/nilai/export_nilaipdf/'+bambang+'/'+hujan+'/'+novianto;
        }


    </script>
            <!-- Modal Form untuk Import Nilai -->
<div class="modal fade" id="import_nilai2" role="dialog">
    <div class="modal-dialog" role="document">
        
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Import Nilai
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>
                <form  id="forminport"  enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <br>
                        <?php $get_thn = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik='$id_thn'")[0]; ?>
                        <select class="form-control" id="select_tahun_akademik2" disabled style="width:100%;" required>
                            <option value=""><?php echo $get_thn['tahun_akademik'];?></option>
                        </select>
                        <input type="hidden" name="tahun_akademik" value="<?= $get_thn['tahun_akademik'];?>">
                    </div>

                    <input type="hidden" name="matkul" value="<?= $matkul ?>">
                    <input type="hidden" name="semester" value="<?= $semester ?>">
                    <input type="hidden" name="tahun_akademik" value="<?= $tahun_akademik ?>">

                    <div class="form-group">
                        <label>Upload</label>
                        <div class="input-group">
                            <input type="file" class="form-control" name="file" required>
                        </div>
                    </div>
                </div>
             
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success" >Proses</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
                   </form>
            </div>
        
    </div>
</div> 
<!-- End Section -->
  </body>
</html>