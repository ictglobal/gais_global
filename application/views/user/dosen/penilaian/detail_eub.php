<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Detail Evaluasi Umpan Balik</h2>
                    <!-- <input type="text" name="ayam" value="<?= $kode; ?>" id="kode_tahun_akademik"> -->
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>    
                                            <th class="text-center" rowspan="2" width="4%" >No&nbsp;</th> <!-- column 1 -->
                                            <th class="text-center" colspan="4">PENILAIAN EVALUASI UMPAN BALIK KELAS <?= $kelas." TAHUN AKADEMIK ".$kode; ?>&nbsp;</th> <!-- column 2&3 --> 
                                        </tr>
                                        <tr>
                                            <th class="text-center">Pertanyaan&nbsp;</th> <!-- column 2 -->
                                            <th class="text-center" width="5%">Jumlah&nbsp;</th> <!-- column 3 -->
                                            <th class="text-center" width="5%">Responden&nbsp;</th> <!-- column 3 -->
                                            <th class="text-center" width="12%">Rata-rata&nbsp;</th> <!-- column 4 -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <!-- <?php foreach($quesioner as $v => $value): ?>
                                            
                                        <?php endforeach; ?> -->
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $quesioner[0]['pertanyaan'] ?></td>
                                                <td align="center"><?= $p1 ?></td>
                                                <td align="center"><?= $r1 ?></td>
                                                <td align="center"><?= number_format($p1/$r1, 2, ',', '') ?></td>
                                            </tr>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $quesioner[1]['pertanyaan'] ?></td>
                                                <td align="center"><?= $p2 ?></td>
                                                <td align="center"><?= $r2 ?></td>
                                                <td align="center"><?= number_format($p2/$r2, 2, ',', '') ?></td>
                                            </tr>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $quesioner[2]['pertanyaan'] ?></td>
                                                <td align="center"><?= $p3 ?></td>
                                                <td align="center"><?= $r3 ?></td>
                                                <td align="center"><?= number_format($p3/$r3, 2, ',', '') ?></td>
                                            </tr>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $quesioner[3]['pertanyaan'] ?></td>
                                                <td align="center"><?= $p4 ?></td>
                                                <td align="center"><?= $r4 ?></td>
                                                <td align="center"><?= number_format($p4/$r4, 2, ',', '') ?></td>
                                            </tr>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $quesioner[4]['pertanyaan'] ?></td>
                                                <td align="center"><?= $p5 ?></td>
                                                <td align="center"><?= $r5 ?></td>
                                                <td align="center"><?= number_format($p5/$r5, 2, ',', '') ?></td>
                                            </tr>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $quesioner[5]['pertanyaan'] ?></td>
                                                <td align="center"><?= $p6 ?></td>
                                                <td align="center"><?= $r6 ?></td>
                                                <td align="center"><?= number_format($p6/$r6, 2, ',', '') ?></td>
                                            </tr>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $quesioner[6]['pertanyaan'] ?></td>
                                                <td align="center"><?= $p7 ?></td>
                                                <td align="center"><?= $r7 ?></td>
                                                <td align="center"><?= number_format($p7/$r7, 2, ',', '')?></td>
                                            </tr>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $quesioner[7]['pertanyaan'] ?></td>
                                                <td align="center"><?= $p8 ?></td>
                                                <td align="center"><?= $r8 ?></td>
                                                <td align="center"><?= number_format($p8/$r8, 2, ',', '') ?></td>
                                            </tr>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $quesioner[8]['pertanyaan'] ?></td>
                                                <td align="center"><?= $p9 ?></td>
                                                <td align="center"><?= $r9 ?></td>
                                                <td align="center"><?= number_format($p9/$r9, 2, ',', '') ?></td>
                                            </tr>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $quesioner[9]['pertanyaan'] ?></td>
                                                <td align="center"><?= $p10 ?></td>
                                                <td align="center"><?= $r10 ?></td>
                                                <td align="center"><?= number_format($p10/$r10, 2, ',', '') ?></td>
                                            </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
          <?php //$this->load->view('template/modal'); ?>
    </div>
    <script type="text/javascript">
        function click_detail_eub(id_kls){
            var encoded_url = encodeURIComponent(id_kls);
            window.location.href = '<?= base_url()?>perkuliahan/evaluasi_umpan_balik/detail_eub?detail='+encoded_url;
        }

        function select_eub(){
            var id = $('#theSelect2').val();
            var encoded_url = encodeURIComponent(id);
            // console.log(encoded_url);
            // window.location.href = '<?= base_url()?>perkuliahan/evaluasi_umpan_balik/tampil_eub?th='+encoded_url;
            window.open('<?= base_url()?>perkuliahan/evaluasi_umpan_balik/tampil_eub?th='+encoded_url, '_blank');
        }
    </script>
  </body>
</html>