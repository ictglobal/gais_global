<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Saran Evaluasi Umpan Balik</h2>
                    <!-- <input type="text" name="ayam" value="<?= $kode; ?>" id="kode_tahun_akademik"> -->
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <!-- <tr>    
                                            <th class="text-center" width="5%">No</th>
                                            <th class="text-center">Saran</th>
                                        </tr> -->
                                        <tr>    
                                            <th class="text-center" rowspan="2" width="1%">No&nbsp;</th> <!-- column 1 -->
                                            <th class="text-center" colspan="2" width="" >SARAN DARI KELAS <?= $kode_kelas." TAHUN AKADEMIK ".$ta; ?>&nbsp;</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center" width="14%">Saran&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($saran as $key => $value): ?>
                                            <tr>
                                                <td align="center"></td>
                                                <td><?= $value->saran ?></td>
                                            </tr>
                                        <?php endforeach; ?>   
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
          <?php //$this->load->view('template/modal'); ?>
    </div>
    <script type="text/javascript">
        function click_detail_eub(id_kls){
            var encoded_url = encodeURIComponent(id_kls);
            window.location.href = '<?= base_url()?>perkuliahan/evaluasi_umpan_balik/detail_eub?detail='+encoded_url;
        }

        function select_eub(){
            var id = $('#theSelect2').val();
            var encoded_url = encodeURIComponent(id);
            // console.log(encoded_url);
            // window.location.href = '<?= base_url()?>perkuliahan/evaluasi_umpan_balik/tampil_eub?th='+encoded_url;
            window.open('<?= base_url()?>perkuliahan/evaluasi_umpan_balik/tampil_eub?th='+encoded_url, '_blank');
        }
    </script>
  </body>
</html>