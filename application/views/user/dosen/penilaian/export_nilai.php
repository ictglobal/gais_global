<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Nilai Mahasiswa</title>
</head>
<style type="text/css">
	/*body{font-family: arial; background-color: #ccc;}*/
	.rangkasurat{
		width: 820px;
		margin: 0 auto;
		background-color: #fff;
		height: 500px;
		padding: 20px;
	}
	.ditable{border-bottom: 5px solid #000; padding: 2px;width: 100%;};
	.tengah{text-align: center;line-height: 5px;};
	.fontdi{font-family: arial narrow;};
	.fontket{font-family:arial narrow;font-size: 15px;};
</style>
<body>

<?php  
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=nilai_mahasiswa.xls");
?>
<table border="1" width="100%">
			<thead>
				<tr>
					<th class="text-center">No</th>
		            <th class="text-center">NIM</th>
		            <th class="text-center">Nama Mahasiswa</th>
		            <th class="text-center">Kelas</th>
		            <!-- <th class="text-center">Matakuliah</th> -->
		            <th class="text-center">Absensi 10%</th>
		            <th class="text-center">Tugas 20%</th>
					<th class="text-center">Formatif 10%</th>
					<th class="text-center">Sikap 5%</th>
					<th class="text-center">UTS 25%</th>
					<th class="text-center">UAS 30%</th>
					<th class="text-center">Total Nilai</th>
					<th class="text-center">Grade</th>
					<!-- <th class="text-center">Semester</th> -->
				</tr>
			</thead>

			<tbody>
				<?php $i = 1; 
					foreach ($muncul as $key => $value):
						$get_mhs = $this->user->get_data("nama, id_kelas","tbl_mahasiswa", "WHERE nim = '$value->nim'")[0];
				?>
					<tr style="font-family: arial narrow;">
						<td align="center"><?= $i++  ?></td>
						<td align="center"><?php echo $value->nim; ?></td>
						<td><?php echo $get_mhs['nama'] ?></td>
						<td width="100px"><?php echo $get_mhs['id_kelas'] ?></td>
						<!-- <td><?php echo  $value->kode_matkul;?></td> -->
						<td align="center"><?php echo $value->absen; ?></td>
						<td align="center"><?php echo $value->tugas; ?></td>
						<td align="center"><?php echo $value->formatif; ?></td>
						<td align="center"><?php echo $value->perilaku; ?></td>
						<td align="center"><?php echo $value->uts; ?></td>
						<td align="center"><?php echo $value->uas; ?></td>
						<td align="center"><?php echo $value->total; ?></td>
						<td align="center"><?php echo $value->grade; ?></td>
						<!-- <td><?php echo $value->semester; ?></td> -->
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

</body>
</html>