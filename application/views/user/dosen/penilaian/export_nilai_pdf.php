<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Nilai <?= $get_nama_dosen['username_dosen']." ".$get_tahun_akademik?></title>
</head>
<style type="text/css">
	/*body{font-family: arial; background-color: #ccc;}*/
	.rangkasurat{
		width: 950px;
		margin: 0 auto;
		background-color: #fff;
		height: 100px;
		padding: 20px;
	}
	.ditable{border-bottom: 5px solid #000; padding: 2px;width: 100%;};
	.tengah{text-align: center;line-height: 5px;};
	.fontdi{font-family: arial narrow;};
	.fontket{font-family:arial narrow;font-size: 15px;};
</style>
<body class="rangkasurat">
	<div >
		<table class="ditable">
			<tr>
				<td><img src="<?php echo base_url();?>temp/images/logo-global-institute.png" width="100px"></td>
				<td class="tengah" align="center">
					<b style="font-family: arial narrow;font-size: 40px;">KOMPONEN NILAI MAHASISWA</b><br>
					<b style="font-family: arial narrow;font-size: 20px;">INSTITUT TEKNOLOGI DAN BISNIS BINA SARANA GLOBAL</b>
					<div style="font-family: arial narrow;font-size: 16px;text-transform: uppercase;"> SEMESTER <?= $get_ganjil_genap;?> TAHUN AKADEMIK <?= $get_tahun_akademik?></div>
				</td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td>Dosen</td>
				<td>:</td>
				<td><?= $get_nama_dosen['dosen']. " ".$get_nama_dosen['gelar']?></td>
			</tr>
			<tr>
				<td>Matakuliah / SKS</td>
				<td>:</td>
				<td><?= $get_nama_matkul['nama_matkul']. " / ".$get_nama_matkul['sks']?></td>
			</tr>
		</table>
		<br>
		<table style="border-collapse: collapse;" border="1" width="100%">
			<thead>
				<tr>
					<th class="text-center">No</th>
		            <th class="text-center">NIM</th>
		            <th class="text-center">Nama Mahasiswa</th>
		            <th class="text-center">Kelas</th>
		            <!-- <th class="text-center">Matakuliah</th> -->
		            <th class="text-center">Absensi 10%</th>
		            <th class="text-center">Tugas 20%</th>
					<th class="text-center">Formatif 10%</th>
					<th class="text-center">Sikap 5%</th>
					<th class="text-center">UTS 25%</th>
					<th class="text-center">UAS 30%</th>
					<th class="text-center">Total Nilai</th>
					<th class="text-center">Grade</th>
					<!-- <th class="text-center">Semester</th> -->
				</tr>
			</thead>

			<tbody>
				<?php $i = 1; 
					foreach ($muncul as $key => $value):
						$get_mhs = $this->user->get_data("nama, id_kelas","tbl_mahasiswa", "WHERE nim = '$value->nim'")[0];
				?>
					<tr style="font-family: arial narrow;">
						<td align="center"><?= $i++  ?></td>
						<td align="center"><?php echo $value->nim; ?></td>
						<td><?php echo $get_mhs['nama'] ?></td>
						<td width="100px"><?php echo $get_mhs['id_kelas'] ?></td>
						<!-- <td><?php echo  $value->kode_matkul;?></td> -->
						<td align="center"><?php echo $value->absen; ?></td>
						<td align="center"><?php echo $value->tugas; ?></td>
						<td align="center"><?php echo $value->formatif; ?></td>
						<td align="center"><?php echo $value->perilaku; ?></td>
						<td align="center"><?php echo $value->uts; ?></td>
						<td align="center"><?php echo $value->uas; ?></td>
						<td align="center"><?php echo $value->total; ?></td>
						<td align="center"><?php echo $value->grade; ?></td>
						<!-- <td><?php echo $value->semester; ?></td> -->
					</tr>
				<?php endforeach; ?>
			</tbody>
	</table>
	<br>
<table width="100%">
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;">Ketentuan Nilai :</td>
				<td style="font-family:arial narrow;font-size: 15px;" width="28%">
					Tangerang,
					<?php 
						$tanggal= mktime(date("m"),date("d"),date("Y"));
						// echo "".date("d-M-Y", $tanggal)."";
						$tgl = date("Y-m-d", $tanggal);
						echo $this->functions->tanggal_indo($tgl);
					?>
				</td>
			</tr>
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;"><b>Nilai Akhir</b></td>
				<td style="font-family:arial narrow;font-size: 15px;" width="28%">Dosen Pengajar,</td>
			</tr>
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;">10 % Absen + 20 % (Tugas) +10% (Formatif)+ 5% (Perilaku)+ 25 % (UTS) + 30 % (UAS)</td>

			</tr>
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;">A = 80 - 100</td>
			</tr>
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;">B = 70 - 79.99</td>
			</tr>
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;">C = 60 - 69.99</td>
			</tr>
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;">D = 40 - 59.99</td>
			</tr>
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;">E = 0 - 39.99</td>
				<td style="font-family:arial narrow;font-size: 15px;" width="28%"><b><u><?= $get_nama_dosen['dosen']. " ".$get_nama_dosen['gelar']?></u></b></td>
			</tr>
		</table>

		<br>
		<table>
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;"><b><i>Nb.</i></b></td>
			</tr>
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;">1. Dosen Yang bersangkutan harus membubuhkan tanda tangan bila komponen nilai telah siap dikirim ke akademik.</td>
			</tr>
			<tr>
				<td style="font-family:arial narrow;font-size: 15px;">2. Dosen mengisi nama dosen, matakuliah dan sks.</td>
			</tr>
			<!-- <tr>
				<td style="font-family:arial narrow;font-size: 15px;">3. Jika terdapat nama mahasiswa yang tidak tercantum tetapi aktif dalam perkuliahan, dapat dicantumkan pada kolom kosong diatas</td>
			</tr> -->
		</table>
	</div>
	<script type="text/javascript">
		window.print();
	</script>
</body>
</html>