<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Evaluasi Umpan Balik</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form id='formselect'>
                                    <div class="col-sm-3">
                                        <label for="">Filter</label>
                                        <select class="form-control" id="tahun_akademik2" onchange="selection()" required>
                                            <option value="">Pilih Tahun Akademik</option>
                                            <?php foreach($filter_ta as $key => $value): ?>
                                            <option value="<?= $this->encryption->encrypt($value->id_tahunakademik);?>">
                                                <?= $value->tahun_akademik; ?>
                                            </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </form>
                                <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th>Matakuliah</th>
                                            <th>Kelas</th>
                                            <th width="10%">Jumlah</th>
                                            <th class="text-center" width="20%">Opsi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url().'temp/jquery-3.6.0.js'?>"></script>
    <script type="text/javascript">
        function selection() {
            var id = $('#tahun_akademik2').val();
            var encoded_url = encodeURIComponent(id);
            // console.log(encoded_url);
            window.location.href = '<?= base_url()?>perkuliahan/evaluasi_umpan_balik/tampil_eub?th='+encoded_url;
        }
    </script>
  </body>
</html>