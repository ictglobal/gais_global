<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <?php if($this->session->userdata('level')==6): ?>
                <div style="text-align: left;">
                    <button class="btn btn-primary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#tambah_dosen">Tambah</button>
                </div>
            <?php endif;?>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Dosen</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="5%">No</th>
                                            <th class="text-center" width="7%">Username</th>
                                            <th class="text-center" width="7%">NIDN</th>
                                            <th class="text-center" width="">Nama</th>
                                            <th class="text-center" width="14%">Jenis Kelamin</th>
                                            <th class="text-center" width="17%">Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php 
                                            foreach ($dosen as $key => $value):
                                        ?>
                                        <tr>
                                            <td align="center"></td>
                                            <td><?= $value->username_dosen;?></td>
                                            <td><?= $value->nidn;?></td>
                                            <td><?= $value->dosen;?></td>
                                            <td align="center"><?= $value->jenis_kelamin;?></td>
                                            <td align="center">
                                                <button type="button" class="btn btn-primary btn-sm" onclick="detail_dos('<?=$this->encryption->encrypt($value->username_dosen)?>');"> 
                                                    <span class="fa fa-eye fa-lg"></span>
                                                </button>

                                                <button type="button" class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#edit_dosen<?=$value->username_dosen?>"> 
                                                    <span class="fa fa-edit fa-lg"></span>
                                                </button>
                                    
                                                <button type="button" class="btn btn-danger btn-sm hapus" id="<?=$value->username_dosen?>">
                                                    <span class="fa fa-trash-o fa-lg"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </body>
</html>

<!-- Modal Form untuk Tambah Konsentrasi -->
<div class="modal fade" id="tambah_dosen" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>akademik/dosen/tambah_dosen" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-plus-square mr-1"></i>Tambah Dosen
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Username Dosen </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan username dosen" minlength="7" maxlength="7" class="form-control" name="username" onkeypress="return isNumberKey(event)" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nomor Induk Dosen Nasional (NIDN) </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan NIDN Dosen" class="form-control" minlength="10" maxlength="10" name="nidn" onkeypress="return isNumberKey(event)" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nama Dosen</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan Nama Dosen" class="form-control" name="nama_dosen" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Gelar Akademik</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan Gelar" class="form-control" name="gelar" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Jenis Kelamin </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select class="form-control" id="pilih_jeniskelamin" style="width: 91%;" name="jk" required>
                                <option value="" disabled selected>--Pilih Jenis Kelamin--</option>
                                <option value="Laki - laki">Laki - laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Jabatan Fungsional </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select class="form-control" id="pilih_jafung" style="width: 91%;" name="jafung" required>
                                <option value="" disabled selected>--Pilih Jabatan Fungsional--</option>
                                <option value="-">-</option>
                                <option value="Asisten Ahli">Asisten Ahli</option>
                                <option value="Lektor">Lektor</option>
                                <option value="Lektor Kepala">Lektor Kepala</option>
                                <option value="Profesor">Profesor</option>
                            </select>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Jenjang Pendidikan </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select class="form-control" id="theSelect2" style="width: 91%;" name="jenjang" required>
                                <option value="" disabled selected>--Pilih Jenjang--</option>
                                <option value="-">-</option>
                                <option value="Asisten Ahli">Diploma</option>
                                <option value="Lektor">Sarjana</option>
                                <option value="Lektor Kepala">Magister</option>
                                <option value="Profesor">Doktoral</option>
                            </select>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label>E-Mail</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="email" placeholder="Masukan E-mail" class="form-control" name="email" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nomor Telepon</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan No Telp" class="form-control" name="telp" minlength="12" maxlength="12" onkeypress="return isNumberKey(event)" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Alamat</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <textarea name="alamat" class="form-control" placeholder="Masukan Alamat"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="password" placeholder="Masukan Password" class="form-control" name="password" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 

<?php foreach ($dosen as $key => $value2) { ?>

<div class="modal fade" id="edit_dosen<?=$value2->username_dosen?>" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>akademik/dosen/edit_dosen" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-plus-square mr-1"></i>Ubah Data Diri Dosen
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Username Dosen </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan username dosen" minlength="7" maxlength="7" class="form-control" value="<?=$value2->username_dosen ?>" name="username" onkeypress="return isNumberKey(event)" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nomor Induk Dosen Nasional (NIDN) </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan NIDN Dosen" class="form-control" minlength="10" maxlength="10" name="nidn" value="<?=$value2->nidn ?>" onkeypress="return isNumberKey(event)" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nama Dosen</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan Nama Dosen" value="<?=$value2->dosen?>" class="form-control" name="nama_dosen" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Gelar Akademik</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan Gelar" value="<?=$value2->gelar ?>" class="form-control" name="gelar" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Jenis Kelamin </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select class="form-control" id="pilih_jeniskelamin" style="width: 91%;" name="jk" required>
                                <option value="<?=$value->jenis_kelamin ?>"><?=$value2->jenis_kelamin ?></option>
                                <option value="Laki - laki">Laki - laki</option>
                                <option value="Perempuan">Perempuan</option>
                            </select>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Jabatan Fungsional </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select class="form-control" id="pilih_jafung" style="width: 91%;" name="jafung" required>
                                <option value="<?=$value2->jafung ?>"><?=$value2->jafung ?></option>
                                <option value="-">-</option>
                                <option value="Asisten Ahli">Asisten Ahli</option>
                                <option value="Lektor">Lektor</option>
                                <option value="Lektor Kepala">Lektor Kepala</option>
                                <option value="Profesor">Profesor</option>
                            </select>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Jenjang Pendidikan </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select class="form-control" id="theSelect2" style="width: 91%;" name="jenjang" required>
                                <option value="<?=$value2->jenjang ?>"><?=$value2->jenjang ?></option>
                                <option value="-">-</option>
                                <option value="Asisten Ahli">Diploma</option>
                                <option value="Lektor">Sarjana</option>
                                <option value="Lektor Kepala">Magister</option>
                                <option value="Profesor">Doktoral</option>
                            </select>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label>E-Mail</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="email" placeholder="Masukan E-mail" value="<?=$value2->email?>" class="form-control" name="email" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nomor Telepon</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan No Telp" class="form-control" name="telp" minlength="11" maxlength="13" value="<?=$value2->telp?>" onkeypress="return isNumberKey(event)" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Alamat</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <textarea  name="alamat" class="form-control" placeholder="Masukan Alamat"><?=$value2->alamat  ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Password</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="password" value="<?=$value2->password?>" placeholder="Masukan Password" class="form-control" name="password" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<?php }?>

<script type="text/javascript">
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : evt.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function detail_dos(id){
        var url = encodeURIComponent(id);
        window.location.href = '<?= base_url()?>akademik/dosen/detail_dosen?kode='+url;
    }
</script>
<!-- End Section -->