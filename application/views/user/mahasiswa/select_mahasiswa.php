<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <!-- <div>
                <button class="btn btn-primary btn-md" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#tambah_mahasiswa"><span class="fa fa-pencil"></span> Tambah</button>
            </div> -->
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Mahasiswa</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <label class="form-label">Cari Nama dan NIM <code>tekan ENTER</code></label>
                                    <br>
                            <div class="col-sm-3">
                                <?php echo validation_errors(); ?>
                                <input id="cari_mhs" name="cari" class="form-control" type="text" onkeydown="selection(this)"><br>
                            </div>
                            <div class="col-sm-3">
                                <button class="btn btn-primary btn-md" id="cari_mhs" onclick="selection_button(this)"><span class="fa fa-search"></span> Cari</button>
                            </div>
                        </div>
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <!-- <th class="text-center">No</th> -->
                                    <th class="text-center" width="5%">No</th>
                                    <th class="text-center">NIM</th>
                                    <th class="text-center">Nama</th>
                                    <th class="text-center">Kelas</th>
                                    <th class="text-center">Tempat, Tanggal Lahir</th>
                                    <th class="text-center">Keterangan</th>
                                    <th class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    foreach ($mhs as $key => $value):
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?= $value['nim'];?></td>
                                    <td><?= $value['nama'];?></td>
                                    <td><?= $value['id_kelas']?></td>
                                    <td><?= $value['tempat_lahir'].", ".$value['tgl_lahir']?></td>
                                    <td><?= $value['status']?></td>
                                    <td>
                                        <button type="button" class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#edit_mahasiswa<?=$value['nim']?>"> 
                                            <span class="fa fa-edit fa-lg"></span> Edit
                                        </button>
                                        <button type="button" class="btn btn-danger btn-sm" onclick="">
                                            <span class="fa fa-trash-o fa-lg"></span> Delete
                                        </button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url().'temp/jquery-3.6.0.js'?>"></script>
    <script type="text/javascript">
        function stringToHex (tmp) {
            var str = '';
            var tmp_len = tmp.length;
            var c = 0;
            for (var i=0; i < tmp_len; i++) {
                c = tmp.charCodeAt(i);
                str += c.toString(16);
            }
            return str;
        }

        function selection(ele) {
            var cari = $('#cari_mhs').val();
            var enc = stringToHex(cari);
            // var encoded_url = encodeURIComponent();
            if(event.key === 'Enter'){
                if (cari == ""){
                    // console.log("GOBLOK");
                }else{
                    window.location.href = '<?= base_url()?>akademik/mahasiswa/cari_mhs?id='+enc;
                }
            }
        }

        function selection_button(ele) {  
            var cari = $('#cari_mhs').val();
            var enc = stringToHex(cari);  
            // var encoded_url = encodeURIComponent();
            if (cari == ""){
                // window.location.href = '<?= base_url()?>akademik/mahasiswa/';
            }else{
                window.location.href = '<?= base_url()?>akademik/mahasiswa/cari_mhs?id='+enc;
            }
        }
    </script>
  </body>
</html>

<?php 
    foreach ($mhs as $key => $value):
        $edit_mhs = $this->user->get_data("*", "tbl_mahasiswa", "WHERE nim = '$value[nim]'")[0];
?>
<!-- Modal Form untuk Edit Mahasiswa -->
<div class="modal fade" id="edit_mahasiswa<?=$value['nim']  ?>" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?=base_url()?>akademik/mahasiswa/edit" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-edit mr-1"></i>Edit Mahasiswa
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body"> 
                    <div class="form-group">
                        <input type="text" name="" value="<?=$edit_mhs['nim']?>">
                        <label class="form-label">Tahun Akademik</label>
                        <div class="input-group">
                            <select class="form-control" id="select_tahun_akademik8" name="ta" style="width:100%;" required>
                                <option value="$edit_mhs['tahun_akademik']"><?= $edit_mhs['tahun_akademik'] ?></option>
                                    <?php 
                                        $tg_awal= date('Y')-13;
                                        $tgl_akhir= date('Y')+1;
                                        for ($i=$tgl_akhir; $i>=$tg_awal; $i--){
                                            $tgl_akhir = $i + 1;
                                            $th = $i." - ".$tgl_akhir;
                                    ?>
                                <option value="<?= $th?>"><?= $i." - ".$tgl_akhir; ?></option>
                                    <?php } ?>
                            </select>
                        </div>

                        <label class="form-label">NIM</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" class="form-control" name="nim" value="<?=$edit_mhs['nim']?>" required  >
                        </div>

                        <label class="form-label">NISN</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" class="form-control" name="nisn" value="<?=$edit_mhs['nisn']?>" required>
                        </div>

                        <label class="form-label">NIK</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="16" class="form-control" name="nik_mahasiswa" value="<?=$edit_mhs['nik']?>" required>
                        </div>

                        <label class="form-label">Nama</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                            <input type="text" class="form-control" name="nama" value="<?=$edit_mhs['nama']?>" style="text-transform:uppercase" required>
                        </div>

                        <label class="form-label">Jenis Kelamin</label>
                        <div class="input-group">
                            <select class="form-control" name="jk_tambah" id="jk3" style="width:100%;" required>
                                <?php  ?>
                                <option value="<?=$edit_mhs['jenis_kelamin']?>">
                                    <?php 
                                        if($edit_mhs['jenis_kelamin']=="L"):
                                            echo "Laki - laki";
                                        else:
                                            echo "Perempuan";
                                        endif;
                                    ?>
                                </option>
                                <option value="L">Laki - laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                        </div>

                        <label class="form-label">Tempat Lahir</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="tempat_lahir" value="<?=$edit_mhs['tempat_lahir']?>" required>
                        </div>

                        <label class="form-label">Tanggal Lahir</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input id="birthday" class="date-picker form-control" name="tanggal_lahir" type="text" required="required" type="text" onfocus="this.type='date'" onmouseover="this.type='date'" onclick="this.type='date'" onblur="this.type='text'" onmouseout="timeFunctionLong(this)" value="<?=$edit_mhs['tgl_lahir']?>">
                                <script>
                                    function timeFunctionLong(input) {
                                        setTimeout(function() {
                                        input.type = 'text';
                                    }, 60000);
                                }
                                </script>
                            <!-- <input type="date" name="tanggal_lahir" class="date-picker form-control" value="<?=$edit_mhs['tgl_lahir']?>" > -->
                        </div>  

                        <label class="form-label">Kelas</label>
                        <div class="input-group">
                            <select class="form-control" name="kelas" id="kelas2" style="width:100%;" required>
                                <option value="">--Pilih Kelas---</option>
                                <?php foreach ($kelas as $key => $value) { ?>
                                    <option value="<?= $value->id_kelas ?>"><?=$value->id_kelas ?></option>
                                <?php }  ?>
                            </select>
                        </div>

                        <label class="form-label">Agama</label>
                        <div class="input-group">
                            <select class="form-control" name="agama" id="agama" style="width:100%;" required>
                                <option value="">--Pilih Agama---</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Katolik">Katolik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                                <option value="Konghucu">Konghucu</option>
                            </select>
                        </div>

                        <label class="form-label">Fakultas</label>
                        <div class="input-group">
                            <select required name="fakultas" class="form-control" id="fakultas" style="width:100%;" onchange="get_fakultas()">
                                <option value="">--Pilih Fakultas---</option>
                            <?php 
                                foreach($fakultas as $key => $value):
                            ?>  
                                <option value="<?= $value->id_fakultas;?>"><?= $value->nama_fakultas;?></option>
                            <?php endforeach   ?>   
                            </select>  
                        </div>

                        <label class="form-label">Program Studi</label>
                        <div class="input-group">
                            <select required name="prodi" class="form-control" id="prodi" style="width:100%;" onchange="get_prodi()">
                                 <option value="">--Pilih Program Studi---</option>
                            </select>
                        </div>

                        <!-- <label class="form-label">Konsentrasi</label>
                        <div class="input-group">
                            <select class="form-control" name="konsentrasi" id="konsentrasi2">
                                <option value="">--Pilih Konsentrasi---</option>
                            </select>
                        </div> -->

                        <label class="form-label">Program Kuliah</label>
                        <div class="input-group">
                            <select class="form-control" name="pilihan_kuliah" id="pilihan_kuliah" style="width:100%;" required>
                                <option>--Pilih Program Kuliah---</option>
                                <option value="R">Reguler (Kelas Pagi)</option>
                                <option value="NR">Non Reguler (Kelas Malam/Shift/JSM)</option>
                            </select>
                        </div>

                        <label class="form-label">NIK Ayah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="16" class="form-control" name="nik_ayah" required>
                        </div>

                        <label class="form-label">Nama Ayah</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" name="nama_ayah" style="text-transform:uppercase" required>
                        </div>

                        <label class="form-label">NIK Ibu</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="16" class="form-control" name="nik_ibu" required>
                        </div>

                        <label class="form-label">Nama Ibu</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" style="text-transform:uppercase" name="nama_ibu" required>
                        </div>

                        <label class="form-label">Nomor Telepon</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" class="form-control" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="telp" style="text-transform:uppercase" required>
                        </div>

                        <label class="form-label">Alamat Lengkap</label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <textarea name="alamat_lengkap" class="form-control" required></textarea>
                        </div>

                            <!-- <label class="form-label">Password</label>
                            <div class="input-group">
                                <input type="password" name="pass" id="password" class="form-control" data-toggle="password"required>
                            </div> -->
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- End Section -->
<?php endforeach; ?>