<?php $i=1;
        foreach ($mahasiswa as $key => $value):
            $ktr_kelas = 'Harap Input Kuliah';
            if ($value->id_kuliah == 'R') {
                $ktr_kelas = 'Reguler';
            }
            elseif ($value->id_kuliah == 'M') {
                $ktr_kelas = 'Malam';
            }
            elseif ($value->id_kuliah == 'SH') {
                $ktr_kelas = 'Shift';
            }
            elseif ($value->id_kuliah == 'JSM') {
                $ktr_kelas = 'Jumat Sabtu';
            }
            if($value->status_bayar == 'L1'){
                $status_bayar = 'Lunas';
            }else{
                $status_bayar = 'Belum Lunas';
            }
?>

<tr>
    <td align="center"></td>
    <td><?= $value->nim;?></td>
    <td><?= $value->nama;?></td>
    <td><?= $value->id_kelas;?></td>
    <td> 
        <?php if($value->id_kuliah == 'R'): ?>
            Reguler
        <?php else: ?>
            Non Reguler
        <?php endif; ?>
    </td>
    <td><?= $value->status;?></td>
    <td>
        <button type="button" class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#editmahasiswa<?= $value->nim;?>"> 
            <span class="fa fa-edit fa-lg"></span> Edit
        </button>
        <button type="button" class="btn btn-danger btn-sm" onclick="hapusMahasiswa('<?= $value->nim;?>')">
            <span class="fa fa-trash-o fa-lg"></span> Delete
        </button>
        <!-- Modal Form untuk Edit Mahasiswa -->
<div class="modal fade" id="editmahasiswa<?= $value->nim;?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>akademik/mahasiswa/edit_file" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Edit Modul
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="mb-3">
                        <label class="form-label">NIM</label>
                        <input type="text" class="form-control" name="nim" value="<?= $value->nim;?>">
                    </div>
                    
                    <div class="mb-3">
                        <label class="form-label">Nama</label>
                        <input type="text" class="form-control" name="nama" value="<?= $value->nama;?>">
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Jenis Kelamin</label>
                        <?php 
                            $jk = $value->jenis_kelamin;
                            if($jk == 'L'):
                                $jkktr = 'Laki - Laki';
                            else:
                                $jkktr = 'Perempuan';
                            endif;                                                                            
                        ?>
                        <select class="form-control" name="jenis_kelamin" id="theSelect">
                                <option value="<?= $jk ?>"><?= $jkktr ?></option>
                                <?php if($jk == 'L'): ?>
                                    <option value="P">Perempuan</option>
                                <?php else: ?>
                                    <option value="L">Laki - Laki</option>
                                <?php endif; ?>
                        </select>                                                   
                    </div>
                    
                    <div class="mb-3">
                        <label class="form-label">Handphone</label>
                        <input type="text" class="form-control" name="telp" value="<?= $value->telp;?>">
                    </div>
                    
                    <div class="mb-3">
                        <label class="form-label">Tempat Lahir</label>
                        <input type="text" class="form-control" name="tempat_lahir" value="<?= $value->tempat_lahir;?>">
                    </div>
                    
                    <div class="mb-3">
                        <label class="form-label">Tanggal Lahir</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="tgl_lahir2" value="<?= $value->tgl_lahir2;?>" aria-describedby="emailHelp">
                    </div>
                    
                    <div class="mb-3">
                        <label class="form-label">Program Studi</label>
                        <?php 
                            $id_programstudy = $value->id_programstudi;
                            $p = $this->user->query_all("SELECT * FROM tbl_programstudi WHERE id_programstudi = '$id_programstudy'")->result(); 
                        ?>
                        <select name="" id="" class="form-control">
                            <option value="<?= $p[0]->id_programstudi ?>"><?= $p[0]->nama_programstudi ?></option>
                            <?php 
                                foreach($programStudy as $prg): 
                                    if($prg->id_programstudi != $id_programstudy): 
                            ?>
                                <option value="<?= $prg->id_programstudi ?>"><?= $prg->nama_programstudi ?></option>
                            <?php 
                                    endif; 
                                endforeach 
                            ?>
                        </select>
                    </div>
                    
                    <div class="mb-3">
                        <label class="form-label">Kelas</label>
                        <select name="id_kelas" class="form-control">
                            <option value="<?= $value->id_kelas;?>"><?= $value->id_kelas;?></option>
                            <?php 
                                foreach($kelas as $kls): 
                                    if($kls->id_kelas != $value->id_kelas):
                            ?>
                                <option value="<?= $kls->id_kelas ?>"><?= $kls->id_kelas ?></option>
                            <?php 
                                    endif;
                                endforeach; 
                            ?>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Kuliah</label>      
                        <select name="id_kuliah" class="form-control" id="id_kuliah">
                            <option value="<?= $value->id_kuliah;?>"><?= $ktr_kelas;?></option>
                            <?php if($value->id_kuliah != 'R'): ?>
                                <option value="R">Reguler</option>
                            <?php endif; ?>
                            <?php if($value->id_kuliah != 'M'): ?>
                                    <option value="M">Malam</option>
                            <?php endif; ?>
                            <?php if($value->id_kuliah != 'SH'): ?>
                                <option value="SH">Shift</option>
                            <?php endif; ?>
                            <?php if($value->id_kuliah != 'JSM'): ?>
                                <option value="JSM">Jumat Sabtu</option>
                            <?php endif; ?>
                        </select>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Status</label>
                        <select name="status" class="form-control" id="status2">
                            <option value="<?= $value->status;?>"><?= $value->status;?></option>
                                <?php if($value->status != 'Aktif'): ?>
                                    <option value="Aktif">Aktif</option>
                                <?php endif; ?>
                                <?php if($value->status != 'Tidak Aktif'): ?>
                                    <option value="Tidak Aktif">Tidak Aktif</option>
                                <?php endif; ?>
                                <?php if($value->status != 'Drop Out'): ?>
                                    <option value="Drop Out">Drop Out</option>
                                <?php endif; ?>
                                <?php if($value->status != 'Lulus'): ?>
                                    <option value="Lulus">Lulus</option>
                                <?php endif; ?>
                                <?php if($value->status != 'Cuti'): ?>
                                    <option value="Cuti">Cuti</option>
                                <?php endif; ?>
                        </select>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Status Bayar</label>
                        <select name="status_bayar" class="form-control" id="status">
                            <option value="<?= $value->status_bayar;?>"><?= $status_bayar ?></option>
                            <?php if($value->status_bayar != 'L1'): ?>
                                <option value="L1">Lunas</option>
                            <?php else: ?>
                                <option value="L2">Belum Lunas</option>
                            <?php endif; ?>
                        </select>
                    </div>
                    
                    <div class="mb-3">
                        <label class="form-label">Alamat</label>
                        <textarea name="alamat" class="form-control" cols="30" rows="10"><?= $value->alamat;?></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>
    </td>
</tr>
<?php endforeach;?>
                                                                        