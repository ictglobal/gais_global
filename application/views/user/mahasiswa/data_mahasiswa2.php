<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Mahasiswa</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li> -->
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                           
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Filter Kelas</label>
                                
                                <select class="form-control kelas2" name="kelas" id="theSelect">
                                    <?php foreach($kelas as $key => $kls): ?>
                                        <?php $id = str_replace(" ","-",$kls->id_kelas); ?>
                                    <option value="<?= $id ?>"><?= $kls->id_kelas ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                           
                           
                            <button type="button" onclick="selectKelas()" class="btn btn-primary">Submit</button>
                            
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th class="text-center">NIM</th>
                                            <th class="text-center">Nama</th>
                                            <th class="text-center">Kelas</th>
                                            <th class="text-center">Kuliah</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody id="vewKelas">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script>
        $( document ).ready(function() {
    var id = <?php echo json_encode($id) ?>;
        if (id != '') {
            tbl(id);
        }
});
        
       
        function selectKelas() {
             var data = $('#kelas').serialize();

            var id = $('.kelas2').val();
           
            $('#vewKelas').load('<?= base_url()?>akademik/mahasiswa/selectKelas/'+id);
        }
        function tbl(id) {
           
            $('#vewKelas').load('<?= base_url()?>akademik/mahasiswa/selectKelas/'+id);
        }

        function hapusMahasiswa(id) {
            let text = "Lanjut Hapus";
            if (confirm(text) == true) {
                window.location.href = "<?= base_url();?>akademik/mahasiswa/hapus_file/"+id;
            } else {
                // alert('Hapus di batalkan')
            }
        }
    </script>
  </body>
</html>