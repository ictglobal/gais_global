<div class="col-md-6 col-sm-6">
  <div class="x_panel">
    <div class="x_title">
        <h2>Data Orang Tua</h2>
                      <!-- <small><?//php echo $this->session->userdata('role');?></small> -->
        
        <ul class="nav navbar-right panel_toolbox">
          <li class="dropdown" style="visibility: hidden;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <i class="fa fa-wrench"></i>
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
          </li>
          
          <li>
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </li>
                      
          <li>
            <a class="close-link">
              <i class="fa fa-close"></i>
            </a>
          </li>
        </ul>
          
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
      <div class="table-responsive">
        <table class="table table-striped jambo_table bulk_action">
          <thead>
            <tr class="headings">
              <th class="bulk-actions" colspan="7">
                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
              </th>
            </tr>
          </thead>

          <?php if ($this->session->userdata('level')==3): 
                    foreach ($biodata_mahasiswa as $key => $value) {
          ?>
          <tbody>
            <tr class="even pointer">
              <td>NIK Ayah</td>
                <td>:</td>
                <td><?= $value->nik_ayah;?></td>
            </tr>

            <tr class="even pointer">
              <td>Nama Ayah</td>
              <td>:</td>
              <td><?= $value->nama_ayah;?></td>
            </tr>

            <tr class="even pointer">
              <td>NIK Ibu</td>
              <td>:</td>
              <td><?= $value->nik_ibu;?></td>
            </tr>

            <tr class="even pointer">
              <td>Nama Ibu</td>
              <td>:</td>
              <td><?= $value->nama_ibu;?></td>
            </tr>
          </tbody>
          <?php } ?>
          <?php endif; ?>
        
        </table>
      </div>
    </div>
  </div>
</div>

<div class="col-md-6 col-sm-6">
  <div class="x_panel">
    <div class="x_title">
        <h2>Data Lainnya</h2>
                      <!-- <small><?//php echo $this->session->userdata('role');?></small> -->
        
        <ul class="nav navbar-right panel_toolbox">
          <li class="dropdown" style="visibility: hidden;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <i class="fa fa-wrench"></i>
            </a>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">Settings 1</a>
              <a class="dropdown-item" href="#">Settings 2</a>
            </div>
          </li>
          
          <li>
            <a class="collapse-link">
              <i class="fa fa-chevron-up"></i>
            </a>
          </li>
          <li>
            <a class="close-link">
              <i class="fa fa-close"></i>
            </a>
          </li>
        </ul>
        <div class="clearfix"></div>
    </div>

  <div class="x_content">
    <div class="table-responsive">
      <table class="table table-striped jambo_table bulk_action">
        <thead>
          <tr class="headings">
            <th class="bulk-actions" colspan="7">
              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
            </th>
          </tr>
        </thead>
        <tbody>
        <?php  foreach ($biodata_mahasiswa as $key => $value) { ?>

          <tr class="even pointer">
            <td>Program Studi</td>
            <td>:</td>
            <td>
              <?php foreach ($prodi as $key) {
                  if ($value->id_prodi === $key->id_prodi):
                      echo $key->prodi;    
                  endif;
              }?> 
            </td>
          </tr>

          <tr class="even pointer">
            <td>Program Kuliah</td>
            <td>:</td>
            <td>
              <?php echo $value->id_kuliah;?> 
            </td>
          </tr>

          <tr class="even pointer">
            <td>Konsentrasi</td>
            <td>:</td>
            <td>
              <?php foreach ($prodi as $key) {
                      if ($value->id_prodi === $key->id_prodi):
                        echo $value->id_prodi.' '.'('.$key->konsentrasi.')';    
                      endif;
              }?> 
            </td>
          </tr>

          <tr class="even pointer">
            <td>Kelas Sekarang</td>
            <td>:</td>
            <td><?php echo $value->id_kelas;?> </td>
          </tr>

        <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>