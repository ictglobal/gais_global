<!DOCTYPE html>
<html lang="en">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <!-- page content -->
                <div class="right_col" role="main">
                    <div>
                        <button class="btn btn-primary btn-md" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#tambah_mahasiswa"><span class="fa fa-pencil"></span> Tambah</button>
                        <button class="btn btn-success btn-md" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#export_mahasiswa"><span class="fa fa-file-excel-o"></span> Export</button>
                        <button class="btn btn-success btn-md" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#import_mahasiswa"><span class="fa fa-cloud-download"></span> Import</button>
                    </div>
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Daftar Mahasiswa</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                <li class="dropdown" style="visibility:hidden;">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Settings 1</a>
                                        <a class="dropdown-item" href="#">Settings 2</a>
                                    </div>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label class="form-label">Cari Nama dan NIM <code>tekan ENTER</code></label>
                                    <br>
                                    <div class="col-sm-3">
                                        <?php echo validation_errors(); ?>
                                        
                                        <input id="cari_mhs" name="cari" class="form-control" type="text" onkeydown="selection(this)">
                                        <br>
                                    </div>
                                    <div class="col-sm-3">
                                        <!-- <input type="button" id="cari_mhs2" name="cari" class="btn btn-primary btn-md" onclick="selection_button(this)" value="Cari Data"> -->
                                        <button class="btn btn-primary btn-md" id="cari_mhs" onclick="selection_button(this)"><span class="fa fa-search"></span> Cari</button>
                                    </div>
                                    <table id="datatable" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="text-center" width="5%">No</th>
                                                <th class="text-center">NIM</th>
                                                <th class="text-center">Nama</th>
                                                <th class="text-center">TTL</th>
                                                <th class="text-center">Kelas</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script src="<?= base_url().'temp/jquery-3.6.0.js'?>"></script>
    <script type="text/javascript">
        function selection(ele) {  
            var cari = $('#cari_mhs').val();
            var enc = stringToHex(cari);  
            // var encoded_url = encodeURIComponent();
            if(event.key === 'Enter'){
                if (cari == ""){
                    // console.log("Inputan nya jgn KOSONG WOYYY");
                }else{
                    window.location.href = '<?= base_url()?>akademik/mahasiswa/cari_mhs?id='+enc;
                }
            }
        }

        function selection_button(ele) {  
            var cari = $('#cari_mhs').val();
            var enc = stringToHex(cari);  
            // var encoded_url = encodeURIComponent();
            if (cari == ""){
                window.location.href = '<?= base_url()?>akademik/mahasiswa/';
            }else{
                window.location.href = '<?= base_url()?>akademik/mahasiswa/cari_mhs?id='+enc;
            }
        }

        function stringToHex (tmp) {
            var str = '';
            var tmp_len = tmp.length;
            var c = 0;
            for (var i=0; i < tmp_len; i++) {
                c = tmp.charCodeAt(i);
                str += c.toString(16);
            }
            return str;
        }
    </script>
  </body>
</html>

<!-- Modal Form untuk Export Mahasiswa -->
    <div class="modal fade" id="export_mahasiswa" role="dialog">
        <div class="modal-dialog" role="document">
            <form action="<?= base_url()?>akademik/mahasiswa/export_mahasiswa" method="post" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <i class="fa fa-cloud-upload mr-1"></i>Export Mahasiswa
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label>Tahun Akademik</label>
                            <br>
                            <select class="form-control" name="tahun_akademik199" id="select_tahun_akademik7" style="width:100%;" required>
                                <option value="">Pilih Tahun Akademik</option>
                                <?php foreach($tahun_akademik_a as $thn => $value): ?>
                                        <option value="<?= $value->id_tahunakademik ?>">
                                             <?= $value->tahun_akademik ?>
                                        </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Proses</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </form>
        </div>
    </div> 
<!-- End Section -->

<!-- Modal Form untuk Import Mahasiswa -->
    <div class="modal fade" id="import_mahasiswa" role="dialog">
        <div class="modal-dialog" role="document">
            <form action="<?= base_url()?>akademik/mahasiswa/import_mahasiswa" method="post" enctype="multipart/form-data">
                <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">
                            <i class="fa fa-cloud-download mr-1"></i>Import Mahasiswa
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <!-- <div class="form-group">
                            <label>Tahun Akademik</label>
                            <br>
                            <select class="form-control" id="select_tahun_akademik8" name="ta" style="width:100%;" required>
                                <option value="">Pilih Tahun Akademik</option>
                                    <?php 
                                        $tg_awal= date('Y')-13;
                                        $tgl_akhir= date('Y')+1;
                                        for ($i=$tgl_akhir; $i>=$tg_awal; $i--){
                                            $tgl_akhir = $i + 1;
                                            $th = $i." - ".$tgl_akhir;
                                    ?>
                                <option value="<?= $th?>"><?= $i." - ".$tgl_akhir; ?></option>
                                    <?php } ?>
                            </select>
                        </div> -->

                        <div class="form-group">
                            <label>File Upload <code>*.xls, *.xlsx, *.xlsm, *.xlsb</code></label>
                            <div class="input-group">
                                <input type="file" class="form-control" name="file" required>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Proses</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </form>
        </div>
    </div> 
<!-- End Section -->