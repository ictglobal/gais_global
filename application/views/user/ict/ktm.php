<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title_pdf;  ?></title>
    <!-- style -->
    <!-- ini untuk icon -->
    <link rel="icon" href="<?= base_url();?>temp/images/logo-global-institute.png" type="image/ico" />
    <link rel="stylesheet" href="<?= base_url()?>ktm/style/main.css" media="screen,print">

    <!-- google fonts gaya (poppins)-->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet" media="screen,print">

    <!-- css bootstrap 5 cdn -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous" media="screen,print">
  </head>
  <body class="bg-light">
    <!-- menu utama -->
     
    <div class="main-content">
      <div class="bg-ktm mt-5">
        <div class="row text-center">
          <div class="col-lg-2 card-left">
            <img src="<?= base_url()?>ktm/assets/image/text.png" alt="text-ktm" class="text-image">
          </div>

          <div class="col-lg-10 card-right mt-4">
            <img src="<?= base_url()?>ktm/assets/image/logo.webp" alt="logo" class="logo">
            <!-- <?php echo base_url();?>temp/images/logo-global-institute.png -->
            <div class="foto-mahasiswa mt-2">
              <!-- <?php 
                foreach ($get_mhs_foto as $key => $value):
                  $k = $this->user->get_data("*", "tbl_mahasiswa_foto", "WHERE nim = '$value->nim'");
                  if(count($k) <= 0):
              ?>
                  <img src="<?php echo base_url()."./temp/images/no_image.png"?>" alt="foto" class="foto">
              <?php else: ?>
                  <img src="<?php echo base_url()."./file_upload/mahasiswa/foto/".$value->file_foto;?>" alt="foto" class="foto">
              <?php
                  endif; 
                endforeach; 
              ?> -->

              <?php
                foreach ($get_mhs_foto as $key => $value):  
                  $k = $this->user->get_data("*", "tbl_mahasiswa_foto", "WHERE nim = '$value->nim'");
                    if(count($k) <= 0):                                               
                ?>
                  <img src="<?= base_url()?>temp/images/no_image.png" alt="foto" class="foto">
                <?php   
                  else:  $test_gbr = $k[0]; ?>
                  <img src="<?php echo base_url().'./file_upload/mahasiswa/foto/'.$test_gbr['file_foto'] ?>" alt="foto" class="foto">
                <?php 
                  endif;
                endforeach; 
              ?>
            </div>
            <img src='https://chart.googleapis.com/chart?chs=70x70&cht=qr&chl=<?= $x->nim?>&choe=UTF-8'>
             <div class="data-mahasiswa">
              <p class="text-nama"><?= $x->nama?></p>
              <p class="nim"><?= $x->nim?></p>
              <p class="prodi">
                <span class="badge badge-style">
                    <?php  
                        $t = $this->user->get_data("*", "tbl_programstudi", "WHERE id_programstudi = '$x->id_programstudi'")[0];
                        echo $t['nama_programstudi'];
                    ?>
                </span>
              </p>
              
             </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end menu utama -->

    <!-- js bootstrap 5 cdn -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous"></script>
    <script type="text/javascript">
        // window.print();
    </script>
  </body>
</html>