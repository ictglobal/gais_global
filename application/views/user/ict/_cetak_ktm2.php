<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <style>
        #my_camera{
            width: 320px;
            height: 240px;
            border: 1px solid black;
            }
    </style>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
<!--             <button class="btn btn-primary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#export_kalender">Tambah</button>
 -->            <div class="x_panel">
                <div class="x_title">
                    <h2>Cetak Kartu Tanda Mahasiswa <code><?=$thn_akad;?></code></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form id='formselect'>
                                    <div class="col-sm-3">
                                        <label for="">Filter Tahun Akademik</label>
                                        <select class="form-control" id="tahun_akademik2" onchange="selection()" required>
                                            <option value="">Pilih Tahun Akademik</option>
                                            <?php 
                                                $tg_awal= date('Y')-7;
                                                $tgl_akhir= date('Y')+1;
                                                for ($i=$tgl_akhir; $i>=$tg_awal; $i--){
                                                    $tgl_akhir = $i + 1;
                                                    $th = $i." - ".$tgl_akhir;
                                                    // $kode = $this->encrypt->encode($th);
                                                    $kode = $this->encryption->encrypt($th);
                                            ?>
                                                <option value="<?= $kode?>">
                                                    <?= $i." - ".$tgl_akhir; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </form>
                                <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center">No</th> -->
                                            <th class="text-center" width="5%">No</th>
                                            <th class="text-center" width="7%">NIM</th>
                                            <th class="text-center">Nama</th>
                                            <th class="text-center">Program Studi</th>
                                            <th class="text-center">Konsentrasi</th>
                                            <th class="text-center" width="27%">Action</th>
                                            <!-- <th class="text-center">Tahun Akademik</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php  
                                            foreach ($get_mhs as $key => $value):
                                        ?>
                                            <tr>
                                                <td></td>
                                                <td><?=$value->nim ?></td>
                                                <td><?=$value->nama ?></td>
                                                <td>
                                                    <?php 
                                                        $t = $this->user->get_data("*", "tbl_programstudi", "WHERE id_programstudi = '$value->id_programstudi'")[0];
                                                        echo $t['nama_programstudi'];
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        $konsentrasi = $this->user->get_data("*", "tbl_konsentrasi", "WHERE id_programstudi = '$value->id_programstudi'")[0];
                                                        echo $konsentrasi['nama_konsentrasi']." (".$konsentrasi['id_konsentrasi'].")"; 
                                                    ?>
                                                </td>
                                                <td align="center">
                                                    <button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <span class="fa fa-edit fa-sm"> Photo</span>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                      <a class="dropdown-item" href="#form_capture_foto<?= $value->nim?>" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#form_capture_foto<?= $value->nim?>"> <span class="fa fa-file-photo-o fa-sm"></span> Capture Photo </a>

                                                      <a class="dropdown-item" href="#form_foto" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#form_foto<?= $value->nim?>"> <span class="fa fa-cloud-upload fa-sm"> </span> Upload Photo</a>
                                                    </div>

                                                    <button class="btn btn-info btn-sm" onclick="detail_()"> 
                                                        <span class="fa fa-eye fa-sm"> Detail</span>
                                                    </button>

                                                    <!-- <button class="btn btn-primary btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#form_foto"><span class="fa fa-edit fa-sm"></span> Foto</button> -->
                                                 
                                                    <button class="btn btn-success btn-sm" onclick="cetak_ktm('<?=$this->encryption->encrypt($value->nim)?>')">
                                                        <span class="fa fa-print fa-sm"> Cetak</span>
                                                    </button>
                                                </td>
                                            </tr>
                                        <?php 
                                            endforeach;
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <!-- Modal Form untuk Upload Foto KTM -->
    <div class="modal fade" id="form_foto<?= $value->nim?>" role="dialog">
        <div class="modal-dialog" role="document">
            <form action="<?= base_url()?>ict/ict/upload_foto_mahasiswa" method="post" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <i class="fa fa-cloud-upload mr-1"></i>Upload Foto
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <input type="text" name="nim" value="<?= $value->nim?>">
                        <div class="form-group">
                            <label class="form-label">File Upload</label>
                            <div class="input-group">
                                <input type="file" id="imgInp" class="form-control" name="file_foto" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label">Preview</label>
                            <div class="input-group">
                                <picture>
                                    <?php  
                                        $k = $this->user->get_data("*", "tbl_mahasiswa_foto", "WHERE nim = '$vl->nim'");
                                        if(count($k) <= 0):                                               
                                    ?>
                                    <img src="<?= base_url()?>temp/images/no_image.png" id="blah" alt="foto mahasiswa" class="img-fluid img-thumbnail" style="width: 400%; height: 100%;">
                                    <?php   
                                        else:  $test_gbr = $k[0]; ?>
                                        <img src="<?php echo base_url().'./file_upload/mahasiswa/foto/'.$test_gbr['file_foto'] ?>" id="blah" alt="foto mahasiswa" class="img-fluid img-thumbnail" style="width: 200%; height: 100%;">
                                    <?php 
                                        endif;
                                    ?>
                                </picture>
                                    

                                    <!-- <img id="blah" src="<?= base_url()?>ktm/assets/image/foto.png" alt="foto mahasiswa" class="img-fluid img-thumbnail" style="width: 100%; height: 100%;"> -->
                                
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
    <?php  
        $no=0;
        foreach ($get_mhs as $key2 => $vl):
    ?>

    <!-- Modal Form untuk Capture Foto KTM -->
    <div class="modal fade" id="form_capture_foto<?= $vl->nim?>" role="dialog">
        <div class="modal-dialog" role="document">
            <form action="<?= base_url()?>ict/ict/upload_foto_mahasiswa" method="post" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <i class="fa fa-cloud-upload mr-1"></i>Capture Foto KTM
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                    </div>
                    
                    <div class="modal-body">
                        <input type="text" name="nim" value="<?= $vl->nim?>">
                        <div class="form-group">
                            <label class="form-label">Layar Kamera</label>
                            <div class="input-group">
                                <input type=button value="Configure" onClick="configure()">
                                <input type=button value="Take Snapshot" onClick="take_snapshot()">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label">Take Gambar</label>
                            <div id="my_camera"></div>
                        </div>
                        <div class="form-group">
                            <label>Preview</label>
                            <div id="results" ></div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php endforeach; ?>
    <!-- End Section -->

    <script type="text/javascript">
         // Konfigurasi dan pengaturan kamera
         function configure(){
            Webcam.set({
               width: 320,
               height: 240,
               image_format: 'jpeg',
               jpeg_quality: 90
            });
            Webcam.attach( '#my_camera' );
        }

        // preload shutter audio clip
         // var shutter = new Audio();
         // shutter.autoplay = false;
         // shutter.src = navigator.userAgent.match(/Firefox/) ? 'shutter.ogg' : 'shutter.mp3';
         
         function take_snapshot() {
            // play sound effect
            // shutter.play();
         
            //  snapshot dan mendapatkan data gambar
            Webcam.snap( function(data_uri) {
               // display results in page
               document.getElementById('results').innerHTML = 
                   '<img id="imageprev" src="'+data_uri+'"/>';
             } );
         
             Webcam.reset();
         }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $("#imgInp").change(function(){
            readURL(this);
        });

        function selection() {
            var id = $('#tahun_akademik2').val();
            var encoded_url = encodeURIComponent(id);
            window.location.href = '<?= base_url()?>ict/ict/tampil_mhs?th='+encoded_url;
        }

        function cetak_ktm(nim){
            // alert('ayam');
            // window.location.href='<?= base_url()?>ict/ict/cetak_ktm/'+nim;
            // window.open('<?= base_url()?>ict/ict/cetak_ktm/'+nim, '_blank');
            // var id = $('#nim').val();
            var encoded_url = encodeURIComponent(nim);
            window.open('<?= base_url()?>ict/ict/cetak_ktm?id='+encoded_url, '_blank');
        }

        function detail_(){
            window.open('<?=base_url()?>ict/ict/detail_mhs', '_blank');
        }
    </script>
  </body>
</html>