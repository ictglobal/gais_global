<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Status GAIS <?= $kelas1;?>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form id='formselect'>
                                <div class="col-sm-3">
                                        <label for="">Filter Kelas</label>
                                        <select class="form-control" id="theSelect3" onchange="selection()" required>
                                            <option value="">Pilih Kelas</option>
                                                <?php foreach ($kelas as $key => $kls): ?>
                                                    <?php 
                                                    // $kode = implode('_',explode(' ',))
                                                    $kode = $this->encryption->encrypt($kls->id_kelas);?>
                                                <option value="<?= $kode ?>">
                                                    <?= $kls->id_kelas; ?>
                                                </option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <!-- <div class="col-sm-3">
                                        <label for="">Kelas</label>
                                        <select class="form-control" id="theSelect2" required>
                                            <option>Pilih Status</option>
                                            <option value="L1">Lunas</option>
                                            <option value="BL">Belum Lunas</option>
                                        </select>
                                    </div> -->
                                    <!-- <div class="col-sm-3">
                                        <br>
                                        <button type="button" onclick="selection()" class="btn btn-primary btn-lg">Filter</button>
                                    </div> -->
                                    </form>
                                    <br>
                                <table id="datatable-responsive" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="10%">NIM</th>
                                            <th class="text-center">Nama</th>
                                            <th class="text-center" width="15%">Jenis Kelamin</th>
                                            <!-- <th class="text-center">Kelas</th> -->
                                            <!-- <th class="text-center">Kuliah</th> -->
                                            <th class="text-center" width="10%">Status</th>
                                            <th class="text-center">Aksi</th>                   
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($mhs as $key => $val): ?>
                                            <form id="form<?= $val->nim; ?>" method="post">
                                                <input type="hidden" name="nim" value="<?= $val->nim; ?>">
                                                <input type="hidden" name="status_bayar" value="<?= $val->status_bayar; ?>">
                                            </form>
                                        <tr>
                                            <td><?= $val->nim;?></td>
                                            <td><?= $val->nama;?></td>
                                            <td>
                                                <?php 
                                                    if($val->jenis_kelamin == "L"):
                                                        echo "Laki-laki";
                                                    else:
                                                        echo "Perempuan";
                                                    endif;
                                                ?>
                                            </td>
                                            <!-- <td><?= $val->id_kelas; ?></td> -->
                                            <!-- <td><?= $val->id_kuliah;?></td> -->
                                            <td><?= $val->status;?></td>
                                            <td align="center" width="10%">
                                                <input type="checkbox" class="js-switch" <?php if($val->status_bayar == 'L1'): ?> checked <?php endif ?> onchange="update_status('<?= $val->nim; ?>')" />
                                            </td>
 
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
        function update_status(nim){         
            var data = $('#form'+nim).serialize();
            // console.log(data);
            $.ajax({
                    type: 'POST',
                    url: '<?= base_url()?>finance/keuangan/update_status/'+nim,
                    data: data,
                    success: function(data) {
                        console.log(data);
                    }
            });

            $.ajax({
                    type: 'POST',
                    url: '<?= base_url()?>finance/keuangan/insert_status/'+nim,
                    data: data,
                    success: function(data) {
                        console.log(data);
                    }
            });
            // $('#tes').load('<?= base_url()?>finance/keuangan/update_status/'+nim);
        }

        function selection() {
            var id = $('#theSelect3').val();
            var encoded_url = encodeURIComponent(id);
            window.location.href = '<?= base_url()?>finance/keuangan/select_mhs?id='+encoded_url;
        }
    </script>
        <!-- Sweet Alert -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <!-- jQuery -->
    <script src="<?= base_url();?>temp/vendors/jquery/dist/jquery.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/jquery/dist/jquery-ui.js"></script>

    <!-- Bootstrap -->
    <script src="<?= base_url();?>temp/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="<?= base_url();?>temp/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?= base_url();?>temp/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="<?= base_url();?>temp/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="<?= base_url();?>temp/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?= base_url();?>temp/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="<?= base_url();?>temp/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="<?= base_url();?>temp/vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="<?= base_url();?>temp/vendors/Flot/jquery.flot.js"></script>
    <script src="<?= base_url();?>temp/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="<?= base_url();?>temp/vendors/Flot/jquery.flot.time.js"></script>
    <script src="<?= base_url();?>temp/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="<?= base_url();?>temp/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="<?= base_url();?>temp/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="<?= base_url();?>temp/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="<?= base_url();?>temp/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="<?= base_url();?>temp/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="<?= base_url();?>temp/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="<?= base_url();?>temp/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- Switchery -->
    <script src="<?= base_url();?>temp/vendors/switchery/dist/switchery.min.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="<?= base_url();?>temp/vendors/moment/min/moment.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- jQuery autocomplete -->
    <script src="<?= base_url();?>temp/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js"></script>
    
    <!-- Datatables -->
    <script src="<?= base_url();?>temp/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/pdfmake/build/vfs_fonts.js"></script>
  </body>
</html>