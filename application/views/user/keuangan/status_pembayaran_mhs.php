<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Status Pembayaran <small>Mahasiswa</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                   
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form id='formselect'>
                                <div class="col-sm-3">
                                        <label for="">Filter Kelas</label>
                                        <select class="form-control" id="theSelect2" onchange="selection()" required name="th_akdmk_keuangan">
                                            <option value="">Pilih Kelas</option>
                                                <?php foreach ($kelas as $key => $kls): ?>
                                                    <?php 
                                                    // $kode = implode('_',explode(' ',))
                                                    // $kode = $this->encrypt->encode($kls->id_kelas); 
                                                    $kode = $this->encryption->encrypt($kls->id_kelas);?>
                                                <option value="<?= $kode ?>">
                                                    <?= $kls->id_kelas; ?>
                                                </option>
                                                <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <!-- <div class="col-sm-3">
                                        <label for="">Kelas</label>
                                        <select class="form-control" id="theSelect2" required>
                                            <option>Pilih Status</option>
                                            <option value="L1">Lunas</option>
                                            <option value="BL">Belum Lunas</option>
                                        </select>
                                    </div> -->
                                    <!-- <div class="col-sm-3">
                                        <br>
                                        <button type="button" onclick="selection()" class="btn btn-primary btn-lg">Filter</button>
                                    </div> -->
                                    </form>
                                    <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            
                                            <!-- <th class="text-center">No</th> -->
                                            <th class="text-center" width="10%">NIM</th>
                                            <th class="text-center" >Nama</th>
                                            <th class="text-center" width="10%">Jenis Kelamin</th>
                                            <th class="text-center">Kelas</th>
                                            <!-- <th class="text-center">Kuliah</th> -->
                                            <th class="text-center">Status</th>
                                            <th class="text-center">Aksi</th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
        function selection() {
            var id = $('#theSelect2').val();
            var encoded_url = encodeURIComponent(id);
            window.location.href = '<?= base_url()?>finance/keuangan/select_mhs?id='+encoded_url;
        }
    </script>
  </body>
</html>