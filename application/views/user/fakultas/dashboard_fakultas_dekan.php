<div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Dashboard</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="">
                  <div class="x_content">
                    <?php 
                        foreach ($data_fakultas as $key => $value) {
                    ?>
                    <div class="row">
                      

                      <?php if ($this->session->userdata('id_fakultas')==1):?>

                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-users"></i>
                          </div>
                          <div class="count"><?= $this->user->count_data_mahasiswa('2');?></div>
                          <h3>Total Mahasiswa</h3>
                          <p>Fakultas: 
                            <?php
                                $fakul   = $this->user->get_data("*", "tbl_fakultas", "where id_fakultas ='$value->id_fakultas'")[0];
                                echo $fakul['nama_fakultas'];
                            ?>   
                          </p>
                        </div>
                      </div>

                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-graduation-cap"></i>
                          </div>
                          <div class="count"><?= $this->user->count_data_mahasiswa_prodi_SI();?></div>

                          <h3>Total Mahasiswa</h3>
                          
                          <p>Program Studi: 
                              <?php
                                  $program = $this->user->get_data('*', 'tbl_programstudi', "where id_fakultas = '$value->id_fakultas'")[0];
                                  echo $program['nama_programstudi'];
                              ?>
                          </p>
                        </div>
                      </div>
                      
                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-user-md"></i>
                          </div>
                          <div class="count"><?= $this->user->count_data_mahasiswa_prodi_TI();?></div>

                          <h3>Total Mahasiswa</h3>
                          
                          <p>Program Studi: Teknik Informatika</p>
                        </div>
                      </div>

                      <!-- <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-user-md"></i>
                          </div>
                          <div class="count"><?= $this->user->count_data_mahasiswa_prodi_TI_CS();?></div>

                          <h3>Total Mahasiswa</h3>
                          
                          <p>Program Studi: CS</p>
                        </div>
                      </div> -->
                        
                      <?php elseif ($this->session->userdata('id_fakultas')==2):?>

                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-users"></i>
                          </div>
                          <div class="count"><?= $this->user->count_data_mahasiswa_BD();?></div>

                          <h3>Total Mahasiswa</h3>
                          

                          <p>Fakultas: 
                            <?php
                                $fakul   = $this->user->get_data("*", "tbl_fakultas", "where id_fakultas ='$value->id_fakultas'")[0];
                                echo $fakul['nama_fakultas'];
                            ?>   
                          </p>
                        </div>
                      </div>

                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-user-md"></i>
                          </div>
                          
                          <div class="count"><?php echo $this->user->count_data_mahasiswa('1');?></div>

                          <h3>Total Mahasiswa</h3>
                          <p>Program Studi: <?= $value->nama_fakultas;?></p>
                        </div>
                      </div>
                      <?php endif;?>
                    </div>
                    <?php }?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
