<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
          <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Atur Jadwal Penginputan Nilai</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown" style="visibility: hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <div>
                        &nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-primary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#tampil_atur_jadwal">Tambah</button>
                    </div>

                    <div class="table-responsive">
                      <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                              <th class="text-center" width="4%">No</th>
                              <th class="text-center" width="10%">Nama Dosen</th>
                              <th class="text-center">Tanggal dan Waktu Mulai</th>
                              <th class="text-center">Tanggal dan Waktu Selesai</th>
                              <th class="text-center" width="5%">Tahun Akademik</th>
                              <th class="text-center" width="5%">Status Input</th>
                              <th class="text-center" width="20%">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php //$i=1;
                            foreach ($atur_jadwal as $key => $value) { 
                        ?>
                            <tr>
                                <td></td>
                                <td><?= $value['username_dosen'];?></td>
                                <td><?= $value['tgl_mulai']." || ".$value['waktu_mulai']?></td>
                                <td><?= $value['tgl_selesai']." || ".$value['waktu_selesai']?></td>
                                <td>
                                    <?= $tahun_akademik;?>
                                </td>
                                <td align="center">
                                    <input type="checkbox" id="ayam" class="js-switch" <?php if($value['status_input'] == 'Aktif'): ?> checked <?php endif ?> onchange="update_status('<?= $value['id_atur']; ?>')"/>
                                </td>
                                <td align="center">
                                    <button type="button" class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#edit_tampil_atur_jadwal<?= $value['id_atur']; ?>">
                                            <span class="fa fa-edit fa-lg"></span> Edit
                                    </button>
                                    <button type="button" class="btn btn-danger btn-sm" onclick="location.href=''"> Delete
                                            <span class="fa fa-trash-o fa-lg"></span>
                                    </button>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>          
          <!-- top tiles -->
    </div>
      </div>
    </div>

    <script type="text/javascript">
        function update_status(id_atur){         
            var data = document.getElementById('ayam').value
            // console.log(data);
            $.ajax({
                    type: 'POST',
                    url: '<?= base_url()?>perkuliahan/nilai/update_status_input_nilai/'+id_atur,
                    data: data,
                    success: function(data) {
                        console.log(data);
                    }
            });
        }
    </script>
  </body>
</html>

    <!-- Modal Form untuk tambah  Atur Jadwal Input Nilai-->
<div class="modal fade" id="tampil_atur_jadwal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url() ?>perkuliahan/nilai/set_input_nilai" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Tambah Waktu
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label>Tanggal Mulai</label>
                        <input class="form-control" class='date' type="date" name="tgl_mulai" required='required'>
                    </div>

                    <div class="form-group">
                        <label>Waktu Mulai</label>
                        <input class="form-control" class='time' type="time" name="waktu_mulai" required='required'>
                    </div>

                    <div class="form-group">
                        <label>Tanggal Selesai</label>
                        <input class="form-control" class='date' type="date" name="tgl_selesai" required='required'>
                    </div>

                    <div class="form-group">
                        <label>Waktu Selesai</label>
                        <input class="form-control" class='time' type="time" name="waktu_selesai" required='required'>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->
<?php 
    // foreach ($atur_jadwal as $key => $value) {
        //$edit = $this->user->get_data('*', 'tbl_dosen', "where username_dosen = '$value->username_dosen'")[0];
?>
<!-- Modal Form untuk Tampil Atur Jadwal -->
<!-- <div class="modal fade" id="edit_tampil_atur_jadwal<?= $value->id_atur ?>" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url() ?>perkuliahan/nilai/set_input_nilai" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-cloud-upload mr-1"></i>Tambah Waktu
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <div class="form-group">
                            <label>Nama Dosen</label>
                            <input type="text" name="dosen" class="form-control" required value="$value->username_dosen">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Mulai</label>
                        <input class="form-control" class='date' type="date" name="tgl_mulai" required='required'>
                    </div>

                    <div class="form-group">
                        <label>Waktu Mulai</label>
                        <input class="form-control" class='time' type="time" name="waktu_mulai" required='required'>
                    </div>

                    <div class="form-group">
                        <label>Tanggal Selesai</label>
                        <input class="form-control" class='date' type="date" name="tgl_selesai" required='required'>
                    </div>

                    <div class="form-group">
                        <label>Waktu Selesai</label>
                        <input class="form-control" class='time' type="time" name="waktu_selesai" required='required'>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div>  -->
<?php //} ?>
<!-- End Section -->