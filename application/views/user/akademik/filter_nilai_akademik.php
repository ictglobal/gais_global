<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Penilaian Mata Kuliah</h3>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>List yang sudah mengumpulkan</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                
                <div class="x_content">
                    <table>
                        <tr>
                            <td>Nama Dosen</td>
                            <td>:</td>
                            <td><?= $dosen;?></td>
                        </tr>
                        <tr>
                            <td>Tahun Akademik</td>
                            <td>:</td>
                            <td><?= $tahun;?></td>
                        </tr>
                    </table>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                            <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            
                                            <th class="text-center" width="8%" align="text-center">No</th>
                                            <th class="text-center">Kelas</th>
                                            <th class="text-center">Mata Kuliah</th>
                                            <th class="text-center">Status</th>  
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php 
                                            $no = 1;
                                            foreach($hasil as $hsl): ?>
                                        <tr>
                                            <td><?= $no++;?></td>
                                            <td><?= $hsl['id_kelas'] ?></td>
                                            <td><?= $hsl['nama_matkul'] ?></td>
                                            <?php if($hsl['status']): ?>
                                            <td align="center"><span class="badge badge-info">Sudah Input</span></td>
                                            <?php else: ?>
                                                 <td align="center"><span class="badge badge-danger">Belum Input</span></td>
                                                <?php endif; ?>
                                        </tr>
                                         <?php endforeach;?>
                                    </tbody>
                                </table>    

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
      </div>
    </div>
  </body>
</html>