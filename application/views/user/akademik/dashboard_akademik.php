<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Dashboard</h3>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="row">
              <div class="col-md-12">
                <div class="">
                  <div class="x_content">
                    <div class="row">
            
                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-users"></i>
                          </div>
                          <div class="count">775</div>
                          <h3>Total Mahasiswa Aktif</h3>
                          <p>Program Studi Sistem Informasi</p>
                        </div>
                      </div>

                      <div class="animated flipInY col-lg-3 col-md-3 col-sm-6  ">
                        <div class="tile-stats">
                          <div class="icon"><i class="fa fa-users"></i>
                          </div>
                          <div class="count">775</div>
                          <h3>Total Mahasiswa Lulus</h3>
                          <p>Program Studi: Sistem Informasi</p>
                        </div>
                      </div>
 
                  </div>
                </div>
              </div>
            </div>

            </div>
        </div>
      </div>
    </div>
  </body>
</html>