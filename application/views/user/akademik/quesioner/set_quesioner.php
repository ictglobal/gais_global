<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>
                        Halaman Set Quesioner
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable-responsive" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="1%">No</th>
                                            <th class="text-center">Aksi</th>                   
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php
                                                $no = 1; 
                                                foreach ($quesioner as $key => $value) {
                                            ?>
                                                <td><?= $no++; ?></td>
                                                <td align="center">
                                                    <input type="checkbox" class="js-switch" <?php if($value['status'] == 1): ?> checked <?php endif ?> onchange="update('<?= $this->encryption->encrypt($value['status'])?>')" >
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
        function update(id){
            window.location.href = '<?= base_url()?>perkuliahan/quesioner/update_ques?id='+encodeURIComponent(id);
        }
    </script>
  </body>
</html>