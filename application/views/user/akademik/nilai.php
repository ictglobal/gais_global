<!DOCTYPE html>
  <html lang="en">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Penilaian Mata Kuliah</h3>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>List yang sudah mengumpulkan</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x-content">
                    <form action="<?= base_url()?>perkuliahan/nilai/filter_nilai_akademik" method="post">
                    <div class="row mb-5">
                                <div class="col-sm-3" id="tahun_akademik">
                                    <label for="">Tahun Akademik</label>

                                    <select id="select_page" name="tahun_akademik"  class="form-control" required>
                                        <option value="">Pilih Tahun Akademik</option>
                                        
                                        <?php foreach($tahun as $thn): ?>
                                        <option value='<?= $thn['id_tahunakademik'] ?>'>
                                            <?= $thn['tahun_akademik'] ?>
                                        </option>
                                        <?php endforeach; ?>
                                    </select>
                                    
                                    <input type="hidden" id="thakademik" value="<?= $id_tahunakademik ?>" >
                                </div>

                                <div class="col-sm-3">
                                    <label for="">Dosen</label>

                                    <select class="form-control" id="theSelect" onchange="selectkelas()" name="username_dosen">
                                        <option value="">Pilih Dosen</option>
                                        <?php foreach($dosen as $dsn): ?>
                                            <option value="<?= $dsn['username_dosen'] ?>"><?= $dsn['dosen'] ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>

                                <!-- <div class="col-sm-3">
                                    <label for="">Mata Kuliah</label>
                                    <select name="matakuliah" id="selectkelass"  class="form-control" required>
                                         <option value="">Pilih Mata Kuliah</option>
                                        

                                    </select>
                                </div> -->
                            </div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Filter</button>
                        </form>
                </div>
            </div>
        </div>
        
      </div>
    </div>

    <script src="<?= base_url().'temp/jquery-3.6.0.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //change selectboxes to selectize mode to be searchable
            $("select").select2();
            // $("#smt2").load("");
        });
    </script>

    <script>
        function selectkelas() {
            var username = $('#theSelect').val();
            var tahun_akademik = $('#thakademik').val();
            $('#selectkelass').load('<?= base_url()?>/perkuliahan/nilai/selectkelas/'+username+'/'+tahun_akademik);
        }
    </script>
  </body>
</html>