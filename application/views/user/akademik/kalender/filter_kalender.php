<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <?php if($this->session->userdata('level')==6): ?>
            <button class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#export_kalender">Import</button>
            <?php endif; ?>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Kalender Akademik
                        <small><?= $tahun_akademik1?></small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form id='formselect'>
                                <div class="col-sm-3">
                                        <label for="">Filter Tahun Akademik</label>
                                        <select class="form-control" id="tahun_akademik2" onchange="selection()" required>
                                            <option value="">Pilih Tahun Akademik</option>
                                            <?php 
                                                $tg_awal= date('Y')-7;
                                                $tgl_akhir= date('Y')+1;
                                                for ($i=$tgl_akhir; $i>=$tg_awal; $i--){
                                                    $tgl_akhir = $i + 1;
                                                    $th = $i." - ".$tgl_akhir;
                                                    $kode = $this->encryption->encrypt($th);
                                                // $kode = $this->encrypt->encode($th);
                                            ?>
                                                <option value="<?= $kode?>"><?= $i." - ".$tgl_akhir; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    </form>
                                    <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <!-- <th class="text-center">No</th> -->
                                            <th class="text-center" width="5%">No</th>
                                            <th class="text-center">Mulai</th>
                                            <th class="text-center">Selesai</th>
                                            <th class="text-center">Kegiatan</th>
                                            <th class="text-center">Keterangan</th>
                                            <!-- <th class="text-center">Tahun Akademik</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no=1; 
                                            foreach ($kalender as $key => $value):
                                        ?>
                                            <tr>
                                                <td><?= $no++?></td>
                                            <td>
                                                <?php 
                                                    echo $value->tgl_mulai;    
                                                ?>
                                            </td>
                                            <td>
                                                <?php 
                                                    echo $value->tgl_selesai;    
                                                ?>
                                            </td>
                                            <td><?= $value->ket1?></td>
                                            <td><?= $value->ket2?></td>
                                           <!--  <td>
                                                <?php 
                                                    // $kk = $this->user->get_data("tahun_akademik","tbl_tahunakademik","WHERE id_tahunakademik = '$value->id_tahunakademik'")[0];
                                                    // echo $kk['tahun_akademik'];
                                                ?>        
                                            </td> -->
                                            <!-- <td><?= $value->tahun_akademik?></td> -->
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url().'temp/jquery-3.6.0.js'?>"></script>
            <!-- jQuery -->
    <!-- <script src="<?= base_url();?>temp/vendors/jquery/dist/jquery.min.js"></script> -->
    <!-- Bootstrap -->
   <script src="<?= base_url();?>temp/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Datatables -->
    <script src="<?= base_url();?>temp/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?= base_url();?>temp/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

    <!-- Custom Theme Scripts -->
    <!-- <script src="<?= base_url();?>temp/build/js/custom.min.js"></script> -->
    <script type="text/javascript">

        function selection() {
            var id = $('#tahun_akademik2').val();
            var encoded_url = encodeURIComponent(id);
            // window.location.href = '<?= base_url()?>perkuliahan/kalender_akademik/select_kalender?th='+encoded_url;
            window.location.href = '<?= base_url()?>perkuliahan/kalender_akademik/select_kalender?th='+encoded_url;
        }
    </script>

  </body>
</html>