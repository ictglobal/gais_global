<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div style="text-align: left;">
                <button class="btn btn-primary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#tambah_konsentrasi">Tambah</button>
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Konsentrasi</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable-responsive" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="5%" >No.</th>
                                            <th class="text-center">Fakultas</th>
                                            <th class="text-center">Nama Program Studi</th>
                                            <th class="text-center">Kode Konsentrasi</th>
                                            <th class="text-center">Nama Konsentrasi</th>
                                            <th class="text-center" width="20%">Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $i=1;
                                            foreach ($konsentrasi as $key => $value):
                                        ?>
                                        <tr>
                                            <td align="center"><?= $i++;?></td>
                                            <td>
                                                <?php
                                                    $fakultas2 = $this->user->get_data('*', 'tbl_fakultas', "where id_fakultas = '$value->id_fakultas'")[0];
                                                    echo $fakultas2['nama_fakultas'];
                                                ?>    
                                            </td>
                                            <td>
                                                <?php
                                                    $prodi = $this->user->get_data('*', 'tbl_programstudi', "where id_programstudi = '$value->id_programstudi'")[0];
                                                    echo $prodi['nama_programstudi'];
                                                ?>    
                                            </td>
                                            <td><?= $value->id_konsentrasi;?></td>
                                            <td><?= $value->nama_konsentrasi;?></td>
                                            <td align="center">
                                                <button type="button" class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#edit_konsentrasi<?=$value->id_konsentrasi?>"> 
                                                    <span class="fa fa-edit fa-lg"></span> Edit
                                                </button>
                                                
                                                <button type="button" class="btn btn-danger btn-sm hapus" id="<?=$value->id_konsentrasi?>">
                                                    <span class="fa fa-trash-o fa-lg"></span> Delete
                                                </button>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
    </div>

  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script type="text/javascript">
    $(".hapus").click(function(e){
        e.preventDefault();
        var id = $(this).closest('button').attr('id');
        
        Swal({
                title: "Konfirmasi?",
                text: "Apakah anda yakin ingin hapus data ini !",
                type: "warning",
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
        }).then((isConfirm) => {
            if (isConfirm.value) {
                $.ajax({
                    url: '<?= base_url()?>akademik/konsentrasi/hapus_konsentrasi/'+id,
                    type: 'DELETE',
                    error: function() {
                        alert('Something is wrong');
                    },
                    success: function(data) {
                        window.location.href = '<?= base_url()?>akademik/konsentrasi/tampil_message'
                            // console.log(id);
                    }
                });
            }
            return false;
        });
        return true;
    }); 
  </script>
</html>

<!-- Modal Form untuk Tambah Konsentrasi -->
<div class="modal fade" id="tambah_konsentrasi" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>akademik/konsentrasi/tambah_konsentrasi" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-plus-square mr-1"></i>Tambah Konsentrasi
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Kode Konsentrasi </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan Kode Konsentrasi" class="form-control" name="kode" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Fakultas </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select class="form-control" id="theSelect" style="width: 91%;" name="fakultas" required>
                                <option value="" disabled selected>--Pilih Fakultas--</option>
                                <?php foreach ($fakultas as $key => $value) { ?>
                                    <option value="<?= $value->id_fakultas ?>"><?= $value->nama_fakultas ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Program Studi </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select class="form-control" id="theSelect2" style="width: 91%;" name="prodi" required>
                                <option value="" disabled selected>--Pilih Program Studi--</option>
                                <?php foreach ($program_s as $key => $value) { ?>
                                    <option value="<?= $value['id_programstudi'] ?>"><?= $value['nama_programstudi'] ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nama Konsentrasi </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan Nama Konsentrasi" class="form-control" name="konsentrasi" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<!-- End Section -->

<!-- Modal Form untuk Edit Konsentrasi -->
<?php 
    $i = 0;
    foreach ($konsentrasi as $key => $value2) { 
        $get_fakultas           = $this->user->get_data("*", "tbl_fakultas", "WHERE id_fakultas = '$value2->id_fakultas'")[0];
        $get_prodi              = $this->user->get_data("*", "tbl_programstudi", "WHERE id_programstudi = '$value2->id_programstudi'")[0];
        $get_konsentrasi        = $this->user->get_data("*", "tbl_konsentrasi", "WHERE id_konsentrasi = '$value2->id_konsentrasi'")[0];
?>
<div class="modal fade" id="edit_konsentrasi<?=$value2->id_konsentrasi?>" role="dialog">
    <div class="modal-dialog" role="document">
        <form action="<?= base_url();?>akademik/konsentrasi/edit_konsentrasi" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-plus-square mr-1"></i>Ubah Konsentrasi
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <input type="hidden" name="id_konsentrasi" value="<?=$value2->id_konsentrasi ?>">
                    <div class="form-group">
                        <label>Fakultas </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select class="form-control" style="width: 91%;" id="pilihan_fakultas2<?= $i++;?>" name="fakultas" required>
                                <option value="<?=$value2->id_fakultas?>" required><?=$get_fakultas['nama_fakultas']  ?></option>
                                <?php foreach ($fakultas as $key => $value3) { ?>
                                    <option value="<?= $value3->id_fakultas ?>"><?= $value3->nama_fakultas ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nama Program Studi </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <select class="form-control" style="width: 91%;" id="pilihan_prodi2<?= $i++;?>" name="prodi" required>
                                <option value="<?=$value2->id_programstudi?>" required><?=$get_prodi['nama_programstudi']  ?></option>
                                <?php foreach ($program_s as $key => $value4) { ?>
                                    <option value=""><?= $value4['nama_programstudi'] ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nama Konsentrasi </label>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-edit"></i>
                            </span>
                            <input type="text" placeholder="Masukan Nama Konsentrasi" class="form-control" name="konsentrasi" value="<?= $get_konsentrasi['nama_konsentrasi']?>" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </form>
    </div>
</div> 
<?php }?>
<!-- End Section -->