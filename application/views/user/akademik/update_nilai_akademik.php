<!DOCTYPE html>
<html lang="en">
    <style type="text/css">
        .cursor{
            cursor: pointer;
        }
        .tableedit{
            display: none;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div>
                <button class="btn btn-primary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#export_nilai">Export Nilai</button>
                <button class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#import_nilai">Import Nilai</button>
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Penilaian Mahasiswa <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form action="<?= base_url()?>perkuliahan/nilai/update_nilai_akademik" method="post">
                        <div class="row mb-5">
                            <div class="col-sm-3" id="tahun_akademik">
                                <label for="">Tahun Akademik</label>
                                <select id="tahun_akademik2" name="tahun_akademik" class="form-control" onchange="getDosen()" required>
                                    <option value="">Pilih Tahun Akademik</option>
                                   
                                    <?php foreach($tahunAkademik as $thn): ?>
                                        <option value="<?= $thn['id_tahunakademik'] ?>">
                                         <?= $thn['tahun_akademik'] ?>
                                    </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <label for="">Dosen</label>
                                <select name="dosen" class="form-control" name="username_dosen" id="dosen" onchange="getKelas()" required>
                                    <option value="">Pilih Dosen</option>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <label for="">Kelas</label>
                                <select name="kelas" id="kelas" name="kelas" class="form-control" onchange="getSemester()" required>
                                    <option value="">Pilih Kelas</option>
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <label for="">Mata Kuliah</label>
                                <select name="matakuliah" id="matakuliah" name="matakuliah" class="form-control" required>
                                <option value="">Pilih Mata Kuliah</option>
                                    
                                </select>
                            </div>

                            <div class="col-sm-3">
                                <label class="form_control">Semester</label>
                                <select name="semester" id="semester" class="form-control" required>
                                    <option>Pilih Semester</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Filter</button>
                    </form>                   
                </div>
            </div>
        </div>
      </div>
    </div>
    <script>
        function getSemester(){
            var kelas = document.getElementById("kelas").value;
            var tahun_akademik = $('#tahun_akademik2').val();
            var dosen = $('#dosen').val();
            console.log(kelas);
            $('#semester').load('<?= base_url()?>perkuliahan/nilai/selectSemeser/'+tahun_akademik+'/'+dosen+'/'+kelas);
            $('#matakuliah').load('<?= base_url()?>perkuliahan/nilai/getMataKuliah/'+tahun_akademik+'/'+dosen+'/'+kelas);
        }

        // $(document).ready(function () {
        //     //change selectboxes to selectize mode to be searchable
        //     $("select").select2();
        //     // $("#smt2").load("");
        // });

        function getDosen(){
            var tahun_akademik = $('#tahun_akademik2').val();
            $('#dosen').load('<?= base_url()?>perkuliahan/nilai/getDosen/'+tahun_akademik);
        }

        function getKelas(){
            var dosen = $('#dosen').val();
            var tahun_akademik = $('#tahun_akademik2').val();

            $('#kelas').load('<?= base_url()?>perkuliahan/nilai/getKelas/'+tahun_akademik+'/'+dosen);   
        }
    </script>

  </body>
</html>