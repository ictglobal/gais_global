<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div>
                &nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-primary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#tambah_matkul">Tambah</button>
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Mata Kuliah</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th class="text-center" width="5%">Kode</th>
                                            <th class="text-center">Nama</th>
                                            <th class="text-center" width="5%">Semester</th>
                                            <th class="text-center" width="5%">SKS</th>
                                            <th class="text-center">Program Studi</th>
                                            <th class="text-center">Kurikulum</th>
                                            <th class="text-center" width="20%">Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $i=1;
                                            foreach ($matkul as $key => $value):
                                        ?>
                                        <tr>
                                            <td align="center"></td>
                                            <td><?= $value->kode_matkul;?></td>
                                            <td><?= $value->nama_matkul;?></td>
                                            <td align="center"><?= $value->semester;?></td>
                                            <td align="center"><?= $value->sks;?></td>
                                                 <?php $prs = $this->user->get_data('*','tbl_programstudi',' where id_programstudi = '.$value->id_programstudi)[0];?>
                                            <td><?= $prs['nama_programstudi'];?></td>
                                            <td align="center"><?= $value->kurikulum;?></td>
                                            <td align="center">

                                                <button type="button" class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#edit_matkul<?= $value->id_matkul;?>"> 
                                                    <span class="fa fa-edit fa-lg"></span> Edit
                                                </button>
 
                                                <button type="button" class="btn btn-danger btn-sm" onclick="location.href='<?= base_url();?>akademik/matakuliah/hapus/<?= $value->id_matkul;?>';">
                                                    <span class="fa fa-trash-o fa-lg"></span> Delete
                                                </button>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
      </div>
    </div>
    <?php $i=1;
        foreach ($matkul as $key => $value):
    ?>
                                       

    <div class="modal fade" id="edit_matkul<?= $value->id_matkul;?>" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form action="<?= base_url();?>akademik/matakuliah/edit" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id_matkul" value="<?= $value->id_matkul?>">    
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <i class="fa fa-cloud-upload mr-1"></i>Ubah Matakuliah
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label>Kode Mata Kuliah</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->kode_matkul;?>" name="kode_matkul" required>
                            </div>

                            <label>Nama Mata Kuliah</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->nama_matkul;?>" name="nama_matkul" required>
                            </div>
                                                                            
                            <?php 
                                $fakultas = $this->user->get_data('*','tbl_fakultas');
                            ?>
                            <?php 
                                $fk = $this->user->get_data('*','tbl_fakultas',' where id_fakultas = '.$value->id_fakultas)[0];
                            ?>
                                    
                            <label>Fakultas</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <select required name="fakultas" class="form-control" onchange="selFakultas()">
                                    <option value="<?= $fk['id_fakultas'] ?>"><?= $fk['nama_fakultas'] ?> </option>                                                 
                                    <?php 
                                        foreach($fakultas as $key => $val):
                                    ?>  
                                    <option value="<?= $val['id_fakultas'];?>"><?= $val['nama_fakultas'];?></option>
                                    <?php endforeach   ?>   
                                </select>  
                            </div>
                                                                        
                            <?php 
                                $programstudy = $this->user->get_data('*','tbl_programstudi');
                            ?>
                            <?php 
                                $pgty = $this->user->get_data('*','tbl_programstudi',' where id_programstudi = '. $value->id_programstudi)[0];
                            ?>
                            <label>Program Studi</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                                <!-- id prsi = id_program_studi -->
                                <select required name="prodi" class="form-control" onchange="selProdi()">
                                    <option value="<?= $pgty['id_programstudi'] ?>"><?= $pgty['nama_programstudi'] ?> </option>
                                    <?php 
                                        foreach($programstudy as $key => $val): 
                                    ?>  
                                    <option value="<?= $val['id_programstudi'];?>"><?= $val['nama_programstudi'];?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>

                            <label>Semester</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->semester;?>" name="semester" required>
                            </div>

                            <label>SKS</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="number" class="form-control" value="<?= $value->sks;?>" name="sks" required>
                            </div>

                            <label>Kurikulum</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->kurikulum;?>" name="kurikulum" required>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </form>
        </div>
    </div> 
 <?php endforeach;?>

    <?php $this->load->view('template/modal');?>
  </body>
</html>