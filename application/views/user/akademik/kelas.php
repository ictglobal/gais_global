<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div style="text-align: left;">
                <button class="btn btn-primary">Tambah</button>
            </div>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Daftar Kelas</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="4sub%">No.</th>
                                            <th class="text-center">Nama Kelas</th>
                                            <th class="text-center" width="20%">Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $i=1;
                                            foreach ($kelas as $key => $value):
                                        ?>
                                        <tr>
                                            <td align="center"></td>
                                            <td><?= $value->id_kelas;?></td>
                                            <td align="center">
                                                <button type="button" class="btn btn-success btn-sm" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target=""> 
                                                    <span class="fa fa-edit fa-lg"></span> Edit
                                                </button>
                                                
                                                <button type="button" class="btn btn-danger btn-sm" onclick="location.href='';">
                                                    <span class="fa fa-trash-o fa-lg"></span> Delete
                                                </button>
                                            </td>
                                        </tr>
                                        <?php endforeach;?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </body>
</html>