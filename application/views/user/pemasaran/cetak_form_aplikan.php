    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>.: Cetak Form Aplikan :.</title>
    <style type="text/css">
    body,td,th {
        font-size: 18px;
        font-family: Arial, Helvetica, sans-serif;
    }
    </style>
    </head>

    <body>
        <table width="886" border="0" align="center">
            <tr>
                <td height="49"><img src="logo.png" width="198" height="155" /><br /></td>
                <td height="49" colspan="3">
                    <font face="Arial" size="12px">
                        <strong>FORMULIR APLIKAN</strong>
                    </font><br />
                    <font face="Arial">INSTITUT TEKNOLOGI &amp; BISNIS BINA SARANA GLOBAL<br />
                        Jl. Aria Santika No.43 A, RT.003/RW.003, Margasari, Kec. Karawaci, Kota Tangerang, Banten 15114
                    </font><br />
                </td>
            </tr>
            <?php foreach ($_cetak_aplikan as $key => $value):?>

            <font face="Arial">
                <!-- <tr>
                    <td width="295">&nbsp;</td>
                    <td colspan="3"><strong>Nomor : </strong></td>
                </tr> -->
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>

                <tr>
                    <td>Nama Lengkap</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">&nbsp;<?= $value['nama']; ?></td>
                </tr>

                <tr>
                    <td>Jenis Kelamin</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">
                        <p>
                            <label>
                                <?php if($value['jenis_kelamin']=="L"):?>
                                    <input type="radio" name="lk" value="L" checked /><?= "Laki-laki" ?>
                                    <input type="radio" name="pr" value="P" disabled  /><?= "Perempuan" ?>
                                <?php else: ?>
                                    <input type="radio" name="lk" value="L" disabled /><?= "Laki-laki" ?>
                                    <input type="radio" name="pr" value="P" checked  /><?= "Perempuan" ?>
                                <?php endif; ?>
                            </label><br />
                        </p>
                    </td>
                </tr>

                <tr>
                    <td>Tempat / Tgl Lahir</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">&nbsp;<?= $value['tempat_lahir'].", ".$value['tgl_lahir'] ?></td>
                </tr>

                <tr>
                    <td>Alamat Lengkap</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">&nbsp;<?=$value['alamat']; ?></td>
                </tr>

<!--                 <tr>
                    <td>&nbsp;</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">&nbsp;</td>
                </tr> -->

                <tr>
                    <td>No. Telp Rumah</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">&nbsp;<?=$value['telp']; ?></td>
                </tr>

                <tr>
                    <td>No. Handphone</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">&nbsp;<?=$value['telp']; ?></td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">
                        &nbsp;<?=$value['email']; ?> 
                        <?php if($value['email']==""): ?>
                            <?php echo ""; ?>
                        <?php endif; ?>
                    </td>
                </tr>
              
                <tr>
                    <td>Asal Sekolah</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">&nbsp;<?=$value['asal_sekolah']; ?></td>
                </tr>

                <tr>
                    <td>Nama Ibu Kandung</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">&nbsp;<?=$value['orang_tua']; ?></td>
                </tr>

                <tr>
                    <td>Bekerja di Perusahaan</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">&nbsp;<?=$value['asal_sekolah']; ?></td>
                </tr>

                <tr>
                    <td>Jabatan</td>
                    <td width="5"><div align="center">:</div></td>
                    <td colspan="2">&nbsp;<?=$value['jabatan']; ?></td>
                </tr>

                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="4"><div align="center"><strong>PILIHAN PROGRAM STUDI</strong></div></td>
                </tr>
            
                <tr>
                    <td colspan="4">
                        <div align="center">
                            <input type="radio" name="RadioGroup1" value="radio" id="RadioGroup1_0" />Sistem Informasi
                            <input type="radio" name="RadioGroup1" value="radio" id="RadioGroup1_1" />Teknik Informatika
                            <input type="radio" name="RadioGroup1" value="radio" id="RadioGroup1_2" />Bisnis Digital
                        </div>
                    </td>
                </tr>
            
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
            
                <tr>
                    <td colspan="4"><div align="center"><font face="Arial"><strong>PILIHAN KONSENTRASI</strong></font></div></td>
                </tr>

                <tr>
                    <td>
                        <p>
                            <label>
                            <input type="radio" name="RadioGroup2" value="radio" id="RadioGroup2_0" />Computerized Accounting
                            </label><br />
                            <label>
                                <input type="radio" name="RadioGroup2" value="radio" id="RadioGroup2_1" />Management Information System
                            </label><br />
                            <label>
                                <input type="radio" name="RadioGroup2" value="radio" id="RadioGroup2_2" />Logistic & Production Information &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System
                            </label><br />
                        </p>
                    </td>

                    <td colspan="2">
                        <input type="radio" name="RadioGroup2" value="radio" id="RadioGroup2_3" />Software Engineering<br />
                        <input type="radio" name="RadioGroup2" value="radio" id="RadioGroup2_4" />Computer Networking<br />
                        <input type="radio" name="RadioGroup2" value="radio" id="RadioGroup2_5" />Computerized Design &amp; &nbsp;&nbsp;&nbsp;&nbsp;Multimedia
                    </td>

                    <td width="318">
                        <input type="radio" name="RadioGroup2" value="radio" id="RadioGroup2_6" />Digital Business Management
                    </td>
                </tr>

                <tr>
                    <td colspan="4">
                        <div align="center">
                            <font face="Arial">
                                <strong>PILIHAN WAKTU</strong>
                            </font>
                        </div>
                    </td>
                </tr>

                <tr>
                    <td colspan="4"><p align="center">
                        <input type="radio" name="pagi" value="radio" id="RadioGroup3_0" />Pagi 
                        <input type="radio" name="RadioGroup3" value="radio" id="RadioGroup3_1" />Malam 
                        <input type="radio" name="RadioGroup3" value="radio" id="RadioGroup3_2" />KJS
                        <input type="radio" name="RadioGroup3" value="radio" id="RadioGroup3_3" />KS
                        <input type="radio" name="RadioGroup3" value="radio" id="RadioGroup3_4" />Daring
                        <input type="radio" name="RadioGroup3" value="radio" id="RadioGroup3_5" />Shift
                    </td>
                </tr>
              <tr>
                <td>
                    <p>&nbsp;</p>      
                    <label>Sumber Informasi<br /></label>
                    <input type="radio" name="RadioGroup4" value="radio" id="RadioGroup4_0" />Internet / Medsos<br />
                    <input type="radio" name="RadioGroup4" value="radio" id="RadioGroup4_1" />MGM/Agent/DM/HV<br />
                    <input type="radio" name="RadioGroup4" value="radio" id="RadioGroup4_2" />Gedung/Spanduk/Poster<br />
                    <input type="radio" name="RadioGroup4" value="radio" id="RadioGroup4_3" />Lain - Lain<br />
                    ..................................................
                </td>
                <td>
                    <p align="center">&nbsp;</p>
                </td>
                <td>
                    <p align="center">
                        Tangerang, 27 Agustus 2022<br />
                        Tahun Akademik 2022 - 2023<br />
                        Presenter
                    </p>
                    <p align="center">&nbsp;</p>
                    <p align="center">
                        Prof. Dr. Bambang Tri Novianto, M. Kom. PhD
                    </p>
                </td>
              </tr>
            </font>

            <?php endforeach; ?>
        </table>
    </body>
    </html>
