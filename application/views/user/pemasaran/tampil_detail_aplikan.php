<!DOCTYPE html>
<html lang="en">
<body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
          <div class="right_col" role="main">
          <div class="clearfix"></div>
              <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Detail Aplikan</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li class="dropdown" style="visibility: hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <!-- <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li> -->
                      <!-- <li><a class="close-link"><i class="fa fa-close"></i></a> -->
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">
                    <!-- <p>Laman ini berisi biodata pribadi <?= $this->session->userdata('role');?>. Silahkan klik Tombol <code>Edit Data</code> Jika ada perbaikan data</p> -->

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="bulk-actions" colspan="7">
                              <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                            </th>
                          </tr>
                        </thead>

                        <tbody>
                        <?php 
                            foreach ($tampil_aplikan as $key => $value) {
                        ?> 
                            <tr class="even pointer">
                                <td>NISN/NIM</td>
                                <td>:</td>
                                <td><?= $value->nisn;?>
                                    <i class="success fa fa-long-arrow-up"></i>
                                </td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Nama Lengkap</td>
                                <td>:</td>
                                <td><?= $value->nama;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td>
                                    <?php 
                                        if($value->jenis_kelamin == "L"):
                                            echo "Laki -laki";
                                        else:
                                            echo "Perempuan";
                                        endif;
                                    ?>
                                </td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Tempat dan Tanggal Lahir</td>
                                <td>:</td>
                                <td><?= $value->tempat_lahir.", ".$value->tgl_lahir;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?= $value->alamat;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Pendidikan Terakhir</td>
                                <td>:</td>
                                <td><?= $value->asal_sekolah;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Nama Orang Tua</td>
                                <td>:</td>
                                <td><?= $value->orang_tua;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Tanggal Datang</td>
                                <td>:</td>
                                <td><?= $value->tgl_datang;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Email</td>
                                <td>:</td>
                                <td><?= $value->email;?></td>
                            </tr>

                            <tr class="odd pointer">
                                <td>Tahun Lulus</td>
                                <td>:</td>
                                <td><?= $value->tgl_datang;?></td>
                            </tr>
                            
                            <tr class="odd pointer">
                                <td>Presenter</td>
                                <td>:</td>
                                <td><?= $value->presenter;?></td>
                            </tr>

                          <?php } ?>
                        </tbody>
                      </table>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
      </div>
    </div>
  </body>
</html>

<script type="text/javascript">
    function cetak_form_aplikan(){
        window.open('<?= base_url()?>pemasaran/aplikan/cetak_form_aplikan/', '_blank')
    }
</script>


