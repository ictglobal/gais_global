<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
          <button class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#export_aplikan">Export Aplikan</button>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Data Aplikan</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form id='formselect'>
                                <div class="col-sm-3">
                                        <label for="">Filter Tahun Akademik</label>
                                        <select class="form-control" id="theSelect2" onchange="selection()" required>
                                            <option value="">Pilih Tahun Akademik</option>
                                            <?php 
                                                $tg_awal= date('Y')-7;
                                                $tgl_akhir= date('Y')+1;
                                                for ($i=$tgl_akhir; $i>=$tg_awal; $i--){
                                                    $tgl_akhir = $i + 1;
                                                    $th = $i." - ".$tgl_akhir;
                                                    // $kode = $this->encryption->encrypt($th);
                                                    $kode = $this->_security->_encrypt($th);
                                                    // $kode = $this->user->decrypt($kode);

                                            ?>
                                                <option value="<?= $kode?>"><?= $i." - ".$tgl_akhir; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    </form>
                                    <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>         
                                            <th class="text-center">No</th>
                                            <th class="text-center" width="10%">NISN</th>
                                            <th class="text-center" >Nama Calon Mahasiswa</th>
                                            <th class="text-center" width="10%">Jenis Kelamin</th>
                                            <th class="text-center">Asal Sekolah</th>
                                            <!-- <th class="text-center">Program Studi dan Konsentrasi</th> -->
                                            <th class="text-center">No Ponsel</th>
                                            <th class="text-center">Presenter</th>
                                            <th class="text-center">Aksi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url().'temp/jquery-3.6.0.js'?>"></script>
    <script type="text/javascript">
        function selection() {
            var id_ta = $('#theSelect2').val();

            window.location.href = '<?= base_url()?>pemasaran/aplikan/select_aplikan?ta='+id_ta;
        }
    </script>
  </body>
</html>