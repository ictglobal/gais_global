<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Detail Aplikan</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                 <form id='formselect'>
                                <div class="col-sm-3">
                                        <label for="">Filter Tahun Akademik</label>
                                        <select class="form-control" id="theSelect2" onchange="selection()" required>
                                            <option value="">Pilih Tahun Akademik</option>
                                            <?php 
                                                $tg_awal= date('Y')-7;
                                                $tgl_akhir= date('Y')+1;
                                                for ($i=$tgl_akhir; $i>=$tg_awal; $i--){
                                                    $tgl_akhir = $i + 1;
                                                    $th = $i." - ".$tgl_akhir;
                                                    $kode = $this->encryption->encrypt($th);
                                            ?>
                                                <option value="<?= $kode?>"><?= $i." - ".$tgl_akhir; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    </form>
                                    <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>         
                                            <th class="text-center">NISN</th>
                                            <th class="text-center">Nama Calon Mahasiswa</th>
                                            <th class="text-center">Jenis Kelamin</th>
                                            <th class="text-center">Tempat dan Tanggal Lahir</th>
                                            <th class="text-center">Asal Sekolah</th>
                                            <th class="text-center">Kelas</th>
                                            <th class="text-center" width="100%">Alamat</th>
                                            <th class="text-center">Nama Orang Tua</th>
                                            <th class="text-center">Program Kuliah</th>
                                            <th class="text-center">No Ponsel</th>
                                            <th class="text-center">Tahun Lulus</th>
                                            <th class="text-center">Sumber Informasi</th>
                                            <th class="text-center">Presenter</th>
                                            <th class="text-center">Tanggal Datang</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php 
                                                foreach ($tampil_aplikan as $key => $value):
                                            ?>
                                            <td><?= $value->nisn; ?></td>
                                            <td><?= $value->nama; ?></td>
                                            <td><?= $value->jenis_kelamin; ?></td>
                                            <td><?= $value->tempat_lahir.", ".$value->tgl_lahir;?></td>
                                            <td><?= $value->asal_sekolah; ?></td>
                                            <td><?= $value->id_kelas; ?></td>
                                            <td><?= $value->alamat; ?></td>
                                            <td><?= $value->orang_tua; ?></td>
                                            <td><?= $value->id_kuliah; ?></td>
                                            <td><?= $value->telp; ?></td>
                                            <td><?= $value->thn_lulus; ?></td>
                                            <td><?= $value->sumber_informasi; ?></td>
                                            <td><?= $value->presenter; ?></td>
                                            <td><?= date($value->tgl_datang); ?></td>
                                            <?php endforeach; ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url().'temp/jquery-3.6.0.js'?>"></script>
    <script type="text/javascript">
        function selection() {
            var id_ta = $('#theSelect').val();
            var encoded_url = encodeURIComponent(id_ta);
            
            window.location.href = '<?= base_url()?>pemasaran/aplikan/select_aplikan?ta='+encoded_url;
        }
    </script>
  </body>
</html>