<!DOCTYPE html>
  <html lang="en">
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <button class="btn btn-success" onclick="export_aplikan2('<?= $kode; ?>')" >Export Aplikan</button>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Data Aplikan <?= $kode; ?></h2>
                    <!-- <input type="text" name="ayam" value="<?= $kode; ?>" id="kode_tahun_akademik"> -->
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form method="post">
                                    <!-- <input type="text" name="tahun_akad" value="<?= $this->encryption->decrypt($tahun_akademik);?>" > -->
                                    <div class="col-sm-3">
                                            <label for="">Filter Tahun Akademik</label>
                                            <select class="form-control" id="theSelect2" onchange="selection()" required>
                                                <option value="">Pilih Tahun Akademik</option>
                                                <?php 
                                                    $tg_awal= date('Y')-7;
                                                    $tgl_akhir= date('Y')+1;
                                                    for ($i=$tgl_akhir; $i>=$tg_awal; $i--){
                                                        $tgl_akhir = $i + 1;
                                                        $th = $i." - ".$tgl_akhir;
                                                        // $kode = $this->encryption->encrypt($th);
                                                        $kode = $this->_security->_encrypt($th);
                                                ?>
                                                    <option value="<?= $kode?>"><?= $i." - ".$tgl_akhir; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </form>
                                    <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>        
                                            <th class="text-center">No</th>
                                            <th class="text-center">NISN</th>
                                            <th class="text-center">Nama Calon Mahasiswa</th>
                                            <th class="text-center">Jenis Kelamin</th>
                                            <th class="text-center" width="10%">Asal Sekolah</th>
                                            <!-- <th class="text-center">Program Studi dan Konsentrasi</th> -->
                                            <!-- <th class="text-center"width="10%">No Ponsel</th> -->
                                            <th class="text-center">Presenter</th>
                                            <th class="text-center" width="10%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $no = 1;
                                            foreach ($_aplikan as $key => $value):
                                        ?>
                                            <tr>
                                                <td></td>
                                                <td><?= $value->nisn; ?></td>
                                                <td><?= $value->nama; ?></td>
                                                <td>
                                                    <?php 
                                                        if($value->jenis_kelamin == "L"):
                                                            echo "Laki-laki";
                                                        else:
                                                            echo "Perempuan";
                                                        endif; 
                                                    ?>
                                                </td>
                                                <td><?= $value->asal_sekolah; ?></td>
                                                <!-- <td><?= $value->telp; ?></td> -->
                                                <td><?= $value->presenter; ?></td>
                                                <td align="center">
                                        <?php 
                                            $k = $this->encryption->encrypt($value->id_aplikan); 
                                        ?>

                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <span class="fa fa-edit fa-sm"></span>
                                              Action
                                            </button>
                                            <div class="dropdown-menu">
                                              <a class="dropdown-item" onclick="_click_('<?php echo $k; ?>', 'tampil', 'tampil_detail_aplikan')">Detail</a>
                                              <a class="dropdown-item" data-toggle="modal" onclick="cetak_form_aplikan(<?= $value->id_aplikan ?>);" >Cetak</a>
                                              <a class="dropdown-item" href="#ubah_aplikan<?= $value->id_aplikan ?>" data-toggle="modal" data-target="#ubah_aplikan<?= $value->id_aplikan ?>">Edit</a>
                                               <a class="dropdown-item" href="<?= base_url()?>pemasaran/aplikan/hapus_aplikan/<?= $this->encryption->decrypt($k)?>?ta=<?= $x?>">Hapus</a> 
                                              
                                            </div>
                                        </div>
                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
          <?php //$this->load->view('template/modal'); ?>
    </div>
    <script type="text/javascript">
        function selection() {
            var id_ta   = $('#theSelect2').val();
            // var url     = '<?= base_url()?>pemasaran/aplikan/select_aplikan?ta='+id_ta;
            // var encoded_url = encodeURIComponent(id_ta);
            
            window.location.href = '<?= base_url()?>pemasaran/aplikan/select_aplikan?ta='+id_ta;
            // window.open(url, '_blank').focus();
        }

        function _click_(kode, kd, controller){
            var url = encodeURIComponent(kode);
            window.open('<?= base_url()?>pemasaran/aplikan/'+controller+'?'+kd+'='+url, '_blank').focus();
            // window.location.href = '<?= base_url()?>pemasaran/aplikan/'+controller+'?'+kd+'='+url;
        }

        function cetak_form_aplikan(id){
            window.open('<?= base_url()?>pemasaran/aplikan/cetak_form_aplikan/'+id, '_blank')
        }

        function export_aplikan2(th){
            window.location.href = '<?= base_url()?>pemasaran/aplikan/export_aplikan2/'+th;
        }
    </script>


    <?php 
        foreach ($_aplikan as $key => $value) {
            $matkul = $this->user->get_data('*', 'tbl_aplikan', "where id_aplikan = '$value->id_aplikan'")[0];
    ?>
    <!-- Modal Form untuk Edit Aplikan -->
    <div class="modal fade" id="ubah_aplikan<?= $value->id_aplikan?>" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <form action="#" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">
                            <i class="fa fa-edit mr-1"></i>Ubah Data Aplikan
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">        
                        <div class="form-group">
                            <label>ID Aplikan</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control" name="id" value="<?= $value->id_aplikan?>" required readonly>
                            </div>

                            <label>Nama Lengkap</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->nama?>" name="nama" required>
                            </div>

                            <label>NISN</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->nisn?>" name="nisn" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" required>
                            </div>

                            <label>Jenis Kelamin</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <select class="form-control" name="jk">
                                    <!-- <option value="">Pilih Jenis Kelamin</option> -->
                                    <?php  
                                        if($value->jenis_kelamin=="L"):
                                    ?>
                                    <option value="<?= "L"; ?>"><?= "Laki-laki"; ?></option>
                                    <?php else: ?>
                                    <option value="<?= "P"; ?>"><?= "Perempuan"; ?></option>
                                    <?php endif; ?>
                                    <option value="L"><?= "Laki-laki"; ?></option>
                                    <option value="P"><?= "Perempuan"; ?></option>
                                </select>
                            </div>

                            <label>Tempat Lahir</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->tempat_lahir?>" name="tempat_lahir" required>
                            </div>

                            <label>Tanggal Lahir</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-date"></i>
                                </span>
                                <input class="form-control" value="<?= $value->tgl_lahir?>" class='date' type="date" name="tgl_lahir" required>
                            </div>

                            <label>Alamat</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <textarea class="form-control" name="alamat" required><?= $value->alamat?></textarea>
                            </div>

                            <label>Tempat Bekerja</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->nama_perus?>" name="tmp_kerja" required>
                            </div>

                            <label>Jabatan</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->jabatan?>" name="jabatan" required>
                            </div>

                            <label>Handphone</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->telp?>" maxlength="13" onkeypress="return event.charCode >= 48 && event.charCode <= 57" name="hp" required>
                            </div>

                            <label>E-Mail</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="email" class="form-control" value="<?= $value->email?>" name="email" required>
                            </div>

                            <label>Asal Sekolah</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-building"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->asal_sekolah?>" name="asal_sekolah" required>
                            </div>           

                            <label>Tahun Lulus</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->thn_lulus?>" name="tahun_lulus" required>
                            </div>

                            <label>Nama Ibu</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->orang_tua?>" name="nama_ibu" required>
                            </div>

                            <label>Program Studi</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <?php  
                                    $tmpl_ksn = $this->user->get_data("*", "tbl_konsentrasi", "WHERE id_konsentrasi = '$value->id_konsentrasi'")[0];

                                    $tmpl_prd = $this->user->get_data("*", "tbl_programstudi", "WHERE id_programstudi = '$tmpl_ksn[id_programstudi]'");
                                ?>

                                <select class="form-control" name="prodi">
                                    <?php foreach ($tmpl_prd as $key => $value2) { ?>
                                        <option value="<?= $value2['nama_programstudi'] ?>"><?= $value2['nama_programstudi'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <label>Konsentrasi</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <?php  $tmpl_ksn = $this->user->get_data("*", "tbl_konsentrasi", "WHERE id_konsentrasi = '$value->id_konsentrasi'")[0]; ?>
                                <select class="form-control" name="konsentrasi">
                                    <?php foreach ($tmpl_ksn as $key => $value3) { ?>
                                        <option value="<?= $value3['nama_konsentrasi'] ?>"><?= $value3['nama_konsentrasi'] ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <label>Sesi Kuliah</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <!-- <input type="text" class="form-control" value="<?= $value->id_kuliah?>" name="sesi" required> -->
                                <select class="form-control" id="" name="sesi">
                                    <option value="Reguler">Reguler</option>
                                    <option value="Non Reguler">Non Reguler</option>
                                </select>
                            </div>

                            <label>Kelas</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <input type="text" class="form-control" value="<?= $value->id_kelas?>" name="kelas" required>
                            </div>

                            <label>Sumber Informasi</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <select class="form-control"  name="sumber">
                                    <option value="<?php echo "$value->sumber_informasi";?>"><?php echo $value->sumber_informasi ?></option>
                                    <option value="MGM">Member Get Member (MGM)</option>
                                    <option value="Rekan">Rekan</option>
                                    <option value="Gedung">Gedung</option>
                                    <option value="Brosur">Brosur</option>
                                    <option value="Spanduk">Spanduk</option>
                                    <option value="Presentasi">Presentasi</option>
                                    <option value="Website">Website</option>
                                    <option value="Instagram">Instagram</option>
                                    <option value="Facebook">Facebook</option>
                                    <option value="Twitter">Twitter</option>
                                </select>
                            </div>

                            <!-- <label>Tanggal Datang</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-date"></i>
                                </span>
                                <input class="form-control" value="<?= $value->tgl_datang?>" class='date' type="date" name="tgl_datang" required>
                            </div> -->

                            <label>Keterangan</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <textarea class="form-control" name="ket" required><?= $value->ket?></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    </div>
                </div>
            </form>
        </div>
    </div>  
<?php } ?>

<script type="text/javascript">
    function hapusaplikasi(id,th) {
        // alert(th);
        // var encoded_url = encodeURIComponent(th);
            
        window.location.href = '<?= base_url()?>pemasaran/aplikan/hapus_aplikan/'+id+'?ta='+th;
    }
</script>
  </body>
</html>