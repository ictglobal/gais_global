<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <?php $this->load->view('template/script/login_script/css_login');?>
  <title>
     .: Login GAIS V.4 :.
  </title>
</head>
<body class="bg-gray-200" oncontextmenu="return true">
  <div class="container position-sticky z-index-sticky top-0">
    <div class="row">
      <div class="col-12">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg blur border-radius-xl top-0 z-index-3 shadow position-absolute my-3 py-2 start-0 end-0 mx-4">
          <div class="container-fluid ps-2 pe-0">
            <a class="navbar-brand font-weight-bolder ms-lg-0 ms-3 " href="#">
              Selamat Datang Pengguna GAIS V.4
            </a>
            <button class="navbar-toggler shadow-none ms-2" type="button" data-bs-toggle="collapse" data-bs-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon mt-2">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </span>
            </button>
            <div class="collapse navbar-collapse" id="navigation">
              <ul class="navbar-nav mx-auto">
                <li class="nav-item">
                  <a class="nav-link d-flex align-items-center me-2 active" aria-current="page" href="https://global.ac.id">
                    <i class="fa fa-chart-pie opacity-6 text-dark me-1"></i>
                   Buku Manual GAIS
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link me-2" href="https://global.ac.id">
                    <i class="fa fa-user opacity-6 text-dark me-1"></i>
                    Website Utama
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link me-2" href="https://lms.global.ac.id">
                    <i class="fas fa-user-circle opacity-6 text-dark me-1"></i>
                    E-Learning
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link me-2" href="https://global.ac.id">
                    <i class="fas fa-key opacity-6 text-dark me-1"></i>
                    Kelender Akademik
                  </a>
                </li>
              </ul>
              <ul class="navbar-nav d-lg-block d-none">
                <li class="nav-item">
                  <a href="#" class="btn btn-sm mb-0 me-1 bg-gradient-dark">Agenda Kuliah</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <!-- End Navbar -->
      </div>
    </div>
  </div>
  <main class="main-content  mt-0">
    <div class="page-header align-items-start min-vh-100" style="background-image: url('https://images.unsplash.com/photo-1497294815431-9365093b7331?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1950&q=80');">
    <!-- <div class="page-header align-items-start min-vh-100" style="background-image: url('');"> -->
      <span class="mask bg-gradient-dark opacity-6"></span>
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-4 col-md-8 col-12 mx-auto">
            <div class="card z-index-0 fadeIn3 fadeInBottom">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg py-3 pe-1">
                  <h4 class="text-white font-weight-bolder text-center mt-2 mb-0">GLOBAL <br>Academic Information System</h4>
                  <div class="row mt-3">
                    <div class="col-2 text-center ms-auto">
                      <a class="btn btn-link px-3" href="https://facebook.com/binasaranaglobal">
                        <i class="fa fa-facebook text-white text-lg"></i>
                      </a>
                    </div>
                    <div class="col-2 text-center px-1">
                      <a class="btn btn-link px-3" href="https://instagram.com/binasaranaglobal">
                        <i class="fa fa-instagram text-white text-lg"></i>
                      </a>
                    </div>
                    <div class="col-2 text-center me-auto">
                      <a class="btn btn-link px-3" href="https://twitter.com/kuliah_global">
                        <i class="fa fa-twitter text-white text-lg"></i>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="card-body">
                 <!-- <form action="<?php echo site_url("login/ceklogin2")?>" method="POST"> -->
                  <form action="<?php echo site_url("login/validasi")?>" method="POST">
                  <div class="input-group input-group-outline my-3">
                    <label class="form-label">NIM/Username/NIDN/NIK</label>
                    <input type="text" class="form-control validate" name="username">
                  </div>
                  <div class="input-group input-group-outline mb-3">
                    <label class="form-label">Password</label>
                    <input type="password" class="form-control validate" name="password">
                  </div>
                  <!-- <div class="form-check form-switch d-flex align-items-center mb-3">
                    <input class="form-check-input" type="checkbox" id="rememberMe">
                    <label class="form-check-label mb-0 ms-2" for="rememberMe">Ingatkan Saya</label>
                  </div> -->
                  <div class="text-center">
                    <button type="submit" class="btn bg-gradient-primary w-100 my-4 mb-2">Sign in</button>
                  </div>
                  <!--<p class="mt-4 text-sm text-center">
                    Don't have an account?
                    <a href="pages/sign-up.html" class="text-primary text-gradient font-weight-bold">Sign up</a>
                  </p>-->
               </form>
              </div>
            
            </div>
          </div>
        </div>
      </div>
      <!-- ini footernya -->
      <?php $this->load->view('template/script/login_script/js_login');?>
    </div>
  </main>
</body>
</html>



