<!DOCTYPE html>
  <html lang="en">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      <!-- page content -->
        <div class="right_col" role="main">
            <?php if($this->session->userdata('level')==6): ?>
                <div class="btn-group btn-group-md" role="group" aria-label="...">
                    <button class="btn btn-secondary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#tambah_jadwal">Tambah</button>
                    <button class="btn btn-secondary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#export_jadwal">Export</button>
                    <button class="btn btn-secondary" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#import_jadwal">Import</button>
                </div>
            <!-- <button class="btn btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#import_jadwal">Import Jadwal</button> -->
            <?php endif; ?>
            <br>
            <div class="x_panel">
                <div class="x_title">
                    <h2>Jadwal Akademik</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown" style="visibility:hidden;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                        </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card-box table-responsive">
                                <form id='formselect'>
                                    <div class="col-sm-3">
                                        <label for="">Filter</label>
                                        <select class="form-control" id="tahun_akademik2" onchange="selection()" required>
                                            <option value="">Pilih Tahun Akademik</option>
                                            <?php
                                                if($this->session->userdata('level')==2): 
                                                    foreach ($tahun_akademik as $key => $value):
                                                        $tetew = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$value[id_tahunakademik]'")[0];
                                            ?>
                                            <option value="<?= $this->encryption->encrypt($value['id_tahunakademik']);?>">
                                                <?= $tetew['tahun_akademik'];?>
                                            </option>
                                            <?php 
                                                    endforeach; 
                                                elseif($this->session->userdata('level')==6):
                                                    foreach ($tahun_akademik as $key => $value):
                                                        $kode = $this->encryption->encrypt($value['id_tahunakademik']);
                                                        // $kode = $this->_security->_encrypt($value['id_tahunakademik']);
                                            ?>
                                            <option value="<?= $kode?>">
                                                    <?php 
                                                        $ty = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$value[id_tahunakademik]'");
                                                        foreach($ty as $key2 => $value2):
                                                            echo $value2['tahun_akademik'];
                                                        endforeach;
                                                    ?>
                                            </option>
                                            <?php 
                                                    endforeach;
                                                endif; 
                                            ?>
                                        </select>
                                    </div>
                                </form>
                                <br>
                                <table id="datatable" class="table table-striped table-bordered">
                                    <thead>
                                        <?php if($this->session->userdata('level')==2): ?>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th>Kode</th>
                                            <th>Kelas</th>
                                            <th>Nama Matakuliah</th>
                                            <th>Ruang</th>
                                            <th>Mulai || Selesai</th>
                                        </tr>
                                        <?php 
                                            elseif($this->session->userdata('level')==6):
                                        ?>
                                        <tr>
                                            <th class="text-center">No</th>
                                            <th>Kode</th>
                                            <th>Kelas</th>
                                            <th>Nama Matakuliah</th>
                                            <th>Ruang</th>
                                            <th width="10%">Nama Dosen</th>
                                            <th>Mulai || Selesai</th>
                                            <th class="text-center" width="20%">Opsi</th>
                                        </tr>
                                        <?php endif; ?>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
    <script src="<?= base_url().'temp/jquery-3.6.0.js'?>"></script>
    <script type="text/javascript">
        function selection() {
            var id = $('#tahun_akademik2').val();
            var encoded_url = encodeURIComponent(id);
            window.location.href = '<?= base_url()?>perkuliahan/jadwal/tampil_jadwal?th='+encoded_url;
        }
    </script>
  </body>
</html>

<style type="text/css">

hr.style1{
    border-top: 1px solid #8c8b8b;
}


hr.style2 {
    border-top: 3px double #8c8b8b;
}

hr.style3 {
    border-top: 1px dashed #8c8b8b;
}

hr.style4 {
    border-top: 1px dotted #8c8b8b;
}

hr.style5 {
    background-color: #fff;
    border-top: 2px dashed #8c8b8b;
}


hr.style6 {
    background-color: #fff;
    border-top: 2px dotted #8c8b8b;
}

hr.style7 {
    border-top: 1px solid #8c8b8b;
    border-bottom: 1px solid #fff;
}


hr.style8 {
    border-top: 1px solid #8c8b8b;
    border-bottom: 1px solid #fff;
}
hr.style8:after {
    content: '';
    display: block;
    margin-top: 2px;
    border-top: 1px solid #8c8b8b;
    border-bottom: 1px solid #fff;
}

hr.style9 {
    border-top: 1px dashed #8c8b8b;
    border-bottom: 1px dashed #fff;
}

hr.style10 {
    border-top: 1px dotted #8c8b8b;
    border-bottom: 1px dotted #fff;
}


hr.style11 {
    height: 6px;
    background: url(http://ibrahimjabbari.com/images/hr-11.png) repeat-x 0 0;
    border: 0;
}


hr.style12 {
    height: 6px;
    background: url(http://ibrahimjabbari.com/images/hr-12.png) repeat-x 0 0;
    border: 0;
}

hr.style13 {
    height: 10px;
    border: 0;
    box-shadow: 0 10px 10px -10px #8c8b8b inset;
}


hr.style14 { 
  border: 0; 
  height: 1px; 
  background-image: -webkit-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
  background-image: -moz-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
  background-image: -ms-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0);
  background-image: -o-linear-gradient(left, #f0f0f0, #8c8b8b, #f0f0f0); 
}


hr.style15 {
    border-top: 4px double #8c8b8b;
    text-align: center;
}
hr.style15:after {
    content: '\002665';
    display: inline-block;
    position: relative;
    top: -15px;
    padding: 0 10px;
    background: #f0f0f0;
    color: #8c8b8b;
    font-size: 18px;
}

hr.style16 { 
  border-top: 1px dashed #8c8b8b; 
} 
hr.style16:after { 
  content: '\002702'; 
  display: inline-block; 
  position: relative; 
  top: -12px; 
  left: 40px; 
  padding: 0 3px; 
  background: #f0f0f0; 
  color: #8c8b8b; 
  font-size: 18px; 
}


hr.style17 {
    border-top: 1px solid #8c8b8b;
    text-align: center;
}
hr.style17:after {
    content: '§';
    display: inline-block;
    position: relative;
    top: -14px;
    padding: 0 10px;
    background: #f0f0f0;
    color: #8c8b8b;
    font-size: 18px;
    -webkit-transform: rotate(60deg);
    -moz-transform: rotate(60deg);
    transform: rotate(60deg);
}


hr.style18 { 
  height: 30px; 
  border-style: solid; 
  border-color: #8c8b8b; 
  border-width: 1px 0 0 0; 
  border-radius: 20px; 
} 
hr.style18:before { 
  display: block; 
  content: ""; 
  height: 30px; 
  margin-top: -31px; 
  border-style: solid; 
  border-color: #8c8b8b; 
  border-width: 0 0 1px 0; 
  border-radius: 20px; 
}

</style>
<!-- Modal Form untuk Tambah Jadwal -->
<form action="<?= base_url()?>perkuliahan/jadwal/tambah_jadwal" method="post">
    <div class="modal fade" id="tambah_jadwal" role="dialog">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">
                        <i class="fa fa-book mr-1"></i>Tambah Jadwal
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span>&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Tahun Akademik</label>
                        <div class="input-group">
                            <select class="form-control" id="theSelect" style="width: 100%;" name="tahun_akademik" required>
                                <option value="">Pilih Tahun Akademik</option>
                                <?php  
                                    foreach ($tahun_akademik as $key => $value):
                                        $tetew = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$value[id_tahunakademik]'")[0];
                                ?>
                                    <option value="<?= $this->encryption->encrypt($value['id_tahunakademik']);?>">
                                        <?= $tetew['tahun_akademik'];?>
                                    </option>
                                <?php 
                                    endforeach;
                                ?>
                            </select>
                        </div>

                        <label>Matakuliah</label>
                        <div class="input-group">
                            <select class="form-control" id="pilih_matkul_jadwal" style="width: 100%;" name="pmatkul" required>
                                <option value="">Pilih Matakuliah</option>
                                <?php foreach($matkul as $key2 => $value2):?>
                                <option value="<?= $value2['kode_matkul'] ?>">
                                    <?= $value2['kode_matkul']." || ".$value2['nama_matkul']." || ".$value2['sks']." || ".$value2['kurikulum'] ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <label>Dosen</label>
                        <div class="input-group">
                            <select class="form-control" id="dosen" style="width: 100%;" name="dosen" required>
                                <option value="">Pilih Dosen</option>
                                <?php foreach($dosen as $key3 => $value3):?>
                                <option value="<?= $value3['username_dosen'] ?>">
                                    <?= $value3['username_dosen']." || ".$value3['dosen'] ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <label>Kelas</label>
                        <div class="input-group">
                            <select class="form-control" id="kelas" style="width: 100%;" name="kelas" required>
                                <option>Pilih Kelas</option>
                                <?php foreach($kelas as $key4 => $value4):?>
                                <option value="<?= $value4['id_kelas'] ?>">
                                    <?= $value4['id_kelas'] ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div class="input-group">
                            <label>Ruang</label>
                            <select class="form-control" id="ruang" style="width: 100%;" name="ruang" required>
                                <option value="">Pilih Ruang</option>
                                <?php foreach($ruang as $key5 => $value5):?>
                                <option value="<?= $value5['ruang'] ?>">
                                    <?= $value5['ruang'] ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <hr class="style5">
                            <h4 align="center">ATUR JADWAL MATKUL</h4> 
                        <hr class="style5">
                        <div class="input-group">                        
                            <div class="input-group">
                                <div class="col-md-6 col-sm-6">
                                    <label>Tanggal Mulai</label>
                                    <input id="tgl_mulai" name="tgl_mulai" class="date-picker form-control" placeholder="dd-mm-yyyy" type="text" required="required" type="text" onfocus="this.type='date'">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label>Tanggal Selesai</label>
                                    <input id="tgl_selesai" name="tgl_selesai" class="date-picker form-control" placeholder="dd-mm-yyyy" type="text" required="required" type="text" onfocus="this.type='date'">
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="col-md-6 col-sm-6">
                                    <label>Mulai Jam</label>
                                    <input class="form-control" required="required" class='time' type="time" name="tgl_jam_mulai">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label>Sampai Jam</label>
                                    <input class="form-control" required="required" class='time' type="time" name="tgl_jam_selesai">
                                </div>
                            </div>
                        </div>
                        <hr class="style5">
                            <h4 align="center">ATUR JADWAL UJIAN</h4> 
                        <hr class="style5">
                        <div class="input-group"> 
                            <div class="input-group">
                                <div class="col-md-6 col-sm-6">
                                    <label>Tanggal UTS</label>
                                    <input id="tgl_uts" name="tgl_uts" class="date-picker form-control" placeholder="dd-mm-yyyy" type="text" type="text" onfocus="this.type='date'">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label>Tanggal UAS</label>
                                    <input id="tgl_uas" name="tgl_uas" class="date-picker form-control" placeholder="dd-mm-yyyy" type="text" type="text" onfocus="this.type='date'">
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="col-md-6 col-sm-6">
                                    <label>Jam UTS Awal</label>
                                    <input class="form-control" class='time' type="time" name="tgl_jam_uts_awal">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label>Jam UTS Akhir</label>
                                    <input class="form-control" class='time' type="time" name="tgl_jam_uts_akhir">
                                </div>
                            </div>

                            <div class="input-group">
                                <div class="col-md-6 col-sm-6">
                                    <label>Jam UAS Awal</label>
                                    <input class="form-control" class='time' type="time" name="tgl_jam_uas_awal">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <label>Jam UAS Akhir</label>
                                    <input class="form-control" class='time' type="time" name="tgl_jam_uas_akhir">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Tambah</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div> 
</form>
<!-- End Section -->

