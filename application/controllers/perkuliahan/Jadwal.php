<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('akses') != TRUE) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->library('functions');
		$this->load->library('tools/_security');
	}

	public function index(){
		if($this->session->userdata('level')==2):
			$id = $this->session->userdata('username');
			$tes = $this->user->get_data("*", "tbl_jadwal", "WHERE username_dosen = '$id' GROUP BY id_tahunakademik");

			$data = array(
				'tahun_akademik' => $tes,
			);
			return $this->render_page('jadwal', $data);

		elseif($this->session->userdata('level')==6):
			$data = array(
				'tahun_akademik' 	=> $this->user->get_data("id_tahunakademik", "tbl_jadwal", "GROUP BY id_tahunakademik"),
				'matkul'			=> $this->user->get_data("*", "tbl_matakuliah", "ORDER BY nama_matkul ASC"),
				'dosen'				=> $this->user->get_data("*", "tbl_dosen", "ORDER BY dosen ASC"),
				'kelas'				=> $this->user->get_data("*", "tbl_kelas", "ORDER BY id_Kelas ASC"),
				'ruang' 			=> $this->user->get_data("*", "tbl_ruang", "ORDER BY id_ruang")
			);
			return $this->render_page('jadwal', $data);
		endif;
	}

	public function tampil_jadwal(){
		$th 		= $this->input->get('th');
		$th 		= str_replace(" ","+",$th);
		$kode 		= $this->encryption->decrypt($th);

		$jadwal1 	= $this->user->get_data("*", "tbl_jadwal", "WHERE id_tahunakademik = '$kode'");
		$tete 		= $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik='$kode'");
		foreach ($tete as $key => $ayam) {}
		
		if($this->session->userdata('level')==2):
			$id_dosen = $this->session->userdata('username');
			$tes = $this->user->get_data("*", "tbl_jadwal", "WHERE username_dosen = '$id_dosen' GROUP BY id_tahunakademik");

			$data = array(
						'jadwal_thn' => $jadwal1,
						'tahun_akademik10' => $this->user->query_all("SELECT * FROM tbl_tahunakademik ORDER BY tahun_akademik ASC")->result(),
						'jadwal_dosen' 	=> $this->user->get_data("*", "tbl_jadwal", "WHERE username_dosen = '$id_dosen' AND id_tahunakademik = '$ayam[id_tahunakademik]' ORDER BY waktu ASC"),
						'thn' => $ayam['tahun_akademik'],
						'tahun_akademik' => $tes,
					);

			return $this->render_page('jadwal_tampil', $data);

		elseif($this->session->userdata('level')==6):
			$jdw 	= $this->user->get_data("*", "tbl_jadwal", "GROUP BY id_tahunakademik");
			foreach ($jdw as $key => $value) {
				// print_r($value['id_tahunakademik']);
			}

			$data = array(
						'jadwal_thn' 		=> $jadwal1,
						'tahun_akademik10' 	=> $this->user->query_all("SELECT * FROM tbl_tahunakademik ORDER BY tahun_akademik ASC")->result(),
						'thn' 				=> $ayam['tahun_akademik'],
						// 'tahun_akademik' => $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik= '$value[id_tahunakademik]' ORDER BY tahun_akademik"),
						'tahun_akademik' 	=> $jdw,
						'th'				=> $th
						// 'matkul'			=> $matkul,
						// 'dosen'			=> $dosen,
						// 'kelas'			=> $kelas
					);
			
			return $this->render_page('jadwal_tampil', $data);
		endif;	
	}

	public function hapus_jadwal($id=''){
		$th 		= $this->input->get('th');
		$th 		= str_replace(" ","+",$th);
		$kode 		= $this->encryption->decrypt($th);
		
		// print_r($id);
		// return false;

		$this->user->Delete_Query('tbl_jadwal', array('id_jadwal' => $id));
		$this->session->set_flashdata('success', 'Jadwal berhasil dihapus');

		// print_r($kode);
		// return 0;

		return redirect('perkuliahan/jadwal/tampil_jadwal?th='.$th);
	}

	public function import_jadwal(){
		$th = $this->input->post('tahun_akademik');
		$fileName 	= $this->input->post('file');
	  	$config 	= $this->configuration->config_import($fileName, './file_upload/akademik/jadwal/', 2);
	  	$this->upload->initialize($config); 
	  	
	  	if (!$this->upload->do_upload('file')) {
		   	$error = array('error' => $this->upload->display_errors());
		   	// $this->session->set_flashdata('success', "Ada kesalahan dalam upload");
		   	redirect('perkuliahan/jadwal'); 
	  	} else {
		   	$media = $this->upload->data();
		   	$inputFileName = './file_upload/akademik/jadwal/'.$media['file_name'];
		   	try {
				    $inputFileType = IOFactory::identify($inputFileName);
			    	$objReader = IOFactory::createReader($inputFileType);
			    	$objPHPExcel = $objReader->load($inputFileName);
		   	} catch(Exception $e) {
		    		die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		   	}
		   	
		   	$sheet = $objPHPExcel->getSheet(0);
		   	$highestRow = $sheet->getHighestRow();
		   	$highestColumn = $sheet->getHighestColumn();
		   	for ($row = 2; $row < $highestRow ; $row++){  
		   		$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
		   		$tgl_mulai = PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][11], 'YYYY-MM-DD');
		   		$data = array(
				     "id_jadwal"        => NULL,
				     "kode_matkul"      => $rowData[0][1],
				     "id_kelas"         => $rowData[0][4],
				     "ruang"       		=> $rowData[0][6],
				     "username_dosen"   => $rowData[0][7],
				     "hari"       		=> $rowData[0][9],
				     "waktu"       		=> $rowData[0][10],	
				     "mulai"     		=> $tgl_mulai,
				     "uts"      		=> $rowData[0][12],
				     "selesai"      	=> $rowData[0][13],
				     "uas"				=> $rowData[0][14],
				     "id_tahunakademik" => $th,
		    	);
		    	
				$this->user->Add_Query("tbl_jadwal", $data); 
	   		}
	   		$this->session->set_flashdata('success', "Data Jadwal berhasil di export");
  			redirect('perkuliahan/jadwal'); 
  		}
	}

	public function export_jadwal2(){
		print_r("ayam");
		return false;
	}

	public function tambah_jadwal(){
		$id 	= $this->input->post("tahun_akademik");
		$id2 	= $this->encryption->decrypt($id);
		$timestamp = strtotime($this->input->post("tgl_mulai"));
		$day = date('l', $timestamp);

		$data 	= array(
					'id_jadwal' 		=> 	NULL,
					'kode_matkul' 		=> 	$this->input->post("pmatkul"),
					'username_dosen' 	=> 	$this->input->post("dosen"),
					'id_kelas'			=> 	$this->input->post("kelas"),
					'ruang'				=> 	$this->input->post("ruang"),
					'mulai'				=> 	$this->input->post("tgl_mulai"),
					'selesai'			=> 	$this->input->post("tgl_selesai"),
					'waktu'				=> 	$this->input->post("tgl_jam_mulai")." - ".$this->input->post("tgl_jam_selesai"),	
					'hari' 				=> 	$this->functions->get_nama_hari($day),
					'tgl_uts'			=> 	$this->input->post("tgl_uts"),
					'tgl_uas'			=> 	$this->input->post("tgl_uas"),
					'waktu_uts'			=> 	$this->input->post("tgl_jam_uts_awal")." - ".$this->input->post("tgl_jam_uts_akhir"),
					'waktu_uas'			=> 	$this->input->post("tgl_jam_uas_awal")." - ".$this->input->post("tgl_jam_uas_akhir"),
					'id_tahunakademik'	=> 	$id2,
				);

		$this->user->Add_Query("tbl_jadwal", $data);
		$this->session->set_flashdata('success', "Jadwal berhasil ditambahkan");
		return redirect('perkuliahan/jadwal');
	}

	public function edit_jadwal(){
		
	}

	public function export_jadwal(){
		$th 		= $this->input->post('tahun_akademik');
		$th2 		= $this->user->get_data("tahun_akademik", "tbl_tahunakademik", "WHERE id_tahunakademik = '$th'");
		foreach ($th2 as $key => $value) {
			// code...
		}
		$th3= $value['tahun_akademik'];

		  $this->load->library("excel");
		  $object = new PHPExcel();
		  $object->setActiveSheetIndex(0);

		  $table_columns = array("No", "ID Dosen", "Nama Dosen", "Kode Matkul", "Nama Matkul", "SKS", "Hari", "Mulai", "Selesai", "Tanggal UTS", "Tanggal UAS", "Kelas", "Tahun Akademik");

		  $column = 0;

		  foreach($table_columns as $field){
				$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		   		$column++;
		  }

		  $data = $this->user->query_all("
		  		SELECT tbl_dosen.username_dosen, tbl_dosen.dosen, tbl_matakuliah.kode_matkul, tbl_matakuliah.nama_matkul, tbl_matakuliah.sks, tbl_jadwal.hari, tbl_jadwal.mulai, tbl_jadwal.selesai, tbl_jadwal.tgl_uts, tbl_jadwal.tgl_uas, tbl_jadwal.id_kelas FROM tbl_jadwal
				INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen
				INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul
				WHERE tbl_jadwal.id_tahunakademik = '$th'")->result();
		  $excel_row = 2;
		  $i=1;
		  foreach($data as $row){
		  	   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->username_dosen);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->dosen);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->kode_matkul);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->nama_matkul);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->sks);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->hari);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->mulai);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->selesai);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->tgl_uts);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->tgl_uas);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->id_kelas);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $th3);
			   $excel_row++;
		  }

		  $object_writer = IOFactory::createWriter($object, 'Excel5');
		  header('Content-Type: application/vnd.ms-excel');
		  header("Content-Disposition: attachment;filename=Jadwal_Matkul_Periode_$th3.xls");
		  $object_writer->save('php://output');
	}
}