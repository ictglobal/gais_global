<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}    
		$this->load->library('Functions');
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
	}

	public function index(){
		$id = array('username_dosen' => $this->session->userdata('username'));
		$id = $id['username_dosen'];

		$tahun_akademik = $this->user->tahun_akademik();

		// return false;
		$tahunAkademik = $this->user->get_data('*','tbl_tahunakademik'," 
						where tahun_akademik = '$tahun_akademik'")[0];

		$tbl_modulajar = $this->user->get_data('*','tbl_jadwal'," where username_dosen = '$id' and id_tahunakademik = '$tahunAkademik[id_tahunakademik]'");
		$pilih_matkul = $this->user->get_data('*','tbl_jadwal'," where username_dosen = '$id' and id_tahunakademik = '$tahunAkademik[id_tahunakademik]' GROUP BY kode_matkul ORDER BY kode_matkul ASC");
		// print_r($pilih_matkul);
		// return 0;
		$pilih_matkul2 = $this->user->get_kelas_dariKRS($id, $tahunAkademik['id_tahunakademik']);
		

		$semester   = $this->user->get_data("*", "tbl_semester");
		$kelas      = $this->user->get_data('*','tbl_kelas');
		$matakuliah = $this->user->get_data('*','tbl_matakuliah');
		$data = [
					'pilih_matkul2'     => $pilih_matkul2,
					'pilih_matkul'      => $pilih_matkul,
					'tahun_akademik'    => $tahun_akademik,
					'kelas'             => $pilih_matkul2,
					'matakuliah'        => $matakuliah,
					'tbl_modulajar'     => $tbl_modulajar,
					'jadwal3'		    => $this->user->tampil_mtk2($id),
					'dosen'             => $id,
					'id_tahunakademik'  => $tahunAkademik['id_tahunakademik'],
					'semester'          => $semester
				];  
		return $this->render_page('user/dosen/penilaian/nilai',$data);
	}

	public function input_nilai(){   
		$id = array('username_dosen' => $this->session->userdata('username'));
			
		$id = $id['username_dosen'];
		
		$id_th 		= $this->input->post('tahun_akademik');
		$matkul 	= $this->input->post('matakuliah');
		$semester 	= $this->input->post('semester');
	
		if($semester == ''){
			return redirect('/perkuliahan/nilai');
		}

		$muncul = $this->user->get_kelas_dariKRS5($id, $id_th, $matkul);
		// $kls= explode("_",$this->input->post('kelas'));
		// $kls= implode(" ",$kls);

		$data = [
					//'mhs'               => $mhs,
					'muncul'            => $muncul,
					'tahun_akademik'    => $this->input->post('tahun_akademik'),
					// 'kelas'             => $kls,
					'matakuliah'        => $this->input->post('matakuliah'),
					'semester'          => $semester,
					'dosen'             => $id,
					'id_thn'			=> $id_th,
					'matkul'			=> $matkul
				];

			// return false;
		return $this->render_page('user/dosen/penilaian/input_nilai',$data);
	}

	public function input_nilai2($tahun_akademik, $matakuliah, $semester){   
		$id = array('username_dosen' => $this->session->userdata('username'));	
		$id = $id['username_dosen'];

		$change = str_replace('A','/',$semester);
		$space = str_replace('_',' ',$change);
		$muncul = $this->user->get_kelas_dariKRS5($id, $tahun_akademik, $matakuliah);
		$data = [
					//'mhs'               => $mhs,
					'muncul'            => $muncul,
					'tahun_akademik'    => $tahun_akademik,
					// 'kelas'             => $kls,
					'matakuliah'        => $matakuliah,
					'semester'          => $space,
					'dosen'             => $id,
					'id_thn'			=> $tahun_akademik,
					'matkul'			=> $matakuliah
				];

			// return false;
				return $data;
		// return $this->render_page('user/dosen/penilaian/input_nilai',$data);
	}

	public function export_nilai($id, $smt, $th){
		date_default_timezone_set('Asia/Jakarta'); 
		$id_dosen 	= $this->session->userdata('username');
		$matkul 	= $id;
		$semester 	= str_replace('K','/',$smt);
		$semester2 	= str_replace('_',' ',$semester);
		$th2 		= $th;

		// $muncul 	= $this->user->query_where("tbl_khs", "id_tahunakademik ='$th2' AND kode_matkul = '$matkul' AND semester = '$semester2'")->result();
		$muncul = $this->user->get_kelas_dariKRS6($id_dosen, $th, $matkul);
		
		$data = array(
				'matkul' 		=> $matkul,
				'semester'    	=> $semester2,
				'muncul'		=> $muncul,
		);
		return $this->load->view('user/dosen/penilaian/export_nilai', $data);
	}

	public function export_nilaipdf($id, $smt, $th, $dos='', $kls=''){
		$matkul 	= $id;
		$semester 	= str_replace('K','/',$smt);
		$semester2 	= str_replace('_',' ',$semester);
		$th2 		= $th;

		$get_tahun_akademik = $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$th2'")[0];
		$get_th = $get_tahun_akademik['tahun'];
		$get_th_str = str_replace('-',' - ',$get_th);

		$symbol = array('I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII');
		$get_semester 	= str_replace(' / ','  ',$semester2);
		$hapus_symbol = str_replace($symbol,'  ',$get_semester);
		
		$get_matkul = $this->user->get_data("*", "tbl_matakuliah", "WHERE kode_matkul = '$id'")[0];
    	
		if($this->session->userdata('level')==2):
    		$id_dosen 	= $this->session->userdata('username');
			$get_dosen  = $this->user->get_data("*", "tbl_dosen", "WHERE username_dosen = '$id_dosen'")[0];
			$muncul = $this->user->get_export_pdf($id_dosen, $th, $matkul);
    		$data = array(
				'matkul' 		=> $matkul,
				'semester'    	=> $semester2,
				'muncul'		=> $muncul,
				'get_nama_dosen' => $get_dosen,
				'get_nama_matkul' => $get_matkul,
				'get_tahun_akademik' => $get_th_str,
				'get_ganjil_genap' => $hapus_symbol
			);
    		return $this->load->view('user/dosen/penilaian/export_nilai_pdf', $data);

    	elseif($this->session->userdata('level')==6):
    		$get_dosen  = $this->user->get_data("*", "tbl_dosen", "WHERE username_dosen = '$dos'")[0];
    		$kls1 = $kls;
    		$get_kelas 	= str_replace('%20',' ',$kls1);
    		$juragan = $this->user->get_export_pdf_akademik($dos, $th, $matkul, $get_kelas);
    		$data = array(
				'matkul' 		=> $matkul,
				'semester'    	=> $semester2,
				'muncul'		=> $juragan,
				'get_nama_dosen' => $get_dosen,
				'get_nama_matkul' => $get_matkul,
				'get_tahun_akademik' => $get_th_str,
				'get_ganjil_genap' => $hapus_symbol
			);
			// print_r($get_kelas);
			// return false;
    		return $this->load->view('user/dosen/penilaian/export_nilai_pdf', $data);
    	endif;
	}

	public function hitung($input='',$field, $id){
		$this->user->Update_Query('tbl_khs_temp',array($field=>$input),array('id_temp'=>$id));
		return $input;
	}

	public function hitungtotal($id){
		$khs = $this->user->get_data('*','tbl_khs_temp',"  where id_temp = '$id'")[0];		

		$total = $this->user->total_nilai($khs['absen'],$khs['tugas'],$khs['formatif'],$khs['perilaku'],$khs['uts'],$khs['uas']);
		
		$this->user->Update_Query('tbl_khs_temp',array('total'=>$total),array('id_temp'=>$id));
		
		
		echo $total;
	}

	public function grade($id)
	{
		$khs = $this->user->get_data('*','tbl_khs_temp',"  where id_temp = '$id'")[0];
		
		$grade = $this->user->grade($khs['total']);
		
		$this->user->Update_Query('tbl_khs_temp',array('grade'=>$grade),array('id_temp'=>$id));
		$this->user->updatekhs($id);
		echo $grade;
	}

	public function tampil_nilai_akademik(){
		 $tahun_akademik = $this->user->tahun_akademik();
		$tahunAkademik = $this->user->get_data('*','tbl_tahunakademik   ',"  where tahun_akademik = '$tahun_akademik'")[0];
		// return false;
		$data = [
					// 'dosen'    => $this->user->get_data('*','tbl_dosen','WHERE username_dosen = "1150111"'),
					// 'matkul'    => $this->user->get_data('*','tbl_matakuliah','WHERE kode_matkul = "KK-1118"'),
					'dosen'    => $this->user->get_data('*','tbl_dosen', 'ORDER BY dosen'),
					'matkul'    => $this->user->get_data('*','tbl_matakuliah', 'ORDER BY nama_matkul'),
					'tahun'    => $this->user->get_data('*','tbl_tahunakademik',' ORDER BY tahun_akademik DESC'),
					'kelas'    => $this->user->get_data('*','tbl_kelas'),
					'id_tahunakademik' => $tahunAkademik['id_tahunakademik'],
					'tahun_akademik'    =>  $tahun_akademik,
				];
		return $this->render_page('user/akademik/nilai',$data); 
	}

	public function filter_nilai_akademik()
	{
		
		$username_dosen = $this->input->post('username_dosen');
		$dsn = $this->user->get_data("*", "tbl_dosen", "WHERE username_dosen='$username_dosen'")[0];

		// $kode_matkul = $_POST['matakuliah'];
		$id_tahunakademik = $this->input->post('tahun_akademik');
		$tahunAkademik = $this->user->get_data('*','tbl_tahunakademik   ',"  where id_tahunakademik = '$id_tahunakademik'")[0];
		$id_th = $tahunAkademik['tahun_akademik'];
		// MENAMPILKAN DATA PERMATA KULIAH
		// $jadwal = $this->user->get_data('*','tbl_jadwal',"  where kode_matkul = '$kode_matkul' and  username_dosen ='$username_dosen' and id_tahunakademik = '$id_tahunakademik' GROUP BY id_kelas");
		// MENAMPILKAN SEMUA MATAKULIAH YANG DI AJAR
		$jadwal = $this->user->get_data('*','tbl_jadwal',"  where   username_dosen ='$username_dosen' and id_tahunakademik = '$id_tahunakademik'");
		// print_r($id_tahunakademik);
		// return false;
		$hasil = [];
		foreach($jadwal as $jdw){
			// print_r(999);
		// return false;
			$id_kelas = $jdw['id_kelas'];
		   
			// $khs = $this->user->get_data('*','tbl_khs '," where kode_matkul = '$kode_matkul' and id_kelas='$id_kelas' and username_dosen='$username_dosen' and id_tahunakademik='$id_tahunakademik' ORDER BY kode_matkul");
			$khs = $this->user->get_data('*','tbl_khs_temp '," where id_kelas='$id_kelas' and username_dosen='$username_dosen' and id_tahunakademik='$id_tahunakademik' GROUP BY id_kelas");
			
			$matkul = $this->user->get_data('*','tbl_matakuliah',"  where kode_matkul = '$jdw[kode_matkul]'")[0];
			// $khs = $this->user->get_data('*','tbl_khs '," where kode_matkul = '$kode_matkul' and id_kelas='BD 20 P MBD' and username_dosen='$username_dosen' and id_tahunakademik='$id_tahunakademik'");
			  $jab = count($khs);
		   
			$status = '';
			if($jab > 0){
				$status = true;
			}else{
				 $status = false;
			}
			 
			 $arr = array(
				'username_dosen'=>$username_dosen,
				'id_kelas'=>$id_kelas,
				'nama_matkul'=>$matkul['nama_matkul'],
				'status'=>$status,
			);
			 $hasil[] = $arr;
		}
		// print_r($hasil);
		// return false;
		$data = [	
					'hasil' => $hasil,
					'dosen' => $dsn['dosen'],
					'tahun' => $id_th
						
				];
		return $this->render_page('user/akademik/filter_nilai_akademik',$data); 
	}

	// public function selectSemeser($id){
	// 	// echo '<option value="">Pilih Semester</option>';
	// 	$ar = explode('_',$id);
	// 	$tahun = date('y');
	// 	$a = $tahun-$ar[1];
	// 	$k = $this->user->tahun_akademik();
	// 	$arr = explode('-',$k);
	// 	$prm1 = $arr[1]%2;
	// 	$prm2 = $a%2;
	// 	if($prm1 == $prm2){
	// 		$a = $a+2;
	// 	}else{
	// 		$a = $a*2;
	// 	}
		
				
	// 	$semester = $this->user->get_data('*','tbl_semester'," where id_semester = '$a'")[0];
	// 	// print_r($semester['semester']);
	// 	echo '<option value="'.$semester['semester'].'">'.$semester['semester'].'</option>';
	// }

	public function selectSemeser($tahun_akademik='',$dosen='',$kelas=''){
		$id_kelas = explode('_', $kelas);
        $id_kelas = implode(' ',$id_kelas);
		// $pilih_matkul = $this->user->get_data('*','tbl_jadwal'," where username_dosen = '$dosen' and id_tahunakademik = '$tahun_akademik' and id_kelas = '$id_kelas'");
		$pilih_matkul = $this->user->get_kelas_dariKRS4($dosen, $tahun_akademik, $id_kelas);
		// print_r($pilih_matkul);
		// return false;
		foreach ($pilih_matkul as $key => $value) {
			$kode_matkul = $value['kode_matkul'];
			$matakuliah = $this->user->get_data('*','tbl_matakuliah'," where kode_matkul = '$kode_matkul'")[0];
			echo '<option value="'.$matakuliah['semester'].'">'.$matakuliah['semester'].'</option>';
		}
	}

	public function selectkelas($username='',$tahun='')
	{
		$jadwal = $this->user->get_data('*','tbl_jadwal'," where username_dosen = '$username' AND id_tahunakademik = '$tahun' GROUP BY kode_matkul");
		// print_r($jadwal);
		foreach ($jadwal as $key => $value) {
			$kode_matkul = $value['kode_matkul'];
			$matakuliah = $this->user->get_data('*','tbl_matakuliah'," where kode_matkul = '$kode_matkul'")[0];
			echo '<option value="'.$matakuliah['kode_matkul'].'">'.$matakuliah['nama_matkul'].'</option>';
		}
	}

	public function tampil_update_nilai_akademik(){
		$dosen = $this->user->get_data("*", "tbl_dosen"); 
		$tahun_akademik = $this->user->tahun_akademik();
		$tahunAkademik = $this->user->get_data('*','tbl_tahunakademik'," where tahun_akademik = '$tahun_akademik'")[0];
		// print_r($tahunAkademik);
		// return false;
		$data = array(
					'dosen' 			=> $dosen,
					'tahun_akademik'    => $tahun_akademik,
					'id_tahunakademik'  => $tahunAkademik['id_tahunakademik'],
					'tahunAkademik' => $this->user->get_data('*','tbl_tahunakademik',' ORDER BY tahun_akademik DESC'),
				);

		return $this->render_page('user/akademik/update_nilai_akademik',$data);
	}

	public function getDosen($tahun_akademik=''){
		echo '<option value="">Pilih Dosen</option>';
		$jadwal = $this->user->get_data('*','tbl_jadwal',"  WHERE id_tahunakademik = '$tahun_akademik' GROUP BY username_dosen");
		
		foreach ($jadwal as $key => $value):
			$dosen = $this->user->get_data('*','tbl_dosen',"  WHERE username_dosen = '$value[username_dosen]'")[0];
			echo '<option value="'.$value['username_dosen'].'">'.$dosen['dosen'].'</option>';
		endforeach;
	}

	public function getKelas($tahun_akademik='', $dosen=''){
		echo '<option value="">Pilih Kelas</option>';
		$getKelas2 = $this->user->get_kelas_dariKRS($dosen, $tahun_akademik);

		foreach ($getKelas2 as $key => $value):
			$id_kelas = explode(' ', $value['id_kelas']);
                                            $id_kelas = implode('_',$id_kelas);
			echo '<option value="'.$id_kelas.'">'.$value['id_kelas'].'</option>';
		endforeach;	
	}

	public function getMataKuliah($tahun_akademik='',$dosen='',$kelas=''){
		$id_kelas = explode('_', $kelas);
        $id_kelas = implode(' ',$id_kelas);
		// $pilih_matkul = $this->user->get_data('*','tbl_jadwal'," where username_dosen = '$dosen' and id_tahunakademik = '$tahun_akademik' and id_kelas = '$id_kelas'");
		$pilih_matkul = $this->user->get_kelas_dariKRS4($dosen, $tahun_akademik, $id_kelas);
		// print_r($pilih_matkul);
		// return false;
		foreach ($pilih_matkul as $key => $value) {
			$kode_matkul = $value['kode_matkul'];
			$matakuliah = $this->user->get_data('*','tbl_matakuliah'," where kode_matkul = '$kode_matkul'")[0];
			echo '<option value="'.$kode_matkul.'">'.$matakuliah['nama_matkul'].' - '.$value['nama_programstudi'].'</option>';
		}
	}

	public function update_nilai_akademik(Type $var = null)
	{
		// $id = $_POST['dosen'];
		$id = $this->input->post('dosen');
		$kls= explode("_",$this->input->post('kelas'));
		$kls= implode(" ",$kls);

		$id_th = $this->input->post('tahun_akademik');
		$semester = $this->input->post('semester');
		
		$muncul = $this->user->get_kelas_dariKRS3($id, $id_th, $kls);
		// if($semester == "Pilih Semester"):
		// 	$this->session->set_flashdata('error', "Pilih Semesternya coy");
		// else:
			
		// endif;

		$data = [
					//'mhs'               => $mhs,
					'muncul'            => $muncul,
					'tahun_akademik'    => $this->input->post('tahun_akademik'),
					'kelas'             => $kls,
					'matakuliah'        => $this->input->post('matakuliah'),
					'semester'          => $semester,
					'dosen'             => $id
				];
			if(empty($data['muncul'])):
				// throw new Exception("Error Processing Request", 1);
				return $this->load->view('errors/index.html');
			endif;

			return $this->render_page('user/dosen/penilaian/input_nilai',$data);

		// $id_kelas = $this->input->post('kelas');
		// $mhs = $this->user->get_data('*','tbl_mahasiswa',"where id_kelas = '$kls'");
	}

	public function tampil_atur_jadwal_input_nilai(){
		$query = $this->user->get_data("*", "tbl_atur_jadwal_input_nilai");
		$tahun_akademik = $this->user->tahun_akademik();
		$tahunAkademik = $this->user->get_data('*','tbl_tahunakademik'," where tahun_akademik = '$tahun_akademik'")[0];

		$data = array(
				'atur_jadwal' => $query,
				'tahun_akademik' => $tahun_akademik,
		);
		return $this->render_page('user/akademik/atur_jadwal_input_nilai', $data);
	}

	public function set_input_nilai()
	{	
		$tgl_mulai = $this->input->post('tgl_mulai');
		$tgl_selesai = $this->input->post('tgl_selesai');
		$waktu_mulai = $this->input->post('waktu_mulai');
		$waktu_selesai = $this->input->post('waktu_selesai');

		$tahun_akademik = $this->user->tahun_akademik();
		$tahunAkademik = $this->user->get_data('*','tbl_tahunakademik'," 
						where tahun_akademik = '$tahun_akademik'")[0];
		$jadwal = $this->user->get_data('*','tbl_jadwal',"  WHERE id_tahunakademik = '$tahunAkademik[id_tahunakademik]' GROUP BY username_dosen");
		$cek = $this->user->get_data('*','tbl_atur_jadwal_input_nilai');
		
		foreach ($jadwal as $key => $value) {	
			$insert = array(
				'tgl_mulai' => $tgl_mulai,
				'waktu_mulai' => $waktu_mulai,
				'tgl_selesai' => $tgl_selesai,
				'waktu_selesai' => $waktu_selesai,
				'username_dosen' => $value['username_dosen'],
				'id_tahunakademik' =>$tahunAkademik['id_tahunakademik'] ,
			);
			$cek = $this->user->get_data('*','tbl_atur_jadwal_input_nilai',"  WHERE 
			tgl_mulai = '$tgl_mulai' and
			waktu_mulai = '$waktu_mulai' and
			tgl_selesai = '$tgl_selesai' and
			waktu_selesai = '$waktu_selesai' and
			username_dosen = '$value[username_dosen]' and
			id_tahunakademik = '$tahunAkademik[id_tahunakademik]'
			");
			if(count($cek) <=0){
				$this->user->Add_Query('tbl_atur_jadwal_input_nilai',$insert);	
				$this->session->set_flashdata('error','Jadwal tidak bisa dimasukan');			
			}
		}
		return redirect('perkuliahan/nilai/tampil_atur_jadwal_input_nilai');	
	}

	public function update_status_input_nilai($id_atur){
		$status = $this->user->query_all("SELECT * FROM tbl_atur_jadwal_input_nilai WHERE id_atur = '$id_atur'")->result()[0];
		if($status->status_input == 'Aktif'){
			$status_input = 'Aktif';			
		}else{
			$status_input = 'Non Aktif';	
		}
		$data = array(
					'status_input'	=>	$status_input,
				);
		$update = $this->user->Update_Query('tbl_atur_jadwal_input_nilai', $data, array('id_atur'=>	$id_atur));
		return $update;
	}

	public function export_nilai_bagian_akademik(){
		$th = $this->input->post('tahun_akademik');
		$th2 		= $this->user->get_data("tahun_akademik", "tbl_tahunakademik", "WHERE id_tahunakademik = '$th'");
		foreach ($th2 as $key => $value) {
			// code...
		}
		$th3= $value['tahun_akademik'];
		// echo $th;
		// return false;
		  $this->load->library("excel");
		  $object = new PHPExcel();
		  $object->setActiveSheetIndex(0);

		  $table_columns = array("No", "NIM", "Nama Mahasiswa", "Kelas", "Kode Matkul", "Nama Matkul", "SKS", "Absen", "Tugas", "Formatif", "Sikap", "UTS", "UAS", "Total", "Grade", "Semester");

		  $column = 0;

		  foreach($table_columns as $field){
				$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		   		$column++;
		  }

		  // $data = $this->user->get_export_pdf_dosen($id_dosen, $th, $matkul);
		  $data = $this->user->query_all("
		  		SELECT tbl_khs.nim, tbl_khs.kode_matkul, tbl_matakuliah.nama_matkul, tbl_mahasiswa.nama, tbl_mahasiswa.id_kelas, tbl_matakuliah.sks, tbl_khs.semester ,tbl_khs.absen, tbl_khs.tugas, tbl_khs.formatif, tbl_khs.perilaku,tbl_khs.uts,tbl_khs.uas,tbl_khs.total,tbl_khs.grade FROM tbl_khs INNER JOIN tbl_matakuliah ON tbl_khs.kode_matkul = tbl_matakuliah.kode_matkul INNER JOIN tbl_mahasiswa ON tbl_khs.nim = tbl_mahasiswa.nim WHERE tbl_khs.id_tahunakademik = '$th' GROUP BY tbl_khs.nim, tbl_khs.kode_matkul ORDER BY tbl_mahasiswa.nama ASC, tbl_mahasiswa.id_kelas")->result();
		  $excel_row = 2;
		  $i=1;
		  foreach($data as $row){
		  	   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nim);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nama);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->id_kelas);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->kode_matkul);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->nama_matkul);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->sks);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->absen);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->tugas);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->formatif);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->perilaku);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $row->uts);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $row->uas);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->total);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row->grade);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $row->semester);
			   $excel_row++;
		  }

		  $object_writer = IOFactory::createWriter($object, 'Excel5');
		  header('Content-Type: application/vnd.ms-excel');
		  header("Content-Disposition: attachment;filename=Export_Nilai_Mahasiswa_Periode $th3.xls");
		  $object_writer->save('php://output');
	}

	public function import_nilai(){
		$th 		= $this->input->post('tahun_akademik');
		$fileName 	= $this->input->post('file');

		$config 	= $this->configuration->config_import($fileName, './file_upload/akademik/nilai/', 2);
	  	$this->upload->initialize($config);
	  	
	  	if (!$this->upload->do_upload('file')):
		   	$err = $this->upload->display_errors();
		   	$this->session->set_flashdata('error', $err);
		   	redirect('perkuliahan/nilai/tampil_update_nilai_akademik'); 
	  	else:
		   	$media = $this->upload->data();
		   	$inputFileName = './file_upload/akademik/nilai/'.$media['file_name'];

		   	try {
				    $inputFileType = IOFactory::identify($inputFileName);
			    	$objReader = IOFactory::createReader($inputFileType);
			    	$objPHPExcel = $objReader->load($inputFileName)->getSheet(0);
		   	} catch(Exception $e) {
		    		die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		   	}
			$highestRow 	= $objPHPExcel->getHighestRow();
			$highestColumn 	= $objPHPExcel->getHighestColumn();
			$data_input = $this->user->validasiExelInputNilai($highestRow,$highestColumn,$objPHPExcel,$th);


			if($data_input['prmval'] == 11){
				// ada format data yang tidak sesuai
				$this->session->set_flashdata('error','Format data tidak sesuai');
			 
			}elseif($data_input['prmval'] == 12){
	    		$this->session->set_flashdata('error',$data_input['alert']);	
	    	}else{
	    		// print_r($data_input['inputtmp']);
	    	// return false;
	    	foreach ($data_input['inputtmp'] as $key => $inputtmp) {
	    		// print_r($inputtmp);
	    		
	    		$this->user->crudTmpKhs($inputtmp,$data_input['inputkhs'],$key);
	    		
	    		}
	    	}
		endif;
		$this->session->set_flashdata('success','Data berhasil di import');
		return redirect('/perkuliahan/nilai/tampil_update_nilai_akademik');
	}

	public function import_nilai_mhs_dosen(){
		// return 1;
		$id_dosen 	= $this->session->userdata('username');
		$th 		= $this->input->post('tahun_akademik');
		$matkul 	= $this->input->post('matkul');
		$fileName 	= $this->input->post('file');
		$semester 	= str_replace('K','/',$this->input->post('semester'));
		$semester2 	= str_replace('_',' ',$semester);
		$config 	= $this->configuration->config_import($fileName, './file_upload/dosen/nilai/', 2);
		
		$change = str_replace('/','A',$semester2);
		$space = str_replace(' ','_',$change);
		// print_r($space);
		// return false;
	  	$this->upload->initialize($config);

		// echo $th. "<br>".$semester2."<br>".$id_dosen;
		// // print_r($config);
		// return false;

		if (!$this->upload->do_upload('file')):
		   	$err = $this->upload->display_errors();
		   	$this->session->set_flashdata('error', $err);
		   	redirect('perkuliahan/nilai/input_nilai'); 
	  	else:
		   	$media = $this->upload->data();
		   	$inputFileName = './file_upload/dosen/nilai/'.$media['file_name'];
		   	try {
				    $inputFileType = IOFactory::identify($inputFileName);
			    	$objReader = IOFactory::createReader($inputFileType);
			    	$objPHPExcel = $objReader->load($inputFileName)->getSheet(0);
		   	} catch(Exception $e) {
		    		die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		   	}

		   	$highestRow 	= $objPHPExcel->getHighestRow();
		   	$highestColumn 	= $objPHPExcel->getHighestColumn();
		   	$columnData = $objPHPExcel->rangeToArray('A' . 1 . ':' . $highestColumn . 1, NULL, TRUE, FALSE)[0];

		   	if($columnData[0] != "No" || $columnData[1] != "NIM" || $columnData[2] != "Nama Mahasiswa" || $columnData[3] != "Kelas" || $columnData[4] != "Absen" || $columnData[5] != "Tugas" || $columnData[6] != "Formatif" || $columnData[7] != "Sikap" || $columnData[8] != "UTS" || $columnData[9] != "UAS" || count($columnData) != 10):
		   		$this->session->set_flashdata('warning', 'Format data tidak sesuai dengan yang diexport');
		   	
		   		// echo "0";
		   		return 0;
		   	endif;

		   	for ($row = 2; $row < $highestRow ; $row++){  
		     	$rowData = $objPHPExcel->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE)[0];
		     	//A2:M++
		     	$total = $this->user->total_nilai($rowData[4],$rowData[5],$rowData[6],$rowData[7],$rowData[8],$rowData[9]);
				$grade = $this->user->grade($total);
		   		$data = array( 
				     "nim"     				=> $rowData[1],
				     "id_kelas"      		=> $rowData[3],
				     "absen"      			=> $rowData[4],
				     "tugas"      			=> $rowData[5],
				     "formatif"      		=> $rowData[6],
				     "perilaku"      		=> $rowData[7],
				     "uts"      			=> $rowData[8],
				     "uas"      			=> $rowData[9],
				     "semester" 			=> $semester2,
				     "total" 				=> $total,
				     "grade" 				=> $grade,
		    	);

		    	$cek_id_tmp = $this->user->cek_tmp_khs($id_dosen, $th, $data['nim'], $data['id_kelas'], $data['semester'],$data);
		   		// print_r($cek_id_tmp);
		    	// return false;
		    	// echo "-".$cek_id_tmp."<br>";
		    }
		   $this->session->set_flashdata('success', 'Data berhasil diimport');
		   // return redirect('perkuliahan/nilai/input_nilai2/'.$th.'/'.$matkul.'/'.$space);
		 	echo "<script>document.getElementById('tess').innerHTML = 'dddd';</script>ewqrewr";
		   		return 1;
		endif;
	}

	public function export_nilai_excel($id_matkul, $smt, $th){
		  // $this->load->model("excel_export_model");
			$id_dosen = $this->session->userdata('username');
			$matkul   = $id_matkul;
			$semester 	= str_replace('K','/',$smt);
			$smts 	= str_replace('_',' ',$semester);

		  $this->load->library("excel");
		  $object = new PHPExcel();

		  $object->setActiveSheetIndex(0);

		  $table_columns = array("No", "NIM", "Nama Mahasiswa", "Kelas", "Absen", "Tugas", "Formatif", "Sikap", "UTS", "UAS");

		  $column = 0;

		  foreach($table_columns as $field){
				$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		   		$column++;
		  }

		  $data = $this->user->get_export_pdf_dosen($id_dosen, $th, $matkul);
		  $excel_row = 2;
		  $i=1;
		  foreach($data as $row){
		  	   $object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->nim);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nama);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->id_kelas);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->absen);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->tugas);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->formatif);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->perilaku);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->uts);
			   $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->uas);
			   // $object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->age);
			   $excel_row++;
		  }

		  $object_writer = IOFactory::createWriter($object, 'Excel5');
		  header('Content-Type: application/vnd.ms-excel');
		  header('Content-Disposition: attachment;filename="Nilai Mahasiswa.xls"');
		  $object_writer->save('php://output');

		  // return redirect($this->input_nilai());
 	}

	public function select_smt($id=''){
		$matakuliah =  $this->user->get_data('*','tbl_matakuliah'," where kode_matkul = '$id'")[0];
		echo '<option value="'.$matakuliah['semester'].'">'.$matakuliah['semester'].'</option>';
		// print_r($id);
		// return false;
		// return redirect('perkuliahan/nilai/input_nilai');
	}
}