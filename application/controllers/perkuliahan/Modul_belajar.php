<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modul_belajar extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('akses') != TRUE) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function tampil_modulbahanajar(){
		$id = array('username_dosen' => $this->session->userdata('username'));	
		$data = array(
					'modul_ajar' 	=> $this->user->query_where('tbl_modulajar', $id)->result(),
					'biodata_dosen'	=> $this->user->query_where('tbl_dosen', $id)->result(),
					'jadwal2'		=> $this->user->tampil_mtk($this->session->userdata('username')),
					'jadwal3'		=> $this->user->tampil_mtk2($this->session->userdata('username')),
				);

		// print_r($this->user->tampil_mtk($this->session->userdata('username')));
		return $this->render_page('user/dosen/modul/vupload', $data);
	}

	public function tampil_modulRPS(){
		if ($this->session->userdata('level') == 2): //Dosen
			$id = array('username_dosen' => $this->session->userdata('username'));	
			return $this->tampil_modulRPS_Dosen($id);

		elseif($this->session->userdata('level')==4): //Fakultas
			$id = array('id_fakultas' => $this->session->userdata('id_fakultas'));
			return $this->tampil_modulRPS_Fakultas($id);

		elseif($this->session->userdata('level')==5): //Kaprodi
			// $id = array('id_kaprodi' => $this->session->userdata('id'));
			return $this->tampil_modulRPS_kaprodi();

		endif;
	}

	private function tampil_modulRPS_kaprodi(){
		$id_prodi = $this->session->userdata('id');
		$cek_idprodi= $this->user->get_data('*','tbl_user_kaprodi',"WHERE id_kaprodi='$id_prodi'")[0];
		$cek_prodi = $this->user->get_data('*','tbl_programstudi', "WHERE id_programstudi ='$cek_idprodi[id_programstudi]'")[0];

		$data = array(
					'rps'		=> $this->user->query_all("SELECT * FROM tbl_rps WHERE id_kaprodi = $id_prodi")->result(),
					'matkul'	=> $this->user->get_data('*','tbl_matakuliah',"WHERE id_programstudi = '$cek_prodi[id_programstudi]' ORDER BY nama_matkul ASC"),
				);

		return $this->render_page('user/dosen/rps/vrps', $data);
	}

	private function tampil_modulRPS_Dosen($id){
		$data = array(
					'biodata_dosen' => $this->user->query_where('tbl_dosen', $id)->result(),
					'modul_ajar' 	=> $this->user->query_where('tbl_modulajar', $id)->result(),
					'rps'			=> $this->user->query_where('tbl_rpsdetail', $id)->result(),
					// 'detail_rps'	=> $this->user->query_where('tbl_rps', $id)->result()
				);
		return $this->render_page('user/dosen/rps/vrps', $data);
	}

	private function tampil_modulRPS_Fakultas($id){
		$data = array(
					'rps'			=> $this->user->query_where('tbl_rps', $id)->result(),
					'detail_rps'	=> $this->user->query_all('SELECT * FROM tbl_rpsdetail')->result(),
					'biodata_dosen'	=> $this->user->query_all('SELECT * FROM tbl_dosen')->result(),
				);
		return $this->render_page('user/dosen/rps/vrps', $data);
	}

	public function upload_file(){
		if ($this->session->userdata('upload_file')==1 || $this->session->userdata('upload_file')==2):
			$this->upload_modul();
		elseif($this->session->userdata('upload_file')==1 || $this->session->userdata('upload_file')==3):
			$this->upload_rps();
		endif;
    }

    private function upload_modul(){
    	$file = $_FILES['upload_file']['name'];
    	$ext = pathinfo($file, PATHINFO_EXTENSION);

    	$config = array(
    				'allowed_types'	=> "doc|docx|xls|xlsx|ppt|pptx|pdf|rar|zip",
    				'upload_path'	=> "./file_upload/modul/",
    				'overwrite'		=> TRUE,
    				'max_size'		=> 10000,
    				'remove_spaces'	=> TRUE,
    				'encrypt_name'	=> TRUE
    			);

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_file')):
            $this->session->set_flashdata('error', $this->upload->display_errors());
            // $this->session->set_flashdata('error', "Ukuran modul terlalu besar");
            
        else:
        	if ($this->session->userdata('level')==1 || $this->session->userdata('level')==2):
        		if($ext == "doc" || $ext == "docx" || $ext == "xls" || $ext == "xlsx" || $ext == "ppt" || $ext == "pptx" || $ext == "pdf" || $ext == "rar" || $ext == "zip"):
        			
        			// $search_matkul = $this->input->post('search_data');
        			// // $matkul = $this->user->get_data('*', 'tbl_matakuliah', "where nama_matkul = '$search_matkul'")[0];
        			$kode_matkul =$this->input->post('matkul');
        			$cek = count($this->user->get_data('*', 'tbl_modulajar', "where kode_matkul = '$kode_matkul'"));

        			$upload_data = $this->upload->data('file_name');
        			if ($cek <=0):
        				$data = array(
							'id_modul'			=> NULL,
							'kode_matkul' 		=> $this->input->post('matkul'),
							'username_dosen' 	=> $this->session->userdata('username'),
							'file'				=> $upload_data
						);
						$this->user->Add_Query('tbl_modulajar', $data);
						$this->session->set_flashdata('success', 'Modul berhasil ditambahkan');
					else:
						$this->session->set_flashdata('warning', 'Gagal Menyimpan Modul');
					endif;
        		else:
        			$this->session->set_flashdata('error', "Format file tidak didukung");
        		endif;
			else:
				$this->session->set_flashdata('error', "Error 404 Not Found");
        	endif;
        endif;
        return redirect('perkuliahan/modul_belajar/tampil_modulbahanajar'); 	 	
    }

	function upload_rps(){

		date_default_timezone_set('Asia/Karachi'); # add your city to set local time zone
        $now    = date('Y-m-d H:i:s');
        $ta 	= $this->input->post('tahun_akademik');
        print_r($ta);
        return false;
        $ayam 	= $this->akademik->getUsername_Dosen("$ta")[0];
        

		$file = $_FILES['upload_file']['name'];
    	$ext = pathinfo($file, PATHINFO_EXTENSION);

    	$config = array(
    		'upload_path'			=> './file_upload/rps/',
    		'allowed_types'			=> 'pdf',
    		'overwrite'				=> TRUE,
    		'max_size'				=> 2000,
    		'remove_spaces'			=> TRUE,
    		'encrypt_name'			=> TRUE
    	); 
    	$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_file')):
            $this->session->set_flashdata('error', $this->upload->display_errors());
            
        else:
        	if ($this->session->userdata('level')==5):
        		if($ext == "pdf"):
        			$search_matkul = $this->input->post('matkul');
        			$upload_data = $this->upload->data('file_name');
        			$cek = count($this->user->get_data('*', 'tbl_rps', "where kode_matkul = '$search_matkul'"));
        			if ($cek <=0):
        				$data = array(
							'id_rps'			=> NULL,
							'id_kaprodi'		=> $this->session->userdata('id'),
							'id_fakultas'		=> $this->session->userdata('id_fakultas'),
							'id_programstudi'	=> $this->session->userdata('id_prodi'),
							'kode_matkul' 		=> $search_matkul,
							'tahun'				=> $this->input->post('tahun_akademik'),
							'file'				=> $upload_data,
							'waktu_buat'		=> $now
						);
						$this->user->Add_Query('tbl_rps', $data);
						$this->session->set_flashdata('success', 'Modul berhasil ditambahkan');
        			else:
        				unlink("file_upload/rps/".$upload_data);
        				$this->session->set_flashdata('warning', 'Modul sudah ada');
        			endif;	
        		else:
        			$this->session->set_flashdata('error', "Format file tidak didukung");
        		endif;
			else:
				$this->session->set_flashdata('error', "Error 404 Not Found");
        	endif;
        endif;
        return redirect('perkuliahan/modul_belajar/tampil_modulRPS'); 
	}

	public function edit_file(){
		if($this->session->userdata('edit_file')==1 || $this->session->userdata('edit_file')==2):
			$this->edit_modulajar();
		elseif($this->session->userdata('edit_file')==1 || $this->session->userdata('edit_file')==3):
			$this->edit_modulRPS();	
		endif;
	}

	function edit_modulajar(){
		$file = $_FILES['upload_file']['name'];
    	$ext = pathinfo($file, PATHINFO_EXTENSION);

    	$config = array(
    		'upload_path'			=> './file_upload/modul/',
    		'allowed_types'			=> 'doc|docx|xls|xlsx|ppt|pptx|pdf|rar|zip',
    		'overwrite'				=> TRUE,
    		'max_size'				=> 10000,
    		'remove_spaces'			=> TRUE,
    		'encrypt_name'			=> TRUE
    	);
        
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_file')):
            $this->session->set_flashdata('error', $this->upload->display_errors());
            
        else:
        	if ($this->session->userdata('level')==5 || $this->session->userdata('level')==2):
        		if($ext == "doc" || $ext == "docx" || $ext == "xls" || $ext == "xlsx" || $ext == "ppt" || $ext == "pptx" || $ext == "pdf" || $ext == "rar" || $ext == "zip"):
        			$search_matkul = $this->input->post('search_data');
					$matkul = $this->user->get_data('*', 'tbl_matakuliah', "where nama_matkul = '$search_matkul'")[0];
					$kode_matkul = $matkul['kode_matkul'];
					$modul = $this->user->get_data('*', 'tbl_modulajar', "where kode_matkul = '$kode_matkul'")[0];
        			
        			unlink("file_upload/modul/".$modul['file']);

        			$upload_data = $this->upload->data('file_name');
        			$data = array(
						'username_dosen' 	=> $this->session->userdata('username'),
						'file'				=> $upload_data
					);
					$where = array('kode_matkul' => $matkul['kode_matkul'],);

					$this->session->set_flashdata('success', 'Modul berhasil diubah');
					$this->user->Update_Query('tbl_modulajar', $data, $where);
        		else:
        			$this->session->set_flashdata('error', "Format file tidak didukung");
        		endif;
			else:
				$this->session->set_flashdata('error', "Error 404 Not Found");
        	endif;
        endif;
        return redirect('perkuliahan/modul_belajar/tampil_modulbahanajar');
	}

	function edit_modulRPS(){
		date_default_timezone_set('Asia/Karachi'); # add your city to set local time zone
        $now    = date('Y-m-d H:i:s');

		$file = $_FILES['upload_file']['name'];
    	$ext = pathinfo($file, PATHINFO_EXTENSION);

    	$config = array(
    		'upload_path'			=> './file_upload/rps/',
    		'allowed_types'			=> 'pdf',
    		'overwrite'				=> TRUE,
    		'max_size'				=> 2000,
    		'remove_spaces'			=> TRUE,
    		'encrypt_name'			=> TRUE
    	);

    	$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_file')):
            $this->session->set_flashdata('error', $this->upload->display_errors());
            
        else:
        	if ($this->session->userdata('level')==5):
        		if($ext == "pdf"):
        			$search_matkul = $this->input->post('search_data');
        			$matkul = $this->user->get_data('*', 'tbl_matakuliah', "where nama_matkul = '$search_matkul'")[0];
        	
        			$upload_data = $this->upload->data('file_name');
        			$data = array(
						'kode_matkul' 		=> $matkul['kode_matkul'],
						'file'				=> $upload_data,
						'waktu_edit'		=> $now
					);
					$where = array('kode_matkul' => $matkul['kode_matkul'],);

					$this->session->set_flashdata('success', 'Modul RPS berhasil diubah');
					$this->user->Update_Query('tbl_rps', $data, $where);
        		else:
        			$this->session->set_flashdata('error', "Format file tidak didukung");
        		endif;
			else:
				$this->session->set_flashdata('error', "Error 404 Not Found");
        	endif;
        endif;
        return redirect('perkuliahan/modul_belajar/tampil_modulRPS');
	}

	public function delete_($id){
		if($this->session->userdata('hapus_file')==1 || $this->session->userdata('hapus_file')==2):
			$this->hapus_modulbahanajar($id);
		elseif($this->session->userdata('hapus_file')==1 || $this->session->userdata('hapus_file')==3):
			$this->hapus_modulRPS($id);
		elseif($this->session->userdata('hapus_file')==1 || $this->session->userdata('hapus_file')==4):
			$this->hapus_file_RPSDosen();
		endif;
	}

	function hapus_modulbahanajar($id){
		$modul = $this->user->get_data('*', 'tbl_modulajar', "where id_modul = '$id'")[0];
		unlink("file_upload/modul/".$modul['file']);
		
		$this->user->Delete_Query('tbl_modulajar', array('id_modul' => $id));
		$this->session->set_flashdata('success', "Modul berhasil dihapus");
		return redirect('perkuliahan/modul_belajar/tampil_modulbahanajar');
	}

	function hapus_modulRPS($id){
		$modul = $this->user->get_data('*', 'tbl_rps', "where id_rps = '$id'")[0];
		unlink("file_upload/rps/".$modul['file']);
		
		$this->user->Delete_Query('tbl_rps', array('id_rps' => $id));
		$this->session->set_flashdata('success', "Modul RPS berhasil dihapus");
		return redirect('perkuliahan/modul_belajar/tampil_modulRPS');
	}

	//ini untuk fakultas
	function hapus_file_RPSDosen(){
		$search_dosen = $this->input->post('search_data');
    	$dosen_ = $this->user->get_data('*', 'tbl_dosen', "where username_dosen = '$search_dosen'")[0];

    	$search_matkul = $this->input->post('matkul');
    	$matkul = $this->user->get_data('*', 'tbl_matakuliah', "where nama_matkul = '$search_matkul'")[0];

    	$a = $this->input->post('id');
    	$b = $matkul['kode_matkul'];
    	$c = $dosen_['username_dosen'];

    	$mtk = $this->user->get_data("*", "tbl_rpsdetail", "where id_rps='$a' AND kode_matkul='$b' AND username_dosen='$c'")[0];

    	$id = $mtk['id_rpsdetail'];

    	$this->user->Delete_Query('tbl_rpsdetail', array('id_rpsdetail' => $id));
		$this->session->set_flashdata('success', "Data berhasil dihapus");
		return redirect('perkuliahan/modul_belajar/tampil_modulRPS');
	}
}
