<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalender_akademik extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('akses') != TRUE) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		// $this->load->library('encrypt');
	}

	public function index(){
		if ($this->session->userdata('level') == 2):
			$this->tampil_kalender_dosen();
		elseif($this->session->userdata('level')==3):
			$this->tampil_kalender_mahasiswa();
		elseif($this->session->userdata('level')==4):
			$this->tampil_kalender_fakultas();			
		elseif($this->session->userdata('level')==5):
			$this->tampil_kalender_kaprodi();
		elseif($this->session->userdata('level')==6):
			$this->tampil_kalender_baak();
		elseif($this->session->userdata('level')==7):
			$this->tampil_kalender_keuangan();
		elseif($this->session->userdata('level')==8):
			$this->tampil_kalender_pemasaran();
		elseif($this->session->userdata('level')==9):
			$this->tampil_kalender_ict();
		endif;
	}

	private function tampil_kalender_dosen(){
		$id 					= array('username_dosen' => $this->session->userdata('username'));
		$data['biodata_dosen'] 	= $this->user->query_where('tbl_dosen', $id)->result();
		$data['modul_ajar'] 	= $this->user->query_where('tbl_modulajar', $id)->result();
		// $data['rps'] 			= $this->user->query_where('tbl_rpsdetail', $id)->result();

		return $this->render_page('user/akademik/kalender/create_kalender_akademik', $data);
	}

	private function tampil_kalender_mahasiswa(){
		$data['biodata_mahasiswa'] 	= $this->user->query_where('tbl_mahasiswa', array('nim' => $this->session->userdata('nim')))->result();
		$data['prodi'] 				= $this->user->query_where('tbl_prodi', array('id_prodi' => $this->session->userdata('prodi')))->result();

		return $this->render_page('user/akademik/kalender/create_kalender_akademik', $data);	
	}

	private function tampil_kalender_fakultas(){
		$id 					= array('id_fakultas' => $this->session->userdata('id_fakultas'));
		$data['rps'] 			= $this->user->query_where('tbl_rps', $id)->result();

		return $this->render_page('user/akademik/kalender/create_kalender_akademik', $data);
	}

	private function tampil_kalender_kaprodi(){
		$id_kaprodi 			= array('id_kaprodi' => $this->session->userdata('id_kaprodi'));
		$data['rps']			= $this->user->query_where('tbl_rps', $id_kaprodi)->result();
		
		return $this->render_page('user/akademik/kalender/create_kalender_akademik', $data);
	}

	private function tampil_kalender_baak(){
		$id = array('id_user_akademik' => $this->session->userdata('id'));
		$dt = $this->user->query_where('tbl_user_akademik', $id)->result();
		$ta = $this->user->get_data("*", "tbl_tahunakademik"," ORDER BY tahun_akademik");

		$data = array(
					'akademik' => $dt,
					'tahun_akademik' => $ta,
				);

		return $this->render_page('user/akademik/kalender/create_kalender_akademik', $data);	
	}

	private function tampil_kalender_keuangan(){
		$id 					= array('id_finance' => $this->session->userdata('id'));
		$data['keuangan'] 		= $this->user->query_where('tbl_user_keuangan', $id)->result();

		return $this->render_page('user/akademik/kalender/create_kalender_akademik', $data);
	}

	private function tampil_kalender_pemasaran(){
		$id 					= array('id_pemasaran' => $this->session->userdata('id'));
		$data['pemasaran'] 		= $this->user->query_where('tbl_user_pemasaran', $id)->result();

		return $this->render_page('user/akademik/kalender/create_kalender_akademik', $data);	
	}

	private function tampil_kalender_ict(){
		$id 				= array('id_ict' => $this->session->userdata('id'));
		$data['ict'] 		= $this->user->query_where('tbl_user_ict', $id)->result();

		return $this->render_page('user/akademik/kalender/create_kalender_akademik', $data);	
	}

	public function select_kalender(){
		$id = $this->input->get('th');
		$kode = $this->encryption->decrypt($id);
		
		$kalender = $this->user->query_all("SELECT * FROM tbl_kalender WHERE tahun_akademik = '$kode'")->result();
		$data = array(
					'kalender'=> $kalender,
					'tahun_akademik1' => $kode
				);
		
		return $this->render_page("user/akademik/kalender/filter_kalender", $data);
	}

	public function export_kalender(){
		$th = $this->input->post('tahun_akademik');
		$fileName = $this->input->post('file', TRUE);
	 	$config['upload_path'] = './file_upload/akademik/kalender/'; 
	  	$config['file_name'] = $fileName;
	  	$config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
	  	$config['max_size'] = 100000;

	  	$this->load->library('upload', $config);
	  	$this->upload->initialize($config); 
	  	
	  	if (!$this->upload->do_upload('file')) {
		   	$error = array('error' => $this->upload->display_errors());
		   	// $this->session->set_flashdata('success', "Ada kesalahan dalam upload");
		   	redirect('perkuliahan/kalender_akademik'); 
	  	} else {
		   	$media = $this->upload->data();
		   	$inputFileName = './file_upload/akademik/kalender/'.$media['file_name'];
		   	try {
				    $inputFileType = IOFactory::identify($inputFileName);
			    	$objReader = IOFactory::createReader($inputFileType);
			    	$objPHPExcel = $objReader->load($inputFileName);
		   	} catch(Exception $e) {
		    		die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		   	}
		   	
		   	$sheet = $objPHPExcel->getSheet(0);
		   	$highestRow = $sheet->getHighestRow();
		   	$highestColumn = $sheet->getHighestColumn();
		   	for ($row = 2; $row < $highestRow ; $row++){  
		     	$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
		     	$tgl_mulai 	= PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][0], 'YYYY-MM-DD');
		     	$tgl_selesai = PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][1], 'YYYY-MM-DD');
	    
		   		$data = array(
				     "id_ka"        		=> NULL,
				     "tgl_mulai"       		=> $tgl_mulai,	
				     "tgl_selesai"     		=> $tgl_selesai,
				     "ket1"      			=> $rowData[0][2],
				     "ket2"      			=> $rowData[0][3],
				     "tahun_akademik"		=> $th
		    	);
				$this->user->Add_Query("tbl_kalender", $data); 
	   		}
	   		$this->session->set_flashdata('success', "Data kalender berhasil di export");
  			redirect('perkuliahan/kalender_akademik'); 
  		}
	}
}