<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('akses') != TRUE) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	// public function index(){

	// }

	public function sk_mengajar(){
		$id_dosen 	= $this->session->userdata('username');
		if($this->session->userdata('level')==2):
			$jadwal 	= $this->user->get_data("*", "tbl_jadwal", "WHERE username_dosen = '$id_dosen' GROUP BY id_tahunakademik");
			$data = array(
				'tahun_akademik' => $jadwal,
			);
			return $this->render_page("user/dosen/dokumen/sk/tampil_sk", $data);
		elseif($this->session->userdata('level')==6):
			$jadwal 	= $this->user->get_data("*", "tbl_jadwal", "GROUP BY id_tahunakademik");
			$data = array(
				'tahun_akademik' 	=> $jadwal,
				'get_dosen' 		=> $this->user->query_all("SELECT * FROM tbl_dosen ORDER BY dosen")->result(),
			);
			return $this->render_page("user/dosen/dokumen/sk/tampil_sk", $data);
		endif;
	}

	public function tambah_sk(){
		$file 	= $_FILES['file_sk']['name'];
    	$ext 	= pathinfo($file, PATHINFO_EXTENSION);

		$th 	= $this->encryption->decrypt($this->input->post('tahun_akademik', TRUE));
		$dosen 	= $this->input->post('dosen', TRUE);

	  	$config = array(
    				'allowed_types'	=> "pdf",
    				'upload_path'	=> "./file_upload/dosen/dokumen/",
    				'overwrite'		=> TRUE,
    				'max_size'		=> 20000,
    				'remove_spaces'	=> TRUE,
    				'encrypt_name'	=> TRUE
    			);

	  	print_r($ext);
	  	return false;
		
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
	  	
	  	if (!$this->upload->do_upload('file_sk')) {
		   	$error = array('error' => $this->upload->display_errors());
		   	// $this->session->set_flashdata('success', "Ada kesalahan dalam upload");
		   	redirect('perkuliahan/dokumen/sk_mengajar'); 
	  	} else {
	  		if($ext == "pdf"):
        		$kode_matkul =$this->input->post('matkul');
        		$cek = count($this->user->get_data('*', 'tbl_modulajar', "where kode_matkul = '$kode_matkul'"));

        		$upload_data = $this->upload->data('file_sk');
        		if ($cek <=0):
        			$data = array(
						'id_modul'			=> NULL,
						'kode_matkul' 		=> $this->input->post('matkul'),
						'username_dosen' 	=> $this->session->userdata('username'),
						'file'				=> $upload_data
					);
					$this->user->Add_Query('tbl_sk_ajar', $data);
					$this->session->set_flashdata('success', "Data berhasil ditambahkan");
				else:
					$this->session->set_flashdata('warning', 'Gagal Menyimpan Modul');
				endif;
        	else:
        		$this->session->set_flashdata('error', "Format file tidak didukung");
        	endif;
	   		
  			redirect('perkuliahan/dokumen/sk_mengajar'); 
  		}
	}

	public function tampil_sk_mengajar(){
		$id 	= $this->input->get('th');
		$dec 	= $this->encryption->decrypt($id);
		print_r($dec);
		return false;
	}
}