<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluasi_umpan_balik extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('akses') != TRUE) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		$id_dosen = $this->session->userdata('username');
		$data = array(
				'tahun_akademik' 	=> $this->user->get_data("*", "tbl_tahunakademik", "ORDER BY tahun_akademik"),
				'id_dosen'			=> $id_dosen,
				'filter_ta' 		=> $this->user->filter_tahun_akademik_dosen($id_dosen)->result(),
			);
		return $this->render_page("user/dosen/penilaian/eub", $data);
	}

	public function tampil_eub(){
		$th 		= $this->input->get('th');
		$id_dosen 	= $this->session->userdata('username');
		$convert 	= $this->encryption->decrypt($th);
		
		$tahun 		= $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$convert'");
		foreach ($tahun as $key => $value){}
		$h1 		= $this->user->select_eub_berdasarkan_username_dosen($id_dosen, $convert, "filter");

		$tot = 0;
		foreach($h1 as $key1 => $vh):
			$tot += $vh->imk;
		endforeach;

		$total_keseluruhan = $tot;
		$indeks = count($h1);
		$kalkulasi = $total_keseluruhan / $indeks;
		$keterangan = $this->keterangan_penilaian_eub($kalkulasi);

		$data = array(
				'kode' 				=> $value['tahun_akademik'],
				'tahun_akademik' 	=> $this->user->get_data("*", "tbl_tahunakademik", "ORDER BY tahun_akademik"),
				'eub' 				=> $this->user->select_eub_berdasarkan_username_dosen($id_dosen, $convert, "filter"),
				'filter_ta' 		=> $this->user->filter_tahun_akademik_dosen($id_dosen)->result(),
				'totttt'			=> $total_keseluruhan,
				'indeks'			=> $kalkulasi,
				'keterangan_nilai'  => $keterangan,
				'id_dosen'			=> $id_dosen,
				'thn' 				=> $convert
			);

		return $this->render_page("user/dosen/penilaian/select_eub", $data);
	}

	private function keterangan_penilaian_eub($nilai){
		if($nilai >= 3.75 && $nilai <= 4):
			$ket = "Pengajaran anda sudah sangat baik, Pertahankan";
		elseif($nilai >= 3.5 && $nilai < 3.75):
			$ket = "Pengajaran anda sudah baik, Pertahankan";
		elseif($nilai >= 3 && $nilai < 3.5):
			$ket = "Pengajaran anda cukup baik, pertahankan dan tingkatkan lagi";
		elseif($nilai >= 2.7 && $nilai < 3 ):
			$ket = "Pengajaran anda tidak cukup baik dalam mengajar";
		elseif($nilai >= 0 && $nilai < 2.7):
			$ket = "Pengajaran anda masih jauh dari harapan baik";
		else:
			$ket = "Melebihi batas maksimal";
		endif;
		return $ket;
	}

	public function detail_eub(){
		$id_dosen 	= $this->session->userdata('username');
		$kode 		= $this->input->get("detail");
		$dec 		= $this->encryption->decrypt($kode);
		$change 	= str_replace('||','+',$dec);
		$change2 	= str_replace('_','-',$change);

		$get_idkelas  = strtok($change2, '-'); //mendapatkan id kelasnya
		$get_idth = substr($change2, strpos($change2, "+") + 1);//mendapatkan id tahun akademik

		$perubahan  = strtok($change2, '+');
		$get_idjadwal = substr($perubahan, strpos($perubahan, "-") + 1); //mendapatkan id_jadwal    
		
		$quesioner = $this->user->get_data("*", "tbl_quesioner");
		foreach ($quesioner as $key3 => $ques) {}

		$tahun 		= $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik = '$get_idth'");
		foreach ($tahun as $key => $value){}

		$toto = $this->user->detail_eub_model($id_dosen, $get_idth, $get_idkelas)->result();
		foreach($toto as $ket =>$va):
		endforeach;

		$data = array(
				'kode' 			=> $value['tahun_akademik'],
				'kelas' 		=> $get_idkelas,
				'quesioner' 	=> $quesioner,
				'quesi'			=> $ques,
				'_detail_eub' 	=> $toto, 
				'p1' 			=> $va->tp1,
				'p2' 			=> $va->tp2,
				'p3' 			=> $va->tp3,
				'p4' 			=> $va->tp4,
				'p5' 			=> $va->tp5,
				'p6' 			=> $va->tp6,
				'p7' 			=> $va->tp7,
				'p8' 			=> $va->tp8,
				'p9' 			=> $va->tp9,
				'p10' 			=> $va->tp10,
				'r1' 			=> $va->r1,
				'r2' 			=> $va->r2,
				'r3' 			=> $va->r3,
				'r4' 			=> $va->r4,
				'r5' 			=> $va->r5,
				'r6' 			=> $va->r6,
				'r7' 			=> $va->r7,
				'r8' 			=> $va->r8,
				'r9' 			=> $va->r9,
				'r10' 			=> $va->r10,
		);

		return $this->render_page("user/dosen/penilaian/detail_eub", $data);
	}

	public function detail_saran(){
		$kod 			= $this->input->get('ayam');
		$koe 			= $this->encryption->decrypt($kod);
		
		$get_iddosen 	= $this->session->userdata('username');
		$get_idkelas 	= strtok($koe, '_');
		$get_idta		= substr($koe, strpos($koe, "_") + 1);

		$get_ta 		= $this->user->get_data("*", "tbl_tahunakademik", "WHERE id_tahunakademik ='$get_idta'");
		$get_saran 		= $this->user->get_saran($get_iddosen, $get_idta, $get_idkelas)->result();
		foreach($get_ta as $key => $value):endforeach;

		$data 			= array(
							'saran' 		=> $get_saran,
							'kode_kelas' 	=> $get_idkelas,
							'ta' 			=> $value['tahun_akademik']
						);
		
		return $this->render_page("user/dosen/penilaian/detail_saran", $data);
	}

}
