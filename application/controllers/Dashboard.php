<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('akses') != TRUE) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		if ($this->session->userdata('level')==2):
			return $this->tampil_dashboard_user_dosen(); // tampil dashboard dosen
		elseif ($this->session->userdata('level')==3): 
			return $this->tampil_dashboard_user_mahasiswa(); //tampil dashboard mahasiswa
		elseif ($this->session->userdata('level')==4):
			if($this->session->userdata('role')=="Sekretaris" || $this->session->userdata('role')=="Dekan"):
				return $this->tampil_dashboard_user_fakultas(); //tampil dashboard fakultas
			endif;
		elseif ($this->session->userdata('level')==5):
			return $this->tampil_dashboard_user_kaprodi(); //tampil dashboard kaprodi

		elseif ($this->session->userdata('level')==6):
			return $this->tampil_dashboard_user_akademik(); //tampil dashboard akademik

		elseif($this->session->userdata('level')==7):
			return $this->tampil_dashboard_user_keuangan();

		elseif($this->session->userdata('level')==8):
			return $this->tampil_dashboard_user_pemasaran();

		elseif($this->session->userdata('level')==9):
			return $this->tampil_dashboard_user_ict();
		endif;	
	}	

	private function tampil_dashboard_user_dosen(){
		$id = array('username_dosen' => $this->session->userdata('username'));
		$data = array(
					'modul_ajar' 	=> $this->user->query_where('tbl_modulajar', $id)->result(),
					'biodata_dosen'	=> $this->user->query_where('tbl_dosen', $id)->result(),
					'jadwal'		=> $this->user->query_where('tbl_jadwal', $id)->result() 
				);
		//$data['rps'] 			= $this->user->query_where('tbl_rps', $id)->result();		
		
		return $this->render_page('dashboard', $data);
	}

	private function tampil_dashboard_user_mahasiswa(){
		$data['biodata_mahasiswa'] 	= $this->user->query_where('tbl_mahasiswa', array('nim' => $this->session->userdata('nim')))->result();
		$data['prodi'] 				= $this->user->query_where('tbl_prodi', array('id_prodi' => $this->session->userdata('prodi')))->result();

		return $this->render_page('dashboard', $data);
	}

	private function tampil_dashboard_user_fakultas(){
		$id = array('id_fakultas' => $this->session->userdata('id_fakultas'));
		$data['user_fakultas'] 		= $this->user->query_where('tbl_user_fakultas', array('id_user_fakultas' => $this->session->userdata('id')))->result();
		$data['data_fakultas'] 		= $this->user->query_where('tbl_fakultas', $id)->result();
		$data['data_prodi'] 		= $this->user->query_where('tbl_programstudi', $id)->result();
		$data['rps'] 				= $this->user->query_where('tbl_rps', $id)->result();

		return $this->render_page('dashboard', $data);
	}

	private function tampil_dashboard_user_kaprodi(){
		$id = array(
					'id_kaprodi' => $this->session->userdata('id_kaprodi'),
					'id_fakultas' => $this->session->userdata('id_fakultas'),
					'id_programstudi' => $this->session->userdata('id_prodi'),
					'nama_kaprodi' => $this->session->userdata('nama'),
			 );
		$id2 = array(
					'id_fakultas' => $this->session->userdata('id_fakultas'),
			 	);

		$data['kaprodi'] 		= $this->user->query_where('tbl_user_kaprodi', $id)->result();
		$data['rps'] 			= $this->user->query_where('tbl_rps', $id2)->result();
		return $this->render_page('dashboard', $data);
	}

	private function tampil_dashboard_user_akademik(){
		$id = array('id_user_akademik' => $this->session->userdata('id'));

		$data['akademik'] = $this->user->query_where('tbl_user_akademik', $id)->result();
		return $this->render_page('dashboard', $data);
	}

	private function tampil_dashboard_user_keuangan(){
		$id = array('id_finance' => $this->session->userdata('username'));
		$data = array(
					'keuangan' 		 => $this->user->query_where('tbl_user_keuangan', $id)->result(),
				);
		return $this->render_page('dashboard', $data);
	}

	private function tampil_dashboard_user_pemasaran(){
		$id = array('id_pemasaran' => $this->session->userdata('username'));
		$data = array(
					'pemasaran' 		 => $this->user->query_where('tbl_user_pemasaran', $id)->result(),
				);
		return $this->render_page('dashboard', $data);
	}

	private function tampil_dashboard_user_ict(){
		$id = array('id_ict' => $this->session->userdata('username'));
		$data = array(
					'ict' 		 => $this->user->query_where('tbl_user_ict', $id)->result(),
				);

		// print_r($id);	
		// return false;
		return $this->render_page('dashboard', $data);
	}

	public function edit_user(){
		if ($this->session->userdata('level')==2):
			$this->edit_user_dosen();
		elseif ($this->session->userdata('level')==3):
			$this->edit_user_mahasiswa();
		endif;
		return redirect('dashboard');
	}

	private function edit_user_dosen(){
		$data = array(
				'dosen'			=> $this->input->post('nama'),
				'gelar' 		=> $this->input->post('gelar'),
				'jenis_kelamin'	=> $this->input->post('jk'),
				'jafung' 		=> $this->input->post('jafung'),
				'jenjang' 		=> $this->input->post('jenjang'),
				'email' 		=> $this->input->post('email'),
				'telp' 			=> $this->input->post('no'),
				'alamat' 		=> $this->input->post('alamat')
		);
		$where = array(
				'nidn' 			=> $this->input->post('nidn')
		);

		$this->user->Update_Query('tbl_dosen', $data, $where);
		$this->session->set_flashdata('success', "Data anda berhasil diubah");
	}

	private function edit_user_mahasiswa(){
		// print_r($_POST);
		$data = array(
				'nama'			=> $this->input->post('nama'),
				'nik'			=> $this->input->post('nik_mahasiswa'),
				'tempat_lahir'	=> $this->input->post('tempat_lahir'),
				'tgl_lahir2'		=> $this->input->post('tgl'),
				'jenis_kelamin'	=> $this->input->post('jk'),
				'agama'			=> $this->input->post('agama'),
				'telp'			=> $this->input->post('no'),
				'alamat'		=> $this->input->post('alamat'),
				'nik_ayah'		=> $this->input->post('nik_ayah'),
				'nik_ibu'		=> $this->input->post('nik_ibu'),
				'nama_ayah'		=> $this->input->post('nama_ayah'),
				'nama_ibu'		=> $this->input->post('nama_ibu')
		);
		$this->user->Update_Query('tbl_mahasiswa', $data, array('nim' => $this->input->post('nim')));
		$this->session->set_flashdata('success', "Data anda berhasil diubah");

	}

	public function cari_matkul(){
        $kode	= $this->input->post('search_data', TRUE);
		$query 	= $this->user->cari_($kode);
  		$json 	= array();
  		
  		foreach ($query as $key => $value) {
  			if ($value->kode_matkul==""):
  				echo "Mata kuliah tidak ditemukan";
  			else:
  				$words = explode(" ", $value->prodi);
				$acronym = "";

				foreach ($words as $w) {
  					$acronym .= $w[0];
				}
  				$json[] = $value->kode_matkul.' || '.$acronym.' || '.$value->nama_matkul;
  				// $json[] = $value->nama_matkul;
  			endif;
  		} 
  		echo json_encode($json);
    }

    public function cari_dosen(){
		$kode	= $this->input->post('search_data', TRUE);
		$query 	= $this->user->cari_dosen($kode);
  		$json 	= array();
  		foreach ($query as $key => $value) {
  			$json[] = $value->dosen;
  		} 
  		echo json_encode($json);
	}	

	public function cari_matkul2(){
		$kode	= $this->input->post('search_data', TRUE);
		
		if($this->session->userdata('id_kaprodi')=='1'):
			$this->cari_matkul_berdasarkan_prodi($kode, 1);
		elseif($this->session->userdata('id_kaprodi')=='2'):
			$this->cari_matkul_berdasarkan_prodi($kode, 2);
		elseif($this->session->userdata('id_kaprodi')=='3'):
			$this->cari_matkul_berdasarkan_prodi($kode, 3);
		endif;
    }

	function cari_matkul_berdasarkan_prodi($kode, $id){	
		$query 	= $this->user->cari_2($kode, $id);
  		$json 	= array();
  		foreach ($query as $key => $value) {
  			if ($value->kode_matkul==""):
  				echo "Mata kuliah tidak ditemukan";
  			else:
  				$json[] = $value->nama_matkul;
  			endif;
  		} 
  		echo json_encode($json);
	}

	    public function upload_file(){
		if ($this->session->userdata('upload_file')==1 || $this->session->userdata('upload_file')==2):
			$this->upload_modul();
		elseif($this->session->userdata('upload_file')==1 || $this->session->userdata('upload_file')==3):
			$this->upload_rps();
		endif;
    }

    function upload_modul(){
    	$file = $_FILES['upload_file']['name'];
    	$ext = pathinfo($file, PATHINFO_EXTENSION);

    	$config = array(
    				'allowed_types'	=> "doc|docx|xls|xlsx|ppt|pptx|pdf|rar|zip",
    				'upload_path'	=> "./file_upload/modul/",
    				'overwrite'		=> TRUE,
    				'max_size'		=> 10000,
    				'remove_spaces'	=> TRUE,
    				'encrypt_name'	=> TRUE
    			);

        /*$config['upload_path']       = './file_upload/modul/';
		$config['allowed_types']     = 'doc|docx|xls|xlsx|ppt|pptx|pdf|rar|zip';
		$config['overwrite']         = true;
		$config['max_size']          = 10000; // 10MB
		$config['remove_spaces'] 	 = TRUE;
		$config['encrypt_name'] 	 = TRUE;*/

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_file')):
            $this->session->set_flashdata('error', $this->upload->display_errors());
            
        else:
        	if ($this->session->userdata('level')==1 || $this->session->userdata('level')==2):
        		if($ext == "doc" || $ext == "docx" || $ext == "xls" || $ext == "xlsx" || $ext == "ppt" || $ext == "pptx" || $ext == "pdf" || $ext == "rar" || $ext == "zip"):
        			
        			// $search_matkul = $this->input->post('search_data');
        			// // $matkul = $this->user->get_data('*', 'tbl_matakuliah', "where nama_matkul = '$search_matkul'")[0];
        			$kode_matkul =$this->input->post('matkul');
        			$cek = count($this->user->get_data('*', 'tbl_modulajar', "where kode_matkul = '$kode_matkul'"));

        			$upload_data = $this->upload->data('file_name');
        			if ($cek <=0):
        				$data = array(
							'id_modul'			=> NULL,
							'kode_matkul' 		=> $this->input->post('matkul'),
							'username_dosen' 	=> $this->session->userdata('username'),
							'file'				=> $upload_data
						);
						$this->user->Add_Query('tbl_modulajar', $data);
						$this->session->set_flashdata('success', 'Modul berhasil ditambahkan');
					else:

						$this->session->set_flashdata('warning', 'Gagal Menyimpan Modul');
					endif;
        		else:
        			$this->session->set_flashdata('error', "Format file tidak didukung");
        		endif;
			else:
				$this->session->set_flashdata('error', "Error 404 Not Found");
        	endif;
        endif;
        return redirect('dashboard/tampil_modulbahanajar'); 	
    }

	function upload_rps(){

		date_default_timezone_set('Asia/Karachi'); # add your city to set local time zone
        $now    = date('Y-m-d H:i:s');

		$file = $_FILES['upload_file']['name'];
    	$ext = pathinfo($file, PATHINFO_EXTENSION);

    	$config = array(
    		'upload_path'			=> './file_upload/rps/',
    		'allowed_types'			=> 'pdf',
    		'overwrite'				=> TRUE,
    		'max_size'				=> 2000,
    		'remove_spaces'			=> TRUE,
    		'encrypt_name'			=> TRUE
    	);

    	$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_file')):
            $this->session->set_flashdata('error', $this->upload->display_errors());
            
        else:
        	if ($this->session->userdata('level')==5):
        		if($ext == "pdf"):
        			$search_matkul = $this->input->post('search_data');
        			$matkul = $this->user->get_data('*', 'tbl_matakuliah', "where nama_matkul = '$search_matkul'")[0];
        	
        			$upload_data = $this->upload->data('file_name');
        			$kode_matkul = $matkul['kode_matkul'];
        			$cek = count($this->user->get_data('*', 'tbl_rps', "where kode_matkul = '$kode_matkul'"));
			
        			if ($cek <=0) {
        				$data = array(
						'id_rps'			=> NULL,
						'id_kaprodi'		=> $this->session->userdata('id_kaprodi'),
						'id_fakultas'		=> $this->session->userdata('id_fakultas'),
						'id_programstudi'	=> $this->session->userdata('id_prodi'),
						'kode_matkul' 		=> $matkul['kode_matkul'],
						'file'				=> $upload_data,
						'waktu_buat'		=> $now
					);
					$this->user->Add_Query('tbl_rps', $data);
					$this->session->set_flashdata('success', 'Modul berhasil ditambahkan');
        			}else{
        				$this->session->set_flashdata('warning', 'Modul sudah ada');
        			}
        			
        		else:
        			$this->session->set_flashdata('error', "Format file tidak didukung");
        		endif;
			else:
				$this->session->set_flashdata('error', "Error 404 Not Found");
        	endif;
        endif;
        return redirect('dashboard/tampil_modulRPS');
	}

	public function tambah_detail_RPS_Detail(){
		date_default_timezone_set('Asia/Karachi'); # add your city to set local time zone
        $now    = date('Y-m-d H:i:s');

		$search_matkul = $this->input->post('matkul');
    	$matkul = $this->user->get_data('*', 'tbl_matakuliah', "where nama_matkul = '$search_matkul'")[0];

		$search_dosen = $this->input->post('search_data');
    	$dosen_ = $this->user->get_data('*', 'tbl_dosen', "where dosen = '$search_dosen'")[0];

    	$username_dosen = $dosen_['username_dosen'];
    	$kode_matkul = $matkul['kode_matkul'];
    	$ts1 = $this->user->get_data('*', 'tbl_rpsdetail', "where kode_matkul = '$kode_matkul'");
    		
    	$c = 0;
    	foreach ($ts1 as $key => $tss) {
    		if ($tss['username_dosen'] == $username_dosen  ) {
    			$c++;
    		}
    	}
    	if ($c <= 0) {
    		$data = array(
				'id_rpsdetail' 			=> NULL,
				'id_rps'				=> $this->input->post('id'),
				'kode_matkul'			=> $matkul['kode_matkul'],
				'username_dosen'		=> $dosen_['username_dosen'],
				'waktu_buat'			=> $now
		);
		$this->user->Add_Query('tbl_rpsdetail', $data);
		$this->session->set_flashdata('success', 'Berhasil ditambahkan');
    	}else{
    		$this->session->set_flashdata('warning', 'Dosen telah terdaftar');
    	}
		
		return redirect('dashboard/tampil_modulRPS');
	}

    public function edit_file(){
		if($this->session->userdata('edit_file')==1 || $this->session->userdata('edit_file')==2):
			$this->edit_modulajar();
		elseif($this->session->userdata('edit_file')==1 || $this->session->userdata('edit_file')==3):
			$this->edit_modulRPS();	
		endif;
	}

	function edit_modulajar(){
		$file = $_FILES['upload_file']['name'];
    	$ext = pathinfo($file, PATHINFO_EXTENSION);

    	$config = array(
    		'upload_path'			=> './file_upload/modul/',
    		'allowed_types'			=> 'doc|docx|xls|xlsx|ppt|pptx|pdf|rar|zip',
    		'overwrite'				=> TRUE,
    		'max_size'				=> 10000,
    		'remove_spaces'			=> TRUE,
    		'encrypt_name'			=> TRUE
    	);
        
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_file')):
            $this->session->set_flashdata('error', $this->upload->display_errors());
            
        else:
        	if ($this->session->userdata('level')==5 || $this->session->userdata('level')==2):
        		if($ext == "doc" || $ext == "docx" || $ext == "xls" || $ext == "xlsx" || $ext == "ppt" || $ext == "pptx" || $ext == "pdf" || $ext == "rar" || $ext == "zip"):
        			$search_matkul = $this->input->post('search_data');
					$matkul = $this->user->get_data('*', 'tbl_matakuliah', "where nama_matkul = '$search_matkul'")[0];
					$kode_matkul = $matkul['kode_matkul'];
					$modul = $this->user->get_data('*', 'tbl_modulajar', "where kode_matkul = '$kode_matkul'")[0];
        			
        			unlink("file_upload/modul/".$modul['file']);

        			$upload_data = $this->upload->data('file_name');
        			$data = array(
						'username_dosen' 	=> $this->session->userdata('username'),
						'file'				=> $upload_data
					);
					$where = array('kode_matkul' => $matkul['kode_matkul'],);

					$this->session->set_flashdata('success', 'Modul berhasil diubah');
					$this->user->Update_Query('tbl_modulajar', $data, $where);
        		else:
        			$this->session->set_flashdata('error', "Format file tidak didukung");
        		endif;
			else:
				$this->session->set_flashdata('error', "Error 404 Not Found");
        	endif;
        endif;
        return redirect('dashboard/tampil_modulbahanajar');
	}

	function edit_modulRPS(){
		date_default_timezone_set('Asia/Karachi'); # add your city to set local time zone
        $now    = date('Y-m-d H:i:s');

		$file = $_FILES['upload_file']['name'];
    	$ext = pathinfo($file, PATHINFO_EXTENSION);

    	$config = array(
    		'upload_path'			=> './file_upload/rps/',
    		'allowed_types'			=> 'pdf',
    		'overwrite'				=> TRUE,
    		'max_size'				=> 2000,
    		'remove_spaces'			=> TRUE,
    		'encrypt_name'			=> TRUE
    	);

    	$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('upload_file')):
            $this->session->set_flashdata('error', $this->upload->display_errors());
            
        else:
        	if ($this->session->userdata('level')==5):
        		if($ext == "pdf"):
        			$search_matkul = $this->input->post('search_data');
        			$matkul = $this->user->get_data('*', 'tbl_matakuliah', "where nama_matkul = '$search_matkul'")[0];
        	
        			$upload_data = $this->upload->data('file_name');
        			$data = array(
						'kode_matkul' 		=> $matkul['kode_matkul'],
						'file'				=> $upload_data,
						'waktu_edit'		=> $now
					);
					$where = array('kode_matkul' => $matkul['kode_matkul'],);

					$this->session->set_flashdata('success', 'Modul RPS berhasil diubah');
					$this->user->Update_Query('tbl_rps', $data, $where);
        		else:
        			$this->session->set_flashdata('error', "Format file tidak didukung");
        		endif;
			else:
				$this->session->set_flashdata('error', "Error 404 Not Found");
        	endif;
        endif;
        return redirect('dashboard/tampil_modulRPS');
	}

	public function delete_($id){
		if($this->session->userdata('hapus_file')==1 || $this->session->userdata('hapus_file')==2):
			$this->hapus_modulbahanajar($id);
		elseif($this->session->userdata('hapus_file')==1 || $this->session->userdata('hapus_file')==3):
			$this->hapus_modulRPS($id);
		elseif($this->session->userdata('hapus_file')==1 || $this->session->userdata('hapus_file')==4):
			$this->hapus_file_RPSDosen();
		endif;
	}

	function hapus_modulbahanajar($id){
		$modul = $this->user->get_data('*', 'tbl_modulajar', "where id_modul = '$id'")[0];
		unlink("file_upload/modul/".$modul['file']);
		
		$this->user->Delete_Query('tbl_modulajar', array('id_modul' => $id));
		$this->session->set_flashdata('success', "Modul berhasil dihapus");
		return redirect('dashboard/tampil_modulbahanajar');
	}

	function hapus_modulRPS($id){
		$modul = $this->user->get_data('*', 'tbl_rps', "where id_rps = '$id'")[0];
		unlink("file_upload/modul/".$modul['file']);
		
		$this->user->Delete_Query('tbl_rps', array('id_rps' => $id));
		$this->session->set_flashdata('success', "Modul RPS berhasil dihapus");
		return redirect('dashboard/tampil_modulRPS');
	}

	//ini untuk fakultas
	function hapus_file_RPSDosen(){
		$search_dosen = $this->input->post('search_data');
    	$dosen_ = $this->user->get_data('*', 'tbl_dosen', "where username_dosen = '$search_dosen'")[0];

    	$search_matkul = $this->input->post('matkul');
    	$matkul = $this->user->get_data('*', 'tbl_matakuliah', "where nama_matkul = '$search_matkul'")[0];

    	$a = $this->input->post('id');
    	$b = $matkul['kode_matkul'];
    	$c = $dosen_['username_dosen'];

    	$mtk = $this->user->get_data("*", "tbl_rpsdetail", "where id_rps='$a' AND kode_matkul='$b' AND username_dosen='$c'")[0];

    	$id = $mtk['id_rpsdetail'];

    	$this->user->Delete_Query('tbl_rpsdetail', array('id_rpsdetail' => $id));
		$this->session->set_flashdata('success', "Data berhasil dihapus");
		return redirect('dashboard/tampil_modulRPS');
	}
}