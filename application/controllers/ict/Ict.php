<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ict extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if ($this->session->userdata('akses') != TRUE) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->library('pdfgenerator');
	}

	public function index(){
		$id 				= array('id_ict' => $this->session->userdata('id'));
		$data['ict'] 		= $this->user->query_where('tbl_user_ict', $id)->result();

		return $this->render_page('user/ict/_cetak_ktm', $data);
	}

	public function tampil_mhs(){
		$th 		= $this->input->get('th');
		$dec 		= $this->encryption->decrypt($th);
		$get_mhs 	= $this->user->query_where("tbl_mahasiswa", "tahun_akademik = '$dec'")->result();
		foreach ($get_mhs as $key => $value) {

		}
		$data = array(
				'dec' 	  => $dec,
				'get_mhs' => $get_mhs,
				'x' 	  => $get_mhs[0],
				'thn_akad'=> $dec,
				'nim1' 	  => $value->nim
			);
		// $get_foto	= $this->user->query_where("tbl_mahasiswa_foto", "nim= '$data[x]'")->result();
		
		// return false;
		return $this->render_page('user/ict/_cetak_ktm2', $data);
	}

	public function cetak_ktm(){
		$id 	= $this->input->get('id');
		$enc 	= $this->encryption->decrypt($id);

		// print_r($enc);
		// return false;
		$ayam = array(
				'data_mhs' => $this->user->query_where("tbl_mahasiswa", "nim = '$enc'")->result(),
			);
		$x = $ayam['data_mhs'][0];
		
		$data = array(
					'x' => $x,
					'get_mhs_foto' 	=> $this->user->query_where("tbl_mahasiswa_foto", "nim = '$enc'")->result(),
					'title_pdf' => 'Cetak KTM'
				);
        return $this->load->view('user/ict/ktm', $data);
		// $html = $this->load->view('user/ict/ktm', $data, true);    
		// // $html = "ayam";
		
		// // filename dari pdf ketika didownload
        // $file_pdf = 'KTM_Mahasiswa';
        // // setting paper
        // $paper = 'F4';
        // //orientasi paper potrait / landscape
        // $orientation = "portrait";
        
        // $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);
	}

	public function upload_capture_foto_mahasiswa(){
		
	}

	public function upload_foto_mahasiswa(){
		date_default_timezone_set('Asia/Karachi'); # add your city to set local time zone
        $now    = date('Y-m-d H:i:s');

		$nim 	= $this->input->post('nim');
		$file 	= $_FILES['file_foto']['name'];
    	$ext 	= pathinfo($file, PATHINFO_EXTENSION);

		$config = array(
    				'allowed_types'	=> "jpg|jpeg|png|",
    				'upload_path'	=> "./file_upload/mahasiswa/foto/",
    				'overwrite'		=> TRUE,
    				'max_size'		=> 2000,
    				'remove_spaces'	=> TRUE,
    				'encrypt_name'	=> TRUE
    			);

		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('file_foto')):
            $this->session->set_flashdata('error', $this->upload->display_errors());
            // $this->session->set_flashdata('error', "Ukuran modul terlalu besar");
        else:
        	if($this->session->userdata('level')==9):
				if($ext=="jpg" || $ext=="jpeg" || $ext=="png"):
					$cek_nim = count($this->user->get_data("*", "tbl_mahasiswa_foto", "WHERE nim = '$nim'"));
					if($cek_nim<=0):
						$data = array(
								'id_foto' 		=> 	NULL,
								'nim'			=>	$nim,
								'file_foto' 	=> 	$this->upload->data('file_name'),
								'created_by'	=> 	$now
						);

						$this->user->Add_Query('tbl_mahasiswa_foto', $data);
						$this->session->set_flashdata('success', 'Foto berhasil ditambahkan');
					endif;
				else:
					$this->session->set_flashdata('error', "Format file tidak didukung");
				endif;
			endif;
        endif;
        return redirect('ict/ict/tampil_mhs');

		// print_r($nim);
		// print_r($file);
		// print_r($ext);

		// $this->load->library('upload', $config);
		// $this->upload->initialize($config);

		// $cek_data = $this->user->get_data("*", "tbl_foto", "WHERE nim = '$nim'");
	}

	public function detail_mhs(){
		echo "ayam";
	}
}