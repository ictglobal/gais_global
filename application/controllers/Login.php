<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
	}
	
	public function index()	{
		if ($this->session->userdata('akses')) {
			return redirect('dashboard');
		}else{
			return $this->load->view('login');	
		}
	}
	public function login_validate(){
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|max_length[255]');
	}

	public function validasi(){
		$this->login_validate();
		if($this->form_validation->run() == FALSE):
			// $this->session->set_flashdata('error', "Gak bisa masuk");
			redirect(base_url('login'));
		else:
			$this->ceklogin2();
		endif;
	}

	public function ceklogin2(){
		//$pass = hash("sha256", sha1($this->input->post('password')));
		$user 				= $this->input->post('username');
		$pass 				= $this->input->post('password');

		$user_dosen 	= $this->log_user('username_dosen', $user, 'password', $pass, 'tbl_dosen');
		$user_mahasiswa = $this->log_user('nim', $user, 'pass', $pass, 'tbl_mahasiswa');
		$user_fakultas 	= $this->log_user('username', $user, 'pass', $pass, 'tbl_user_fakultas');
		$user_kaprodi 	= $this->log_user('username', $user, 'pass', $pass, 'tbl_user_kaprodi');
		$user_akademik 	= $this->log_user('username', $user, 'pass', $pass, 'tbl_user_akademik');
		$user_keuangan	= $this->log_user('username', $user, 'password', $pass, 'tbl_user_keuangan');
		$user_pemasaran = $this->log_user('username', $user, 'password', $pass, 'tbl_user_pemasaran');
		$user_ict 		= $this->log_user('username', $user, 'password', $pass, 'tbl_user_ict');

		if ($user_dosen->num_rows() > 0):
			$data = $user_dosen->row_array();
			if ($data['role']==2):
				$data  = array(
					'akses'					=> TRUE,
					'level'					=> 2,
                    'nidn'        			=> $user_dosen->row()->nidn,
                    'username'  			=> $user_dosen->row()->username_dosen,
                    'nama'		    		=> $user_dosen->row()->dosen,
                    'role'					=> "Dosen",
                    'edit_profil'			=> 2,
                    'edit_file'				=> 2,
					'hapus_file'			=> 2,
					'upload_file'			=> 2,
					'modul_matkul'			=> 1, //akses modul bahan ajar dosen
					'modul_rps'				=> 1, //akses modul RPS dosen 
            	);
            	$this->session->set_userdata($data);
            	$this->session->set_flashdata('success', "Selamat Datang"." ".$this->session->userdata('nama'));
				return redirect('dashboard'); 
			endif;
		elseif ($user_mahasiswa->num_rows() > 0):
        	$data = $user_mahasiswa->row_array();
        	if ($data['role']==3):
				$data  = array(
					'akses'					=> TRUE,
					'level'					=> 3,
                    'nim'        			=> $user_mahasiswa->row()->nim,
                    'nama'  				=> $user_mahasiswa->row()->nama,
                    'alamat'				=> $user_mahasiswa->row()->alamat,
                    'prodi'					=> $user_mahasiswa->row()->id_prodi,
                    'role'					=> "Mahasiswa",
                    'edit_profil'			=> 3
            	);
            	$this->session->set_userdata($data);
            	$this->session->set_flashdata('success', "Selamat Datang"." ".$this->session->userdata('nama'));
				return redirect('dashboard'); 
			endif;

		elseif ($user_fakultas->num_rows() > 0):
			$data = $user_fakultas->row_array();
			if ($data['role']==4):
				if ($data['jabatan']=="Sekretaris"):
            		$data  = array(
						'akses'					=> TRUE,
						'level'					=> 4,
						'level2'				=> 1,
                   		'id'        			=> $user_fakultas->row()->id_user_fakultas,
                   		'id_fakultas'			=> $user_fakultas->row()->id_fakultas,
                   		'nama_fakultas'			=> $user_fakultas->row()->nama_fakultas,
                   		'nama'					=> $user_fakultas->row()->nama,
                   		'jabatan'				=> $user_fakultas->row()->jabatan,
                   		'telp'					=> $user_fakultas->row()->telp,
                   		'nik'					=> $user_fakultas->row()->nik,
                   		'user'					=> $user_fakultas->row()->username,
                   		'edit_profil'			=> 4,
						'edit_file'				=> 4,
						'hapus_file'			=> 4,
                   		'role'				    => "Sekretaris",
                   		'modul_rps'				=> 2 //akses modul RPS Sekretaris 
            		);
            	elseif ($data['jabatan']=="Dekan"):
            		$data  = array(
						'akses'					=> TRUE,
						'level'					=> 4,
						'level2'				=> 2,
                   		'id'        			=> $user_fakultas->row()->id_user_fakultas,
                   		'id_fakultas'			=> $user_fakultas->row()->id_fakultas,
                   		'nama'					=> $user_fakultas->row()->nama,
                   		'jabatan'				=> $user_fakultas->row()->jabatan,
                   		'telp'					=> $user_fakultas->row()->telp,
                   		'nik'					=> $user_fakultas->row()->nik,
                   		'user'					=> $user_fakultas->row()->username,
                   		'edit_profil'			=> 4,
						'edit_file'				=> 4,
                   		'role'				    => "Dekan",
                   		'modul_rps'				=> 3 //akses modul RPS Dekan 
            		);
            	endif;
            	$this->session->set_userdata($data);
            	$this->session->set_flashdata('success', "Selamat Datang"." ".$this->session->userdata('nama'));
				return redirect('dashboard');
			endif;	

		elseif ($user_kaprodi->num_rows() > 0):
        	$data = $user_kaprodi->row_array();
        	if ($data['role']==5):
				$data  = array(
					'akses'					=> TRUE,
					'level'					=> 5,
                    'id'        			=> $user_kaprodi->row()->id_kaprodi,
                    'id_fakultas'			=> $user_kaprodi->row()->id_fakultas,
                    'id_prodi'				=> $user_kaprodi->row()->id_programstudi,
                    'nama'					=> $user_kaprodi->row()->nama_kaprodi,
                    'role'					=> "Kaprodi",
                    'edit_profil'			=> 5,
					'edit_file'				=> 3,
					'hapus_file'			=> 3,
					'upload_file'			=> 3,
					'modul_rps'				=> 4 //akses modul RPS Kaprodi 
            	);
            	$this->session->set_userdata($data);
            	$this->session->set_flashdata('success', "Selamat Datang"." ".$this->session->userdata('nama'));
				return redirect('dashboard'); 
			endif;

		elseif ($user_akademik->num_rows() > 0):
			$data = $user_akademik->row_array();
			if ($data['role']==6):
				if ($data['jabatan']=="Wakil Rektor Akademik"):
            		$data  = array(
					'akses'					=> TRUE,
					'level'					=> 6,
                    'id'        			=> $user_akademik->row()->id_user_akademik,
                    'nama'					=> $user_akademik->row()->nama,
                    'jabatan'				=> $user_akademik->row()->jabatan,
                    'edit_profil'			=> 6,
                    'role'				    => "Warek BAAK",
                    'modul_rps'				=> 5 //akses modul RPS wakil rektor
            	);
            	elseif ($data['jabatan']=="Kepala BAAK"):
            		$data  = array(
            		'akses'					=> TRUE,
					'level'					=> 6,
                    'id'        			=> $user_akademik->row()->id_user_akademik,
                    'nama'					=> $user_akademik->row()->nama,
                    'jabatan'				=> $user_akademik->row()->jabatan,
                    'edit_profil'			=> 6,
                    'role'				    => "BAAK",
                    'modul_rps'				=> 6, //akses modul RPS kepala baak
                    
                    'fakultas'				=> 1,
                    'prodi'					=> 1,
                    'konsentrasi'			=> 1
            	);
            	elseif ($data['jabatan']=="Staff"):
            		$data  = array(
            		'akses'					=> TRUE,
					'level'					=> 6,
                    'id'        			=> $user_akademik->row()->id_user_akademik,
                    'nama'					=> $user_akademik->row()->nama,
                    'jabatan'				=> $user_akademik->row()->jabatan,
                    'edit_profil'			=> 6,
                    'role'				    => "Staff",
                    'modul_rps'				=> 7 //akses modul RPS ketua
            	);

            	endif;
            	$this->session->set_userdata($data);
            	$this->session->set_flashdata('success', "Selamat Datang"." ".$this->session->userdata('nama'));
				return redirect('dashboard');
			endif;
		
		elseif ($user_keuangan->num_rows() > 0):
			$data = $user_keuangan->row_array();
			if ($data['role']==7):
				if ($data['jabatan']=="Kepala Keuangan"):
            		$data  = array(
					'akses'					=> TRUE,
					'level'					=> 7,
                    'id'        			=> $user_keuangan->row()->id_finance,
                    'nama'					=> $user_keuangan->row()->petugas,
                    'jabatan'				=> $user_keuangan->row()->jabatan,
                    'edit_profil'			=> 7,
                    'role'				    => "Finance",
            	);
            	elseif ($data['jabatan']=="Staff Keuangan"):
            		$data  = array(
            		'akses'					=> TRUE,
					'level'					=> 7,
                    'id'        			=> $user_keuangan->row()->id_finance,
                    'nama'					=> $user_keuangan->row()->petugas,
                    'jabatan'				=> $user_keuangan->row()->jabatan,
                    'edit_profil'			=> 7,
                    'role'				    => "Finance",
            	);
            	elseif ($data['jabatan']=="Kasir"):
            		$data  = array(
            		'akses'					=> TRUE,
					'level'					=> 7,
                    'id'        			=> $user_keuangan->row()->id_finance,
                    'nama'					=> $user_keuangan->row()->petugas,
                    'jabatan'				=> $user_keuangan->row()->jabatan,
                    'edit_profil'			=> 7,
                    'role'				    => "Finance",
            	);
            	elseif ($data['jabatan']=="Akuntansi"):
            		$data  = array(
            		'akses'					=> TRUE,
					'level'					=> 7,
                    'id'        			=> $user_keuangan->row()->id_finance,
                    'nama'					=> $user_keuangan->row()->petugas,
                    'jabatan'				=> $user_keuangan->row()->jabatan,
                    'edit_profil'			=> 7,
                    'role'				    => "Finance",
            	);

            	endif;
            	$this->session->set_userdata($data);
            	$this->session->set_flashdata('success', "Selamat Datang"." ".$this->session->userdata('nama'));
				return redirect('dashboard');
			endif;

		elseif ($user_pemasaran->num_rows() > 0):
			$data = $user_pemasaran->row_array();
			if ($data['role']==8):
				if ($data['jabatan']=="Kepala Marketing"):
            		$data  = array(
					'akses'					=> TRUE,
					'level'					=> 8,
                    'id'        			=> $user_pemasaran->row()->id_pemasaran,
                    'nama'					=> $user_pemasaran->row()->nama_petugas,
                    'jabatan'				=> $user_pemasaran->row()->jabatan,
                    'edit_profil'			=> 8,
                    'role'				    => "Marketing",
            	);
            	elseif ($data['jabatan']=="Staff Marketing"):
            		$data  = array(
            		'akses'					=> TRUE,
					'level'					=> 8,
                    'id'        			=> $user_pemasaran->row()->id_pemasaran,
                    'nama'					=> $user_pemasaran->row()->petugas,
                    'jabatan'				=> $user_pemasaran->row()->jabatan,
                    'edit_profil'			=> 8,
                    'role'				    => "Marketing",
            	);

            	endif;
            	$this->session->set_userdata($data);
            	$this->session->set_flashdata('success', "Selamat Datang"." ".$this->session->userdata('nama'));
				return redirect('dashboard');
			endif;

		elseif ($user_ict->num_rows() > 0):
			$data = $user_ict->row_array();
			if ($data['role']==9):
				if ($data['jabatan']=="Kepala Biro Teknologi dan Sistem Informasi"):
            		$data  = array(
					'akses'					=> TRUE,
					'level'					=> 9,
                    'id'        			=> $user_ict->row()->id_ict,
                    'nama'  				=> $user_ict->row()->nama_petugas,
                    'username'				=> $user_ict->row()->username,
                    'role'					=> "ICT",
                    'edit_profil'			=> 9
            	);
            	elseif ($data['jabatan']=="Staff Biro Teknologi dan Sistem Informasi"):
            		$data  = array(
            		'akses'					=> TRUE,
					'level'					=> 9,
                    'id'        			=> $user_ict->row()->id_ict,
                    'nama'  				=> $user_ict->row()->nama_petugas,
                    'username'				=> $user_ict->row()->username,
                    'role'					=> "Biro Sistem Informasi",
                    'edit_profil'			=> 9
            	);

            	endif;
            	$this->session->set_userdata($data);
            	$this->session->set_flashdata('success', "Selamat Datang"." ".$this->session->userdata('nama'));
				return redirect('dashboard');
			endif;

		else:
			$this->session->set_flashdata('error', "Username atau Password tidak ditemukan");
			redirect('login');
		endif;
	}

	private function log_user($user_field, $user, $pass_field , $pass, $table){
		$arr = array($user_field => $user, $pass_field => $pass);
		$user_account = $this->user->query_where($table, $arr);
		
		return $user_account;
	}

	public function logout(){	
		foreach ($user_data as $key):
            $this->session->unset_userdata($key);
        endforeach;
	    $this->session->sess_destroy();
        return redirect('login');
	}
}
