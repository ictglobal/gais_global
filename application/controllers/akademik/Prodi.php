<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		return $this->tampil_prodi();
	}

	private function tampil_prodi(){
		$data = array(
					'prodi' => $this->user->query_all("SELECT * FROM tbl_programstudi")->result(),
					'fakultas' => $this->user->query_all("SELECT * FROM tbl_fakultas")->result(),
				);
		return $this->render_page('user/akademik/prodi', $data);
	}

	public function tambah_prodi(){
		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
		$nama_prodi 	= $this->input->post('nama_prodi', TRUE);
		$cek_prodi 		= count($this->user->get_data("*", "tbl_programstudi", "WHERE nama_programstudi = '$nama_prodi'"));

		if($cek_prodi <= 0):
			$data = array(
						'id_fakultas' 		=> $this->input->post('id_fakultas', TRUE), 
						'nama_programstudi'	=> $nama_prodi,
						// 'yang_ngebuat'	 	=> $this->session->userdata('nama'),
						// 'yang_ngerubah'		=> '-',
						'created_at'		=> date('Y-m-d H:i:s')
					);
			$this->user->Add_Query('tbl_programstudi', $data);
			$this->session->set_flashdata('success', "Data berhasil ditambahkan");
			return redirect('akademik/prodi');
		else:
			$this->session->set_flashdata('warning', "Nama program studi sudah ada");
			return redirect('akademik/prodi');
		endif;
	}

	public function edit_prodi(){
		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
		$nama_programstudi = $this->input->post('nama_prodi', TRUE);
		$data = array(
					'id_fakultas' 		=> $this->input->post('id_fakultas', TRUE),
					'nama_programstudi' => $nama_programstudi,
					// 'yang_ngerubah'		=> $this->session->userdata('nama'),
					'updated_at'		=> date('Y-m-d H:i:s')
				);
		$cek = count($this->user->get_data('*', 'tbl_programstudi', "where nama_programstudi = '$nama_programstudi'"));
		if($cek <=0):
			$where = array('id_programstudi' => $this->input->post('id_prodi'));
			$this->user->Update_Query('tbl_programstudi', $data, $where);
			$this->session->set_flashdata('success', 'Data berhasil di ubah');
			return redirect('akademik/prodi');
		else:
			$this->session->set_flashdata('warning', 'Nama program studi sudah ada');
			return redirect('akademik/prodi');
		endif;
	}

	public function hapus_prodi($id){
		$where = array('id_programstudi' => $id);
		return $this->user->Delete_Query('tbl_programstudi',$where);
	}

	public function tampil_message(){
		$this->session->set_flashdata('success', 'Data berhasil di hapus');
		return redirect('akademik/prodi');
	}

}