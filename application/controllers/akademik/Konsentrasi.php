<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Konsentrasi extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		$data = array(
					'fakultas' 		=> $this->user->query_all("SELECT * FROM tbl_fakultas")->result(),
					'program_s'		=> $this->user->get_data("*", "tbl_programstudi"),
					// 'prodi' => $this->user->query_all("SELECT * FROM tbl_programstudi")->result(),
					'konsentrasi' 	=> $this->user->query_all("SELECT * FROM tbl_konsentrasi")->result()
				);
		return $this->render_page('user/akademik/konsentrasi', $data);
	}

	public function tambah_konsentrasi(){
		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone

		$kode_konsentrasi	= $this->input->post('kode', TRUE);
		$fakultas 			= $this->input->post('fakultas', TRUE);
		$prodi 				= $this->input->post('prodi', TRUE);
		$konsentrasi 		= $this->input->post('konsentrasi', TRUE);

		$cek_kode_konsentrasi 	= count($this->user->get_data("*", "tbl_konsentrasi", "WHERE id_konsentrasi = '$kode_konsentrasi'"));
		$cek_nama_konsentrasi	= count($this->user->get_data("*", "tbl_konsentrasi", "WHERE nama_konsentrasi = '$konsentrasi'"));

		if($cek_kode_konsentrasi == 1 || $cek_nama_konsentrasi == 1):
			$this->session->set_flashdata('warning', "Kode atau nama konsentrasi sudah ada");
			return redirect('akademik/konsentrasi');
		else:
			$data = array(
						'id_konsentrasi'	=> $kode_konsentrasi,
						'id_fakultas' 		=> $fakultas, 
						'id_programstudi'	=> $prodi,
						'nama_konsentrasi'	=> $konsentrasi,
						'created_at'		=> date('Y-m-d H:i:s')
					);
			$this->user->Add_Query('tbl_konsentrasi', $data);
			$this->session->set_flashdata('success', "Data berhasil ditambahkan");
			return redirect('akademik/konsentrasi');
		endif;
	}

	public function edit_konsentrasi(){
		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone

		$kode_konsentrasi 	= $this->input->post("id_konsentrasi", TRUE);
		$fakultas 			= $this->input->post("fakultas", TRUE);
		$prodi 				= $this->input->post("prodi", TRUE);
		$konsentrasi 		= $this->input->post("konsentrasi", TRUE);

		$data = array(
					'id_konsentrasi'	=> $kode_konsentrasi,
					'id_fakultas'		=> $fakultas,
					'id_programstudi'	=> $prodi,
					'nama_konsentrasi'	=> $konsentrasi,
					'updated_at'		=> date('Y-m-d H:i:s')
				);
		// print_r($data);
		// return false;

		$where = array('id_konsentrasi' => $kode_konsentrasi);
		$this->user->Update_Query('tbl_konsentrasi', $data, $where);
		$this->session->set_flashdata('success', 'Data berhasil di ubah');
		return redirect('akademik/konsentrasi');
	}

	public function hapus_konsentrasi($id){
		$where = array('id_konsentrasi' => $id);
		return $this->user->Delete_Query('tbl_konsentrasi',$where);
	}

	public function tampil_message(){
		$this->session->set_flashdata('success', 'Data berhasil di hapus');
		return redirect('akademik/konsentrasi');
	}

}