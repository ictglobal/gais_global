<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
		$this->load->library('form_validation');
		$this->load->library('functions');
		$this->load->library('tools/configuration');
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
	}

	public function index(){
		return $this->tampil_mahasiswa();
	}

	public function pilih_prodi($prm,$id){
		if($prm == 'fakultas'):
			$id_prodi 	= array('id_fakultas' => $id);
			$data = array(
					'prm' => $prm,
					'id' => $this->user->query_where('tbl_programstudi', $id_prodi)->result(),
					);
			// echo "<option value=''>---Pilih Program Studi---</option>"; 
			// foreach($data['id'] as $key => $value):
			// 	"<option value=".$value->id_programstudi.">".$value->nama_programstudi."</option>";
			// endforeach;			
		else:
			$id_konsen	= array('id_programstudi' => $id);
			$data = array(
						'prm' => $prm,
						'id2' => $this->user->query_where('tbl_konsentrasi', $id_konsen)->result()
					);
			// echo "<option value=''>---Pilih Program Konsentrasi---</option>"; 
			// foreach($data['id2'] as $key => $vl):
			// 	"<option value='$vl->id_konsentrasi'>$vl->nama_konsentrasi</option>";
			// endforeach;
		endif;
		return $this->load->view('template/component/_selectoption_prodi', $data);
	}

	private function tampil_mahasiswa(){
		$data = array(
					'mahasiswa' 		=> $this->user->query_all("SELECT * FROM tbl_mahasiswa")->result(),
					'kelas' 			=> $this->user->query_all("SELECT * FROM tbl_kelas")->result(),
					'tahun_akademik_a' 	=> $this->user->query_all("SELECT * FROM tbl_tahunakademik")->result(),
					'fakultas'			=> $this->user->query_all("SELECT * FROM tbl_fakultas")->result(),
					// 'id'				=> $id,
					// 'tahun_akademik'	=> $this->user->get_data("*", "tbl_mahasiswa"),
					// 'tahun_akademik'	=> $this->user->query_all("SELECT * FROM tbl_tahunakademik")->result(),
				);

		return $this->render_page('user/mahasiswa/data_mahasiswa', $data);
	}

	public function export_mahasiswa(){
		print_r("ayam");
		return false;
	}

	public function import_mahasiswa(){
		$file 		= $_FILES['file']['name'];
    	$ext 		= pathinfo($file, PATHINFO_EXTENSION);
		$fileName 	= $this->input->post('file');
		
	  	$config 	= $this->configuration->config_import($fileName, './file_upload/mahasiswa/data_mahasiswa/', 2);
	  	$this->upload->initialize($config); 
	  	
	  	if($ext == "xls" || $ext == "xlsx" || $ext == "xlsm" || $ext == "xlsb"):
		  	if (!$this->upload->do_upload('file')) {
			   	$error = array('error' => $this->upload->display_errors());
			   	// $this->session->set_flashdata('success', "Ada kesalahan dalam upload");
			   	redirect('user/mahasiswa/data_mahasiswa'); 
		  	} else {
			   	$media = $this->upload->data();
			   	$inputFileName = './file_upload/mahasiswa/data_mahasiswa/'.$media['file_name'];
			   	try {
					    $inputFileType = IOFactory::identify($inputFileName);
				    	$objReader = IOFactory::createReader($inputFileType);
				    	$objPHPExcel = $objReader->load($inputFileName);
			   	} catch(Exception $e) {
			    		die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			   	}
			   	
			   	$sheet = $objPHPExcel->getSheet(0);
			   	$highestRow = $sheet->getHighestRow();
			   	$highestColumn = $sheet->getHighestColumn();
			   	for ($row = 2; $row < $highestRow ; $row++){  
			   		$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
			   		$tgl_lahir 	= PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][8], 'YYYY-MM-DD');
		    
			   		$data = array(
					     "id_mhs"        		=> NULL,
					     "username"       		=> $rowData[0][1],
					     "nim"     				=> $rowData[0][1],
					     "nama"      			=> $rowData[0][2],
					     "nisn"        			=> $rowData[0][3],
					     "password"         	=> $rowData[0][4],
					     "pass"         		=> $rowData[0][4],
					     "jenis_kelamin"   		=> $rowData[0][5],
					     "telp"         		=> $rowData[0][6],
					     "tempat_lahir"         => $rowData[0][7],
					     "tgl_lahir"          	=> $tgl_lahir,
					     "agama"              	=> $rowData[0][9],
					     "id_prodi"           	=> $rowData[0][10],
					     "id_kelas"           	=> $rowData[0][11],
					     "id_kuliah"           	=> $rowData[0][12],
					     "status"           	=> $rowData[0][13],
					     "status_bayar"    		=> $rowData[0][14],
					     "alamat"         		=> $rowData[0][15],
					     "nik"           		=> $rowData[0][16],
					     "nama_ibu"   			=> $rowData[0][17],
					     "nik_ibu"				=> $rowData[0][18],
					     "nama_ayah"			=> $rowData[0][19],
					     "nik_ayah" 			=> $rowData[0][20],
					     "tahun_akademik"       => $rowData[0][21],
					     "intake"     			=> $rowData[0][22],
					     "id_fakultas"          => $rowData[0][23],
					     "id_programstudi"      => $rowData[0][24],
					     "role"					=> 3
			    	);
			    	
					$this->user->Add_Query("tbl_mahasiswa", $data); 
		   		}
		   		$this->session->set_flashdata('success', "Data mahasiswa berhasil di export");
	  			return redirect('user/mahasiswa/data_mahasiswa'); 
	  		}
	  	else:
	  		$this->session->set_flashdata('warning', "Format file tidak didukung");
	  		return redirect('user/mahasiswa/data_mahasiswa');
	  	endif;
	}

	public function selectKelas($id)
	{
	    $id = str_replace("-"," ",$id);
		$kelas = $this->user->query_all("SELECT * FROM tbl_kelas WHERE id_kelas ='$id'")->result();
		$id_kelas = $kelas[0]->id_kelas;
		$data = array(
					'mahasiswa' => $this->user->query_all("SELECT * FROM tbl_mahasiswa Where id_kelas = '$id_kelas'")->result(),
					'programStudy' => $this->user->query_all("SELECT * FROM tbl_programstudi")->result(),
					'kelas' => $this->user->query_all("SELECT * FROM tbl_kelas")->result(),
				);
		return $this->load->view('user/mahasiswa/selectKelas', $data);
	}

	public function tambah(){
		$nim 		= $this->input->post("nim");
		$pass 		= $this->input->post('pass');
		$cek_nim 	= count($this->user->get_data('nim', 'tbl_mahasiswa', "where nim = '$nim'"));

		if($cek_nim < 1):
			if(($nim <= 10 && $nim > 10) || 
			   ($this->input->post('nik_mahasiswa') <= 16 && $this->input->post('nik_mahasiswa') > 16) || 
			   ($this->input->post('nik_ayah') <= 16 && $this->input->post('nik_ayah') > 16) || 
			   ($this->input->post('nik_ibu') <= 16 && $this->input->post('nik_ibu') > 16)
			  ):
				$this->session->set_flashdata('warning', 'Periksa kembali NIK dan NISN, ada yang kurang');
				return redirect('akademik/mahasiswa');
			else:
				$data = array(
						'id_mhs'			=> NULL,
						'username'			=> $nim,
						'nim'				=> $nim,
						'nisn'				=> $this->input->post('nisn'),
						'nik'				=> $this->input->post('nik_mahasiswa'),
						'nama'				=> $this->input->post('nama'),						
						'password'			=> $this->input->post('tanggal_lahir'),
						'pass'				=> $this->input->post('tanggal_lahir'),
						'jenis_kelamin'		=> $this->input->post('nisn'),
						'status'			=> 'Aktif',
						'telp'				=> $this->input->post('telp'),
						'tempat_lahir'		=> $this->input->post('tempat_lahir'),
						'tgl_lahir'			=> $this->input->post('tanggal_lahir'),
						'id_fakultas'		=> $this->input->post('fakultas'),
						'id_programstudi'	=> $this->input->post('prodi'),
						'id_kuliah'			=> $this->input->post('pilihan_kuliah'),
						'telp'				=> $this->input->post('telp'),
						'agama'				=> $this->input->post('agama'),
						'telp'				=> $this->input->post('telp'),
						'nik_ayah'			=> $this->input->post('nik_ayah'),
						'nik_ibu'			=> $this->input->post('nik_ibu'),
						'nama_ayah'			=> $this->input->post('nama_ayah'),
						'nama_ibu'			=> $this->input->post('nama_ibu'),
						'tahun_akademik'	=> $this->input->post('ta'),
						'role'				=> 3,
					);

				// print_r($data);
				$this->user->Add_Query('tbl_mahasiswa', $data);
				$this->session->set_flashdata('success', 'Data berhasil ditambahkan');
				return redirect('akademik/mahasiswa');	
			endif;	
		else:
			$this->session->set_flashdata('warning', 'Periksa kembali NIM nya');
		endif;
	}
	
	public function edit_mahasiswa(){
		$nim = $this->input->post('nim');
		
		$mhs = $this->user->query_all("SELECT * FROM tbl_mahasiswa Where nim = '$nim'")->result();
		$id_kelas = $mhs[0]->id_kelas;
		$kelas = $this->user->query_all("SELECT * FROM tbl_kelas Where id_kelas = '$id_kelas'")->result();
		$this->user->Update_Query('tbl_mahasiswa',$_POST,array('nim'=> $nim));
		$this->session->set_flashdata('success', 'Data berhasil Edit');
		return redirect('/akademik/mahasiswa/index/'.$kelas[0]->id, 'location');
	}

	public function hapus_mahasiswa($id=''){
		$nim = $id;
		$mhs = $this->user->query_all("SELECT * FROM tbl_mahasiswa Where nim = '$nim'")->result();
		$id_kelas = $mhs[0]->id_kelas;
		$kelas = $this->user->query_all("SELECT * FROM tbl_kelas Where id_kelas = '$id_kelas'")->result();
		$this->user->Delete_Query('tbl_mahasiswa',array('nim'=>$id));
		$this->session->set_flashdata('success', 'Data berhasil Hapus');
		redirect('/akademik/mahasiswa/index/'.$kelas[0]->id, 'location');
	}

	public function cari_mhs(){
		$id 	= $this->input->get('id');
		$enc 	= $this->functions->hexToStr($id);
		// print_r($id);
		// return false;

		$this->form_validation->set_rules('cari','Cari Nama dan NIM','trim|required|xss_clean|regex_match[/a-z0-9/]');
		if ($this->form_validation->run() != FALSE){
            echo "Form validation gak oke";
        }else{
        	$mhs = $this->user->get_data("*", "tbl_mahasiswa","WHERE nim LIKE '%$enc%' OR nama LIKE '%$enc%'");

			$data = array(
						'mhs' => $mhs
					);
			if (empty($data['mhs'])){
				return $this->render_page('user/mahasiswa/select_mahasiswa', $data);
        	}
			return $this->render_page('user/mahasiswa/select_mahasiswa', $data);
        }
	}

	private function hexToStr($hex){
	    $string='';
	    for ($i=0; $i < strlen($hex)-1; $i+=2){
	        $string .= chr(hexdec($hex[$i].$hex[$i+1]));
	    }
	    return $string;
	}
}