<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fakultas extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		return $this->tampil_fakultas();
	}

	private function tampil_fakultas(){
		$data = array(
					'fakultas' => $this->user->query_all("SELECT * FROM tbl_fakultas")->result()
				);
		return $this->render_page('user/akademik/fakultas', $data);
	}

	public function tambah_fakultas(){
		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
		$nama_fakultas 	= $this->input->post('nama_fakultas', TRUE);
		$cek_fakultas	= count($this->user->get_data("*", "tbl_fakultas", "WHERE nama_fakultas = '$nama_fakultas'"));
		if($cek_fakultas <= 0):
			$data 		= array(
							'nama_fakultas' 	=> $nama_fakultas, 
							// 'yang_ngebuat'	 	=> $this->session->userdata('nama'),
							// 'yang_ngerubah'		=> '-',
							'created_at'		=> date('Y-m-d H:i:s')
						);
			$this->user->Add_Query('tbl_fakultas', $data);
			$this->session->set_flashdata('success', "Data berhasil ditambahkan");
			return redirect('akademik/fakultas');
		else:
			$this->session->set_flashdata('warning', 'Nama Fakultas sudah ada');
			return redirect('akademik/fakultas'); 
		endif;
	}

	public function edit_fakultas(){
		date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
		$fak = $this->input->post('nama_fakultas', TRUE);
		$data = array(
					'nama_fakultas' 	=> $fak,
					// 'yang_ngerubah'		=> $this->session->userdata('nama'),
					'updated_at'		=> date('Y-m-d H:i:s')
				);
		$cek = count($this->user->get_data('*', 'tbl_fakultas', "where nama_fakultas = '$fak'"));
		if($cek <=0):
			$where = array('id_fakultas' => $this->input->post('kode_fakultas'));
			$this->user->Update_Query('tbl_fakultas', $data, $where);
			$this->session->set_flashdata('success', 'Data berhasil di ubah');
			return redirect('akademik/fakultas');
		else:
			$this->session->set_flashdata('warning', 'Nama fakultas sudah ada');
			return redirect('akademik/fakultas');
		endif;
	}

	public function hapus_fakultas($id){
		$where = array('id_fakultas' => $id);
		return $this->user->Delete_Query('tbl_fakultas',$where);
	}

	public function test($id){
		print_r($id);
		return false;
	}

	public function tampil_message(){
		$this->session->set_flashdata('success', 'Data berhasil di hapus');
		// $this->message->success("Data berhasil di hapus");
		return redirect('akademik/fakultas');
	}

}