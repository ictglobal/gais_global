<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Matakuliah extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		return $this->tampil_matkul();
	}

	private function tampil_matkul(){
		$data = array(
					'matkul' 		=> $this->user->query_all("SELECT * FROM tbl_matakuliah")->result(),
					'prodi'  		=> $this->user->query_all("SELECT * FROM tbl_programstudi")->result(),
					'fakultas'  	=> $this->user->query_all("SELECT * FROM tbl_fakultas")->result(),
					'konsentrasi'	=> $this->user->query_all("SELECT * FROM tbl_konsentrasi")->result()
				);
		return $this->render_page('user/akademik/matakuliah', $data);
	}

	public function pilih_prodi($prm,$id){
		if($prm == 'fakultas'):
			$id_prodi 	= array('id_fakultas' => $id);
			$data = array(
					'prm' => $prm,
					'id' => $this->user->query_where('tbl_programstudi', $id_prodi)->result(),
					);
		else:
			$id_konsen	= array('id_prodi' => $id);
			$data = array(
					'prm' => $prm,
					'id2' => $this->user->query_where('tbl_konsentrasi', $id_konsen)->result()
					);
		endif;
		return $this->load->view('template/component/_selectoption_prodi', $data);
	}

	public function save(){
		$kode_matkul = $this->input->post('kode_matkul');
		$cek = count($this->user->get_data('kode_matkul', 'tbl_matakuliah', "where kode_matkul = '$kode_matkul'"));
		// print_r($cek);
		// return 0;
		if($cek < 1):
			$data = array(
						'id_matkul'			=> NULL,
						'kode_matkul'		=> $kode_matkul,
						'id_fakultas'		=> $this->input->post('fakultas'),
						'id_programstudi'	=> $this->input->post('prodi'),
						'semester'			=> $this->input->post('semester'),
						'nama_matkul'		=> $this->input->post('nama_matkul'),
						'sks'				=> $this->input->post('sks'),
						'kurikulum'			=> $this->input->post('kurikulum')
					);
			$this->user->Add_Query('tbl_matakuliah', $data);
			$this->session->set_flashdata('success', 'Mata Kuliah berhasil ditambahkan');
			return redirect('akademik/matakuliah');
		else:
			$this->session->set_flashdata('warning', 'Gagal menyimpan data');
			return redirect('akademik/matakuliah');
		endif;
	}
	
	public function edit(){
		// print_r($_POST);
		$id_matkul = $_POST['id_matkul'];
		$data = array(
						
						// 'kode_matkul'		=> $this->input->post('kode_matkul'),
						'id_fakultas'		=> $this->input->post('fakultas'),
						'id_programstudi'	=> $this->input->post('prodi'),
						'semester'			=> $this->input->post('semester'),
						'nama_matkul'		=> $this->input->post('nama_matkul'),
						'sks'				=> $this->input->post('sks'),
						'kurikulum'			=> $this->input->post('kurikulum')
					);
					$where = array('id_matkul' => $id_matkul);
					$this->user->Update_Query('tbl_matakuliah', $data,$where);
					$this->session->set_flashdata('success', 'Mata Kuliah berhasil di ubah');
					return redirect('akademik/matakuliah');
	}

	public function hapus($id)
	{
		$where = array('id_matkul' => $id);
			$this->user->Delete_Query('tbl_matakuliah',$where);
			$this->session->set_flashdata('success', 'Mata Kuliah berhasil di hapus');
		return redirect('akademik/matakuliah');
	}

}