<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		return $this->tampil_kelas();
	}

	private function tampil_kelas(){
		$data = array(
					'kelas' => $this->user->query_all("SELECT * FROM tbl_kelas")->result()
				);
		return $this->render_page('user/akademik/kelas', $data);
	}

}