<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		return $this->tampil_dosen();
	}

	private function tampil_dosen(){
		$data = array(
					'dosen' => $this->user->query_all("SELECT * FROM tbl_dosen")->result()
				);
		return $this->render_page('user/dosen/data_dosen', $data);
	}

	public function tambah_dosen(){
		$username_dosen = $this->input->post("username", TRUE);
		$nidn 			= $this->input->post("nidn", TRUE);
		$nama_dosen 	= $this->input->post("nama_dosen", TRUE);
		$gelar_akademik	= $this->input->post("gelar", TRUE);
		$jenis_kelamin	= $this->input->post("jk", TRUE);
		$jafung 		= $this->input->post("jafung", TRUE);
		$jenjang 		= $this->input->post("jenjang", TRUE);
		$email 			= $this->input->post("email", TRUE);
		$telp 			= $this->input->post("telp", TRUE);
		$alamat 		= $this->input->post("alamat", TRUE);
		$password 		= $this->input->post("password", TRUE);		

		$data = array(
				'id_dosen' 			=> NULL,
				'username_dosen' 	=> $username_dosen,
				'nidn'				=> $nidn,
				'dosen'				=> $nama_dosen,
				'gelar'				=> $gelar_akademik,
				'jenis_kelamin' 	=> $jenis_kelamin,
				'jafung'			=> $jafung,
				'jenjang'			=> $jenjang,
				'email'				=> $email,
				'telp'				=> $telp,
				'password'			=> $password,
				'alamat'			=> $alamat,
				'role'				=> 2
			);

		$this->user->Add_Query('tbl_dosen', $data);
		$this->session->set_flashdata('success', "Data berhasil ditambahkan");
		return redirect('akademik/dosen');
	}
	
	public function edit_dosen(){
		$username_dosen = $this->input->post("username", TRUE);
		$nidn 			= $this->input->post("nidn", TRUE);
		$nama_dosen 	= $this->input->post("nama_dosen", TRUE);
		$gelar_akademik	= $this->input->post("gelar", TRUE);
		$jenis_kelamin	= $this->input->post("jk", TRUE);
		$jafung 		= $this->input->post("jafung", TRUE);
		$jenjang 		= $this->input->post("jenjang", TRUE);
		$email 			= $this->input->post("email", TRUE);
		$telp 			= $this->input->post("telp", TRUE);
		$alamat 		= $this->input->post("alamat", TRUE);
		$password 		= $this->input->post("password", TRUE);	

		$data = array(
				'id_dosen' 			=> NULL,
				'username_dosen' 	=> $username_dosen,
				'nidn'				=> $nidn,
				'dosen'				=> $nama_dosen,
				'gelar'				=> $gelar_akademik,
				'jenis_kelamin' 	=> $jenis_kelamin,
				'jafung'			=> $jafung,
				'jenjang'			=> $jenjang,
				'email'				=> $email,
				'telp'				=> $telp,
				'password'			=> $password,
				'alamat'			=> $alamat,
				'role'				=> 2
			);

		$where = array('username_dosen' => $username_dosen);
		$this->user->Update_Query('tbl_dosen', $data, $where);
		$this->session->set_flashdata('success', 'Data berhasil di ubah');
		return redirect('akademik/dosen');	
	}

	public function hapus_dosen(){
		print_r("hapus");
		return false;
	}

	public function detail_dosen(){
		$enc 	= $this->input->get('kode');
		$kode 	= $this->encryption->decrypt($enc);
		$tampil = $this->user->get_data_result("*", "tbl_dosen", "WHERE username_dosen = '$kode'");
		
		$data 	= array(
					'tampil_dosen'	=> $tampil,
				);
		return $this->render_page("user/dosen/data_detail_dosen", $data);
	}
}