<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penasehat_akademik extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		return $this->tampil_penasehat_akademik();
	}

	private function tampil_penasehat_akademik(){
		$data = array(
					'pa' => $this->user->query_all("SELECT * FROM tbl_user_penasehat_akademik")->result()
				);
		return $this->render_page('user/Penasehat_akademik/tampil_data_penasehat_akademik', $data);
	}

}