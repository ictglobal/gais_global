<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sk_mengajar extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		return $this->tampil_prodi();
	}

	private function tampil_sk(){
		$data = array(
					'sk' => $this->user->query_all("SELECT * FROM tbl_programstudi")->result()
				);
		return $this->render_page('user/akademik/prodi', $data);
	}

}