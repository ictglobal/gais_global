<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends MY_Controller {
	public function __construct(){
		parent::__construct();
		// $this->load->library('encrypt');
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
	}

	public function index(){
		$data = array(
					// 'mhs'	=> $this->user->query_all("SELECT * FROM tbl_mahasiswa LIMIT 50")->result(),
					'kelas'	=> $this->user->query_all("SELECT * FROM tbl_mahasiswa GROUP BY id_kelas")->result()
				);
		return $this->render_page("user/keuangan/status_pembayaran_mhs", $data);
	}

	public function insert_status($nim){
		$mahasiswa = $this->user->query_all("SELECT * FROM tbl_mahasiswa WHERE nim = '$nim'")->result()[0];
		$tahun_akademik = $this->user->tahun_akademik();
		$th = $this->user->get_data('*','tbl_tahunakademik'," where tahun_akademik = '$tahun_akademik'")[0];

		if($mahasiswa->status_bayar == 'BL'):
			$status_bayar = 'L1';			
		else:
			$status_bayar = 'BL';	
		endif;

		$data = array(
						'id_spa'			=> NULL,
						'id_finance' 		=> $this->session->userdata('id'),
						'nim' 				=> $nim,
						'id_tahunakademik'  => $th['id_tahunakademik'],
						'status_bayar'		=> $status_bayar,
						'created_by'		=> date("Y-m-d H:i:s")
				);
		$insert = $this->user->Add_Query('tbl_audit_status_pembayaran', $data);
		return $insert;
	}

	public function update_status($nim){
		$mahasiswa = $this->user->query_all("SELECT * FROM tbl_mahasiswa WHERE nim = '$nim'")->result()[0];

		if($mahasiswa->status_bayar == 'BL'){
			$status_bayar = 'L1';			
		}else{
			$status_bayar = 'BL';	
		}
		$data = array(
					'status_bayar'	=>	$status_bayar,
					// 'nim'			=>	$nim
				);
		$update = $this->user->Update_Query('tbl_mahasiswa', $data, array('nim'=>	$nim));
		return $update;
	}

	public function select_mhs(){
		$id = $this->input->get('id');
		$kode = $this->encryption->decrypt($id);

		$mahasiswa = $this->user->query_all("SELECT * FROM tbl_mahasiswa WHERE id_kelas = '$kode' ORDER BY nama")->result();
		$mahasiswa = $this->user->query_all("SELECT * FROM tbl_mahasiswa WHERE id_kelas = '$kode' AND status = 'Aktif' ORDER BY nama")->result();
		$data = array(
				'mhs'=> $mahasiswa,
				'kelas1' => $kode,
				'kelas'	=> $this->user->query_all("SELECT * FROM tbl_mahasiswa GROUP BY id_kelas")->result()
			);
		return $this->render_page("user/keuangan/select_mhs", $data);
	}
}