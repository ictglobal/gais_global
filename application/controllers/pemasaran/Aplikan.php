<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aplikan extends MY_Controller {
	public function __construct(){
		parent::__construct();
		if (!$this->session->userdata('akses')) {
			$this->session->set_flashdata('error', "Silahkan lakukan login terlebih dahulu");
			return redirect('login');
		}
		$this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
		$this->load->library('tools/_security');
	}

	public function index(){
		$data = array(
					'aplikan' => $this->user->query_all("SELECT * FROM tbl_aplikan")->result()
				);
		return $this->render_page('user/pemasaran/data_aplikan', $data);
	}

	public function registrasi_aplikan(){
		// $data = array(
		// 			'aplikan_registrasi' => $this->user->get_data("*", "tbl_mahasiswa")
		// 		);
		return $this->render_page("user/pemasaran/data_aplikan_registrasi");
	}

	public function select_registrasi_aplikan(){
		$id = $this->input->get('ta');
		$kode = $this->encryption->decrypt($id);

		$data = array(
				'_aplikan_registrasi' => $this->user->get_data("*", "tbl_mahasiswa", "WHERE tahun_akademik"),
			);
		print_r($kode);
		return false;
	}

	public function select_aplikan(){
		$id = $this->input->get('ta');
		$x = strtr( $id, " ", "+" );
		$kode = $this->_security->_decrypt($x);
		$aplikan = $this->user->query_all("SELECT * FROM tbl_aplikan WHERE tahun_akademik = '$kode' ORDER BY nama")->result();
		$data = array(
					'_aplikan'=> $aplikan,
					'tahun_akademik' => $id,
					'kode' => $kode,
					'x' => $x
				);
		return $this->render_page("user/pemasaran/select_aplikan", $data);
	}

	public function hapus_aplikan($id='',$ta=''){
		$taa = $this->input->get('ta');
		$x = strtr( $taa, " ", "+" );

		$this->user->Delete_Query('tbl_aplikan', array('id_aplikan' => $id));
		$this->session->set_flashdata('success', "Data aplikan berhasil dihapus");
		return redirect("pemasaran/aplikan/select_aplikan?ta=$x");
	}

	public function tampil_detail_aplikan(){
		$id = $this->input->get('tampil');
		$kode = $this->encryption->decrypt($id);
		$data 	= array(
						'tampil_aplikan' => $this->user->query_all("SELECT * FROM tbl_aplikan WHERE id_aplikan = '$kode'")->result(),
				  ); 
		return $this->render_page("user/pemasaran/tampil_detail_aplikan", $data);	
	}

	public function edit_aplikan(){
		$id = $this->input->get('id');
		$kode = $this->encryption->decrypt($id);
		print_r($kode);
		return false;
	}

	public function cetak_form_aplikan($id){
		$data = array(
					'_cetak_aplikan' => $this->user->get_data("*", "tbl_aplikan", "WHERE id_aplikan = '$id'")
				);
		return $this->load->view("user/pemasaran/cetak_form_aplikan", $data);
	}

	public function export_aplikan(){
		$fileName = $this->input->post('file', TRUE);
	 	$config['upload_path'] = './file_upload/aplikan/'; 
	  	$config['file_name'] = $fileName;
	  	$config['allowed_types'] = 'xls|xlsx|csv|ods|ots';
	  	$config['max_size'] = 100000;

	  	$this->load->library('upload', $config);
	  	$this->upload->initialize($config); 
	  	
	  	if (!$this->upload->do_upload('file')) {
		   	$error = array('error' => $this->upload->display_errors());
		   	// $this->session->set_flashdata('success', "Ada kesalahan dalam upload");
		   	redirect('pemasaran/aplikan'); 
	  	} else {
		   	$media = $this->upload->data();
		   	$inputFileName = './file_upload/aplikan/'.$media['file_name'];
		   	try {
				    $inputFileType = IOFactory::identify($inputFileName);
			    	$objReader = IOFactory::createReader($inputFileType);
			    	$objPHPExcel = $objReader->load($inputFileName);
		   	} catch(Exception $e) {
		    		die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		   	}
		   	
		   	$sheet = $objPHPExcel->getSheet(0);
		   	$highestRow = $sheet->getHighestRow();
		   	$highestColumn = $sheet->getHighestColumn();
		   	for ($row = 2; $row < $highestRow ; $row++){  
		     	$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
		     	$tgl_lahir 	= PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][6], 'YYYY-MM-DD');
		     	$tgl_datang = PHPExcel_Style_NumberFormat::toFormattedString($rowData[0][22], 'YYYY-MM-DD');
	    
		   		$data = array(
				     // "id_aplikan"        	=> $rowData[0][0],
				     "tahun_akademik"       => $rowData[0][1],
				     "nama"     			=> $rowData[0][2],
				     "nisn"      			=> $rowData[0][3],
				     "jenis_kelamin"        => $rowData[0][4],
				     "tempat_lahir"         => $rowData[0][5],
				     "tgl_lahir"          	=> $tgl_lahir,
				     "alamat"              	=> $rowData[0][7],
				     "nama_perus"           => $rowData[0][8],
				     "jabatan"           	=> $rowData[0][9],
				     "telp"           		=> $rowData[0][10],
				     "email"           		=> $rowData[0][11],
				     "asal_sekolah"         => $rowData[0][12],
				     "thn_lulus"           	=> $rowData[0][13],
				     "orang_tua"   			=> $rowData[0][14],
				     "id_prodi"				=> $rowData[0][15],
				     "id_konsentrasi"		=> $rowData[0][16],
				     "id_kuliah" 			=> $rowData[0][17],
				     "id_kelas"           	=> $rowData[0][18],
				     "sumber_informasi"     => $rowData[0][19],
				     "presenter"           	=> $rowData[0][20],
				     "intake"           	=> $rowData[0][21],
				     // "status_m"           	=> $rowData[0][21],
				     "tgl_datang"           => $tgl_datang,
				     "ket"           		=> $rowData[0][23],
		    	);
		    	$this->user->Add_Query("tbl_aplikan", $data); 
	   		}
	   		$this->session->set_flashdata('success', "Data aplikan berhasil di export");
  			redirect('pemasaran/aplikan'); 
  		}
	}

	public function export_aplikan2($th){
		$change = str_replace('%20',' ',$th);
		
		$data = $this->user->query_where("tbl_aplikan", "tahun_akademik = '$change' ORDER BY nama")->result();
		// return false;
		$this->load->library("excel");
		$object = new PHPExcel();

		$object->setActiveSheetIndex(0);

		$table_columns = array("No", "Tanggal Datang", "NISN", "Nama Aplikan", "Jenis Kelamin","Tempat Lahir", "Tanggal Lahir", "Alamat", "Asal Sekolah", "Tahun Lulus", "Nama Orang Tua", "Program Studi", "Konsentrasi", "Sesi", "Kelas", "Sumber Informasi", "Presenter", "Intake", "Tahun Akademik", "Keterangan");

		$column = 0;

		foreach($table_columns as $field){
				$object->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
		   		$column++;
		}

		$excel_row = 2;
		$i=1;
		foreach($data as $row){
			$data2 = $this->user->query_where("tbl_konsentrasi", "id_konsentrasi = '$row->id_konsentrasi'")->result();
			foreach ($data2 as $key => $value2) {
				$data3 = $this->user->query_where("tbl_programstudi", "id_programstudi = '$value2->id_programstudi'")->result();
				foreach ($data3 as $key => $value3) {
					$object->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $i++);
					$object->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $row->tgl_datang);
					$object->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $row->nisn);
					$object->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $row->nama);
					$object->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $row->jenis_kelamin);
					$object->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $row->tempat_lahir);
					$object->getActiveSheet()->setCellValueByColumnAndRow(6, $excel_row, $row->tgl_lahir);
					$object->getActiveSheet()->setCellValueByColumnAndRow(7, $excel_row, $row->alamat);
					$object->getActiveSheet()->setCellValueByColumnAndRow(8, $excel_row, $row->asal_sekolah);
					$object->getActiveSheet()->setCellValueByColumnAndRow(9, $excel_row, $row->thn_lulus);
					$object->getActiveSheet()->setCellValueByColumnAndRow(10, $excel_row, $row->orang_tua);
					$object->getActiveSheet()->setCellValueByColumnAndRow(11, $excel_row, $value3->nama_programstudi);
					$object->getActiveSheet()->setCellValueByColumnAndRow(12, $excel_row, $value2->nama_konsentrasi);
					$object->getActiveSheet()->setCellValueByColumnAndRow(13, $excel_row, $row->id_kuliah);
					$object->getActiveSheet()->setCellValueByColumnAndRow(14, $excel_row, $row->id_kelas);
					$object->getActiveSheet()->setCellValueByColumnAndRow(15, $excel_row, $row->sumber_informasi);
					$object->getActiveSheet()->setCellValueByColumnAndRow(16, $excel_row, $row->presenter);
					$object->getActiveSheet()->setCellValueByColumnAndRow(17, $excel_row, $row->intake);
					$object->getActiveSheet()->setCellValueByColumnAndRow(18, $excel_row, $change);
					$object->getActiveSheet()->setCellValueByColumnAndRow(19, $excel_row, $row->ket);

					$excel_row++;
				}
			}
		}

		$object_writer = IOFactory::createWriter($object, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Aplikan_Periode_'.$change.'.xls"');
		$object_writer->save('php://output');
	}
}