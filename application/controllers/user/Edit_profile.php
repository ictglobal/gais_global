<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edit_profile extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('user');
	}

	public function ubah_password(){
		//$hash = hash("sha256", sha1($this->input->post('password')));
		$hash = $this->input->post('password');
		$data = array('password' => $hash);
		
		if ($this->input->post('password') === $this->input->post('konfirm')):
			//Ini untuk User Dosen
			if($this->session->userdata('level')==2):
				$where = array('username_dosen' => $this->input->post('id'));
				return $this->update_password("tbl_dosen", $data, $where);

			//Ini untuk User Mahasiswa
				
			elseif($this->session->userdata('level')==3):
				$where = array('nim' => $this->input->post('id'));
				return $this->update_password("tbl_mahasiswa", $data, $where);

			//Ini untuk User Fakultas
			elseif($this->session->userdata('level')==4):
				$where = array('username' => $this->input->post('id'));
				return $this->update_password("tbl_user_fakultas", $data, $where);
			
			//Ini untuk User Prodi
			elseif($this->session->userdata('level')==5):
				$where = array('username' => $this->input->post('id'));
				return $this->update_password("tbl_user_kaprodi", $data, $where);

			//Ini untuk User BAAK
			elseif($this->session->userdata('level')==6):
				$where = array('username' => $this->input->post('id'));
				return $this->update_password("tbl_user_akademik", $data, $where);

			//Ini untuk User Keuangan
			elseif($this->session->userdata('level')==7):
				$where = array('username' => $this->input->post('id'));
				return $this->update_password("tbl_user_keuangan", $data, $where);

			//Ini untuk User Pemasaran
			elseif($this->session->userdata('level')==8):
				$where = array('username' => $this->input->post('id'));
				return $this->update_password("tbl_user_pemasaran", $data, $where);	

			else:
				$this->session->set_flashdata('error', 'Data tidak berhasil dimasukan');
				return redirect('dashboard');	
			endif;
		else:
			$this->session->set_flashdata('warning', 'Konfirmasi password tidak benar');
			return redirect('dashboard');	
		endif;	
	}

	private function update_password($table,$data, $where){
		$this->user->Update_Query($table, $data, $where);
		$this->session->set_flashdata('success' , 'Password berhasil diubah');
		return redirect('dashboard');
	}

	public function edit_data_user(){
		if ($this->session->userdata('level')==2):
			$this->edit_user_dosen();
		elseif ($this->session->userdata('level')==3):
			$this->edit_user_mahasiswa();
		endif;
		return redirect('dashboard');
	}

	private function edit_user_dosen(){
		$data = array(
				'nidn' 			=> $this->input->post('nidn'),
				'dosen'			=> $this->input->post('nama'),
				'gelar' 		=> $this->input->post('gelar'),
				'jenis_kelamin'	=> $this->input->post('jk'),
				'jafung' 		=> $this->input->post('jafung'),
				'jenjang' 		=> $this->input->post('jenjang'),
				'email' 		=> $this->input->post('email'),
				'telp' 			=> $this->input->post('no'),
				'alamat' 		=> $this->input->post('alamat')
		);
		$where = array(
				'username_dosen' => $this->session->userdata('username')
		);

		$this->user->Update_Query('tbl_dosen', $data, $where);
		$this->session->set_flashdata('success', "Data anda berhasil diubah");
	}

	private function edit_user_mahasiswa(){
		// print_r($_POST);
		$data = array(
				'nama'			=> $this->input->post('nama'),
				'nik'			=> $this->input->post('nik_mahasiswa'),
				'tempat_lahir'	=> $this->input->post('tempat_lahir'),
				'tgl_lahir2'	=> $this->input->post('tgl'),
				'jenis_kelamin'	=> $this->input->post('jk'),
				'agama'			=> $this->input->post('agama'),
				'telp'			=> $this->input->post('no'),
				'alamat'		=> $this->input->post('alamat'),
				'nik_ayah'		=> $this->input->post('nik_ayah'),
				'nik_ibu'		=> $this->input->post('nik_ibu'),
				'nama_ayah'		=> $this->input->post('nama_ayah'),
				'nama_ibu'		=> $this->input->post('nama_ibu')
		);
		$this->user->Update_Query('tbl_mahasiswa', $data, array('nim' => $this->input->post('nim')));
		$this->session->set_flashdata('success', "Data anda berhasil diubah");

	}
}