<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akademik extends CI_Model {
	public function get_KRS($dosen='', $tahun='', $kelas=''){
    	$query = $this->db->select("tbl_mahasiswa.nim, tbl_mahasiswa.nama,
        							tbl_mahasiswa.id_kelas, tbl_dosen.dosen");
    	$query = $this->db->from("tbl_krs");
    	$query = $this->db->join("tbl_mahasiswa", "tbl_krs.nim = tbl_mahasiswa.nim", "inner");
    	$query = $this->db->join("tbl_jadwal", "tbl_krs.id_jadwal = tbl_jadwal.id_jadwal", "inner");
    	$query = $this->db->join("tbl_dosen", "tbl_jadwal.username_dosen = tbl_dosen.username_dosen", "inner");
    	$query = $this->db->where("tbl_dosen.username_dosen = '$dosen' and tbl_jadwal.id_tahunakademik='$tahun' and tbl_mahasiswa.id_kelas = '$kelas'");
    	$query = $this->db->get();

    	return $query->result_array();
    }

    public function getUsername_Dosen($tahun){
    	$query = $this->db->select("tbl_dosen.username_dosen");
    	$query = $this->db->from("tbl_rps");
    	$query = $this->db->join("tbl_matakuliah", "tbl_rps.kode_matkul = tbl_matakuliah.kode_matkul", "inner");
    	$query = $this->db->join("tbl_jadwal", "tbl_rps.kode_matkul = tbl_jadwal.kode_matkul", "inner");
    	$query = $this->db->join("tbl_dosen", "tbl_jadwal.username_dosen = tbl_dosen.username_dosen", "inner");
    	$query = $this->db->join("tbl_tahunakademik", "tbl_jadwal.id_tahunakademik = tbl_tahunakademik.id_tahunakademik", "inner");
    	$query = $this->db->where("tbl_tahunakademik.tahun = '$tahun'");
    	$query = $this->db->group_by("tbl_tahunakademik.tahun");
    	$query = $this->db->get();

    	return $query->result_array();
    }
    
}