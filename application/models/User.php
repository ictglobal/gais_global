<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Model {

	public function query_all($query){
		return $this->db->query($query);
	}

	public function query_where($table, $where=''){
		return $this->db->get_where($table, $where);
	}

	public function Add_Query($table, $value){
		return $this->db->insert($table, $value);
	}

	public function Add_Query_Batch($table, $value){
		return $this->db->insert_batch($table, $value);
	}

	public function Add_Query2($table, $value, $id=''){
		$this->db->insert($table, $value);
		return $this->get_data("*",$table," ORDER BY $id DESC")[0];

	}

	public function Update_Query($table, $data, $where){
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function Delete_Query($table, $where){
    	return $this->db->delete($table, $where);
    }

	public function get_data($column, $table='', $where=''){
    	$data = $this->db->query('select '.$column.' from '.$table.' '.$where);
    	return $data->result_array();
    }

    public function get_data_result($column, $table='', $where=''){
    	$data = $this->db->query('select '.$column.' from '.$table.' '.$where);
    	return $data->result();
    }

    public function tampil_detail_RPS($id){
		return $this->db->query("SELECT * FROM tbl_detailrps WHERE username_dosen = '$id'")->result();
	}
    
	public function count_data($table){
		return $this->db->count_all($table);
	}

	function count_data_mahasiswa($id){
		$query = $this->db->select('*');
		$query = $this->db->from('tbl_mahasiswa');
		$query = $this->db->where_not_in('id_fakultas', $id);
		return $query->count_all_results();
	}

	function count_data_mahasiswa_BD(){
		$query = $this->db->select('*');
		$query = $this->db->from('tbl_mahasiswa');
		$query = $this->db->where_in('id_prodi', ['BD-1', 'BD-2']);
		//$query = $this->db->where_in('id_prodi', ['BD-1', 'BD-2']);
		return $query->count_all_results();
	}

	function count_data_mahasiswa_prodi_SI(){
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa');
		$this->db->like('id_prodi', substr("SI",0,2), 'both');
		return $this->db->count_all_results();
	}

	function count_data_mahasiswa_prodi_TI(){
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa');
		$this->db->or_like('id_prodi', substr("TI",0,2), 'both');
		$this->db->or_like('id_prodi', substr("CS",0,2), 'both');
		return $this->db->count_all_results();
	}

	function count_data_mahasiswa_prodi_TI_CS(){
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa');
		$this->db->like('id_prodi', substr("CS",0,2), 'both');
		return $this->db->count_all_results();
	}

	function count_data_mahasiswa_prodi_BD(){
		$this->db->select('*');
		$this->db->from('tbl_mahasiswa');
		$this->db->like('id_prodi', substr("BD",0,2), 'both');
		return $this->db->count_all_results();
	}

	public function cari_($title){
        $this->db->like('nama_matkul', $title , 'both');
        $this->db->order_by('nama_matkul', 'ASC');
        $this->db->group_by('nama_matkul');
        $this->db->limit(10);
        return $this->db->get('tbl_matakuliah')->result();
    }

	public function cari_2($title, $id){
		$this->db->like('nama_matkul', $title , 'both');
		$this->db->like('id_programstudi', $id , 'both');
        $this->db->order_by('nama_matkul', 'ASC');
        $this->db->group_by('nama_matkul');
        $this->db->limit(10);
        return $this->db->get('tbl_matakuliah')->result();	
    }

	public function cari_dosen($title){
		$this->db->like('dosen', $title , 'both');
        $this->db->order_by('dosen', 'ASC');
        $this->db->group_by('dosen');
        $this->db->limit(10);
        return $this->db->get('tbl_dosen')->result();
	}

	function tampil_mtk($id){
		$th = $this->tahun_akademik();
		$data['tbl_tahunakademik'] = $this->user->query_all("select * FROM tbl_tahunakademik where tahun_akademik = '$th'")->result_array()[0];
		
		// foreach ($data['tbl_tahunakademik'] as $key => $value) {
			$tahun_akademik = $data['tbl_tahunakademik']['id_tahunakademik'];
		// }
		// return $tahun_akademik;
		
		$data['jadwal'] = $this->user->query_where('tbl_jadwal', "username_dosen='$id' AND id_tahunakademik= '$tahun_akademik' GROUP BY kode_matkul ORDER BY kode_matkul ASC")->result();

		return $data['jadwal'];
	}

	function tampil_mtk2($id){
		$data['tbl_tahunakademik'] = $this->user->query_all("select * FROM tbl_tahunakademik")->result();
		foreach ($data['tbl_tahunakademik'] as $key => $value) {
			$tahun_akademik = $value->id_tahunakademik;
		}
		
		$data['jadwal'] = $this->user->query_where('tbl_jadwal', "username_dosen='$id' AND id_tahunakademik= '$tahun_akademik' ORDER BY kode_matkul ASC")->result();

		return $data['jadwal'];
	}

	function filter_tahun_akademik($thn = NULL){
		if($thn == NULL ):
			$thn = date('Y');
			$bulan = date('n');
			$smt = 1;
			if ($bulan <= 6) {
				$thn = $thn - 1;
				$smt = 2;
			}
			$thn = $thn.'-'.$smt;
		endif;
		//$query = $this->db->select('*')->from('tbl_jadwal')->where("tahun_akademik = '$thn'");
		//$query = $this->get_data("*", "tbl_jadwal", "WHERE tahun_akademik = '$thn'");
		// $query = $this->query_where("tbl_jadwal", "tahun_akademik = '$thn'");
		$query = $this->query_where("tbl_tahunakademik", "tahun_akademik = '$thn'")->result();
		foreach ($query as $key => $value) {
			$id_tahunakademik = $value->id_tahunakademik;
		}
		$dt = $this->query_where("tbl_jadwal", "id_tahunakademik = '$id_tahunakademik'");
		return $dt;
	}

	public function cekviewkhs($idmhs,$tahun_akademik,$kelas,$matakuliah,$semester,$dosen)
	{
		$khs = $this->get_data('*','tbl_khs'," where nim ='$idmhs' && id_tahunakademik = '$tahun_akademik' && id_kelas = '$kelas' && kode_matkul = '$matakuliah' && semester = '$semester'" );
		// $khs = $this->get_data('*','tbl_khs'," where nim ='$idmhs' and id_tahunakademik = 8" );
		
		$co = count($khs);
		if($co <= 0){
			$insert = array(
				'id_tahunakademik' => $tahun_akademik,
				// 'username_dosen'   => $dosen,
				'nim' => $idmhs,
				'id_kelas' => $kelas,
				'kode_matkul' => $matakuliah,
				'absen' => 0,
				'tugas' => 0,
				'formatif' => 0,
				'perilaku' => 0,
				'uts' => 0,
				'uas' => 0,
				'total' => 0,
				'grade' => "",
				'semester' => $semester,
			);

			$this->Add_Query('tbl_khs',$insert);
		}else{
		}
		$khs = $this->get_data('*','tbl_khs'," where nim ='$idmhs' && id_tahunakademik = '$tahun_akademik' && id_kelas = '$kelas' && kode_matkul = '$matakuliah' && semester = '$semester'" )[0];
		
		return $khs;
	}

	public function cekviewktmp($id_khs,$idmhs,$tahun_akademik,$kelas,$matakuliah,$semester,$dosen)
	{
		$tmp = $this->get_data('*','tbl_khs_temp'," where username_dosen = '$dosen' && nim ='$idmhs' && id_tahunakademik = '$tahun_akademik' && id_kelas = '$kelas' && kode_matkul = '$matakuliah' && semester = '$semester'" );
		// $khs = $this->get_data('*','tbl_khs'," where nim ='$idmhs' and id_tahunakademik = 8" );
		
		$co = count($tmp);
		if($co <= 0){
			$insert = array(
				'khs_id' => $id_khs,
				'id_tahunakademik' => $tahun_akademik,
				'username_dosen'   => $dosen,
				'nim' => $idmhs,
				'id_kelas' => $kelas,
				'kode_matkul' => $matakuliah,
				'absen' => 0,
				'tugas' => 0,
				'formatif' => 0,
				'perilaku' => 0,
				'uts' => 0,
				'uas' => 0,
				'total' => 0,
				'grade' => "",
				'semester' => $semester,
			);		
			$this->Add_Query('tbl_khs_temp',$insert);

		// $khs = $this->get_data('*','tbl_khs_temp'," where nim ='$idmhs' && id_tahunakademik = '$tahun_akademik' && id_kelas = '$kelas' && kode_matkul = '$matakuliah' && semester = '$semester'" )[0];

		}
		$khs = $this->get_data('*','tbl_khs_temp'," where username_dosen = '$dosen' && nim ='$idmhs' && id_tahunakademik = '$tahun_akademik' && id_kelas = '$kelas' && kode_matkul = '$matakuliah' && semester = '$semester'" )[0];
		
		return $khs;
	}

	public function cek_tmp_khs($id_dosen, $id_ta, $nim, $id_kelas, $semester,$data){
		$query = $this->query_where("tbl_khs_temp", "username_dosen = '$id_dosen' AND id_tahunakademik = '$id_ta' AND nim = '$nim' AND id_kelas = '$id_kelas' AND semester = '$semester'")->result_array()[0];

		$this->Update_Query('tbl_khs_temp',$data,array('id_temp'=>$query['id_temp']));
		$this->updatekhs($query['id_temp']);
		return $query;
	}

	public function updatekhs($id_tmp='')
	{
		$tmp = $this->user->get_data('*','tbl_khs_temp'," where id_temp = '$id_tmp'")[0];
		$id_temp = $tmp['id_temp'];
		$khs_id = $tmp['khs_id'];
		$tmp2 = $this->user->get_data('*','tbl_khs_temp'," where khs_id = '$khs_id'");
		$nilai_absen = 0;
		$tugas = 0;
		$formatif = 0;
		$perilaku = 0;
		$uts = 0;
		$uas = 0;
		
		foreach ($tmp2 as $key => $tps) {
			$nilai_absen = $nilai_absen + $tps['absen'];
			
			if($tugas < $tps['tugas']){
				$tugas = $tps['tugas'];
			}

			if($formatif < $tps['formatif']){
				$formatif = $tps['formatif'];
			}

			if($perilaku < $tps['perilaku']){
				$perilaku = $tps['perilaku'];
			}

			if($uts < $tps['uts']){
				$uts = $tps['uts'];
			}

			if($uas < $tps['uas']){
				$uas = $tps['uas'];
			}
		}

		// perhitungan nilai absen
		if($nilai_absen > 100){
			$nilai_absen = 100;
		}
		$khs = $this->get_data('*','tbl_khs'," where khs_id = '$khs_id'")[0];

		$total = $this->total_nilai($nilai_absen,$tugas,$formatif,$perilaku,$uts,$uas);
		$grade = $this->grade($total);

		$insert = array(
			'absen' => $nilai_absen,
			'tugas' => $tugas,
			'formatif' => $formatif,
			'perilaku' => $perilaku,
			'uts' => $uts,
			'uas' => $uas,
			'grade' => $grade,
			'total' => $total,
		);
		return $this->Update_Query('tbl_khs',$insert,array('khs_id'=>$khs_id));		
	}

	public function grade($total='')
	{
		if($total >= 80 && $total <= 100):
			$grade = 'A';
		elseif($total >= 70 && $total <= 79.99):
			$grade = 'B';
		elseif($total >= 60 && $total <= 69.99):
			$grade = 'C';
		elseif($total >= 40 && $total <= 59.99):
			$grade = 'D';
		elseif($total >= 0 && $total <= 39.99):
			$grade = 'E';
		endif;

		return $grade;
	}

	public function total_nilai($absen='',$tugas='',$formatif='',$perilaku='',$uts='',$uas='')
	{
		$absen = $absen * 0.1;
		// $absen = $absen * 10;

		$tugas = $tugas * 0.2;
		// $tugas = $tugas * 20;

		$formatif = $formatif * 0.1;
		// $formatif = $formatif * 10;

		$perilaku = $perilaku * 0.05;
		// $perilaku = $perilaku * 5;

		$uts = $uts * 0.25;
		// $uts = $uts * 25;

		$uas = $uas * 0.3;
		// $uas = $uas * 30;
		$total = $absen + $tugas + $formatif + $perilaku + $uts + $uas;
		return number_format((float)$total, 2, '.', '');
		// return $total;
	}

    public function tahun_akademik(){
    	date_default_timezone_set("Asia/Jakarta");
    	$bulan = date('m');
    	$tahun = date('Y');
    	$semester = 1;
    	// semester genap
    	if($bulan >= '03' && $bulan <= '08'){
    		$tahun = $tahun - 1;
    		$semester = 2;
    	}elseif($bulan == '01' || $bulan == '02'){
    		$tahun = $tahun - 1;
    	}else{
    		// echo "gk ada bulan 55";
    	}
    	
    	$hasil = $tahun.'-'.$semester;
    	return $hasil;
    }

     public function get_kelas_dariKRS($dosen, $tahun_akademik){
    	$query = $this->db->select("tbl_mahasiswa.nim, tbl_mahasiswa.id_kelas");
    	$query = $this->db->from("tbl_krs");
    	$query = $this->db->join("tbl_mahasiswa", "tbl_krs.nim = tbl_mahasiswa.nim", "inner");
    	$query = $this->db->join("tbl_jadwal", "tbl_krs.id_jadwal = tbl_jadwal.id_jadwal", "inner");
    	$query = $this->db->join("tbl_dosen", "tbl_jadwal.username_dosen = tbl_dosen.username_dosen", "inner");
    	$query = $this->db->where("tbl_dosen.username_dosen = '$dosen' AND tbl_jadwal.id_tahunakademik = '$tahun_akademik'");
    	$query = $this->db->order_by("id_kelas","ASC");
    	$query = $this->db->group_by("id_kelas");
    	$query = $this->db->get();
    	return $query->result_array();
    }

     public function get_kelas_dariKRS3($dosen, $tahun, $kelas=''){
    	$query = $this->user->query_all("
    	SELECT
		tbl_mahasiswa.nim,
        tbl_mahasiswa.nama,
        tbl_mahasiswa.id_kelas,
        tbl_dosen.dosen
		FROM
		tbl_krs
		INNER JOIN tbl_mahasiswa ON tbl_krs.nim = tbl_mahasiswa.nim
		INNER JOIN tbl_jadwal ON tbl_krs.id_jadwal = tbl_jadwal.id_jadwal
		INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen 
		WHERE tbl_dosen.username_dosen = '$dosen' and tbl_jadwal.id_tahunakademik='$tahun' and tbl_mahasiswa.id_kelas = '$kelas' GROUP BY tbl_mahasiswa.nim" );

    	return $query->result_array();
    }

    public function get_kelas_dariKRS5($dosen, $tahun, $kode_matkul){
    	$query = $this->user->query_all("
    	SELECT
		tbl_mahasiswa.nim,tbl_mahasiswa.nama,tbl_mahasiswa.id_kelas,
        tbl_dosen.dosen, tbl_matakuliah.nama_matkul, tbl_matakuliah.kode_matkul
		FROM
		tbl_krs
		INNER JOIN tbl_mahasiswa ON tbl_krs.nim = tbl_mahasiswa.nim
		INNER JOIN tbl_jadwal ON tbl_krs.id_jadwal = tbl_jadwal.id_jadwal
		INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen 
        INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul
		WHERE tbl_dosen.username_dosen = '$dosen' and tbl_jadwal.id_tahunakademik='$tahun' and tbl_matakuliah.kode_matkul ='$kode_matkul' GROUP BY tbl_mahasiswa.nim ORDER BY tbl_mahasiswa.nama ASC, tbl_mahasiswa.id_kelas ASC" );

    	return $query->result_array();
    }

    public function get_kelas_dariKRS6($dosen, $tahun, $kode_matkul){
    	$query = $this->user->query_all("
    	SELECT 
    	tbl_mahasiswa.nim, tbl_mahasiswa.nama, tbl_mahasiswa.id_kelas, tbl_dosen.dosen, tbl_matakuliah.kode_matkul, tbl_khs.absen, tbl_khs.tugas, tbl_khs.formatif, tbl_khs.perilaku, tbl_khs.uts, tbl_khs.uas, tbl_khs.total, tbl_khs.grade 
    	FROM tbl_khs 
    	INNER JOIN tbl_krs ON tbl_khs.nim = tbl_krs.nim 
    	INNER JOIN tbl_mahasiswa ON tbl_krs.nim = tbl_mahasiswa.nim 
    	INNER JOIN tbl_jadwal ON tbl_krs.id_jadwal = tbl_jadwal.id_jadwal 
    	INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen 
    	INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul 
    	WHERE tbl_dosen.username_dosen = '$dosen' AND tbl_khs.id_tahunakademik = '$tahun' AND tbl_khs.kode_matkul = '$kode_matkul' GROUP BY tbl_mahasiswa.nim ORDER BY tbl_mahasiswa.nama ASC, tbl_mahasiswa.id_kelas ASC" );

    	return $query->result();
    }

    public function get_export_pdf_dosen($dosen, $tahun, $kode_matkul){
    	$query = $this->user->query_all("
    	SELECT 
    	tbl_mahasiswa.nim, tbl_mahasiswa.nama, tbl_mahasiswa.id_kelas, tbl_dosen.dosen, tbl_matakuliah.kode_matkul, tbl_khs_temp.absen, tbl_khs_temp.tugas, tbl_khs_temp.formatif, tbl_khs_temp.perilaku, tbl_khs_temp.uts, tbl_khs_temp.uas, tbl_khs_temp.total, tbl_khs_temp.grade 
    	FROM tbl_khs_temp 
    	INNER JOIN tbl_krs ON tbl_khs_temp.nim = tbl_krs.nim 
    	INNER JOIN tbl_mahasiswa ON tbl_krs.nim = tbl_mahasiswa.nim 
    	INNER JOIN tbl_jadwal ON tbl_krs.id_jadwal = tbl_jadwal.id_jadwal 
    	INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen 
    	INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul 
    	WHERE tbl_dosen.username_dosen = '$dosen' AND tbl_khs_temp.id_tahunakademik = '$tahun' AND tbl_khs_temp.kode_matkul = '$kode_matkul' GROUP BY tbl_mahasiswa.nim ORDER BY tbl_mahasiswa.nama ASC, tbl_mahasiswa.id_kelas ASC" );

    	return $query->result();
    }

	public function get_kelas_dariKRS4($dosen, $tahun, $kelas=''){
    	$query = $this->user->query_all("
    	SELECT
		tbl_mahasiswa.nim, tbl_mahasiswa.nama,
        tbl_mahasiswa.id_kelas, tbl_dosen.dosen,
		tbl_matakuliah.kode_matkul,
        tbl_matakuliah.nama_matkul,
        tbl_matakuliah.id_programstudi,
        tbl_programstudi.nama_programstudi,
        tbl_jadwal.id_jadwal
		FROM
		tbl_krs
		INNER JOIN tbl_mahasiswa ON tbl_krs.nim = tbl_mahasiswa.nim
		INNER JOIN tbl_jadwal ON tbl_krs.id_jadwal = tbl_jadwal.id_jadwal
		INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen
		INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul
        INNER JOIN tbl_programstudi ON tbl_matakuliah.id_programstudi =  tbl_programstudi.id_programstudi
		WHERE tbl_dosen.username_dosen = '$dosen' and tbl_jadwal.id_tahunakademik='$tahun' and tbl_mahasiswa.id_kelas = '$kelas' GROUP BY tbl_matakuliah.kode_matkul" );

    	return $query->result_array();
    } 

    public function get_dosen_dariKRS($nim, $tahun, $kode, $kelas){
    	$query = $this->user->query_all("
    	SELECT tbl_dosen.username_dosen
		FROM
		tbl_krs
		INNER JOIN tbl_jadwal ON tbl_krs.id_jadwal = tbl_jadwal.id_jadwal
		INNER JOIN tbl_mahasiswa ON tbl_krs.nim = tbl_mahasiswa.nim
		INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul
		INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen

		WHERE tbl_mahasiswa.nim = '$nim' and tbl_jadwal.id_tahunakademik='$tahun' and tbl_matakuliah.kode_matkul = '$kode' and tbl_mahasiswa.id_kelas = '$kelas' GROUP BY tbl_matakuliah.kode_matkul" );

    	return $query->result_array();
    }

    public function get_export_pdf($id_dosen, $th, $matkul){
    	$query = $this->user->query_all("
    		SELECT tbl_mahasiswa.nim, tbl_mahasiswa.nama, tbl_mahasiswa.id_kelas, tbl_dosen.dosen, tbl_matakuliah.kode_matkul, tbl_khs_temp.absen, tbl_khs_temp.tugas, tbl_khs_temp.formatif, tbl_khs_temp.perilaku, tbl_khs_temp.uts, tbl_khs_temp.uas, tbl_khs_temp.total, tbl_khs_temp.grade, tbl_khs_temp.semester 
    		FROM tbl_khs_temp 
    		INNER JOIN tbl_krs ON tbl_khs_temp.nim = tbl_krs.nim 
    		INNER JOIN tbl_mahasiswa ON tbl_krs.nim = tbl_mahasiswa.nim 
    		INNER JOIN tbl_jadwal ON tbl_krs.id_jadwal = tbl_jadwal.id_jadwal 
    		INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen 
    		INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul 
    		WHERE tbl_dosen.username_dosen = '$id_dosen' 
    		AND tbl_khs_temp.id_tahunakademik = '$th' 
    		AND tbl_khs_temp.kode_matkul = '$matkul' 
    		GROUP BY tbl_khs_temp.nim 
    		ORDER BY tbl_mahasiswa.nama ASC, tbl_mahasiswa.id_kelas ASC");
    	return $query->result();
    }

    public function get_export_pdf_akademik($id_dosen, $th, $matkul, $id_kelas){
    	$query = $this->user->query_all("
    		SELECT tbl_khs_temp.nim, tbl_mahasiswa.nama, tbl_khs_temp.id_kelas, tbl_dosen.dosen, tbl_khs_temp.kode_matkul, tbl_khs_temp.absen, tbl_khs_temp.tugas, tbl_khs_temp.formatif, tbl_khs_temp.perilaku, tbl_khs_temp.uts, tbl_khs_temp.uas, tbl_khs_temp.total, tbl_khs_temp.grade, tbl_khs_temp.semester 
    		FROM tbl_khs_temp 
    		INNER JOIN tbl_krs ON tbl_khs_temp.nim = tbl_krs.nim 
    		INNER JOIN tbl_mahasiswa ON tbl_krs.nim = tbl_mahasiswa.nim 
    		INNER JOIN tbl_jadwal ON tbl_krs.id_jadwal = tbl_jadwal.id_jadwal 
    		INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen 
    		INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul 
    		WHERE tbl_dosen.username_dosen = '$id_dosen' 
    		AND tbl_khs_temp.id_tahunakademik = '$th' 
    		AND tbl_khs_temp.kode_matkul = '$matkul'
    		AND tbl_khs_temp.id_kelas = '$id_kelas' 
    		GROUP BY tbl_khs_temp.nim 
    		ORDER BY tbl_mahasiswa.nama ASC, tbl_mahasiswa.id_kelas ASC");
    	return $query->result();
    }

    public function cekjadwalinput(){
    	date_default_timezone_set('Asia/Jakarta'); # add your city to set local time zone
    	$username 			= $this->session->userdata('username'); 
    	$th 				= $this->tahun_akademik();
    	$tahun 				= $this->get_data('*','tbl_tahunakademik'," where tahun_akademik = '$th'")[0];
    	$id_tahunakademik 	= $tahun['id_tahunakademik'];
    	$x 					= $this->get_data('*','tbl_atur_jadwal_input_nilai'," where username_dosen = 
    						  '$username'and id_tahunakademik = '$id_tahunakademik'"); 

    	$jdw = 0;
	    foreach ($x as $key =>$value){ 
	     	$mulai = $value['tgl_mulai'].'-'.$value['waktu_mulai'];
	     	$selesai = $value['tgl_selesai'].'-'.$value['waktu_selesai'];

	     	if ($mulai <= date('Y-m-d-H:i:s') && $selesai >= date('Y-m-d-H:i:s')){
	    		$jdw = $jdw+1;	
	    	}
	    }
    // print_r(date('Y-m-d-H:i:s'));
    	return $jdw;
    }

    // memvalidasi uplad exel secara struktur dan datanya
    public function validasiExelInputNilai($highestRow='',$highestColumn='',$objPHPExcel='',$th)
    {	
    	$prmval = 0;
    	$alert = '';
    	$inputtmp = [];
    	$inputkhs = [];
    	$rowData = $objPHPExcel->rangeToArray('A' . 1 . ':' . $highestColumn . 1, NULL, TRUE, FALSE)[0];
    	
    	if($rowData[0] != 'nim' || $rowData[1] != 'id_kelas' || $rowData[2] != 'kode_matkul' || $rowData[3] != 'absen' || $rowData[4] != 'tugas' || $rowData[5] != 'formatif' || $rowData[6] != 'perilaku' || $rowData[7] != 'uts' || $rowData[8] != 'uas' || $rowData[9] != 'total' || $rowData[10] != 'grade' || $rowData[11] != 'semester' ){

    		$prmval = 11;
    		$alert = 'Format Judul input salah';

    	}else{
    			$row = 2;
    			$errnim='';
    			$errkelas='';
    			$errmatkul = '';
    		while ($row < $highestRow) {


	    	$rowData = $objPHPExcel->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE)[0];
	    		$nim = $rowData[0];
	    		$id_kelas = $rowData[1];
	    		$kode_matkul = $rowData[2];
    			
    			// cek nim
    			$ceknim = $this->get_data('nim','tbl_mahasiswa'," where nim = '$nim'");
    			$cekkelas = $this->get_data('id_kelas','tbl_kelas'," where id_kelas = '$id_kelas'");
    			$cekmatakuliah = $this->get_data('kode_matkul','tbl_matakuliah'," where kode_matkul = '$kode_matkul'");
    			// cek apakah nim nya ada dan nimnya tidak kosong
    			if(count($ceknim) == 0 && $nim != ''){
    				$prmval = 12;
    				$errnim = 'Nim '.$nim.' Tidak ditemukan ||';
    				$row = $highestRow+5;
    			}
    			// id_kelas
    			// cek apakah id kelasnya ada dan tidak kosong
    			if(count($cekkelas) == 0  && $id_kelas != ''){
    				$prmval = 12;
    				$errkelas = 'Id kelas '.$id_kelas.' Tidak ditemukan ||';
    				$row = $highestRow+5;
    			}
    			// cek apakah matakuliah nya ada dan tidak kosong
    			if(count($cekmatakuliah) == 0 && $kode_matkul != ''){
    				$prmval = 12;
    				$errmatkul = 'Kode Matakuliah '.$kode_matkul.' Tidak ditemukan ||';
    				$row = $highestRow+5;
    			}
    			// data akan di ambil jika nimnya tidak kosong
    			if($nim != '' && count($ceknim) != 0 && count($cekkelas) != 0 && count($cekmatakuliah) != 0){
    				$total = $this->total_nilai($rowData[3],$rowData[4],$rowData[5],$rowData[6],$rowData[7],$rowData[8]);
					$grade = $this->grade($total);
    				$data = array(
				     // "khs_id"        		=> NULL,
				     "id_tahunakademik"     => $th,	
				     "nim"     				=> $rowData[0],
				     "id_kelas"      		=> $rowData[1],
				     "kode_matkul"      	=> $rowData[2],
				     "absen"      			=> $rowData[3],
				     "tugas"      			=> $rowData[4],
				     "formatif"      		=> $rowData[5],
				     "perilaku"      		=> $rowData[6],
				     "uts"      			=> $rowData[7],
				     "uas"      			=> $rowData[8],
				     "total"      			=> $total,
				     "grade"      			=> $grade,
				     "semester"      		=> $rowData[11],
		    	);
    				$get_dosen = $this->user->get_dosen_dariKRS($data['nim'], $th, $data['kode_matkul'], $data['id_kelas'])[0];
    				$data2 = array(
					    	"id_temp"        		=> NULL,
					    	"username_dosen"		=> $get_dosen['username_dosen'],
					    	
			    		);

    				$inputkhs[] = $data2;
    			}else{
    				$row = $highestRow+5;
    			}
    			

		    	$inputtmp[] = $data;

    			// cek matkul

    			$alert = $errnim.$errkelas.$errmatkul;
    			
    		$row++;


    		}
    	}

    	// $this->dbforge->add_key('photo_id', TRUE);

    	$data = array(
    		'prmval' => $prmval,
    		'alert' => $alert,
    		'inputtmp' => $inputtmp,
    		'inputkhs' => $inputkhs,
    	);

    	return $data;
    }


    public function crudTmpKhs($inputtmp='',$inputkhs='',$key='')
    {
    	

    	//olah data khs start
    	// panggil data khs
    	$khs= $this->get_data("*","tbl_khs","WHERE nim = '$inputtmp[nim]' AND id_kelas = '$inputtmp[id_kelas]' AND kode_matkul = '$inputtmp[kode_matkul]' AND semester = '$inputtmp[semester]' ORDER BY khs_id DESC");
    	// $this->Update_Query('tbl_khs',$inputtmp,array('khs_id'=>$khs[0]['khs_id']));
    	// print_r($inputtmp);
    	// return false;
    	// cek apakah khs telah di input
    	
    	if (count($khs) > 0) {
    		// data khs di temukan dan update khs
    		if(count($khs) == 0){
    			echo 88;
    			return false;
    		}else{
    			
    		}
    		$khs = $khs[0];
    		// code...
    		// update khs
    		// print_r($khs);
    		$this->Update_Query('tbl_khs',$inputtmp,array('khs_id'=>$khs['khs_id']));
    		// ambil data khs terbaru
    		// return false;
    		$khs = $this->get_data('*','tbl_khs'," WHERE khs_id = '$khs[khs_id]'")[0];

    	}else{
    		// khs belum di inputkan
    		// lakukan input khs dan ambil nilai inputannya
    		$khs = $this->Add_Query2("tbl_khs", $inputtmp,'khs_id');
    		
    	}
    	// print_r($khs);
    	// olah data khs end
    	$data_ksh_tmp = array(
    						"id_temp"        		=> NULL,
					    	"khs_id"				=> $khs['khs_id'],
					    	"id_tahunakademik"		=> $inputtmp['id_tahunakademik'],
					    	"username_dosen"		=> $inputkhs[0]['username_dosen'],
					    	"nim"					=> $inputtmp['nim'],
					    	"id_kelas"				=> $inputtmp['id_kelas'],
					    	"kode_matkul"			=> $inputtmp['kode_matkul'],
					    	"absen"					=> $inputtmp['absen'],
					    	"tugas"					=> $inputtmp['tugas'],
					    	"formatif"				=> $inputtmp['formatif'],
					    	"perilaku" 				=> $inputtmp['perilaku'],
					    	"uts"					=> $inputtmp['uts'],
					    	"uas"					=> $inputtmp['uas'],
					    	"total"					=> $inputtmp['total'],
					    	"grade"					=> $inputtmp['grade'],
					    	"semester"				=> $inputtmp['semester'],
    	);
    	$khs_tmp = $this->get_data('*','tbl_khs_temp'," where khs_id = '$khs[khs_id]'");
    	// cek data khs
    	if(count($khs_tmp) > 0){
    		// data temp di temukan dan di lakukan update data tmp
    		$khs_tmp = $khs_tmp[0];
    		// mencegah agar id_tmp nya tidak berubah menjadi null saat di update
    		$data_ksh_tmp['id_temp'] = $khs_tmp['id_temp'];
    		// lakukan update data tmp
    		$khs = $this->Update_Query('tbl_khs_temp',$data_ksh_tmp,array('id_temp' => $khs_tmp['id_temp']));
    		
    	}else{
    		$this->Add_Query('tbl_khs_temp',$data_ksh_tmp);
    	}
    	

    	// olah data khs tmp start
    }

    public function select_eub_berdasarkan_username_dosen($id_dosen, $tahun_akademik, $opsi=''){
    	$opsi == "filter";
    	if($opsi):
    		$query = $this->query_all("SELECT 
    									
    									t_hasil_eub.nim, t_hasil_eub.saran, t_hasil_eub.id_eub, 
    									t_hasil_eub.id_jadwal, t_hasil_eub.p1, t_hasil_eub.p2, 
    									t_hasil_eub.p3, t_hasil_eub.p4, t_hasil_eub.p5, 
    									t_hasil_eub.p6, t_hasil_eub.p7, t_hasil_eub.p8, 
    									t_hasil_eub.p9, t_hasil_eub.p10, 
    									tbl_dosen.username_dosen, tbl_dosen.dosen, 
    									tbl_jadwal.id_kelas, tbl_jadwal.id_tahunakademik,
    									tbl_matakuliah.nama_matkul, tbl_matakuliah.sks, tbl_matakuliah.kode_matkul, 
    									sum(t_hasil_eub.p1) + sum(t_hasil_eub.p2) + sum(t_hasil_eub.p3) + sum(t_hasil_eub.p4) + sum(t_hasil_eub.p5) + sum(t_hasil_eub.p6) + sum(t_hasil_eub.p7) + sum(t_hasil_eub.p8) + sum(t_hasil_eub.p9) + sum(t_hasil_eub.p10) AS sumx, 
    									count(t_hasil_eub.p1) + count(t_hasil_eub.p2) + count(t_hasil_eub.p3) + count(t_hasil_eub.p4) + count(t_hasil_eub.p5) + count(t_hasil_eub.p6) + count(t_hasil_eub.p7) + count(t_hasil_eub.p8) + count(t_hasil_eub.p9) + count(t_hasil_eub.p10) AS countx,
    									(sum(t_hasil_eub.p1) + sum(t_hasil_eub.p2) + sum(t_hasil_eub.p3) + sum(t_hasil_eub.p4) + sum(t_hasil_eub.p5) + sum(t_hasil_eub.p6) + sum(t_hasil_eub.p7) + sum(t_hasil_eub.p8) + sum(t_hasil_eub.p9) + sum(t_hasil_eub.p10)) / (count(t_hasil_eub.p1) + count(t_hasil_eub.p2) + count(t_hasil_eub.p3) + count(t_hasil_eub.p4) + count(t_hasil_eub.p5) + count(t_hasil_eub.p6) + count(t_hasil_eub.p7) + count(t_hasil_eub.p8) + count(t_hasil_eub.p9) + count(t_hasil_eub.p10)) AS imk,
    									sum(t_hasil_eub.p1) AS tp1,
    									sum(t_hasil_eub.p2) AS tp2, 
    									sum(t_hasil_eub.p3) AS tp3, 
    									sum(t_hasil_eub.p4) AS tp4, 
    									sum(t_hasil_eub.p5) AS tp5, 
    									sum(t_hasil_eub.p6) AS tp6, 
    									sum(t_hasil_eub.p7) AS tp7, 
    									sum(t_hasil_eub.p8) AS tp8, 
    									sum(t_hasil_eub.p9) AS tp9, 
    									sum(t_hasil_eub.p10) AS tp10, 
    									count(t_hasil_eub.p1) AS r1, 
    									count(t_hasil_eub.p2) AS r2, 
    									count(t_hasil_eub.p3) AS r3, 
    									count(t_hasil_eub.p4) AS r4, 
    									count(t_hasil_eub.p5) AS r5, 
    									count(t_hasil_eub.p6) AS r6, 
    									count(t_hasil_eub.p7) AS r7, 
    									count(t_hasil_eub.p8) AS r8, 
    									count(t_hasil_eub.p9) AS r9, 
    									count(t_hasil_eub.p10) AS r10,
										(SUM(t_hasil_eub.p1)+SUM(t_hasil_eub.p2)+SUM(t_hasil_eub.p3)+SUM(t_hasil_eub.p4)+
										 SUM(t_hasil_eub.p5)+SUM(t_hasil_eub.p6)+SUM(t_hasil_eub.p7)+SUM(t_hasil_eub.p8)+
										 SUM(t_hasil_eub.p9)+SUM(t_hasil_eub.p10)) as jumlah_keseluruhan

    									FROM t_hasil_eub  
    									INNER JOIN tbl_jadwal ON t_hasil_eub.id_jadwal = tbl_jadwal.id_jadwal
    									INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen
    									INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul 
    									WHERE tbl_jadwal.username_dosen = '$id_dosen' and tbl_jadwal.id_tahunakademik = $tahun_akademik
                                        GROUP BY tbl_jadwal.id_jadwal ORDER BY tbl_jadwal.id_kelas ASC;");
    		return $query->result();
    	else:
    		$query = $this->query_all("SELECT COUNT(*) as jml FROM t_hasil_eub INNER JOIN tbl_jadwal on t_hasil_eub.id_jadwal = tbl_jadwal.id_jadwal INNER JOIN tbl_dosen on tbl_jadwal.username_dosen = tbl_dosen.username_dosen INNER JOIN tbl_matakuliah on tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul WHERE tbl_dosen.username_dosen = '$id_dosen' and tbl_jadwal.id_tahunakademik = '$tahun_akademik' ORDER BY tbl_jadwal.id_kelas")->result();
    		return $query;
    	endif;
    }

    public function filter_tahun_akademik_dosen($id_dosen){
    	$query = $this->query_all("SELECT 
    					t_hasil_eub.username AS username, tbl_jadwal.id_tahunakademik AS id_tahunakademik, 
    					t_hasil_eub.saran AS saran, t_hasil_eub.id_eub AS id_eub, t_hasil_eub.id_jadwal AS id_jadwal, 
    					t_hasil_eub.p1 AS p1, t_hasil_eub.p2 AS p2, t_hasil_eub.p3 AS p3, t_hasil_eub.p4 AS p4, 
    					t_hasil_eub.p5 AS p5, t_hasil_eub.p6 AS p6, t_hasil_eub.p7 AS p7, t_hasil_eub.p8 AS p8, 
    					t_hasil_eub.p9 AS p9, t_hasil_eub.p10 AS p10, 
    					tbl_dosen.username_dosen AS username_dosen, tbl_dosen.dosen AS dosen, 
    					tbl_jadwal.id_kelas AS id_kelas, 
    					tbl_matakuliah.nama_matkul AS nama_matkul, tbl_matakuliah.sks AS sks,
    					tbl_tahunakademik.tahun_akademik
    					FROM tbl_jadwal 
    					INNER JOIN t_hasil_eub ON tbl_jadwal.id_jadwal = t_hasil_eub.id_jadwal 
    					INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen 
    					INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul
    					INNER JOIN tbl_tahunakademik ON tbl_jadwal.id_tahunakademik = tbl_tahunakademik.id_tahunakademik
    					WHERE tbl_jadwal.username_dosen = '$id_dosen' GROUP BY tbl_jadwal.id_tahunakademik ORDER BY tbl_tahunakademik.tahun_akademik
    				");
    	return $query;

    }

    public function detail_eub_model($id_dosen, $id_th, $id_kelas){
    	$query = $this->query_all("SELECT 
    								tbl_jadwal.username_dosen, tbl_matakuliah.nama_matkul, 
    								tbl_dosen.dosen, tbl_jadwal.id_kelas, 
    								tbl_jadwal.id_tahunakademik, tbl_jadwal.id_jadwal, 
									sum(t_hasil_eub.p1) AS tp1, sum(t_hasil_eub.p2) AS tp2, 
									sum(t_hasil_eub.p3) AS tp3, sum(t_hasil_eub.p4) AS tp4, 
									sum(t_hasil_eub.p5) AS tp5, sum(t_hasil_eub.p6) AS tp6, 
									sum(t_hasil_eub.p7) AS tp7, sum(t_hasil_eub.p8) AS tp8, 
									sum(t_hasil_eub.p9) AS tp9, sum(t_hasil_eub.p10) AS tp10, 
									count(t_hasil_eub.p1) AS r1, count(t_hasil_eub.p2) AS r2, 
									count(t_hasil_eub.p3) AS r3, count(t_hasil_eub.p4) AS r4, count(t_hasil_eub.p5) AS r5,
									count(t_hasil_eub.p6) AS r6, count(t_hasil_eub.p7) AS r7, count(t_hasil_eub.p8) AS r8, 
									count(t_hasil_eub.p9) AS r9, count(t_hasil_eub.p10) AS r10
									FROM t_hasil_eub 
									INNER JOIN tbl_jadwal ON t_hasil_eub.id_jadwal = tbl_jadwal.id_jadwal
									INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen
									INNER JOIN tbl_matakuliah ON tbl_jadwal.kode_matkul = tbl_matakuliah.kode_matkul
									WHERE tbl_jadwal.username_dosen = '$id_dosen' and tbl_jadwal.id_tahunakademik = '$id_th'
										  and tbl_jadwal.id_kelas = '$id_kelas'
									GROUP BY t_hasil_eub.id_jadwal ;"
				);
    	return $query;
    }

    public function get_saran($id_dosen, $id_tahunakademik, $id_kelas){
    	$query = $this->query_all("SELECT t_hasil_eub.saran 
    								FROM t_hasil_eub 
    								INNER JOIN tbl_jadwal ON t_hasil_eub.id_jadwal = tbl_jadwal.id_jadwal 
    								INNER JOIN tbl_dosen ON tbl_jadwal.username_dosen = tbl_dosen.username_dosen 
    								WHERE tbl_jadwal.username_dosen = '$id_dosen' and tbl_jadwal.id_tahunakademik = '$id_tahunakademik' and tbl_jadwal.id_kelas = '$id_kelas'");

    	return $query;
    }


}
