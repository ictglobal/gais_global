 <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Akademik</h3>
                <ul class="nav side-menu">
                  <li><a href="dashboard"><i class="fa fa-home"></i> Dashboard</a><span class="label label-success pull-right"></span></a>
                    <ul class="nav child_menu">
                      <!--<li><a href="index.html">Dashboard</a></li>
                      <li><a href="index2.html">Dashboard2</a></li>
                      <li><a href="index3.html">Dashboard3</a></li>-->
                    </ul>
                  </li>

                   <li><a href="kelenderakademik"><i class="fa fa-home"></i> Kelender Akademik<span class="label label-success pull-right"></span></a>
                    <ul class="nav child_menu">
                      <!--<li><a href="index.html">Dashboard</a></li>
                      <li><a href="index2.html">Dashboard2</a></li>
                      <li><a href="index3.html">Dashboard3</a></li>-->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"></i> Akademik<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="form.html">KRS</a></li>
                      <li><a href="form_advanced.html">Jadwal Kuliah</a></li>
                      <li><a href="form_validation.html">Nilai</a></li>
                      <li><a href="form_wizards.html">KHS</a></li>
                      <li><a href="form_upload.html">Transkrip</a></li>
                      <li><a href="form_buttons.html">Kartu Ujian</a></li>
                      <li><a href="form_buttons.html">Susulan / Remedial</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> KKP/KKN <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="general_elements.html">Pendaftaran</a></li>
                      <li><a href="media_gallery.html">Surat Izin</a></li>
                       <li><a href="media_gallery.html">Dosen Pembimbing</a></li>
                      <li><a href="typography.html">Prosedur KKP/KKN</a></li>
                      <li><a href="icons.html">Buku Panduan KKP</a></li>
                      <li><a href="glyphicons.html">Buku Panduan KKN</a></li>
                      <li><a href="widgets.html">Format Proposal KKN</a></li>
                      <li><a href="invoice.html">Format Proposal KKP</a></li>
                      <!--<li><a href="inbox.html">Inbox</a></li>
                      <li><a href="calendar.html">Calendar</a></li>-->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-table"></i> Pembayaran <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="tables.html">Riwayat</a></li>
                      <!--<li><a href="tables_dynamic.html">Table Dynamic</a></li>-->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-bar-chart-o"></i> Skripsi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="chartjs.html">Pendaftaran Skripsi</a></li>
                      <li><a href="chartjs2.html">Dosen Pembimbing</a></li>
                      <li><a href="morisjs.html">Nilai Sidang</a></li>
                      <li><a href="echarts.html">SKL</a></li>
                      <!--<li><a href="other_charts.html">Other Charts</a></li>-->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-clone"></i> Wisuda<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                       <li><a href="fixed_sidebar.html">Informasi Teknis</a></li>
                      <li><a href="fixed_sidebar.html">No. PIN</a></li>
                      <li><a href="fixed_footer.html">Ijazah Digital</a></li>
                    </ul>
                  </li>

                   <li><a><i class="fa fa-clone"></i> Alumni<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar.html">Tracer Study</a></li>
                      <!--<li><a href="fixed_footer.html">Fixed Footer</a></li>-->
                    </ul>
                  </li>

                  <li><a><i class="fa fa-clone"></i> Quesioner Institusi<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar.html">Saran</a></li>
                      <!--<li><a href="fixed_footer.html">Fixed Footer</a></li>-->
                    </ul>
                  </li>
                   <li><a><i class="fa fa-clone"></i> Download<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="fixed_sidebar.html">Repository</a></li>
                      <!--<li><a href="fixed_footer.html">Fixed Footer</a></li>-->
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="menu_section">
                <h3>Non Akademik / SKPI</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Kemahasiswaa DIKTI<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">Prestasi</a></li>
                      <!--<li><a href="projects.html">Projects</a></li>
                      <li><a href="project_detail.html">Project Detail</a></li>
                      <li><a href="contacts.html">Contacts</a></li>
                      <li><a href="profile.html">Profile</a></li>-->
                    </ul>
                  </li>
                  <li><a><i class="fa fa-windows"></i> Kemahasiswaan Non DIKTI<span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="page_403.html">Prestasi</a></li>
                      <!--<li><a href="page_404.html">404 Error</a></li>
                      <li><a href="page_500.html">500 Error</a></li>
                      <li><a href="plain_page.html">Plain Page</a></li>
                      <li><a href="login.html">Login Page</a></li>
                      <li><a href="pricing_tables.html">Pricing Tables</a></li>-->
                    </ul>
                  </li>
                  <!--<li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="#level1_1">Level One</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                  </li>                  
                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>-->
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->