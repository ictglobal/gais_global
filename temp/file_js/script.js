//ini buat select option biar ada searchnya
$("#theSelect").select2();
$("#theSelect2").select2();
$("#theSelect3").select2();
$("#Select_Oi").select2();
$("#tahun_akademik2").select2();
$("#dosen").select2();
$("#ruang").select2();
$("#kelas").select2();
$("#kelas2").select2();
$("#matakuliah").select2();
$("#semester").select2();
$("#select_tahun_akademik").select2(); 
$("#select_tahun_akademik2").select2();
$("#select_tahun_akademik3").select2();
$("#select_tahun_akademik4").select2();
$("#select_tahun_akademik5").select2();
$("#select_tahun_akademik6").select2();
$("#select_tahun_akademik7").select2();
$("#select_tahun_akademik8").select2();
$("#select_tahun_akademik9").select2();
$("#select_tahun_akademik10").select2();
$("#select_tahun_akademik_sk_mengajar").select2();
$("#select_mahasiswa").select2();
$("#select_aktif_gais_mahasiswa").select2();

$("#pilih_kalender").select2();
$("#pilih_kalender2").select2();
$("#pilih_matkul_jadwal").select2();
$("#pilih_dosen_jadwal").select2();
$("#pilih_kelas_jadwal").select2();

$("#pilih_dosen").select2();
$("#sesi_kuliah").select2();
$("#jk").select2();
$("#jk2").select2();
$("#jk3").select2();
$("#sumber_informasi").select2();

$("#agama").select2();
$("#agama2").select2();

$("#fakultas").select2();
$("#prodi").select2();
$("#konsentrasi").select2();

$("#pilihan_kuliah").select2();
$("#pilihan_fakultas").select2();
$("#pilihan_prodi").select2();
$("#pilihan_konsentrasi").select2();

$("#pilihan_fakultas2").select2();
$("#pilihan_prodi2").select2();
$("#pilihan_konsentrasi2").select2();

$("#pilih_jeniskelamin").select2();
$("#pilih_jafung").select2();
$("#pilih_jenjang").select2();

//Ini untuk menonaktifkan inspectelement jika diklik kanan
// document.addEventListener('contextmenu', function(e) {
//     e.preventDefault();
// });

//Ini untuk menonaktifkan inspectelement 
document.onkeydown = function(e) {
    if(event.keyCode == 123) {
        return false;
    }
    if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
        return false;
    }
    else if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
        return false;
    }
    else if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
        return false;
    }
    else if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
        return false;
    }
}

//ini untuk menambah no urut di datatable dan dapat disort sesuai urutannya
var t = $('#datatable').DataTable({
            columnDefs  : [{searchable: false,orderable: false,targets: 0,},],
            order       : [[1, 'asc']],
        });
t.on('order.dt search.dt', function () {
            let i = 1;
            t.cells(null, 0, { 
            search: 'applied', 
            order: 'applied' }).every(
            function (cell) {
                this.data(i++);
        });
    }).draw();